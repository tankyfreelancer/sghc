﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using HIS.Model;

namespace HIS
{
    public class BusinessService : Base, IBusinessService
    {
        public List<string> GetDefaultDate()
        {
            var now = DateTime.Now;
            var month = now.Month;
            var year = Int32.Parse($"{now.Year}".Substring(2, 2));
            var MMyy = new List<string>();
            var months = new List<string> { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
            for (int yy = 20; yy <= year; yy++)
            {
                foreach (var MM in months)
                {
                    // 07/2020 is min table
                    if (yy == 20 && Int32.Parse(MM) < 7)
                    {
                        continue;
                    }
                    if (yy == year && Int32.Parse(MM) > month)
                    {
                        continue;
                    }
                    MMyy.Add($"{MM}{yy}");
                }
            }
            return MMyy;
        }
        public async Task<DebitModel> PatientsDebit(string UserID, string ProviderID, string ServiceID, string Password)
        {
            var requestModel = new
            {
                UserID,
                ProviderID,
                ServiceID,
                Password,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, dynamic>(requestModel, AppSetting.Api.PatientsDebit);
            //  TODO: parse dummies response
            var result = new DebitModel();
            return result;
        }

        public async Task<DebitModel> PaymentInfo(string UserID, string ProviderID, string ServiceID, string ChannelID, string Password, decimal Amount, string TransactionCode, string TransactionDetail, string AdditionalInfo)
        {
            var requestModel = new
            {
                UserID,
                ProviderID,
                ServiceID,
                ChannelID,
                Password,
                RequestTime = Now(),
                Amount,
                TransactionCode,
                TransactionDetail,
                AdditionalInfo,
            };
            var response = await Post<dynamic, dynamic>(requestModel, AppSetting.Api.PaymentInfo);
            //  TODO: parse dummies response
            var result = new DebitModel();
            return result;
        }
        public async Task<BookingModel> Booking(string Booking_date, int Doctor_id, int Subject_id, int Room_id, string Patient_name, string Patient_surname, int Birthyear, string Birthdate, int Social_id, string Mobile, string Patient_code, string Country_code, int City_id, int District_id, int Ward_id, string Address, int Sex, string Bv_time, int Booking_number = 0, int Method_id = 0, decimal Amount = 0, decimal Amount_original = 0, decimal Amount_gate = 0, int Status = 0, bool IsBHYT = false, string Date_create = null, string Date_update = null, long ID = 0, string Transaction_code_tt = null, string Transaction_code_gd = null)
        {
            var requestModel = new
            {
                id = ID,
                transaction_code_tt = Transaction_code_tt,
                transaction_code_gd = Transaction_code_gd,
                booking_date = Booking_date,
                doctor_id = Doctor_id,
                subject_id = Subject_id,
                room_id = Room_id,
                service_id = 0,
                patient_name = Patient_name,
                patient_surname = Patient_surname,
                birthyear = Birthyear,
                birthdate = Birthdate,
                social_id = Social_id.ToString(),
                mobile = Mobile,
                patient_code = Patient_code,
                country_code = Country_code,
                city_id = City_id,
                district_id = District_id,
                ward_id = Ward_id,
                address = Address,
                sex = Sex,
                booking_number = Booking_number,
                bv_time = Bv_time,
                method_id = 7,
                amount = Amount,
                amount_original = 0.0,
                amount_gate = 0.0,
                status = 1,
                isBHYT = IsBHYT,
                date_create = DateTime.Now.ToString("yyyyMMddhhmm"),
                date_update = DateTime.Now.ToString("yyyyMMddhhmm"),
            };
            var response = await Post<dynamic, BookingModel>(requestModel, AppSetting.Api.Booking);
            var result = new BookingModel
            {
                Id = response.Item1.Id,
                TransactionCodeTT = response.Item1.TransactionCodeTT,
                TransactionCodeGD = response.Item1.TransactionCodeGD,
                BookingDate = response.Item1.BookingDate,
                DoctorId = response.Item1.DoctorId,
                SubjectId = response.Item1.SubjectId,
                RoomId = response.Item1.RoomId,
                PatientName = response.Item1.PatientName,
                PatientSurname = response.Item1.PatientSurname,
                Birthyear = response.Item1.Birthyear,
                Birthdate = response.Item1.Birthdate,
                SocialId = response.Item1.SocialId,
                Mobile = response.Item1.Mobile,
                PatientCode = response.Item1.PatientCode,
                CountryCode = response.Item1.CountryCode,
                CityId = response.Item1.CityId,
                DistrictId = response.Item1.DistrictId,
                WardId = response.Item1.WardId,
                Address = response.Item1.Address,
                Sex = response.Item1.Sex,
                Booking_number = response.Item1.Booking_number,
                BvTime = response.Item1.BvTime,
                MethodId = response.Item1.MethodId,
                Amount = response.Item1.Amount,
                AmountOriginal = response.Item1.AmountOriginal,
                AmountGate = response.Item1.AmountGate,
                Status = response.Item1.Status,
                IsBHYT = response.Item1.IsBHYT,
                DateCreate = response.Item1.DateCreate,
                DateUpdate = response.Item1.DateUpdate,

                Successful = response.Item2.Successful,
                Result = response.Item2.Result,
                ErrorCode = response.Item2.ErrorCode,
                ErrorMessage = response.Item2.ErrorMessage,
            };
            if (response.Item1.Id == 0)
            {
                result.Id = requestModel.id;
                result.TransactionCodeTT = requestModel.transaction_code_tt;
                result.TransactionCodeGD = requestModel.transaction_code_gd;
                result.BookingDate = requestModel.booking_date;
                result.DoctorId = requestModel.doctor_id;
                result.SubjectId = requestModel.subject_id;
                result.RoomId = requestModel.room_id;
                result.PatientName = requestModel.patient_name;
                result.PatientSurname = requestModel.patient_surname;
                result.Birthyear = requestModel.birthyear;
                result.Birthdate = requestModel.birthdate;
                result.Mobile = requestModel.mobile;
                result.PatientCode = requestModel.patient_code;
                result.CountryCode = requestModel.country_code;
                result.CityId = requestModel.city_id;
                result.DistrictId = requestModel.district_id;
                result.WardId = requestModel.ward_id;
                result.Address = requestModel.address;
                result.Sex = requestModel.sex;
                result.Booking_number = requestModel.booking_number;
                result.BvTime = requestModel.bv_time;
                result.MethodId = requestModel.method_id;
                result.Amount = requestModel.amount;
                result.Status = requestModel.status;
                result.IsBHYT = requestModel.isBHYT;
                result.DateCreate = requestModel.date_create;
                result.DateUpdate = requestModel.date_update;
            }
            try
            {
                result.SocialId = int.Parse(requestModel.social_id);
                result.AmountOriginal = Convert.ToDecimal(requestModel.amount_original, System.Globalization.CultureInfo.InvariantCulture);
                result.AmountGate = Convert.ToDecimal(requestModel.amount_gate, System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return result;
        }
        public async Task<bool> DeleteBooking(long ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, ResponseData>(requestModel, AppSetting.Api.DeleteBooking);
            return response.Item2.Successful;
        }

        public async Task<List<BookingModel>> GetBooking()
        {
            var requestModel = new
            {
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<BookingModel>>(requestModel, AppSetting.Api.GetBooking);
            return response.Item1;
        }
        public async Task<List<BookingModel>> GetBookingOfDay(string DateTime, string BookingDate)
        {
            var requestModel = new
            {
                DateTime,
                RequestTime = Now(),
                BookingDate,
            };
            var response = await Post<dynamic, List<BookingModel>>(requestModel, AppSetting.Api.GetBookingOfDay);
            return response.Item1;
        }
        public async Task<BookingModel> GetBookingByID(string ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, BookingModel>(requestModel, AppSetting.Api.GetBookingByID);
            return response.Item1;
        }
        public async Task<List<EncounterModel>> GetEncounter(string PatientCode, List<string> MMyy)
        {
            if (MMyy == null || !MMyy.Any())
            {
                MMyy = GetDefaultDate();
            }
            var requestModel = new
            {
                PatientCode,
                MMyy,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<EncounterModel>>(requestModel, AppSetting.Api.GetEncounter);
            return response.Item1;
        }
        public async Task<List<TestResultModel>> GetTestResults(string PatientCode, string EncounterCode)
        {
            var MMyy = GetDefaultDate();
            var requestModel = new
            {
                ID = PatientCode,
                EncounterCode,
                lstMMyy = string.Join("+", MMyy.ToArray()),
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<TestResultModel>>(requestModel, AppSetting.Api.GetTestResults);
            return response.Item1;
        }
        public async Task<List<ImageDiagnosticResultModel>> GetImageDiagnosticResults(string PatientCode, string EncounterCode)
        {
            var MMyy = GetDefaultDate();
            var requestModel = new
            {
                ID = PatientCode,
                EncounterCode,
                lstMMyy = string.Join("+", MMyy.ToArray()),
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<ImageDiagnosticResultModel>>(requestModel, AppSetting.Api.GetImageDiagnosticResults);
            return response.Item1;
        }
        public async Task<List<PrescriptionModel>> GetPrescription(string PatientCode, string EncounterCode, string FromDate = "", string ToDate = "", string lstMMyy = "")
        {
            var MMyy = GetDefaultDate();
            if (String.IsNullOrEmpty(lstMMyy))
            {
                lstMMyy = string.Join("+", MMyy.ToArray());
            }
            var requestModel = new
            {
                PatientCode,
                EncounterCode,
                lstMMyy,
                FromDate,
                ToDate
            };
            var response = await Post<dynamic, List<PrescriptionModel>>(requestModel, AppSetting.Api.GetPrescription);
            return response.Item1;
        }
        public async Task<PatientsModel> GetPatientByID(string ID)
        {
            var requestModel = new
            {
                ID,
            };
            var response = await Post<dynamic, PatientsModel>(requestModel, AppSetting.Api.GetPatientByID);
            return response.Item1;
        }
        public async Task<PatientsModel> GetPatientsByQrcode(string QRCode)
        {
            var requestModel = new
            {
                QRCode,
            };
            var response = await Post<dynamic, PatientsModel>(requestModel, AppSetting.Api.GetPatientsByQrcode);
            return response.Item1;
        }
        public async Task<PatientsRequestModel> PostPatients(PatientsRequestModel model)
        {
            var response = await Post<PatientsRequestModel, PatientsRequestModel>(model, AppSetting.Api.PostPatients);
            return response.Item1;
        }
        public async Task<bool> PostPatientsPhone(string ID, string Mobile)
        {
            var requestModel = new
            {
                ID,
                Mobile,
            };
            var response = await Post<dynamic, ResponseData>(requestModel, AppSetting.Api.PostPatientsPhone);
            return response.Item2.Successful;
        }
        public async Task<InsuranceModel> PostPatientsInsurance(string ID, string Ngay)
        {
            var requestModel = new
            {
                ID,
                Ngay,
            };
            var response = await Post<dynamic, InsuranceModel>(requestModel, AppSetting.Api.PostPatientsInsurance);
            return response.Item1;
        }
        public async Task<PatientsPrescriptionModel> GetPrescriptionOfDay(string DateTime)
        {
            var requestModel = new
            {
                DateTime,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, PatientsPrescriptionModel>(requestModel, AppSetting.Api.GetPrescriptionOfDay);
            return response.Item1;
        }

        public async Task<List<ReexaminationModel>> GetReexamination()
        {
            var requestModel = new
            {
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<ReexaminationModel>>(requestModel, AppSetting.Api.GetReexamination);
            return response.Item1;
        }

        public async Task<ReexaminationModel> GetReexaminationByID(string ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, ReexaminationModel>(requestModel, AppSetting.Api.GetReexaminationByID);
            return response.Item1;
        }

        public async Task<HealthzModel> Healthz()
        {
            var requestModel = new
            {
            };
            var response = await Get<dynamic, HealthzModel>(requestModel, "api/Healthz");
            return response;
        }
    }
}
