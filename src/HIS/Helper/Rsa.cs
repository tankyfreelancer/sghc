using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using HIS.Model;
using System.Xml;
using System.Security.Cryptography;

namespace HIS.Helpers
{
    public class RsaHelper
    {
        public static string Encryption(string strText, string publicKey)
        {
            try
            {
                string rsl = "";
                int length = 86;
                var testData = Encoding.UTF8.GetBytes(strText);
                int count = (int)Math.Ceiling(testData.Length / (float)length);
                using (var rsa = new RSACryptoServiceProvider(1024))
                {
                    try
                    {
                        LoadRSAXMLString(rsa, publicKey);
                        byte[] subData = new byte[length];
                        for (int i = 0; i < count; i++)
                        {
                            if (i == count - 1)
                            {
                                subData = new byte[testData.Length % length];
                                Array.Copy(testData, i * length, subData, 0, testData.Length % length);
                            }
                            else
                            {
                                Array.Copy(testData, i * length, subData, 0, length);
                            }
                            var encryptedData = rsa.Encrypt(subData, true);
                            var base64Encrypted = Convert.ToBase64String(encryptedData);
                            rsl += base64Encrypted + "~";
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    finally
                    {
                        rsa.PersistKeyInCsp = false;
                    }
                }
                return rsl;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static string SignData(string data, string privateKey, string partnerCode)
        {
            try
            {
                data = partnerCode + data + partnerCode + DateTime.Now.ToString("ddMMyyyy");
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                LoadRSAXMLString(rsa, privateKey);
                byte[] dataBytes = UnicodeEncoding.UTF8.GetBytes(data);
                byte[] signedData = rsa.SignData(dataBytes, CryptoConfig.MapNameToOID("SHA1"));
                return Convert.ToBase64String(signedData);
            }
            catch
            {
                return "";
            }
        }

        private static void LoadRSAXMLString(RSACryptoServiceProvider rsa, string publickey)
        {
            RSAParameters parameters = new RSAParameters();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(publickey);

            if (xmlDoc.DocumentElement.Name.Equals("RSAKeyValue"))
            {
                foreach (XmlNode node in xmlDoc.DocumentElement.ChildNodes)
                {
                    switch (node.Name)
                    {
                        case "Modulus":
                            parameters.Modulus = (string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText));
                            break;
                        case "Exponent":
                            parameters.Exponent = (string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText));
                            break;
                        case "P":
                            parameters.P = (string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText));
                            break;
                        case "Q":
                            parameters.Q = (string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText));
                            break;
                        case "DP":
                            parameters.DP = (string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText));
                            break;
                        case "DQ":
                            parameters.DQ = (string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText));
                            break;
                        case "InverseQ":
                            parameters.InverseQ = (string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText));
                            break;
                        case "D":
                            parameters.D = (string.IsNullOrEmpty(node.InnerText) ? null : Convert.FromBase64String(node.InnerText));
                            break;
                    }
                }
            }
            else
            {
                throw new Exception("Invalid XML RSA key.");
            }
            rsa.ImportParameters(parameters);
        }
        public static string DecryptV2(string encodedText, string key = "MQ-Gratisoft@2021")
        {
            TripleDESCryptoServiceProvider desCryptoProvider = new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider hashMD5Provider = new MD5CryptoServiceProvider();

            byte[] byteHash;
            byte[] byteBuff;

            byteHash = hashMD5Provider.ComputeHash(Encoding.UTF8.GetBytes(key));
            desCryptoProvider.Key = byteHash;
            desCryptoProvider.Mode = CipherMode.ECB; //CBC, CFB
            byteBuff = Convert.FromBase64String(encodedText);

            string plaintext = Encoding.UTF8.GetString(desCryptoProvider.CreateDecryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
            return plaintext;
        }

        public static string EncryptV2(string source, string key = "MQ-Gratisoft@2021")
        {
            TripleDESCryptoServiceProvider desCryptoProvider = new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider hashMD5Provider = new MD5CryptoServiceProvider();

            byte[] byteHash;
            byte[] byteBuff;

            byteHash = hashMD5Provider.ComputeHash(Encoding.UTF8.GetBytes(key));
            desCryptoProvider.Key = byteHash;
            desCryptoProvider.Mode = CipherMode.ECB;
            byteBuff = Encoding.UTF8.GetBytes(source);

            string encoded =
                Convert.ToBase64String(desCryptoProvider.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
            return encoded;
        }

    }
}
