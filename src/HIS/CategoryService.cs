﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HIS.Model;

namespace HIS
{
    public class CategoryService : Base, ICategoryService
    {
        public async Task<List<ScheduleModel>> GetSchedules(int? Doctor_ID = null, int? Room_ID= null, string Start_time = null, string End_time = null)
        {
            if (string.IsNullOrEmpty(Start_time) || string.IsNullOrEmpty(End_time))
            {
                Start_time = Now(format: "yyyyMMdd");
                End_time = Now(7, "yyyyMMdd");
            }
            var requestModel = new
            {
                // Doctor_ID,
                // Room_ID,
                Start_time,
                End_time,
            };
            var response = await Post<dynamic, List<ScheduleModel>>(requestModel, AppSetting.Api.GetSchedules);
            return response.Item1;
        }
        public async Task<List<ServiceModel>> GetServices()
        {
            var requestModel = new
            {
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<ServiceModel>>(requestModel, AppSetting.Api.GetServices);
            return response.Item1;
        }
        public async Task<ServiceModel> GetServiceByID(string ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, ServiceModel>(requestModel, AppSetting.Api.GetServiceByID);
            return response.Item1;
        }
        public async Task<List<BranchModel>> GetBranches()
        {
            var requestModel = new
            {
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<BranchModel>>(requestModel, AppSetting.Api.GetBranches);
            return response.Item1;
        }
        public async Task<BranchModel> GetBrancheByID(int ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, BranchModel>(requestModel, AppSetting.Api.GetBrancheByID);
            return response.Item1;
        }
        public async Task<List<DoctorModel>> GetDoctors()
        {
            var requestModel = new
            {
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<DoctorModel>>(requestModel, AppSetting.Api.GetDoctors);
            return response.Item1;
        }
        public async Task<DoctorModel> GetDoctorByID(int ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, DoctorModel>(requestModel, AppSetting.Api.GetDoctorByID);
            return response.Item1;
        }
        public async Task<List<SubjectModel>> GetSubjects()
        {
            var requestModel = new
            {
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<SubjectModel>>(requestModel, AppSetting.Api.GetSubjects);
            return response.Item1;
        }
        public async Task<SubjectModel> GetSubjectByID(int ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, SubjectModel>(requestModel, AppSetting.Api.GetSubjectByID);
            return response.Item1;
        }
        public async Task<List<RoomModel>> GetRooms()
        {
            var requestModel = new
            {
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<RoomModel>>(requestModel, AppSetting.Api.GetRooms);
            return response.Item1;
        }
        public async Task<RoomModel> GetRoomByID(int ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, RoomModel>(requestModel, AppSetting.Api.GetRoomByID);
            return response.Item1;
        }
        public async Task<List<RoomTimeSlotModel>> GetRoomTimeSlots()
        {
            var requestModel = new
            {
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<RoomTimeSlotModel>>(requestModel, AppSetting.Api.GetRoomTimeSlots);
            return response.Item1;
        }
        public async Task<RoomTimeSlotModel> GetRoomTimeSlotByID(int ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, RoomTimeSlotModel>(requestModel, AppSetting.Api.GetRoomTimeSlotByID);
            return response.Item1;
        }
        public async Task<List<PriceModel>> GetPriceList()
        {
            var requestModel = new
            {
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<PriceModel>>(requestModel, AppSetting.Api.GetPriceList);
            return response.Item1;
        }
        public async Task<List<CountryModel>> GetCountries()
        {
            var requestModel = new
            {
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<CountryModel>>(requestModel, AppSetting.Api.GetCountries);
            return response.Item1;
        }
        public async Task<CountryModel> GetCountryByID(string ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, CountryModel>(requestModel, AppSetting.Api.GetCountryByID);
            return response.Item1;
        }
        public async Task<List<CityModel>> GetCities(string country_code)
        {
            var requestModel = new
            {
                country_code,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<CityModel>>(requestModel, AppSetting.Api.GetCities);
            return response.Item1;
        }

        public async Task<CityModel> GetCityByID(int ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, CityModel>(requestModel, AppSetting.Api.GetCityByID);
            return response.Item1;
        }
        public async Task<List<DistrictModel>> GetDistricts(string city_id)
        {
            var requestModel = new
            {
                city_id,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<DistrictModel>>(requestModel, AppSetting.Api.GetDistricts);
            return response.Item1;
        }
        public async Task<DistrictModel> GetDistrictByID(string ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, DistrictModel>(requestModel, AppSetting.Api.GetDistrictByID);
            return response.Item1;
        }
        public async Task<List<JobModel>> GetJobs()
        {
            var requestModel = new
            {
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<JobModel>>(requestModel, AppSetting.Api.GetJobs);
            return response.Item1;
        }
        public async Task<JobModel> GetJobByID(string ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, JobModel>(requestModel, AppSetting.Api.GetJobByID);
            return response.Item1;
        }
        public async Task<List<NationModel>> GetNations()
        {
            var requestModel = new
            {
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<NationModel>>(requestModel, AppSetting.Api.GetNations);
            return response.Item1;
        }
        public async Task<NationModel> GetNationByID(string ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, NationModel>(requestModel, AppSetting.Api.GetNationByID);
            return response.Item1;
        }
        public async Task<List<WardModel>> GetWards(string district_id)
        {
            var requestModel = new
            {
                district_id,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, List<WardModel>>(requestModel, AppSetting.Api.GetWards);
            return response.Item1;
        }
        public async Task<WardModel> GetWardByID(string ID)
        {
            var requestModel = new
            {
                ID,
                RequestTime = Now(),
            };
            var response = await Post<dynamic, WardModel>(requestModel, AppSetting.Api.GetWardByID);
            return response.Item1;
        }
    }
}
