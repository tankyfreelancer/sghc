using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    public class AppSetting
    {
        public AppSetting(){
            Api = new API();
            RSA = new RSA();
        }
        [JsonProperty(PropertyName = "Api")]
        public API Api { get; set; }
        [JsonProperty(PropertyName = "RSA")]
        public RSA RSA { get; set; }
    }
    public class API
    {
        [JsonProperty(PropertyName = "Base")]
        public string Base { get; set; }

        [JsonProperty(PropertyName = "PatientsDebit")]
        public string PatientsDebit { get; set; }
        [JsonProperty(PropertyName = "PaymentInfo")]
        public string PaymentInfo { get; set; }
        [JsonProperty(PropertyName = "Booking")]
        public string Booking { get; set; }
        [JsonProperty(PropertyName = "DeleteBooking")]
        public string DeleteBooking { get; set; }
        [JsonProperty(PropertyName = "GetBooking")]
        public string GetBooking { get; set; }
        [JsonProperty(PropertyName = "GetBookingOfDay")]
        public string GetBookingOfDay { get; set; }
        [JsonProperty(PropertyName = "GetBookingByID")]
        public string GetBookingByID { get; set; }
        [JsonProperty(PropertyName = "GetEncounter")]
        public string GetEncounter { get; set; }
        [JsonProperty(PropertyName = "GetTestResults")]
        public string GetTestResults { get; set; }
        [JsonProperty(PropertyName = "GetImageDiagnosticResults")]
        public string GetImageDiagnosticResults { get; set; }
        [JsonProperty(PropertyName = "GetPrescription")]
        public string GetPrescription { get; set; }
        [JsonProperty(PropertyName = "GetPatientByID")]
        public string GetPatientByID { get; set; }
        [JsonProperty(PropertyName = "GetPatientsByQrcode")]
        public string GetPatientsByQrcode { get; set; }
        [JsonProperty(PropertyName = "PostPatients")]
        public string PostPatients { get; set; }
        [JsonProperty(PropertyName = "PostPatientsPhone")]
        public string PostPatientsPhone { get; set; }
        [JsonProperty(PropertyName = "PostPatientsInsurance")]
        public string PostPatientsInsurance { get; set; }
        [JsonProperty(PropertyName = "GetPrescriptionOfDay")]
        public string GetPrescriptionOfDay { get; set; }
        [JsonProperty(PropertyName = "GetReexamination")]
        public string GetReexamination { get; set; }
        [JsonProperty(PropertyName = "GetReexaminationByID")]
        public string GetReexaminationByID { get; set; }


        [JsonProperty(PropertyName = "GetSchedules")]
        public string GetSchedules { get; set; }
        [JsonProperty(PropertyName = "GetServices")]
        public string GetServices { get; set; }
        [JsonProperty(PropertyName = "GetServiceByID")]
        public string GetServiceByID { get; set; }
        [JsonProperty(PropertyName = "GetBranches")]
        public string GetBranches { get; set; }
        [JsonProperty(PropertyName = "GetBrancheByID")]
        public string GetBrancheByID { get; set; }
        [JsonProperty(PropertyName = "GetDoctors")]
        public string GetDoctors { get; set; }
        [JsonProperty(PropertyName = "GetDoctorByID")]
        public string GetDoctorByID { get; set; }
        [JsonProperty(PropertyName = "GetSubjects")]
        public string GetSubjects { get; set; }
        [JsonProperty(PropertyName = "GetSubjectByID")]
        public string GetSubjectByID { get; set; }
        [JsonProperty(PropertyName = "GetRooms")]
        public string GetRooms { get; set; }
        [JsonProperty(PropertyName = "GetRoomByID")]
        public string GetRoomByID { get; set; }
        [JsonProperty(PropertyName = "GetRoomTimeSlots")]
        public string GetRoomTimeSlots { get; set; }
        [JsonProperty(PropertyName = "GetRoomTimeSlotByID")]
        public string GetRoomTimeSlotByID { get; set; }
        [JsonProperty(PropertyName = "GetPriceList")]
        public string GetPriceList { get; set; }
        [JsonProperty(PropertyName = "GetCountries")]
        public string GetCountries { get; set; }
        [JsonProperty(PropertyName = "GetCountryByID")]
        public string GetCountryByID { get; set; }
        [JsonProperty(PropertyName = "GetCities")]
        public string GetCities { get; set; }
        [JsonProperty(PropertyName = "GetCityByID")]
        public string GetCityByID { get; set; }
        [JsonProperty(PropertyName = "GetDistricts")]
        public string GetDistricts { get; set; }
        [JsonProperty(PropertyName = "GetDistrictByID")]
        public string GetDistrictByID { get; set; }
        [JsonProperty(PropertyName = "GetWards")]
        public string GetWards { get; set; }
        [JsonProperty(PropertyName = "GetWardByID")]
        public string GetWardByID { get; set; }
        [JsonProperty(PropertyName = "GetJobs")]
        public string GetJobs { get; set; }
        [JsonProperty(PropertyName = "GetJobByID")]
        public string GetJobByID { get; set; }
        [JsonProperty(PropertyName = "GetNations")]
        public string GetNations { get; set; }
        [JsonProperty(PropertyName = "GetNationByID")]
        public string GetNationByID { get; set; }
    }
    public class RSA
    {
        [JsonProperty(PropertyName = "PrivateKey")]
        public string PrivateKey { get; set; }
        [JsonProperty(PropertyName = "PublicKey")]
        public string PublicKey { get; set; }
        [JsonProperty(PropertyName = "PartnerCode")]
        public string PartnerCode { get; set; }
    }
}
