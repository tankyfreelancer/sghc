using Newtonsoft.Json;

namespace HIS.Model
{
    // Debit,PaymentInfoModel
    public class RequestOTPModel
    {
  	    [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }
        [JsonProperty(PropertyName = "session_id")]
        public string SessionId { get; set; }
        [JsonProperty(PropertyName = "BrandName")]
        public string BrandName { get; set; }
        [JsonProperty(PropertyName = "Phone")]
        public string Phone { get; set; }
        [JsonProperty(PropertyName = "Message")]
        public string Message { get; set; }
        [JsonProperty(PropertyName = "RequestId")]
        public string RequestId { get; set; }
    }
    public class ResponseOTPModel {
        [JsonProperty(PropertyName = "MessageId")]
        public string MessageId { get; set; }
        [JsonProperty(PropertyName = "Phone")]
        public string Phone { get; set; }
        [JsonProperty(PropertyName = "BrandName")]
        public string BrandName { get; set; }
        [JsonProperty(PropertyName = "Message")]
        public string Message { get; set; }
        [JsonProperty(PropertyName = "PartnerId")]
        public string PartnerId { get; set; }
        [JsonProperty(PropertyName = "Telco")]
        public string Telco { get; set; }
        [JsonProperty(PropertyName = "error")]
        public int Error { get; set; }
        [JsonProperty(PropertyName = "error_description")]
        public string ErrorDescription { get; set; }
        public string HisError { get; set; }
    }
}