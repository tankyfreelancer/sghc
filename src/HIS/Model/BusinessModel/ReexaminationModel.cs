﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // ReexaminationModel
    public class ReexaminationModel :BaseModel
    {
        [JsonProperty(PropertyName = "PatientCode")]
        public string PatientCode { get; set; }
        [JsonProperty(PropertyName = "ReexaminationDate")]
        public string ReexaminationDate { get; set; }
        [JsonProperty(PropertyName = "RoomID")]
        public string RoomID { get; set; }
        [JsonProperty(PropertyName = "DoctorID")]
        public string DoctorID { get; set; }
        [JsonProperty(PropertyName = "SubjectID")]
        public string SubjectID { get; set; }
    }
}
