﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // Debit,PaymentInfoModel
    public class DebitModel :BaseModel
    {
        [JsonProperty(PropertyName = "ReturnCode")]
        public string ReturnCode { get; set; }
        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "Amount")]
        public string Amount { get; set; }
        [JsonProperty(PropertyName = "ResponseTime")]
        public string ResponseTime { get; set; }
        [JsonProperty(PropertyName = "AdditionalData")]
        public string AdditionalData { get; set; }
        [JsonProperty(PropertyName = "IDKhoa")]
        public string IDKhoa { get; set; }
        [JsonProperty(PropertyName = "MaBN")]
        public string MaBN { get; set; }
        [JsonProperty(PropertyName = "IDNoiThucHienCLS")]
        public string IDNoiThucHienCLS { get; set; }
        [JsonProperty(PropertyName = "STTNoiThucHienCLS")]
        public string STTNoiThucHienCLS { get; set; }
        [JsonProperty(PropertyName = "TenNoiThucHien")]
        public string TenNoiThucHien { get; set; }
    }
}
