﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // PatientsModel
    public class PatientsModel : BaseModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "surname")]
        public string Surname { get; set; }
        [JsonProperty(PropertyName = "sex")]
        public int Sex { get; set; }
        [JsonProperty(PropertyName = "birthyear")]
        public int Birthyear { get; set; }
        [JsonProperty(PropertyName = "birthdate")]
        public string Birthdate { get; set; }
        [JsonProperty(PropertyName = "mobile")]
        public string Mobile { get; set; }
        [JsonProperty(PropertyName = "social_id")]
        public string SocialId { get; set; }
        [JsonProperty(PropertyName = "country")]
        public Country Country { get; set; }
        [JsonProperty(PropertyName = "country_code")]
        public string CountryCode { get; set; }
        [JsonProperty(PropertyName = "city")]
        public City City { get; set; }
        [JsonProperty(PropertyName = "city_id")]
        public string CityId { get; set; }
        [JsonProperty(PropertyName = "district")]
        public District District { get; set; }
        [JsonProperty(PropertyName = "district_id")]
        public string DistrictId { get; set; }
        [JsonProperty(PropertyName = "ward")]
        public Ward Ward { get; set; }
        [JsonProperty(PropertyName = "ward_id")]
        public string WardId { get; set; }
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }
        [JsonProperty(PropertyName = "bhyt_code")]
        public string BhytCode { get; set; }
    }
    // PatientsModel
    public class PatientsRequestModel : BaseModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "surname")]
        public string Surname { get; set; }
        [JsonProperty(PropertyName = "sex")]
        public int Sex { get; set; }
        [JsonProperty(PropertyName = "Birthyear")]
        public int Birthyear { get; set; }
        [JsonProperty(PropertyName = "birthdate")]
        public string Birthdate { get; set; }
        [JsonProperty(PropertyName = "mobile")]
        public string Mobile { get; set; }
        [JsonProperty(PropertyName = "social_id")]
        public string SocialId { get; set; }
        [JsonProperty(PropertyName = "country_code")]
        public string CountryCode { get; set; }
        [JsonProperty(PropertyName = "city_id")]
        public string CityId { get; set; }
        [JsonProperty(PropertyName = "district_id")]
        public string DistrictId { get; set; }
        [JsonProperty(PropertyName = "ward_id")]
        public string WardId { get; set; }
        [JsonProperty(PropertyName = "Address")]
        public string Address { get; set; }
        // adding new request Jun 24, 2023
        public string Booking_date { get; set; }
        public string Patient_name { get; set; }
        public string Patient_surname { get; set; }
        public string Patient_code { get; set; }
        public string Note { get; set; }
        public bool isBHYT { get; set; }

        [Required]
        public string transaction_code_tt { get; set; }

        [Required]
        public string transaction_code_gd { get; set; }

        [StringLength(12)]
        [Required]
        public string booking_date { get; set; }

        [Required]
        public string doctor_id { get; set; }

        [Required]
        public int subject_id { get; set; }

        [Required]
        public int room_id { get; set; }

        [Required]
        public int service_id { get; set; }

        [Required]
        public string patient_name { get; set; }
        public string patient_surname { get; set; }

        [Required]
        public int birthyear { get; set; }

        [StringLength(8)]
        [Required]
        public string patient_code { get; set; }


        public string address { get; set; }

        [StringLength(12)]
        [Required]
        public string date_create { get; set; }

        [StringLength(12)]
        [Required]
        public string date_update { get; set; }


        [Required]
        public int booking_number { get; set; }
        public string bv_time { get; set; }

        [Required]
        public int method_id { get; set; }

        [Required]
        public decimal amount { get; set; }

        [Required]
        public decimal amount_original { get; set; }

        [Required]
        public decimal amount_gate { get; set; }

        [Required]
        public int status { get; set; }


        public string AppID { get; set; }

        public string BookingID { get; set; }

        public string QRCode { get; set; }


        public string Idgoi { get; set; }

        public int patient_type { get; set; }
    }
    public class Country : BaseModel
    {

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }
    }
    public class City : BaseModel
    {

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "country_code")]
        public string CountryCode { get; set; }
    }
    public class District : BaseModel
    {

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "city_id")]
        public string CityId { get; set; }
    }
    public class Ward : BaseModel
    {

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "district_id")]
        public string DistrictId { get; set; }
    }
}
