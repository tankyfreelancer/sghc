﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // EncounterModel
    public class EncounterModel :BaseModel
    {
        [JsonProperty(PropertyName = "CreateDate")]
        public string CreateDate { get; set; }
        [JsonProperty(PropertyName = "RoomID")]
        public string RoomID { get; set; }
        [JsonProperty(PropertyName = "RoomName")]
        public string RoomName { get; set; }
        [JsonProperty(PropertyName = "EncounterCode")]
        public string EncounterCode { get; set; }
        [JsonProperty(PropertyName = "ICDCode")]
        public string ICDCode { get; set; }
		public string ReExamDate { get; set; }
        public string amountReExam { get; set; }
        public string OutDate { get; set; }
        [JsonProperty(PropertyName = "ICDName")]
        public string ICDName { get; set; }
        [JsonProperty(PropertyName = "DoctorName")]
        public string DoctorName { get; set; }
    }
}
