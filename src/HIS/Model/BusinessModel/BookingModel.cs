﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // Debit,PaymentInfoModel
    public class BookingModel :BaseModel
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }
        [JsonProperty(PropertyName = "transaction_code_tt")]
        public string TransactionCodeTT { get; set; }
        [JsonProperty(PropertyName = "transaction_code_gd")]
        public string TransactionCodeGD { get; set; }
        [JsonProperty(PropertyName = "booking_date")]
        public string BookingDate { get; set; }
        [JsonProperty(PropertyName = "Doctor_id")]
        public int DoctorId { get; set; }
        [JsonProperty(PropertyName = "subject_id")]
        public int SubjectId { get; set; }
        [JsonProperty(PropertyName = "room_id")]
        public int RoomId { get; set; }
        [JsonProperty(PropertyName = "patient_name")]
        public string PatientName { get; set; }
        [JsonProperty(PropertyName = "patient_surname")]
        public string PatientSurname { get; set; }
        [JsonProperty(PropertyName = "birthyear")]
        public int Birthyear { get; set; }
        [JsonProperty(PropertyName = "birthdate")]
        public string Birthdate { get; set; }
        [JsonProperty(PropertyName = "social_id")]
        public int SocialId { get; set; }
        [JsonProperty(PropertyName = "mobile")]
        public string Mobile { get; set; }
        [JsonProperty(PropertyName = "patient_code")]
        public string PatientCode { get; set; }
        [JsonProperty(PropertyName = "country_code")]
        public string CountryCode { get; set; }
        [JsonProperty(PropertyName = "city_id")]
        public int CityId { get; set; }
        [JsonProperty(PropertyName = "district_id")]
        public int DistrictId { get; set; }
        [JsonProperty(PropertyName = "ward_id")]
        public int WardId { get; set; }
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }
        [JsonProperty(PropertyName = "sex")]
        public int Sex { get; set; }
        [JsonProperty(PropertyName = "booking_number")]
        public int Booking_number { get; set; }
        [JsonProperty(PropertyName = "bv_time")]
        public string BvTime { get; set; }
        [JsonProperty(PropertyName = "method_id")]
        public int MethodId { get; set; }
        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }
        [JsonProperty(PropertyName = "amount_original")]
        public decimal AmountOriginal { get; set; }
        [JsonProperty(PropertyName = "amount_gate")]
        public decimal AmountGate { get; set; }
        [JsonProperty(PropertyName = "status")]
        public int Status { get; set; }
        [JsonProperty(PropertyName = "isBHYT")]
        public bool IsBHYT { get; set; }
        [JsonProperty(PropertyName = "date_create")]
        public string DateCreate { get; set; }
        [JsonProperty(PropertyName = "date_update")]
        public string DateUpdate { get; set; }
    }
}
