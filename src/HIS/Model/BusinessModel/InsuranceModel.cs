﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    public class InsuranceModel :BaseModel
    {
        [JsonProperty(PropertyName = "result")]
        public new string Result { get; set; }
         [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }
         [JsonProperty(PropertyName = "expirationdate")]
        public string Expirationdate { get; set; }
    }
}
