﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // ImageDiagnosticResultModel
    public class ImageDiagnosticResultModel :BaseModel
    {
        [JsonProperty(PropertyName = "TypeID")]
        public string TypeID{ get; set; }
        [JsonProperty(PropertyName = "TypeName")]
        public string TypeName{ get; set; }
        [JsonProperty(PropertyName = "CreateDate")]
        public string CreateDate{ get; set; }
        [JsonProperty(PropertyName = "Name")]
        public string Name{ get; set; }
        [JsonProperty(PropertyName = "Description")]
        public string Description{ get; set; }
        [JsonProperty(PropertyName = "Conclusion")]
        public string Conclusion{ get; set; }
        [JsonProperty(PropertyName = "Indication")]
        public string Indication{ get; set; }
        [JsonProperty(PropertyName = "Base64String")]
        public string Base64String{ get; set; }
        [JsonProperty(PropertyName = "ImageBinary")]
        public byte[] ImageBinary{ get; set; }
    }
}
