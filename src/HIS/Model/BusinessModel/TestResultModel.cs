﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // TestResultModel
    public class TestResultModel :BaseModel
    {
        [JsonProperty(PropertyName = "GettingSampleDate")]
        public string GettingSampleDate { get; set; }
        [JsonProperty(PropertyName = "ReturnDate")]
        public string ReturnDate { get; set; }
        [JsonProperty(PropertyName = "TypeOrderNumber")]
        public string TypeOrderNumber { get; set; }
        [JsonProperty(PropertyName = "TypeName")]
        public string TypeName { get; set; }
        [JsonProperty(PropertyName = "SymptonOrderNumber")]
        public string SymptonOrderNumber { get; set; }
        [JsonProperty(PropertyName = "SymptonID")]
        public string SymptonID { get; set; }
        [JsonProperty(PropertyName = "SymptonName")]
        public string SymptonName { get; set; }
        [JsonProperty(PropertyName = "ResultOrderNumber")]
        public string ResultOrderNumber { get; set; }
        [JsonProperty(PropertyName = "ResultID")]
        public string ResultID { get; set; }
        [JsonProperty(PropertyName = "ResultName")]
        public string ResultName { get; set; }
        [JsonProperty(PropertyName = "Result")]
        public new string ResultPoint { get; set; }
        [JsonProperty(PropertyName = "Unit")]
        public string Unit { get; set; }
        [JsonProperty(PropertyName = "UsualIndex")]
        public string UsualIndex { get; set; }
        [JsonProperty(PropertyName = "Note")]
        public string Note { get; set; }
        [JsonProperty(PropertyName = "Base64String")]
        public string Base64String { get; set; }
    }
}
