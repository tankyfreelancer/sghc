﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // PrescriptionModel
    public class PrescriptionModel :BaseModel
    {
        [JsonProperty(PropertyName = "Name")]
        public string Name{ get; set; }
        [JsonProperty(PropertyName = "Quantity")]
        public string Quantity{ get; set; }
        [JsonProperty(PropertyName = "Type")]
        public string Type{ get; set; }
        [JsonProperty(PropertyName = "Dosage")]
        public string Dosage{ get; set; }
        public string DosageText{ get; set; }
        public string Morning{ get; set; }
        public string Noon{ get; set; }
        public string AfterNoon{ get; set; }
        public string Evening{ get; set; }
        [JsonProperty(PropertyName = "CreateDate")]
        public string CreateDate{ get; set; }
    }
        // PatientsPrescriptionModel
    public class PatientsPrescriptionModel :BaseModel
    {
        [JsonProperty(PropertyName ="id")]
        public string Id{ get; set; }
        [JsonProperty(PropertyName ="patientCode")]
        public string PatientCode{ get; set; }
        [JsonProperty(PropertyName ="patientName")]
        public string PatientName{ get; set; }
        [JsonProperty(PropertyName ="roomID")]
        public string RoomID{ get; set; }
        [JsonProperty(PropertyName ="roomName")]
        public string RoomName{ get; set; }
        [JsonProperty(PropertyName ="subjectID")]
        public string SubjectID{ get; set; }
        [JsonProperty(PropertyName ="subjectName")]
        public string SubjectName{ get; set; }
        [JsonProperty(PropertyName ="createDate")]
        public string CreateDate{ get; set; }
        [JsonProperty(PropertyName ="fromDate")]
        public string FromDate{ get; set; }
        [JsonProperty(PropertyName ="toDate")]
        public string ToDate{ get; set; }
        [JsonProperty(PropertyName ="name")]
        public string Name{ get; set; }
        [JsonProperty(PropertyName ="unit")]
        public string Unit{ get; set; }
        [JsonProperty(PropertyName ="dosage")]
        public string Dosage{ get; set; }
        [JsonProperty(PropertyName ="description")]
        public string Description{ get; set; }
        [JsonProperty(PropertyName ="note")]
        public string Note{ get; set; }
    }
}
