using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // "{\"successful\":false,\"result\":null,\"errorCode\":7777,\"errorMessage\":\"Json khong dung format\"}"
    public class BaseModel
    {
        [JsonProperty(PropertyName = "successful")]
        public virtual bool Successful { get; set; }
        [JsonProperty(PropertyName = "result")]
        public virtual string Result { get; set; }
        [JsonProperty(PropertyName = "errorCode")]
        public virtual int ErrorCode { get; set; }
        [JsonProperty(PropertyName = "errorMessage")]
        public virtual string ErrorMessage { get; set; }
    }
}