﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // PriceModel
    public class PriceModel :BaseModel
    {
        [JsonProperty(PropertyName = "ID")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "Price")]
        public string Price { get; set; }
        [JsonProperty(PropertyName = "TypeID")]
        public string TypeID { get; set; }
        [JsonProperty(PropertyName = "TypeName")]
        public string TypeName { get; set; }
        [JsonProperty(PropertyName = "GroupID")]
        public string GroupID { get; set; }
        [JsonProperty(PropertyName = "GroupName")]
        public string GroupName { get; set; }
    }
}

