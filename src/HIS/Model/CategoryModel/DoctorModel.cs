﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // DoctorModel
    public class DoctorModel :BaseModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "fullname")]
        public string Fullname { get; set; }
        [JsonProperty(PropertyName = "birthyear")]
        public int Birthyear { get; set; }
        [JsonProperty(PropertyName = "sex")]
        public bool Sex { get; set; }
        [JsonProperty(PropertyName = "role")]
        public string Role { get; set; }
    }
}
