﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // RoomModel
    public class RoomModel :BaseModel
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "subject_id")]
        public int? SubjectId { get; set; }
        [JsonProperty(PropertyName = "price_bh")]
        public decimal PriceBH { get; set; }
        [JsonProperty(PropertyName = "price_dv")]
        public decimal PriceDV { get; set; }
        [JsonProperty(PropertyName = "idbranch")]
        public string IdBranch { get; set; }
        [JsonProperty(PropertyName = "room_number")]
        public string RoomNumber { get; set; }
    }
    public class RoomTimeSlotModel:BaseModel{
          [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "start_time")]
        public string StartTime { get; set; }
        [JsonProperty(PropertyName = "end_time")]
        public string EndTime { get; set; }
        [JsonProperty(PropertyName = "startnumber")]
        public int StartNumber { get; set; }
        [JsonProperty(PropertyName = "maxnumber")]
        public int MaxNumber { get; set; }
        [JsonProperty(PropertyName = "numberofjumps")]
        public int NumberOfJumps { get; set; }
    }
}
