﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // BranchModel
    public class BranchModel :BaseModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
    }
}

