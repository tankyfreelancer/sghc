﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // ServiceModel
    public class ServiceModel :BaseModel
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "price_dv")]
        public decimal PriceDV { get; set; }
        [JsonProperty(PropertyName = "price_bh")]
        public decimal PriceBH { get; set; }
    }
}
