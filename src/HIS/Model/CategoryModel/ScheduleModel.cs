﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // ScheduleModel
    public class ScheduleModel :BaseModel
    {
        [JsonProperty(PropertyName = "doctor_id")]
        public string DoctorId { get; set; }
        [JsonProperty(PropertyName = "room_id")]
        public string RoomId { get; set; }
        [JsonProperty(PropertyName = "start_time")]
        public string StartTime { get; set; }
        [JsonProperty(PropertyName = "end_time")]
        public string EndTime { get; set; }
    }
}
