﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    // SubjectModel
    public class SubjectModel :BaseModel
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "default_price")]
        public decimal? DefaultPrice { get; set; }
    }
}
