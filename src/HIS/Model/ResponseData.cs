﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace HIS.Model
{
    public class ResponseData
    {
        [JsonProperty(PropertyName = "successful")]
        public bool Successful { get; set; }
        [JsonProperty(PropertyName = "result")]
        public string Result { get; set; }
        [JsonProperty(PropertyName = "errorCode")]
        public string ErrorCode { get; set; }
        [JsonProperty(PropertyName = "errorMessage")]
        public string ErrorMessage { get; set; }
    }
    public class RequestModel
    {
        [JsonProperty(PropertyName = "Data")]
        public string Data { get; set; }
        [JsonProperty(PropertyName = "Signature")]
        public string Signature { get; set; }
    }
    public class HealthzModel 
    {
        [JsonProperty(PropertyName = "start")]
        public string Start { get; set; }
        [JsonProperty(PropertyName = "uptime")]
        public string Uptime { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }
    }
}
