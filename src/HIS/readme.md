 

 TRỤ SỞ CHÍNH
Bệnh viện quận Thủ Đức
Đại chỉ: Số 29, Đường Phú Châu, Khu Phố 5, Phường Tam Phú, Quận Thủ Đức, TP. Hồ Chí Minh Điện thoại: 0966331010
Email: bv.thuduc@tphcm.gov.vn Website:benhvienthuduc.vn.vn


Bệnh viện Quận Thủ Đức – IT MQ Solutions



MÔ TẢ KẾT NỐI WEB API THANH TOÁN HÓA ĐƠN BẰNG BILLING FOR PKH



Tác giả:

Nguyễn Văn Long
Nguyễn Ngọc Sơn
Phan Văn Bảo An
Trịnh Quốc Văn

Tài liệu đặc tả kỹ thuật
Phiên bản 2.1

Tháng 01 - Năm 2020
MỤC LỤC

I.	GIỚI THIỆU	4
II.	API NGHIỆP VỤ	5
1.	TruyVanNo: Lấy thông tin dư nợ của bệnh nhân từ HIS cho Billing	5
2.	ThanhToan: Nhận thông tin trạng thái thanh toán hóa đơn từ mã Billing	6
3.	PostBooking: Đăng ký khám bệnh	7
4.	Delbooking: Xóa đăng ký khám bệnh theo ID	9
5.	GetBooking: Lấy thông tin đăng ký khám bệnh	9
6.	GetBookingOfDay: Lấy thông tin đăng ký khám theo ngày	10
7.	GetBookingByID: Lấy thông tin đăng ký khám bệnh theo ID	11
8.	GetEncounter: Lấy thông tin vào viện	11
9.	GetTestResults: Lấy kết quả xét nghiệm	12
10.	GetImageDiagnosticResults: Lấy kết quả chẩn đoán hình ảnh	13
11.	GetPrescription: Lấy thông tin toa thuốc	13
12.	GetPatientByID: Lấy thông tin Bệnh nhân theo ID (Mã bệnh nhân)	14
13.	GetPatientsQrcode: Lấy thông tin Bệnh nhân theo QRCode	15
14.	PostPatients: Thêm thông tin Bệnh nhân mới	16
15.	PostPatients_mobile: Cập nhật số điện thoại Bệnh nhân	17
16.	PostPatients_checkbh: Kiểm tra thẻ BHYT	17
17.	GetPrescriptionOfDay: Lấy thông tin sử dụng thuốc trong ngày	18
18.	GetReexamination: Lấy thông tin tái khám	18
19.	GetReexaminationByID: Lấy thông tin tái khám theo ID (mã bệnh nhân)	19
III.	API DANH MỤC	19
20.	GetSchedules: Lấy lịch khám bệnh	19
21.	GetServices: Lấy thông tin dịch vụ	20
22.	GetServiceByID: Lây thông tin dịch vụ theo ID	20
23.	GetBranches: Lấy danh sách cơ sở	21
24.	GetBrancheByID: Lấy thông tin cơ sở theo ID	21
25.	GetDoctors: Lấy thông tin Bác Sĩ	22
26.	GetDoctorByID: Lấy thông tin Bác Sĩ theo ID	22
27.	GetSubjects: Lấy danh sách chuyên khoa	23
28.	GetSubjectByID: Lấy thông tin chuyên khoa theo ID	23
29.	GetRooms: Lấy danh sách phòng khám	24
30.	GetRoomByID: Lấy thông tin phòng khám theo ID	24
31.	GetRoomTimeSlots: Lấy danh sách thời gian khám bệnh của phòng	25
32.	GetRoomTimeSlotByID: Lấy thời gian khám bệnh của phòng theo ID	25
33.	GetPriceList: Lấy thông tin giá dịch vụ, giá thuốc, viện phí	26
34.	GetCountries: Lấy thông tin Quốc tịch	26
35.	GetCountryByID: Lấy thông tin Quốc tịch theo ID	27
36.	GetCities: Lấy danh sách Tỉnh/thành phố	27
37.	GetCityByID: Lấy thông tin Tỉnh/thành phố theo ID	28
38.	GetDistricts: Lấy thông tin Quận/huyện	28
39.	GetDistrictByID: Lấy thông tin Quận/huyện theo ID	29
40.	GetWards: Lấy danh sách phường/ xã	29
41.	GetWardByID: Lấy thông tin phường/ xã theo ID	30
42.	GetJobs: Lấy danh sách nghề nghiệp	30
43.	GetJobByID: Lấy thông tin nghề nghiệp theo ID	31
44.	GetNations: Lấy danh sách dân tộc	31
45.	GetNationByID: Lấy thông tin dân tộc theo ID	32

⦁	GIỚI THIỆU
 Tài liệu này mô tả chuẩn kỹ thuật kết nối thanh toán hóa đơn giữa Bệnh viện Quận Thủ Đức – IT MQ Solutions và PKH áp dụng cho các kênh thanh toán mã Billing PKH.





	


Để đảm bảo việc truyền tải dữ liệu được thông suốt và bảo mật, mỗi API trước khi gửi yêu cầu tới máy chủ cần được mã hóa các thông tin và tạo chữ ký số bằng thuật toán RSA.

Tất cả các phương thức POST đều có Body với định dạng Json là:
{
	"Data": Dữ liệu Json của mỗi API khi gửi yêu cầu đã mã hóa
	,"Signature": Chữ ký số được tạo từ Dữ liệu Json + Mã đối tác + Ngày hiện hành
}

⦁	Tạo Data từ việc mã hóa Json sử dụng phương thức Encryption với publicKey
⦁	Tạo signature từ Json + Mã đối tác + Ngày hiện hành với phương thức SignData với privateKey  
⦁	Phương thức Encryption, SignData, publicKey, privateKey BVTD-ITMQS sẽ cung cấp cho PKH






















⦁	API NGHIỆP VỤ
⦁	TruyVanNo: Lấy thông tin dư nợ của bệnh nhân từ HIS cho Billing
  Thông tin môi trường: URL http://118.69.66.93:8080/BillingPKH/TruyVanNo
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
UserID	String(20)	Mã hóa đơn
ProviderID	String(10)	Mã nhà cung cấp
ServiceID	String(10)	Mã dịch vụ
Password	String(20)	Mật khẩu của khách hàng
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss


Kết quả trả về là chuỗi ký tự có định dạng Response
strData = ReturnCode + "|" + Description  + "|" + Amount + "|" + ResponseTime + "|" + AdditionalData + "|" +  IDKhoa  + "|" + MaBN+ "|" + IDNoiThucHienCLS+ "|" + STTNoiThucHienCLS+ "|" + TenNoiThucHien;

{"successful": true,"result": "0|Nguyen Van A|1,000,000|12/11/2019 09:08:24|Thanh toan thanh cong: 123456789012345|0|0","errorCode": 0,"errorMessage": null}


hISDataResponse = new HISDataBillingResponse
{
successful = true,
result = strData,
errorCode = "0",
errorMessage = ""
};
Retunt Ok(hISDataResponse);
⦁	Các mã lỗi:
{"errorCode": 2000,"errorMessage": "Id thanh toan: 123456789012345 khong ton tai"}
{"errorCode": 2001,"errorMessage": "Signature khong hop le"}
{"errorCode": 2004,"errorMessage": " Id thanh toan: 123456789012345 so tien no = 0"}
{"errorCode": 2006,"errorMessage": " Hoa don da thanh toan"}
{"errorCode": 2008,"errorMessage": "Id thanh toan khong hop le"}
{"errorCode": 7777,"errorMessage": "Tham so truyen vao khong hop le"}
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}










⦁	ThanhToan: Nhận thông tin trạng thái thanh toán hóa đơn từ mã Billing
Thông tin môi trường : URL http://118.69.66.93:8080/BillingPKH/ThanhToan
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
UserID	String(20)	Mã hóa đơn
ProviderID	String(10)	Mã nhà cung cấp (ID MoMo, ID VNPay, ID ATM nội địa…)
ServiceID	String(10)	Mã dịch vụ
ChannelID	String(3)	Mã kênh thanh toán 
Password	String(20)	Mật khẩu của khách hàng (nếu cần xác thực để truy vấn thông tin)
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
Amount	Decimal	Số tiền thanh toán (hiện nay mặc định loại tiền thanh toán là VND)
TransactionCode	String(20)	Mã giao dịch trong hệ thống PKH
TransactionDetail	String(100)	Thông tin giao dịch
AdditionalInfo	String(100)	Thông tin bổ sung (nếu có)

Kết quả trả về là chuỗi ký tự có định dạng 3 Định dạnh Response:
* Truy vấn thành công:
{"successful": true,"result": "0|Nguyen Van A|1,000,000|12/11/2019 09:08:24|Thanh toan thanh cong: 123456789012345|0|0","errorCode": 0,"errorMessage": null}

⦁	Truy vấn không thành công:
{"successful": false,"result": "-1|Thanh toan that bai|0|12/11/2019 09:08:24| Thanh toan that bai |0|0","errorCode": 2005,"errorMessage": null}

⦁	Các mã lỗi:
{"errorCode": 2000,"errorMessage": "ID thanh toan 123456789012345 khong hop le"}
{"errorCode": 2001,"errorMessage": "Signature khong hop le"}
{"errorCode": 2004,"errorMessage": "ID thanh toan: 123456789012345 da gach no"}
{"errorCode": 2005,"errorMessage": "So tien gach no khong khop no benh nhan"}
{"errorCode": 2006,"errorMessage": "ID thanh toan: 123456789012345 da thanh toan"}
{"errorCode": 7777,"errorMessage": "Tham so truyen vao khong hop le"}
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}







⦁	PostBooking: Đăng ký khám bệnh
Thông tin môi trường: URL http://118.69.66.93:8080/Booking/PostBooking
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	Long	ID booking
Transaction_code_tt	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
Transaction_code_gd	String	Mã giao dịch
Booking_date	String	Ngày đăng ký khám bệnh
Doctor_id	Int	ID Bác Sĩ
Subject_id	Int	ID Chuyên khoa
Room_id	Int	ID Phòng khám
Patient_name	String	Tên bệnh nhân
Patient_surname	String	Họ tên đệm
Birthyear	Int	Năm sinh
Birthdate	String	Ngày sinh yyyyMMdd
Social_id	Int	Số CMND/Căn cước/Passport
Mobile	String	Điện thoại
Patient_code	String	Mã ệnh nhân
Country_code	String	Mã Quốc tịch
City_id	Int	Mã tỉnh/thành phố
District_id	Int	Mã quận/huyện
Ward_id	Int	Mã phường/xã
Address	String	Địa chỉ số nhà, tòa nhà, tên đường
Sex	Int	Giới tính: 0: Nam, 1: Nữ
Booking_number	Int	Số thứ tự khám
Bv_time	String	Thời gian đăng ký khám: HH:mm
Method_id	Int	
Amount	Decimal	Tiền khám
Amount_original	Decimal	
Amount_gate	Decimal	
Status	Int	Trạng thái
IsBHYT	Int	0: Thu phí, 1: BHYT
Date_create	String	Ngày tạo: yyyyMMddhh24:mm
Date_update	String	Ngày cập nhật: yyyyMMddhh24:mm





Kết quả trả về Json:
{
	"id":651,
"transaction_code_tt":"TTTD-191230639",
"transaction_code_gd":"A1912300004",
"booking_date":"20191231",
"doctor_id":1176,
"subject_id":22,
"room_id":86,
"patient_name":"VÂN",
"patient_surname":"TRỊNH THỊ",
"birthyear":1989,
"birthdate":"19890820",
"social_id":"",
"mobile":"0932753906",
"patient_code":"19532003",
"country_code":"VN",
"city_id":701,
"district_id":70117,
"ward_id":7011701,
"address":"33/14",
"sex":1,
"booking_number":23,
"bv_time":"09:12",
"method_id":7, 
"amount":78700,
"amount_original":0,
"amount_gate":0,
"status":1,
"isBHYT":0,
"date_create":"201912301244",
"date_update":"201912301244"
   },

⦁	Các mã lỗi:

{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}












⦁	Delbooking: Xóa đăng ký khám bệnh theo ID
Thông tin môi trường: URL http://118.69.66.93:8080/Booking/Delbooking
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	Long	ID booking
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
      "Successful": true,
        "Result": "191025173925410025",
        "ErrorCode": “0”,
        "ErrorMessage": "",
   },

⦁	Các mã lỗi:

{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetBooking: Lấy thông tin đăng ký khám bệnh
Thông tin môi trường: URL http://118.69.66.93:8080/Booking/GetBooking
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "idbooking": 191025173925410025,
        "id": 117,
        "status": 2,
        "booking_number": 61,
        "booking_date": "01/11/2019",
        "room_id": 14
     },

⦁	Các mã lỗi:

{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}


⦁	GetBookingOfDay: Lấy thông tin đăng ký khám theo ngày
Thông tin môi trường: URL http://118.69.66.93:8080/Booking/GetBookingOfDay 
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
DateTime	String(10)	Ngày thực hiện đăng ký định dạng dd-MM-yyyy
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
BookingDate	String(10)	Ngày đăng ký khám bệnh định dạng dd-MM-yyyy
* Kết quả trả về Json:
{
        "idbooking": 191025173925410025,
        "id": 117,
        "transaction_code_tt": "TTTDDev-191025105",
        "transaction_code_gd": "W1910250116",
        "booking_date": "11/1/2019 12:00:00 AM",
        "doctor_id": 47,
        "subject_id": 13,
        "room_id": 14,
        "patient_name": "HẰNG",
        "patient_surname": "PHẠM THỊ",
        "patient_code": "18408252",
        "sex": 0,
        "booking_number": 61,
        "method_id": 7,
        "bv_time": "16:00",
        "amount": 52000,
        "status": 2,
        "isBHYT": 1
    },

⦁	Các mã lỗi:

{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}










⦁	GetBookingByID: Lấy thông tin đăng ký khám bệnh theo ID
Thông tin môi trường: URL http://118.69.66.93:8080/Booking/GetBookingByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	Long	ID Booking
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "idbooking": 191025173925410025,
        "id": 117,
        "status": 2,
        "booking_number": 61,
        "booking_date": "01/11/2019",
        "room_id": 14
     },
⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetEncounter: Lấy thông tin vào viện
Thông tin môi trường: URL http://118.69.66.93:8080/MedicalRecords/GetEncounter
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
PatientCode	String(8)	Mã bệnh nhân
MMyy	Array	Thời gian (danh sách tháng năm mmyy: ví dụ “0119”,)
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
* Kết quả trả về định dạng Json:
{
        "CreateDate": Ngày khám
        "RoomID": Mã phòng khám
        "RoomName": Tên phòng khám
        "EncounterCode": Mã quản lý xác định đợt khám
        "ICDCode": Mã chẩn đoán
        "ICDName": Tên chẩn đoán
        "DoctorName": Tên Bác sĩ
    },
⦁	Các mã lỗi:
{"errorCode": 7777,"errorMessage": "Tham so truyen vao khong hop le"}
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetTestResults: Lấy kết quả xét nghiệm
Thông tin môi trường: URL http://118.69.66.93:8080/MedicalRecords/GetTestResults
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
PatientCode	String(8)	Mã bệnh nhân
EncounterCode	String(18)	Mã quản lý xác định đợt vào viện, khám bệnh
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss

* Kết quả trả về Json:
{
        "GettingSampleDate": Ngày lấy mẫu
        "ReturnDate": Ngày có kết quả 
        "TypeOrderNumber": STT loại
        "TypeName": Tên loại 
        "SymptonOrderNumber": STT chỉ định
        "SymptonID": ID chỉ định 
        "SymptonName": Tên chỉ định
        "ResultOrderNumber": STT kết quả
        "ResultID": ID kết quả
        "ResultName": Tên kết quả 
        "Result": Kết quả
        "Unit": Đơn vị tính
        "UsualIndex": Chỉ số bình thường
        "Note": Ghi chú
        "Base64String": "File dữ liệu"
    }

⦁	Các mã lỗi:

{"errorCode": 7777,"errorMessage": "Tham so truyen vao khong hop le"}
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}
















⦁	GetImageDiagnosticResults: Lấy kết quả chẩn đoán hình ảnh
Thông tin môi trường: URL http://118.69.66.93:8080/MedicalRecords/GetImageDiagnosticResults
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
PatientCode	String(8)	Mã bệnh nhân
EncounterCode	String(18)	Mã quản lý xác định đợt vào viện, khám bệnh
* Kết quả trả về Json:
{
        "TypeID": ID loại chẩn đoán hình ảnh
        "TypeName": Tên loại chẩn đoán hình ảnh
        "CreateDate": Ngày thực hiện
        "Name": Tên chẩn đoán hình ảnh
        "Description": Mô tả định dạng rtf
        "Conclusion": Kết luận định dạnh rft
        "Indication": Đề nghị 
"Base64String": Dữ liệu file
    },
⦁	Các mã lỗi:
{"errorCode": 7777,"errorMessage": "Tham so truyen vao khong hop le"}
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetPrescription: Lấy thông tin toa thuốc
Thông tin môi trường: URL http://118.69.66.93:8080/MedicalRecords/GetPrescription
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
PatientCode	String(8)	Mã bệnh nhân
EncounterCode	String(18)	Mã quản lý xác định đợt khám bệnh
* Kết quả trả về Json:
{
"Name": Tên thuốc
        	"Quantity": Số lượng
        	"Type": Dạng thuốc
        	"Dosage": Liều dùng sáng^trưa^chiều^tối: 0^1^0^0
        	"CreateDate": Ngày cấp toa thuốc
    },
⦁	Các mã lỗi:
{"errorCode": 7777,"errorMessage": "Tham so truyen vao khong hop le"}
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetPatientByID: Lấy thông tin Bệnh nhân theo ID (Mã bệnh nhân)
Thông tin môi trường: URL http://118.69.66.93:8080/Patients/GetPatientByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	String(8)	Mã bệnh nhân
 Kết quả trả về Json:
{
    "id": "10101810",
    "name": "HƯNG",
    "surname": "ĐẶNG VIẾT",
    "sex": 0,
    "birthyear": 1979,
    "birthdate": null,
    "mobile": "0909090909",
    "social_id": null,
    "country": {
        "id": "VN",
        "name": "Việt Nam",
        "code": "VN"
    },
    "country_code": "VN",
    "city": {
        "id": 701,
        "name": "Tp. H C M",
        "country_code": "VN"
    },
    "city_id": "701",
    "district": {
        "id": "70133",
        "name": "Q. Thủ Đức",
        "city_id": "701"
    },
    "district_id": "70133",
    "ward": {
        "id": "7013309",
        "name": "P. Linh Xuân",
        "district_id": "70133"
    },
    "ward_id": "7013309",
    "address": "47",
    "bhyt_code": null
   },
⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetPatientsQrcode: Lấy thông tin Bệnh nhân theo QRCode
Thông tin môi trường: URL http://118.69.66.93:8080/Patients/GetPatientsQrcode
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
QRCode	String(512)	Mã QRCode quét trên thẻ BHYT
 Kết quả trả về Json:
{
        	    "id": "18917417",
    "name": "ĐỊNH",
    "surname": "HUỲNH VĂN",
    "sex": 0,
    "birthyear": 1985,
    "birthdate": "19850716",
    "mobile": "0904067509",
    "social_id": null,
    "country": {
        "id": "VN",
        "name": "Việt Nam",
        "code": "VN"
    },
    "country_code": "VN",
    "city": {
        "id": 701,
        "name": "Tp. H C M",
        "country_code": "VN"
    },
    "city_id": "701",
    "district": {
        "id": "70117",
        "name": "Q. 9",
        "city_id": "701"
    },
    "district_id": "70117",
    "ward": {
        "id": "7011703",
        "name": "P. Phước Long B",
        "district_id": "70117"
    },
    "ward_id": "7011703",
    "address": "18a , t3 kp4 18A, T3, KP4, Phường Phước Long B, Quận 9, Thành phố Hồ Chí Minh",
    "bhyt_code": "GD479793653299679037"
     },
⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	PostPatients: Thêm thông tin Bệnh nhân mới
Thông tin môi trường: URL http://118.69.66.93:8080/Patients/PostPatients
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
Name	String(128)	Mã bệnh nhân
Surname	String(128)	Số điện thoại
Sex	Int	Giới tính: 0: Nam, 1: Nữ
BirthYear	Int	Năm sinh
BirthDate	String(8)	Ngày sinh yyyyMMdd
Mobile	String(32)	Số điện thoại
Social_ID	String(32)	Số CMND/Căn cước/Passport
Country_Code	String(8)	Mã quốc tịch
City_ID	String(8)	ID tỉnh/ thành phố
District_ID	String(8)	ID quận/ huyện
Ward_ID	String(8)	ID phường/ xã
Address	String(254)	Địa chỉ số nhà, tòa nhà, tên đường

Kết quả trả về JSon:
{
"id": "20000020",
    	"name": "ĐINH",
    	"surname": "HÀ",
    	"sex": 0,
   	"birthyear": 1989,
    	"birthdate": "19890702",
    	"mobile": "0329757167",
    	"social_id": null,
    	"country": null,
    	"country_code": "VN",
    	"city": null,
    	"city_id": "701",
    	"district": null,
    	"district_id": "70131",
    	"ward": null,
    	"ward_id": "7013121",
    	"address": null
    }

⦁	Các mã lỗi:

{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
  {"errorCode": 9999,"errorMessage": "Mat ket noi"}




⦁	PostPatients_mobile: Cập nhật số điện thoại Bệnh nhân
Thông tin môi trường: URL http://118.69.66.93:8080/Patients/PostPatients_mobile
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	String(8)	Mã bệnh nhân
Mobile	String(10)	Số điện thoại

Kết quả trả về JSon:
{
"successful": true,
    	"result": "",
    	"errorCode": "200",
    	"errorMessage": "Cập nhật số điện thoại thành công"
    }
⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	PostPatients_checkbh: Kiểm tra thẻ BHYT
Thông tin môi trường: URL http://118.69.66.93:8080/Patients/PostPatients_checkbh
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	String(8)	Mã bệnh nhân
Ngay	String(10)	Thời gian kiểm tra thẻ còn hạn sử dụng yyyyMMdd

Kết quả trả về JSon:
{
    "result": true,
    "note": "Thẻ còn giá trị sử dụng! Họ tên : Huỳnh Văn Định, Ngày sinh: 16/07/1985, Giới tính : Nam! (ĐC: 18A- Tổ   3- Khu phố 4, Phường Phước Long B, Quận 9, Thành phố Hồ Chí Minh; Nơi KCBBĐ: 79037; Hạn thẻ: 20/10/2019 - 19/10/2020; Thời điểm đủ 5 năm liên tục: 20/10/2022).",
    "expirationdate": "20201019"
    }

⦁	Các mã lỗi:

{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}



⦁	GetPrescriptionOfDay: Lấy thông tin sử dụng thuốc trong ngày
Thông tin môi trường: URL http://118.69.66.93:8080/Prescription/GetPrescriptionOfDay
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
DateTime	String(10)	Ngày cần lấy lịch sử dụng thuốc dd/MM/yyyy
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
Kết quả trả về JSon:
{
        "id": "200103084509119323", (ID toa thuốc)
        "patientCode": "20000004", (Mã bệnh nhân)
        "patientName": "TEST2", (Họ tên bệnh nhân)
        "roomID": "040", (ID phòng khám)
        "roomName": "Phòng khám Ngoại thần kinh 1", (Tên phòng khám)
        "subjectID": "18", (ID chuyên khoa)
        "subjectName": "Khoa ngoại tổng hợp", (Tên chuyên khoa)
        "createDate": "03/01/2020", (Ngày khám)
        "fromDate": "03/01/2020", (Sử dụng từ ngày)
        "toDate": "23/01/2020", (Sử dụng đến ngày)
        "name": "Acemol (TT)", (Tên thuốc)
        "unit": "Viên", (Đơn vị tính)
        "dosage": "5^5^5^5", (Liều dùng: sáng^trưa^chiều^tối)
        "description": "Uống", (Mô tả đường dùng/cách dùng)
        "note": null (Ghi chú khác nếu có: trước ăn, sốt trên 38 độ …)
    }
⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetReexamination: Lấy thông tin tái khám
Thông tin môi trường: URL http://118.69.66.93:8080/Reexamination/GetReexamination
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss

Kết quả trả về JSon:
{
        "PatientCode": Mã bệnh nhân,
        "ReexaminationDate": Ngày tái khám,
        "RoomID": ID phòng khám,
        "DoctorID": ID Bác sĩ khám,
        "SubjectID": ID chuyên khoa
    }

⦁	GetReexaminationByID: Lấy thông tin tái khám theo ID (mã bệnh nhân)
Thông tin môi trường: URL http://118.69.66.93:8080/Reexamination/GetReexaminationByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	String(8)	Mã bệnh nhân
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss

Kết quả trả về JSon:
{
        "PatientCode": Mã bệnh nhân,
        "ReexaminationDate": Ngày tái khám,
        "RoomID": ID phòng khám,
        "DoctorID": ID Bác sĩ khám,
        "SubjectID": ID chuyên khoa
    }

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	API DANH MỤC
⦁	GetSchedules: Lấy lịch khám bệnh
Thông tin môi trường: URL http://118.69.66.93:8080/Schedules/GetSchedules
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
Doctor_ID	Int	ID Bác Sĩ
Room_ID	Int	ID khoa phòng (phòng khám)
Start_time	String(8)	Thời gian bắt đầu theo định dạng yyyyMMdd
End_time	String(8)	Thời gian kết thúc theo địng dạng yyyyMMdd
 Kết quả trả về Json:
{
        "doctor_id": 38,
        "room_id": 14,
        "start_time": "201912061300",
        "end_time": "201912061630"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetServices: Lấy thông tin dịch vụ
Thông tin môi trường: URL http://118.69.66.93:8080/Services/GetServices
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": 90874,
        "name": "Khám ngoại viện",
        "description": "Phòng khám Ngoại viện (Trung tâm điều dưỡng tâm thần)",
        "price_dv": 89000.0,
        "price_bh": 52000.0
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetServiceByID: Lây thông tin dịch vụ theo ID
Thông tin môi trường: URL http://118.69.66.93:8080/Services/GetServiceByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	String(20)	ID của đối tượng
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": 90874,
        "name": "Khám ngoại viện",
        "description": "Phòng khám Ngoại viện (Trung tâm điều dưỡng tâm thần)",
        "price_dv": 89000.0,
        "price_bh": 52000.0
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}




⦁	GetBranches: Lấy danh sách cơ sở  
Thông tin môi trường: URL http://118.69.66.93:8080/Branches/GetBranches
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
      "id": 1,
        "name": "Trạm Y Tế Phường Bình Chiểu"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetBrancheByID: Lấy thông tin cơ sở theo ID
Thông tin môi trường: URL http://118.69.66.93:8080/Branches/GetBrancheByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	Int	ID của đối tượng
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
      "id": 1,
        "name": "Trạm Y Tế Phường Bình Chiểu"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}










⦁	GetDoctors: Lấy thông tin Bác Sĩ
Thông tin môi trường: URL http://118.69.66.93:8080/Doctors/GetDoctors
HTTP method: POST
HTTP Body: Json trước mã hóa
{
"id": 0
}
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": 128,
        "fullname": "Bs Mai Thuỳ Anh",
        "birthyear": 0,
        "sex": true,
        "role": "Bác sĩ"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetDoctorByID: Lấy thông tin Bác Sĩ theo ID
Thông tin môi trường: URL http://118.69.66.93:8080/Doctors/GetDoctorByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	Int	ID của đối tượng
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": 128,
        "fullname": "Bs Mai Thuỳ Anh",
        "birthyear": 0,
        "sex": true,
        "role": "Bác sĩ"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}




⦁	GetSubjects: Lấy danh sách chuyên khoa
Thông tin môi trường: URL http://118.69.66.93:8080/Subjects/GetSubjects
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": 23,
        "name": "Tai-mũi-họng",
        "description": "Tai-mũi-họng",
        "default_price": null
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetSubjectByID: Lấy thông tin chuyên khoa theo ID
Thông tin môi trường: URL http://118.69.66.93:8080/Subjects/GetSubjectByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	String(20)	ID của đối tượng
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": 23,
        "name": "Tai-mũi-họng",
        "description": "Tai-mũi-họng",
        "default_price": null
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}






⦁	GetRooms: Lấy danh sách phòng khám
Thông tin môi trường: URL http://118.69.66.93:8080/Rooms/GetRooms
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss

Kết quả trả về JSon:
{
        "id": 72,
        "name": "Ngoại trú phòng khám Răng Hàm Mặt",
        "subject_id": "24",
        "description": "Răng Hàm Mặt",
        "price_bh": 52000.0,
        "price_dv": 89000.0,
        "idbranch": "0"
        "room_number": "phòng khám số 1"
    }
⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetRoomByID: Lấy thông tin phòng khám theo ID
Thông tin môi trường: URL http://118.69.66.93:8080/Rooms/GetRoomByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	Int	ID của đối tượng
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss

Kết quả trả về JSon:
{
        "id": 72,
        "name": "Ngoại trú phòng khám Răng Hàm Mặt",
        "subject_id": "24",
        "description": "Răng Hàm Mặt",
        "price_bh": 52000.0,
        "price_dv": 89000.0,
        "idbranch": "0"
        "room_number": "phòng khám số 1"
    }
⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetRoomTimeSlots: Lấy danh sách thời gian khám bệnh của phòng
Thông tin môi trường: URL http://118.69.66.93:8080/Rooms/GetRoomTimeSlots
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss

Kết quả trả về JSon:
{
        "id": 16,
        "start_time": "15:01",
        "end_time": "16:00",
        "startnumber": 51,
        "maxnumber": 10,
        "numberofjumps": 2
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetRoomTimeSlotByID: Lấy thời gian khám bệnh của phòng theo ID
Thông tin môi trường: URL http://118.69.66.93:8080/Rooms/GetRoomTimeSlotByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	Int	Mã khoa phòng (phòng khám)
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss

Kết quả trả về JSon:
{
        "id": 16,
        "start_time": "15:01",
        "end_time": "16:00",
        "startnumber": 51,
        "maxnumber": 10,
        "numberofjumps": 2
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}



⦁	GetPriceList: Lấy thông tin giá dịch vụ, giá thuốc, viện phí
Thông tin môi trường: URL http://118.69.66.93:8080/PriceList/GetPriceList
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "ID": ID dịch vụ,
        "Name": Tên dịch vụ,
        "Price": Đơn giá,
        "TypeID": ID Loại,
        "TypeName": Tên loại,
        "GroupID": ID nhóm,
        "GroupName": Tên nhóm
    },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetCountries: Lấy thông tin Quốc tịch
Thông tin môi trường: URL http://118.69.66.93:8080/Countries/GetCountries
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": "VN",
        "name": "VIET NAM",
        "code": "084"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}




⦁	GetCountryByID: Lấy thông tin Quốc tịch theo ID
Thông tin môi trường: URL http://118.69.66.93:8080/Countries/GetCountryByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	String(8)	ID đối tượng
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": "VN",
        "name": "VIET NAM",
        "code": "084"
   },

⦁	Các mã lỗi:
{"successful": false,"result": null,"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"successful": false,"result": null,"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetCities: Lấy danh sách Tỉnh/thành phố
Thông tin môi trường: URL http://118.69.66.93:8080/Cities/GetCities
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": 711,
        "name": "Bình Dương",
        "country_code": "VN"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}








⦁	GetCityByID: Lấy thông tin Tỉnh/thành phố theo ID
Thông tin môi trường: URL http://118.69.66.93:8080/Cities/GetCityByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	Int	ID của đối tượng
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": 711,
        "name": "Bình Dương",
        "country_code": "VN"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetDistricts: Lấy thông tin Quận/huyện
Thông tin môi trường: URL http://118.69.66.93:8080/Districts/GetDistricts
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": "81907",
        "name": "H. Mỹ Tú",
        "city_id": "819"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}






⦁	GetDistrictByID: Lấy thông tin Quận/huyện theo ID
Thông tin môi trường: URL http://118.69.66.93:8080/Districts/GetDistrictByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	String(20)	ID của đối tượng
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": "81907",
        "name": "H. Mỹ Tú",
        "city_id": "819"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetWards: Lấy danh sách phường/ xã
Thông tin môi trường: URL http://118.69.66.93:8080/Wards/GetWards
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": "1050537",
        "name": "Vạn Thắng",
        "district_id": "10505"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}






⦁	GetWardByID: Lấy thông tin phường/ xã theo ID
Thông tin môi trường: URL http://118.69.66.93:8080/Wards/GetWardByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	String(20)	ID của đối tượng
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": "1050537",
        "name": "Vạn Thắng",
        "district_id": "10505"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetJobs: Lấy danh sách nghề nghiệp
Thông tin môi trường: URL http://118.69.66.93:8080/Jobs/GetJobs
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": 6,
        "name": "Lực lượng vũ trang"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}









⦁	GetJobByID: Lấy thông tin nghề nghiệp theo ID
Thông tin môi trường: URL http://118.69.66.93:8080/Jobs/GetJobByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
ID	Int	ID của đối tượng
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": 6,
        "name": "Lực lượng vũ trang"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}

⦁	GetNations: Lấy danh sách dân tộc
Thông tin môi trường: URL http://118.69.66.93:8080/Nations/GetNations
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": 60,
        "name": "Mán"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}







⦁	GetNationByID: Lấy thông tin dân tộc theo ID
Thông tin môi trường: URL http://118.69.66.93:8080/Nations/GetNationByID
HTTP method: POST
HTTP Body: Json trước mã hóa
Tên tham số	Kiểu dữ liệu	Mô tả
RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
 Kết quả trả về Json:
{
        "id": 60,
        "name": "Mán"
   },

⦁	Các mã lỗi:
{"errorCode": 8888,"errorMessage": "Server benh vien mat ket noi"}
{"errorCode": 9999,"errorMessage": "Mat ket noi"}
