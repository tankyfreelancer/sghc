﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HIS.Model;

namespace HIS
{
    public class FptSmsService : Base, IFptSmsService
    {
        public async Task<ResponseTokenModel> GetToken(RequestTokenModel model)
        {
            var response = await Post<RequestTokenModel, ResponseTokenModel>(model, AppSetting.FptSms.TokenPath, false,AppSetting.FptSms.Url);
            return response.Item1;
        }

        public async Task<ResponseOTPModel> SendOTP(string message, string phoneNumber)
        {
            var sessionId = RandomString(24);
            var requestTokenModel = new RequestTokenModel
            {
                ClientId = AppSetting.FptSms.ClientId,
                ClientSecret = AppSetting.FptSms.ClientSecret,
                GrantType = AppSetting.FptSms.GrantType,
                Scope = AppSetting.FptSms.Scope,
                SessionId = sessionId
            };
            var token = await GetToken(requestTokenModel);
            if (token == null)
            {
                return new ResponseOTPModel
                {
                    HisError = "Token is null"
                };
            }
            if (token.Error != 0)
            {
                return new ResponseOTPModel
                {
                    Error = token.Error,
                    ErrorDescription = token.ErrorDescription,
                    HisError = "Get Token failed"
                };
            }
            var requestModel = new RequestOTPModel
            {
                AccessToken = token.AccessToken,
                SessionId = sessionId,
                BrandName = AppSetting.FptSms.BrandName,
                Message = Base64Encode(message),
                Phone = phoneNumber,
                RequestId = sessionId
            };
            var response = await Post<RequestOTPModel, ResponseOTPModel>(requestModel,  AppSetting.FptSms.SendOTPPath, false,AppSetting.FptSms.Url);
            return response.Item1;
        }
        public static string Base64Encode(string plainText) {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}
