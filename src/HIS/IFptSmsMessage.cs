﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HIS.Model;

namespace HIS
{
    public interface IFptSmsService
    {
        Task<ResponseTokenModel> GetToken(RequestTokenModel model);
        Task<ResponseOTPModel> SendOTP(string message, string phoneNumber);
    }
}
