﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using HIS.Model;
using Microsoft.Extensions.Configuration;
using System.IO;
using HIS.Helpers;
using System.Diagnostics;
// using Nop.Services;
// using Nop.Core.Configuration;
// using Nop.Core.Http;
using Nop.Core.Infrastructure;
using System.Text;
using System.Net.Http.Headers;
using System.Linq;

namespace HIS
{
    public class Base
    {
        public Base()
        {
            var nopConfig = EngineContext.Current.Resolve<Nop.Core.Configuration.NopConfig>();

            var host = nopConfig.HisConfig.Api.Base;
            BaseAddress = new Uri(host);
            AppSetting = nopConfig.HisConfig;
            ResponseData = new ResponseData();
            RequestModel = new RequestModel();

            // AppSetting.RSA.PartnerCode = nopConfig.HisConfig.RSA.PartnerCode;
            // AppSetting.RSA.PublicKey = nopConfig.HisConfig.RSA.PublicKey;
            // AppSetting.RSA.PrivateKey = nopConfig.HisConfig.RSA.PrivateKey;

            // AppSetting.Api.Booking = nopConfig.HisConfig.Api.Booking;
        }
        public Uri BaseAddress { get; set; }
        public Nop.Core.Configuration.HisConfig AppSetting { get; set; }
        public ResponseData ResponseData { get; set; }
        public RequestModel RequestModel { get; set; }

        public string Now(int addDate = 0, string format = null)
        {
            if (!string.IsNullOrEmpty(format))
            {
                return DateTime.Now.AddDays(addDate).ToString(format);
            }
            return DateTime.Now.AddDays(addDate).ToString("dd/MM/yyyy HH:mm:ss");
        }
        public async Task<TData> Get<TData>(string endpoint)
        {

            using var client = new HttpClient
            {
                BaseAddress = BaseAddress
            };
            var response = await client.GetAsync(endpoint);

            var responseContent = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<TData>(responseContent);

            return result;
        }

        public async Task<TData> Get<T, TData>(T model, string endpoint) where TData : new()
        {
            var result = new TData();

            using var client = new HttpClient
            {
                BaseAddress = BaseAddress
            };
            client.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header


            var response = await client.GetAsync(endpoint);
            var responseContent = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
            {
                return result;
            }

            try
            {
                result = JsonConvert.DeserializeObject<TData>(responseContent);
                return result;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
                return new TData();
            }
        }

        public async Task<Tuple<TData, BaseModel>> Post<T, TData>(T model, string endpoint, bool encrypt = true, string url = null) where TData : new()
        {
            // var result = new Tuple<TData, BaseModel>(new TData(), new BaseModel{
            //   Successful = false,
            // 	Result = null,
            // 	ErrorCode = 0,
            // 	ErrorMessage = null
            // });

            using var client = new HttpClient
            {
                BaseAddress = String.IsNullOrEmpty(url) ? BaseAddress : new Uri(url)
            };
            client.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

            string requestJsonModel;
            if (encrypt)
            {
                var jsonModel = JsonConvert.SerializeObject(model);

                RequestModel.Data = RsaHelper.Encryption(jsonModel, AppSetting.RSA.PublicKey);
                RequestModel.Signature = RsaHelper.SignData(jsonModel, AppSetting.RSA.PrivateKey, AppSetting.RSA.PartnerCode);

                requestJsonModel = JsonConvert.SerializeObject(RequestModel);
            }
            else
            {
                requestJsonModel = JsonConvert.SerializeObject(model);
            }

            var body = new StringContent(requestJsonModel, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(endpoint, body);
            var responseContent = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
            {
                // try to use new encrypt method
                var jsonModel = JsonConvert.SerializeObject(model);
                var data = RsaHelper.EncryptV2(jsonModel);
                requestJsonModel = JsonConvert.SerializeObject(new { Data = data });
                body = new StringContent(requestJsonModel, Encoding.UTF8, "application/json");

                response = await client.PostAsync(endpoint, body);
                responseContent = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    return new Tuple<TData, BaseModel>(new TData(), new BaseModel
                    {
                        Successful = false,
                        Result = null,
                        ErrorCode = (int)response.StatusCode,
                        ErrorMessage = responseContent
                    });
                }
            }

            try
            {
                return new Tuple<TData, BaseModel>(JsonConvert.DeserializeObject<TData>(responseContent), new BaseModel
                {
                    Successful = true,
                    Result = null,
                    ErrorCode = (int)response.StatusCode,
                    ErrorMessage = responseContent
                });
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
                return new Tuple<TData, BaseModel>(new TData(), new BaseModel
                {
                    Successful = false,
                    Result = "web has a problem",
                    ErrorCode = 500,
                    ErrorMessage = ex.Message
                });
            }
        }
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
