﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HIS.Model;

namespace HIS
{
    public interface ICategoryService
    {
        // ⦁	GetSchedules: Lấy lịch khám bệnh
        Task<List<ScheduleModel>> GetSchedules(int? Doctor_ID = null, int? Room_ID = null, string Start_time = null, string End_time = null);
        //    ⦁	GetServices: Lấy thông tin dịch vụ
        Task<List<ServiceModel>> GetServices();
        //    ⦁	GetServiceByID: Lây thông tin dịch vụ theo ID
        Task<ServiceModel> GetServiceByID(string ID);
        //    ⦁	GetBranches: Lấy danh sách cơ sở  
        Task<List<BranchModel>> GetBranches();
        //    ⦁	GetBrancheByID: Lấy thông tin cơ sở theo ID
        Task<BranchModel> GetBrancheByID(int ID);
        //    ⦁	GetDoctors: Lấy thông tin Bác Sĩ
        Task<List<DoctorModel>> GetDoctors();
        //    ⦁	GetDoctorByID: Lấy thông tin Bác Sĩ theo ID
        Task<DoctorModel> GetDoctorByID(int ID);
        //    ⦁	GetSubjects: Lấy danh sách chuyên khoa
        Task<List<SubjectModel>> GetSubjects();
        //    ⦁	GetSubjectByID: Lấy thông tin chuyên khoa theo ID
        Task<SubjectModel> GetSubjectByID(int ID);
        // ⦁	GetRooms: Lấy danh sách phòng khám
        Task<List<RoomModel>> GetRooms();
        // ⦁	GetRoomByID: Lấy thông tin phòng khám theo ID
        Task<RoomModel> GetRoomByID(int ID);
        // ⦁	GetRoomTimeSlots: Lấy danh sách thời gian khám bệnh của phòng
        Task<List<RoomTimeSlotModel>> GetRoomTimeSlots();
        // ⦁	GetRoomTimeSlotByID: Lấy thời gian khám bệnh của phòng theo ID
        Task<RoomTimeSlotModel> GetRoomTimeSlotByID(int ID);
        // ⦁	GetPriceList: Lấy thông tin giá dịch vụ, giá thuốc, viện phí
        Task<List<PriceModel>> GetPriceList();
        // ⦁	GetCountries: Lấy thông tin Quốc tịch
        Task<List<CountryModel>> GetCountries();
        // ⦁	GetCountryByID: Lấy thông tin Quốc tịch theo ID
        Task<CountryModel> GetCountryByID(string ID);
        // ⦁	GetCities: Lấy danh sách Tỉnh/thành phố
        Task<List<CityModel>> GetCities(string country_code);
        // ⦁	GetCityByID: Lấy thông tin Tỉnh/thành phố theo ID
        Task<CityModel> GetCityByID(int ID);
        // ⦁	GetDistricts: Lấy thông tin Quận/huyện
        Task<List<DistrictModel>> GetDistricts(string city_id);
        // ⦁	GetDistrictByID: Lấy thông tin Quận/huyện theo ID
        Task<DistrictModel> GetDistrictByID(string ID);
        //  ⦁	GetWards: Lấy danh sách phường/ xã
        Task<List<WardModel>> GetWards(string district_id);
        //  ⦁	GetWardByID: Lấy danh sách phường/ xã
        Task<WardModel> GetWardByID(string ID);
        // ⦁	GetJobs: Lấy danh sách nghề nghiệp
        Task<List<JobModel>> GetJobs();
        //  ⦁	GetJobByID: Lấy danh sách phường/ xã
        Task<JobModel> GetJobByID(string ID);
        // ⦁	GetNations: Lấy danh sách dân tộc
        Task<List<NationModel>> GetNations();
        //  ⦁	GetNationByID: Lấy danh sách phường/ xã
        Task<NationModel> GetNationByID(string ID);
    }
}
