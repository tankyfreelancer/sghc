﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using HIS.Model;

namespace HIS
{
    public interface IBusinessService
    {
        // 1.	TruyVanNo: Lấy thông tin dư nợ của bệnh nhân từ HIS cho Billing
        //UserID String(20)  Mã hóa đơn
        //ProviderID  String(10)  Mã nhà cung cấp
        //ServiceID String(10)  Mã dịch vụ
        //Password    String(20)  Mật khẩu của khách hàng
        //RequestTime String(20)  Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
        Task<DebitModel> PatientsDebit(string UserID, string ProviderID, string ServiceID, string Password);
        //2.	ThanhToan: Nhận thông tin trạng thái thanh toán hóa đơn từ mã Billing	6
        Task<DebitModel> PaymentInfo(string UserID, string ProviderID, string ServiceID, string ChannelID, string Password, decimal Amount, string TransactionCode, string TransactionDetail, string AdditionalInfo);
        //3.	PostBooking: Đăng ký khám bệnh	7
        /*
        {"id":1310,"transaction_code_tt":"TTTD-2005261298","transaction_code_gd":"A2005260002","booking_date":"20200627","doctor_id":97,"subject_id":120,"room_id":25,"service_id":0,"patient_name":"KIỆT","patient_surname":"TIÊU TRÍ","birthyear":1997,"birthdate":"19970731","social_id":"","mobile":"0704199707","patient_code":"16217721","country_code":"VN","city_id":711,"district_id":71171,"ward_id":7117174,"address":"- Nơi làm việc : ., Phường Tân Bình, Thị xã Dĩ An, Tỉnh Bình Dươ - Nơi làm việc : Lầu 12, 35 Nguyễn Huệ, Phường Bến Nghé, Quận 1, Thành phố Hồ Chí Minh","date_create":"202005261143","date_update":"202005261143","sex":0,"booking_number":3,"bv_time":"07:12","method_id":7,"amount":119000,"amount_original":0.0,"amount_gate":0.0,"status":1,"isBHYT":false}
        */ 
        Task<BookingModel> Booking( string Booking_date, int Doctor_id, int Subject_id, int Room_id, string Patient_name, string Patient_surname, int Birthyear, string Birthdate, int Social_id, string Mobile, string Patient_code, string Country_code, int City_id,int District_id, int Ward_id, string Address, int Sex,  string Bv_time,int Booking_number= 0, int Method_id = 0, decimal Amount = 0, decimal Amount_original = 0, decimal Amount_gate = 0, int Status = 0, bool IsBHYT = false, string Date_create = null, string Date_update = null,long ID = 0, string Transaction_code_tt = null, string Transaction_code_gd = null);
        //4.	Delbooking: Xóa đăng ký khám bệnh theo ID	9
        Task<bool> DeleteBooking(long ID);
        //5.	GetBooking: Lấy thông tin đăng ký khám bệnh	9
        // RequestTime	String(20)	Thời gian truy vấn theo định dạng dd/MM/yyyy hh24:mi:ss
        Task<List<BookingModel>> GetBooking();
        //6.	GetBookingOfDay: Lấy thông tin đăng ký khám theo ngày	10
        Task<List<BookingModel>> GetBookingOfDay(string DateTime, string BookingDate);
        //7.	GetBookingByID: Lấy thông tin đăng ký khám bệnh theo ID	11
        Task<BookingModel> GetBookingByID(string ID);
        //8.	GetEncounter: Lấy thông tin vào viện	11
        Task<List<EncounterModel>> GetEncounter(string PatientCode, List<string> MMyy);
        //9.	GetTestResults: Lấy kết quả xét nghiệm	12
        Task<List<TestResultModel>> GetTestResults(string PatientCode, string EncounterCode);
        //10.	GetImageDiagnosticResults: Lấy kết quả chẩn đoán hình ảnh	13
        Task<List<ImageDiagnosticResultModel>> GetImageDiagnosticResults(string PatientCode, string EncounterCode);
        //11.	GetPrescription: Lấy thông tin toa thuốc	13
        Task<List<PrescriptionModel>> GetPrescription(string PatientCode, string EncounterCode, string FromDate="", string ToDate = "", string lstMMyy = "");
        //12.	GetPatientByID: Lấy thông tin Bệnh nhân theo ID(Mã bệnh nhân)  14
        Task<PatientsModel> GetPatientByID(string ID);
        //13.	GetPatientsQrcode: Lấy thông tin Bệnh nhân theo QRCode	15
        Task<PatientsModel> GetPatientsByQrcode(string QRCode);
        //14.	PostPatients: Thêm thông tin Bệnh nhân mới	16
        Task<PatientsRequestModel> PostPatients(PatientsRequestModel model);
        //15.	PostPatients_mobile: Cập nhật số điện thoại Bệnh nhân	17
        Task<bool> PostPatientsPhone(string ID, string Mobile);
        //16.	PostPatients_checkbh: Kiểm tra thẻ BHYT	17
        Task<InsuranceModel> PostPatientsInsurance(string ID, string Ngay);
        //17.	GetPrescriptionOfDay: Lấy thông tin sử dụng thuốc trong ngày	18
        Task<PatientsPrescriptionModel> GetPrescriptionOfDay(string DateTime);
        //18.	GetReexamination: Lấy thông tin tái khám	18
        Task<List<ReexaminationModel>> GetReexamination();
        //19.	GetReexaminationByID: Lấy thông tin tái khám theo ID(mã bệnh nhân)
        Task<ReexaminationModel> GetReexaminationByID(string ID);

        Task<HealthzModel> Healthz();
    }
}
