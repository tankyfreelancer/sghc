﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Common
{
    public partial class EmailModel : BaseNopEntityModel
    {
        public EmailModel()
        {
        }

        public int BranchId { get; set; }
        public string FullName { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public bool Gender { get; set; }
       
        [DataType(DataType.PhoneNumber)]
        [NopResourceDisplayName("Address.Fields.PhoneNumber")]
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
        public string ReturnUrl { get; set; }
        public string ProductName { get; set; }


    }
}