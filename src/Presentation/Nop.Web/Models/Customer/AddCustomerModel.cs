using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Customer
{
    public class AddCustomerModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Token { get; set; }
        [DataType(DataType.EmailAddress)]
        [Required]
        public string Email { get; set; }
        public string Username { get; set; }
        [NoTrim]
        [NopResourceDisplayName("Account.Fields.Password")]
        [Required]
        public string Password { get; set; }
        public string Gender { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? DateOfBirthDay { get; set; }
        [NopResourceDisplayName("Account.Fields.DateOfBirth")]
        public int? DateOfBirthMonth { get; set; }
        [NopResourceDisplayName("Account.Fields.DateOfBirth")]
        public int? DateOfBirthYear { get; set; }
        public DateTime? ParseDateOfBirth()
        {
            if (!DateOfBirthYear.HasValue || !DateOfBirthMonth.HasValue || !DateOfBirthDay.HasValue)
                return null;

            DateTime? dateOfBirth = null;
            try
            {
                dateOfBirth = new DateTime(DateOfBirthYear.Value, DateOfBirthMonth.Value, DateOfBirthDay.Value);
            }
            catch { }
            return dateOfBirth;
        }

        public bool CompanyEnabled { get; set; }
        public bool CompanyRequired { get; set; }
        [NopResourceDisplayName("Account.Fields.Company")]
        public string Company { get; set; }
        public string Address { get; set; }

        public string StreetAddress2 { get; set; }

        public string City { get; set; }

        public int CountryId { get; set; }
        public int StateProvinceId { get; set; }
        public bool PhoneEnabled { get; set; }
        public string Phone { get; set; }
        //time zone
        [NopResourceDisplayName("Account.Fields.TimeZone")]
        public string TimeZoneId { get; set; }
        // HIS
        [NopResourceDisplayName("Account.Fields.City")]
        [Required]
        public int CityId { get; set; }
        [NopResourceDisplayName("Account.Fields.District")]
        [Required]
        public int DistrictId { get; set; }
        [NopResourceDisplayName("Account.Fields.Ward")]
        [Required]
        public int WardId { get; set; }
        [NopResourceDisplayName("Account.Fields.Passport")]
        [Required]
        public string Passport { get; set; }
        [NopResourceDisplayName("Account.Fields.Country")]
        public string CountryCode { get; set; }
        public string Insurance { get; set; }

    }
}