﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Customer
{
    public partial class CustomerInfo : BaseNopModel
    {
        public CustomerInfo()
        {
        }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public bool Sex { get; set; }
        public int DateOfBirthDay { get; set; }
        public int DateOfBirthMonth { get; set; }
        public int DateOfBirthYear { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public int WardId { get; set; }
        public string Address { get; set; }
    }
}