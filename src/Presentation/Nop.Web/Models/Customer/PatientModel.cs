using System;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using Nop.Core.Domain.Customers;
using System.Collections.Generic;
using Nop.Web.Helpers;

namespace Nop.Web.Models.Customer
{
    public partial class PatientModel : BaseNopModel
    {
        public PatientModel()
        {
            Schedules = new List<ScheduleModel>();
            Subjects = new List<HIS.Model.SubjectModel>();
            Doctors = new List<DoctorModel>();
            IDS = new List<HIS.Model.ImageDiagnosticResultModel>();
            Hospitalize = new List<HIS.Model.EncounterModel>();
            TestResults = new List<HIS.Model.TestResultModel>();
            Prescription = new List<HIS.Model.PrescriptionModel>();
            General = new HIS.Model.BookingModel();
            Calendars = new List<CalendarModel>();
            CalendarNextMonths = new List<CalendarModel>();
        }
        public int Id { get; set; }
        public int HisId { get; set; }

        [NopResourceDisplayName("Account.Name")]
        public string Name { get; set; }
        [NopResourceDisplayName("Account.Surname")]
        public string Surname { get; set; }
        [NopResourceDisplayName("Account.Sex")]
        public int Sex { get; set; }
        [NopResourceDisplayName("Account.Birthyear")]
        public int Birthyear { get; set; }
        [NopResourceDisplayName("Account.Birthdate")]
        public string Birthdate { get; set; }
        [NopResourceDisplayName("Account.Mobile")]
        public string Mobile { get; set; }
        [NopResourceDisplayName("Account.SocialId")]
        public string SocialId { get; set; }
        [NopResourceDisplayName("Account.CountryCode")]
        public string CountryCode { get; set; }
        [NopResourceDisplayName("Account.CityId")]
        public string CityId { get; set; }
        [NopResourceDisplayName("Account.DistrictId")]
        public string DistrictId { get; set; }
        [NopResourceDisplayName("Account.WardId")]
        public string WardId { get; set; }
        [NopResourceDisplayName("Account.Address")]
        public string Address { get; set; }
        public string Email { get; set; }
        [NopResourceDisplayName("Account.Passport")]
        public string Passport { get; set; }
        public int CountryId { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
        public string Insurance { get; set; }
        public string HisCityId { get; set; }
        public string HisDistrictId { get; set; }
        public string HisWardId { get; set; }
        public string SubjectName { get; set; }
        public string ExaminationDate { get; set; }
        public string ExaminationTime { get; set; }
        public string DoctorName { get; set; }
        public string RoomName { get; set; }
        public int DoctorId { get; set; }
        public int RoomId { get; set; }
        public int SubjectId { get; set; }
        public int PatientId { get; set; }
        public bool HaveInsurance { get; set; }
        public decimal Price { get; set; }
        public decimal PriceBH { get; set; }
        public decimal PriceDV { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string TTTD { get; set; }
        public string ExpiredTime { get; set; }
        public string BannerUrl { get; set; }

        public List<ScheduleModel> Schedules { get; set; }
        public List<CalendarModel> Calendars { get; set; }
        public List<CalendarModel> CalendarNextMonths { get; set; }
        public List<HIS.Model.SubjectModel> Subjects { get; set; }
        public List<DoctorModel> Doctors { get; set; }
        public List<HIS.Model.TestResultModel> TestResults { get; set; }
        public List<HIS.Model.ImageDiagnosticResultModel> IDS { get; set; }
        public List<HIS.Model.EncounterModel> Hospitalize { get; set; }
        public List<HIS.Model.PrescriptionModel> Prescription { get; internal set; }
        public HIS.Model.BookingModel General { get; internal set; }

        public HIS.Model.PatientsRequestModel Map()
        {
            return new HIS.Model.PatientsRequestModel
            {
                Id = "0",
                Name = Name,
                Surname = Surname,
                Sex = Sex,
                Birthyear = Birthyear,
                Birthdate = $"{Birthyear}{Month}{Day}",
                Mobile = Mobile,
                SocialId = SocialId,
                CountryCode = CountryCode,
                CityId = HisCityId,
                DistrictId = HisDistrictId,
                WardId = HisWardId,
                Address = Address,
            };
        }
        public PatientModel Map(Patient model)
        {
            return new PatientModel
            {
                Id = model.Id,
                Name = model.Name,
                Surname = model.SurName,
                Sex = model.Sex,
                Birthyear = model.BirthYear,
                Birthdate = model.BirthDate,
                Mobile = model.Mobile,
                SocialId = model.SocialId?.ToString(),
                CountryCode = model.CountryCode,
                CityId = model.CityId?.ToString(),
                DistrictId = model.DistrictId?.ToString(),
                WardId = model.WardId?.ToString(),
                Address = model.Address,
                Email = model.Email,
                Passport = model.Passport,
                CountryId = model.CountryId.GetValueOrDefault()
            };
        }
        public PatientModel Map(HIS.Model.PatientsModel model)
        {
            int.TryParse(model.Id, out int id);
            return new PatientModel
            {
                Id = id,
                Name = model.Name,
                Surname = model.Surname,
                Sex = model.Sex,
                Birthyear = model.Birthyear,
                Birthdate = model.Birthdate,
                Mobile = model.Mobile,
                SocialId = model.SocialId?.ToString(),
                CountryCode = model.CountryCode,
                CityId = model.CityId?.ToString(),
                DistrictId = model.DistrictId?.ToString(),
                WardId = model.WardId?.ToString(),
                Address = model.Address,
                // Email = model.Email,
                // Passport = model.Passport,
                // CountryId = model.CountryId.GetValueOrDefault()
            };
        }
    }
    public class CalendarModel
    {
        public CalendarModel()
        {
            Schedules = new List<ScheduleModel>();
        }
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int DayOfWeek { get; set; }
        public int Week { get; set; }
        public List<ScheduleModel> Schedules { get; set; }
    }
    public class ScheduleModel
    {
        public ScheduleModel()
        {
            Doctor = new HIS.Model.DoctorModel();
            Room = new HIS.Model.RoomModel();
        }
        public string DoctorId { get; set; }
        public string RoomId { get; set; }
        public string SubjectId { get; set; }
        public string ScheduleId { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public HIS.Model.DoctorModel Doctor { get; set; }
        public HIS.Model.RoomModel Room { get; set; }
        public HIS.Model.SubjectModel Subject { get; set; }

        public ScheduleModel Map(HIS.Model.ScheduleModel model)
        {
            return new ScheduleModel
            {
                DoctorId = model.DoctorId,
                RoomId = model.RoomId,
                StartTime = TimeHelper.ConvertTime(model.StartTime),
                EndTime = TimeHelper.ConvertTime(model.EndTime)
            };
        }
    }
    public class DoctorModel
    {
        public DoctorModel()
        {
            Schedules = new List<ScheduleTimeModel>();
        }
        public string DoctorId { get; set; }
        public string RoomId { get; set; }
        public string DoctorName { get; set; }
        public string RoomName { get; set; }

        public List<ScheduleTimeModel> Schedules { get; set; }
    }
    public class ScheduleTimeModel
    {
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
    }
    public class BookingModel
    {
        public int DoctorId { get; set; }
        public int RoomId { get; set; }
        public int SubjectId { get; set; }
        public int PatientId { get; set; }
        public bool HaveInsurance { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}