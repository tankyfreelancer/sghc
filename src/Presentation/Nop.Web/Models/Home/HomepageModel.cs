﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Recruit;
using Nop.Services.Seo;
using Nop.Web.Areas.Admin.Models.Media;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.News;
using Nop.Services.Media;
using Nop.Core.Domain.Common;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Nop.Web.Models.Home
{
    public partial class HomepageModel : BaseNopModel
    {
        public HomepageModel()
        {
            Sliders = new List<SliderModel>();
            SliderMobiles = new List<SliderModel>();
            Doctors = new List<DoctorModel>();
            Faculties = new List<CategoryModel>();
            NewsFaculties = new NewsCategoryListModel();
            Newses = new List<NewsItemModel>();
            OtherNewses = new List<NewsItemModel>();
            Packages = new List<Nop.Web.Models.Catalog.ProductOverviewModel>();
            RssFeed = new Nop.Core.Rss.RssFeed();
            MainRss = new Nop.Core.Rss.RssFeed();
            Communications = new List<NewsItemModel>();
            Vendor =
                new Nop.Web.Areas.Admin.Models.Catalog.ManufacturerListModel();
            HotNews = new NewsItemModel();
            HomeSetting = new Setting();
            PopupNews = new NewsItemModel();
        }

        public NewsItemModel PopupNews { get; set; }
        public List<SliderModel> Sliders { get; set; }
        public List<SliderModel> SliderMobiles { get; set; }
        public List<DoctorModel> Doctors { get; set; }
        public NewsCategoryListModel NewsFaculties { get; set; }

        public List<CategoryModel> Faculties { get; set; }

        public List<NewsItemModel> Newses { get; set; }

        public List<NewsItemModel> OtherNewses { get; set; }

        public Nop.Core.Rss.RssFeed RssFeed { get; set; }

        public Nop.Core.Rss.RssFeed MainRss { get; set; }

        public List<Nop.Web.Models.Catalog.ProductOverviewModel> Packages { get; set; }

        public List<NewsItemModel> Communications { get; set; }

        public Nop.Web.Areas.Admin.Models.Catalog.ManufacturerListModel Vendor
        {
            get; set;
        }

        public NewsItemModel HotNews { get; set; }

        public Setting HomeSetting { get; set; }
        public string MainRss1Url { get; set; }
        public string MainRss2Url { get; set; }
        public string BookPictureUrl { get; set; }
        public string DoctorPictureUrl { get; set; }
        public string VendorPictureUrl { get; set; }
        public string DoctorMobilePictureUrl { get; set; }
        public string VendorMobilePictureUrl { get; set; }
    }

    public class DoctorModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Role { get; set; }

        public string Specialize { get; set; }

        public string Experience { get; set; }

        public string ImageURL { get; set; }
    }

    public class Setting
    {
        public string BannerFrontURL { get; set; }

        public string BannerBackURL { get; set; }
    }

    public class AboutPageModel
    {
        public AboutPageModel()
        {
            Faculties = new List<CategoryModel>();
            Communications = new List<NewsItemModel>();
            News = new List<NewsItemModel>();
        }

        public string AboutSlider01 { get; set; }

        public string AboutSlider02 { get; set; }

        public string AboutSlider03 { get; set; }

        public string AboutVision { get; set; }

        public string RssUrl { get; set; }

        public string AboutMission { get; set; }

        public string AboutCoreValue { get; set; }

        public string AboutWhyChooseUs { get; set; }

        public string AboutPresidentAvatarURL { get; set; }

        public string AboutPresidentTitle { get; set; }

        public string AboutPresidentDescription { get; set; }

        public string AboutPresidentContent { get; set; }

        public string AboutVideoUrl { get; set; }
        public string DoctorVideoUrl { get; set; }

        public string AboutPicture01URL { get; set; }

        public string AboutPicture02URL { get; set; }

        public string AboutPicture03URL { get; set; }

        public string AboutPicture04URL { get; set; }
        public string BannerUrl { get; set; }

        public List<NewsItemModel> Communications { get; set; }

        public List<NewsItemModel> News { get; set; }

        public List<CategoryModel> Faculties { get; set; }
    }

    public class AboutRecruitModel
    {
        public AboutRecruitModel()
        {
            RecruitFaculties = new List<RecruitFacultyModel>();
            Jobs = new List<JobModel>();
        }

        public List<RecruitFacultyModel> RecruitFaculties { get; set; }

        public RecruitPagingFilteringModel PagingFilteringContext { get; set; }
        public List<JobModel> Jobs { get; set; }
        public string BannerUrl { get; set; }
    }

    public class RecruitFacultyModel
    {
        public RecruitFacultyModel()
        {
        }

        public RecruitFacultyModel(RecruitFaculty rf)
        {
            Id = rf.Id;
            Name = rf.Name;
            Description = rf.Description;
            DisplayOrder = rf.DisplayOrder;
            CreatedOnUtc = rf.CreatedOnUtc;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int DisplayOrder { get; set; }

        public DateTime CreatedOnUtc { get; set; }
    }

    public class JobModel
    {
        public JobModel(Job item, IUrlRecordService _urlRecordService, IPictureService _pictureService, HomepageSettings _homeSettings)
        {
            Id = item.Id;
            Title = item.Title;
            Short = item.Short;
            Full = item.Full;
            Count = item.Count;
            Position = item.Position;
            RecruitFacultyId = item.RecruitFacultyId;
            DisplayOrder = item.DisplayOrder;
            CreatedOnUtc = item.CreatedOnUtc;
            Active = item.Active;
            SeName = _urlRecordService.GetSeName(item, null, true, true);
            BannerUrl = _pictureService
                            .GetPictureUrl(_homeSettings.BannerRecruitPageId,
                            showDefaultPicture: false);
        }
        public int Id { get; set; }
        public string Title { get; set; }

        public string Short { get; set; }
        public string SeName { get; set; }

        public string Full { get; set; }

        public string Position { get; set; }

        public int? RecruitFacultyId { get; set; }

        public int Count { get; set; }
        public bool Active { get; set; }

        public int DisplayOrder { get; set; }
        public string RecruitFacultyName { get; set; }
        public string BannerUrl { get; set; }

        public DateTime CreatedOnUtc { get; set; }
    }
    public class ResumeModel
    {

        [Required]
        public string FullName { get; set; }

        public bool Gender { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string TempAddress { get; set; }

        [Required]
        public string Major { get; set; }

        [Required]
        public string College { get; set; }

        [Required]
        public string Expirence { get; set; }

        [Required]
        public string Bio { get; set; }
        public IFormFile AttachFile { get; set; }
        [Required]
        public int JobId { get; set; }
        [Required]
        public int CityId { get; set; }
        [Required]
        public int DistrictId { get; set; }
        [Required]
        public int WardId { get; set; }
        [Required]
        public int TempCityId { get; set; }
        [Required]
        public int TempDistrictId { get; set; }
        [Required]
        public int TempWardId { get; set; }
    }
}
