﻿using Nop.Web.Framework.Models;
using Nop.Web.Models.Customer;
using System;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Core.Domain.Customers;
using System.Collections.Generic;
using Nop.Web.Helpers;

namespace Nop.Web.Models.Profile
{
    public partial class ProfileInfoModel : BaseNopModel
    {
        public int CustomerProfileId { get; set; }

        public string AvatarUrl { get; set; }

        public bool LocationEnabled { get; set; }
        public string Location { get; set; }

        public bool PMEnabled { get; set; }

        public bool TotalPostsEnabled { get; set; }
        public string TotalPosts { get; set; }

        public bool JoinDateEnabled { get; set; }
        public string JoinDate { get; set; }

        public bool DateOfBirthEnabled { get; set; }
        public string DateOfBirth { get; set; }
        // his
        public bool IsAuthenticated { get; set; }
        public List<PatientModel> Patients { get; set; }
        
    }
}