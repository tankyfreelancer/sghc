﻿using Nop.Web.Framework.Models;
using Nop.Web.Models.Common;
using Nop.Web.Models.Customer;

namespace Nop.Web.Models.Profile
{
    public partial class ProfileIndexModel : BaseNopModel
    {
        public ProfileIndexModel()
        {
            Customer = new CustomerInfoModel();
            CustomerInfo = new CustomerInfo();
            Address = new AddressModel();
            PasswordModel = new ChangePasswordModel();
            Patient = new PatientModel();
        }
        public int CustomerProfileId { get; set; }
        public string ProfileTitle { get; set; }
        public int PostsPage { get; set; }
        public bool PagingPosts { get; set; }
        public bool Success { get; set; }
        public bool ForumsEnabled { get; set; }
        public bool IsPasswordChanged { get; set; }
        public CustomerInfoModel Customer { get; set; }
        public AddressModel Address { get; set; }
        public PatientModel Patient { get; set; }
        public CustomerInfo CustomerInfo { get; set; }
        public ChangePasswordModel PasswordModel { get; set; }

    }
}