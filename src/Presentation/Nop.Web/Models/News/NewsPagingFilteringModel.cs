﻿using Nop.Web.Framework.UI.Paging;

namespace Nop.Web.Models.News
{
    public partial class NewsPagingFilteringModel : BasePageableModel
    {
        public bool IsNews { get; set; }
        public int? NewsCategoryId { get; set; }
        public string Search { get; set; }
    }
}