﻿using System;
using System.Collections.Generic;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Media;

namespace Nop.Web.Models.News
{
    public class NewsRelated
    {
        public NewsRelated()
        {
            DefaultPictureModel = new PictureModel();
        }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        public string SeName { get; set; }

        public string Title { get; set; }
        public string Short { get; set; }
        public DateTime CreatedOn { get; set; }

        public PictureModel DefaultPictureModel { get; set; }

    }
}
