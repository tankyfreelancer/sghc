﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.News
{
    public partial class NewsCategoryModel : BaseNopModel
    {
        public NewsCategoryModel()
        {
            Communications = new List<NewsItemModel>();
        }
        public string CurrentCategoryName { get; set; }
        public string BannerUrl { get; set; }
        public int CurrentCategoryId { get; set; }
        public NewsItemListModel NewsItemListModel { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public List<NewsItemModel> Communications { get; set; }
        public int DisplayOrder { get; set; }
    }
}