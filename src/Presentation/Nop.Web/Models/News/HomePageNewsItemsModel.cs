﻿using System;
using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.News
{
    public partial class HomepageNewsItemsModel : BaseNopModel, ICloneable
    {
        public HomepageNewsItemsModel()
        {
            NewsItems = new List<NewsItemModel>();
            Communications = new List<NewsItemModel>();
            Packages = new List<NewsItemModel>();
            NewsCategories = new List<NewsCategoryModel>();
            PopupNews = new NewsItemModel();
        }

        public NewsItemModel PopupNews { get; set; }
        public int WorkingLanguageId { get; set; }
        public NewsItemModel HotNews { get; set; }
        public IList<NewsItemModel> NewsItems { get; set; }
        public IList<NewsItemModel> Communications { get; set; }
        public IList<NewsItemModel> Packages { get; set; }
        public IList<NewsCategoryModel> NewsCategories { get; set; }

        public object Clone()
        {
            //we use a shallow copy (deep clone is not required here)
            return MemberwiseClone();
        }
    }
}