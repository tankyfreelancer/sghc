﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.News
{
    public partial class NewsCategoryListModel : BaseNopModel
    {
        public NewsCategoryListModel()
        {
            PagingFilteringContext = new NewsPagingFilteringModel();
            Items = new List<NewsCategorySummaryModel>();
        }

        public int WorkingLanguageId { get; set; }
        public NewsPagingFilteringModel PagingFilteringContext { get; set; }
        public List<NewsCategorySummaryModel> Items { get; set; }
    }
}