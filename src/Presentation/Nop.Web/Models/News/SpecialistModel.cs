﻿using Nop.Core.Domain.News;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Catalog;

namespace Nop.Web.Models.News
{
    public partial class SpecialistModel : BaseNopEntityModel
    {
        public SpecialistModel()
        {
            Category = new NewsCategory();
            News = new NewsItemListModel();
            Doctors = new ProductPaging();
        }

        public NewsCategory Category { get; set; }

        public NewsItemListModel News { get; set; }
        public ProductPaging Doctors { get; set; }
        public string BannerUrl { get; set; }
        
    }
}
