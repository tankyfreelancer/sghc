using Nop.Core.Domain.News;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Media;
using System;

namespace Nop.Web.Models.News
{
    public partial class NewsCategorySummaryModel : BaseNopModel
    {
        public NewsCategorySummaryModel()
        {
            DefaultPictureModel = new PictureModel();
        }
        public NewsCategorySummaryModel(NewsCategory entity)
        {
            Id = entity.Id;
            LanguageId = entity.LanguageId;
            CategoryId = entity.CategoryId;
            Title = entity.Title;
            Short = entity.Short;
            Full = entity.Full;
            Published = entity.Published;
            StartDateUtc = entity.StartDateUtc;
            EndDateUtc = entity.EndDateUtc;
            AllowComments = entity.AllowComments;
            LimitedToStores = entity.LimitedToStores;
            MetaKeywords = entity.MetaKeywords;
            MetaDescription = entity.MetaDescription;
            MetaTitle = entity.MetaTitle;
            Tags = entity.Tags;
            CreatedOnUtc = entity.CreatedOnUtc;
            ShowOnHomePage = entity.ShowOnHomePage;
            IncludeInTopMenu = entity.IncludeInTopMenu;
            MenuSeName = entity.MenuSeName;
            SpecialistIntroduce = entity.SpecialistIntroduce;
            SpecialistService = entity.SpecialistService;
            VideoUrl = entity.VideoUrl;
            DefaultPictureModel = new PictureModel();
        }
        public PictureModel DefaultPictureModel { get; set; }

        /// <summary>
        /// Gets or sets the language identifier
        /// </summary>
        public int Id { get; set; }
        public int LanguageId { get; set; }
        /// <summary>
        /// Gets or sets the category identifier
        /// </summary>
        public int? CategoryId { get; set; }
        /// <summary>
        /// Gets or sets the news title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the short text
        /// </summary>
        public string Short { get; set; }

        /// <summary>
        /// Gets or sets the full text
        /// </summary>
        public string Full { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the news item is published
        /// </summary>
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets the news item start date and time
        /// </summary>
        public DateTime? StartDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the news item end date and time
        /// </summary>
        public DateTime? EndDateUtc { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the news post comments are allowed 
        /// </summary>
        public bool AllowComments { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity is limited/restricted to certain stores
        /// </summary>
        public bool LimitedToStores { get; set; }

        /// <summary>
        /// Gets or sets the meta keywords
        /// </summary>
        public string MetaKeywords { get; set; }

        /// <summary>
        /// Gets or sets the meta description
        /// </summary>
        public string MetaDescription { get; set; }

        /// <summary>
        /// Gets or sets the meta title
        /// </summary>
        public string MetaTitle { get; set; }
        /// <summary>
        /// Gets or sets the blog tags
        /// </summary>
        public string Tags { get; set; }
        /// <summary>
        /// Gets or sets the date and time of entity creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether to show the category on home page
        /// </summary>
        public bool ShowOnHomePage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to include this category in the top menu
        /// </summary>
        public bool IncludeInTopMenu { get; set; }
        public string MenuSeName { get; set; }
        // For specialist
        /// <summary>
        /// Gets or sets the category identifier
        /// </summary>
        public int? SpecialistCategoryId { get; set; }  // ==> doctor facultyId
        public string SpecialistIntroduce { get; set; }
        public string SpecialistService { get; set; }
        public string VideoUrl { get; set; }
        public string SeName { get; set; }
    }
}