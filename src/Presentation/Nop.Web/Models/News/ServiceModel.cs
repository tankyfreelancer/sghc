﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.News;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.News
{
    public partial class ServiceModel : BaseNopEntityModel
    {
        public ServiceModel()
        {
            Categories = new List<NewsCategory>();
            News = new NewsItemListModel();
        }

        public IList<NewsCategory> Categories { get; set; }

        public NewsItemListModel News { get; set; }
        public string BannerUrl { get; set; }
    }
}
