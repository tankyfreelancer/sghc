﻿using Nop.Web.Framework.UI.Paging;

namespace Nop.Web.Models.News
{
    public partial class RecruitPagingFilteringModel : BasePageableModel
    {
        public int? RecruitFacultyId { get; set; }
        public string Search { get; set; }
    }
}