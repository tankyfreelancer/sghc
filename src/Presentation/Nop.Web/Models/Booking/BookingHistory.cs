using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Booking
{
    public class BookingHistory
    {
        public int Id { get; set; }
        public string Booking_number { get; set; }
        public string BookingDate { get; set; }
        public string BvTime { get; set; }
        public string DoctorName { get; set; }
        public string PatientName { get; set; }
        public string Mobile { get; set; }
        public string Status { get; set; }

    }
    public class BookingDetailModel{
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public long HisId { get; set; }

        public string TransactionCodeTT { get; set; }
        public string TransactionCodeGD { get; set; }
        public string BookingDate { get; set; }
        public int DoctorId { get; set; }
        public int SubjectId { get; set; }
        public int RoomId { get; set; }
        public string PatientName { get; set; }
        public string PatientSurname { get; set; }
        public int Birthyear { get; set; }
        public string Birthdate { get; set; }
        public int SocialId { get; set; }
        public string Mobile { get; set; }
        public string PatientCode { get; set; }
        public string CountryCode { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public int WardId { get; set; }
        public string Address { get; set; }
        public int Sex { get; set; }
        public int Booking_number { get; set; }
        public string BvTime { get; set; }
        public int MethodId { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountOriginal { get; set; }
        public decimal AmountGate { get; set; }
        public int Status { get; set; }
        public int IsBHYT { get; set; }
        public int CustomerId { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateUpdate { get; set; }

        public string DoctorName { get; set; }
        public string SubjectName { get; set; }
        public string RoomName { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string WardName { get; set; }

    }
}