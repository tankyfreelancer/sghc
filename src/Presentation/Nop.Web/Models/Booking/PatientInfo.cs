using System;
using System.Collections.Generic;
namespace Nop.Web.Models.Booking
{
    public class GeneralInfo
    {
        public GeneralInfo(){
            Hospitalizies = new List<HIS.Model.EncounterModel>();
        }
        public DateTime CreateDate { get; set; }
        public List<HIS.Model.EncounterModel> Hospitalizies { get; set; }

    }
}