using System.Collections.Generic;
using Nop.Web.Framework.Models;
using Nop.Web.Models.Common;
using Nop.Web.Models.Customer;
using Nop.Web.Models.ShoppingCart;

namespace Nop.Web.Models.Checkout
{
    public partial class CheckoutModel : BaseNopModel
    {
        public CheckoutModel()
        {
            Address = new AddressModel();
            ShoppingCartModel = new ShoppingCartModel();
            CustomerInfo = new CustomerInfo();
        }

        public AddressModel Address { get; set; }
        public ShoppingCartModel ShoppingCartModel { get; set; }
        public CustomerInfo CustomerInfo { get; set; }
        public string NoteOrder { get; set; }
        public int PayMethod { get; set; }
    }
}