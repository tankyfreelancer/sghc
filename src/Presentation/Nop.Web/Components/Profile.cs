﻿using System;
using Microsoft.AspNetCore.Mvc;
using Nop.Services.Customers;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Core;
using Nop.Web.Models.Profile;

namespace Nop.Web.Components
{
    public class ProfileViewComponent : NopViewComponent
    {
        private readonly ICustomerService _customerService;
        private readonly IProfileModelFactory _profileModelFactory;
        private readonly IWorkContext _workContext;

        public ProfileViewComponent(ICustomerService customerService, IProfileModelFactory profileModelFactory,IWorkContext workContext)
        {
            _customerService = customerService;
            _profileModelFactory = profileModelFactory;
            _workContext = workContext;
        }

        public IViewComponentResult Invoke()
        {
            var model = new ProfileInfoModel();
            if (!User.Identity.IsAuthenticated){
                return View(model);
            }
            var customer = _customerService.GetCustomerById(_workContext.CurrentCustomer.Id);
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

             model = _profileModelFactory.PrepareProfileInfoModel(customer);
            return View(model);
        }
    }
}
