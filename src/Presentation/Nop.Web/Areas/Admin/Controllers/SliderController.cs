﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.ExportImport;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Areas.Admin.Models.Media;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class SliderController : BaseAdminController
    {
        #region Fields

        private readonly IAclService _aclService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ICustomerService _customerService;
        private readonly IDiscountService _discountService;
        private readonly IExportManager _exportManager;
        private readonly IImportManager _importManager;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly ISliderService _sliderService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IPictureService _pictureService;
        private readonly IProductService _productService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IStoreService _storeService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IWorkContext _workContext;
        private readonly ISliderModelFactory _sliderModelFactory;

        #endregion

        #region Ctor

        public SliderController(IAclService aclService,
            ICustomerActivityService customerActivityService,
            ICustomerService customerService,
            IDiscountService discountService,
            IExportManager exportManager,
            IImportManager importManager,
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService,
            ISliderService sliderService,
            INotificationService notificationService,
            IPermissionService permissionService,
            IPictureService pictureService,
            IProductService productService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IUrlRecordService urlRecordService,
            ISliderModelFactory sliderModelFactory,
            IWorkContext workContext)
        {
            _aclService = aclService;
            _customerActivityService = customerActivityService;
            _customerService = customerService;
            _discountService = discountService;
            _exportManager = exportManager;
            _importManager = importManager;
            _localizationService = localizationService;
            _localizedEntityService = localizedEntityService;
            _sliderService = sliderService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _pictureService = pictureService;
            _productService = productService;
            _storeMappingService = storeMappingService;
            _storeService = storeService;
            _urlRecordService = urlRecordService;
            _workContext = workContext;
            _sliderModelFactory = sliderModelFactory;
        }

        #endregion

        #region Utilities

        protected virtual void UpdatePictureSeoNames(Slider slider)
        {
            var picture = _pictureService.GetPictureById(slider.PictureId);
            if (picture != null)
                _pictureService.SetSeoFilename(picture.Id, _pictureService.GetPictureSeName(slider.Title));
        }

        #endregion

        #region List

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            //prepare model
            var model = _sliderModelFactory.PrepareSliderSearchModel(new SliderSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(SliderSearchModel searchModel)
        {
            //prepare model
            var model = _sliderModelFactory.PrepareSliderListModel(searchModel);

            return Json(model);
        }

        #endregion

        #region Create / Edit / Delete

        public virtual IActionResult Create()
        {
            //prepare model
            var model = _sliderModelFactory.PrepareSliderModel(new SliderModel(), null);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Create(SliderModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {
                var slider = model.ToEntity<Slider>();
                _sliderService.InsertSlider(slider);


                //update picture seo file name
                UpdatePictureSeoNames(slider);

                if (!continueEditing)
                    return RedirectToAction("List");

                return RedirectToAction("Edit", new { id = slider.Id });
            }

            //prepare model
            model = _sliderModelFactory.PrepareSliderModel(model, null, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult Edit(int id)
        {
            //try to get a slider with the specified id
            var slider = _sliderService.GetSliderById(id);
            if (slider == null)
                return RedirectToAction("List");

            //prepare model
            var model = _sliderModelFactory.PrepareSliderModel(null, slider);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(SliderModel model, bool continueEditing)
        {
            //try to get a slider with the specified id
            var slider = _sliderService.GetSliderById(model.Id);
            if (slider == null)
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                var prevPictureId = slider.PictureId;
                slider = model.ToEntity(slider);

                _sliderService.UpdateSlider(slider);

                //delete an old picture (if deleted or updated)
                if (prevPictureId > 0 && prevPictureId != slider.PictureId)
                {
                    var prevPicture = _pictureService.GetPictureById(prevPictureId);
                    if (prevPicture != null)
                        _pictureService.DeletePicture(prevPicture);
                }

                //update picture seo file name
                UpdatePictureSeoNames(slider);

                if (!continueEditing)
                    return RedirectToAction("List");

                return RedirectToAction("Edit", new { id = slider.Id });
            }

            //prepare model
            model = _sliderModelFactory.PrepareSliderModel(model, slider, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            //try to get a slider with the specified id
            var slider = _sliderService.GetSliderById(id);
            if (slider == null)
                return RedirectToAction("List");

            _sliderService.DeleteSlider(slider);

            //activity log
            _customerActivityService.InsertActivity("DeleteSlider",
                string.Format(_localizationService.GetResource("ActivityLog.DeleteSlider"), slider.Title), slider);

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.Sliders.Deleted"));

            return RedirectToAction("List");
        }

        [HttpPost]
        public virtual IActionResult DeleteSelected(ICollection<int> selectedIds)
        {
            if (selectedIds != null)
            {
                var sliders = _sliderService.GetSlidersByIds(selectedIds.ToArray());
                foreach (var item in sliders)
                {
                    _sliderService.DeleteSlider(item);
                }
            }
            return Json(new { Result = true });
        }
        #endregion
    }
}