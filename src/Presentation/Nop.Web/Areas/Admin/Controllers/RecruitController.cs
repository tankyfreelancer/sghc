﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.News;
using Nop.Core.Domain.Recruit;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.News;
using Nop.Services.Recruit;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Recruit;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class RecruitController : BaseAdminController
    {
        #region Fields

        private readonly ICustomerActivityService _customerActivityService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILocalizationService _localizationService;
        private readonly IRecruitFacultyModelFactory _recruitFacultyModelFactory;
        private readonly IRecruitFacultyService recruitFacultyService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IStoreService _storeService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IPictureService _pictureService;
        private readonly IJobService _jobService;
        private readonly IResumeService _resumeService;

        #endregion

        #region Ctor

        public RecruitController(ICustomerActivityService customerActivityService,
            IEventPublisher eventPublisher,
            ILocalizationService localizationService,
            INewsModelFactory newsModelFactory,
            IRecruitFacultyService newsService,
            IRecruitFacultyModelFactory recruitFacultyModelFactory,
            INotificationService notificationService,
            IPermissionService permissionService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IPictureService pictureService,
            IJobService jobService,
            IResumeService resumeService,
            IUrlRecordService urlRecordService)
        {
            _customerActivityService = customerActivityService;
            _eventPublisher = eventPublisher;
            _localizationService = localizationService;
            recruitFacultyService = newsService;
            _recruitFacultyModelFactory = recruitFacultyModelFactory;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _storeMappingService = storeMappingService;
            _storeService = storeService;
            _urlRecordService = urlRecordService;
            _pictureService = pictureService;
            _jobService = jobService;
            _resumeService = resumeService;
        }

        #endregion



        #region Recruit Faculty

        public virtual IActionResult RecruitFaculty()
        {
            return RedirectToAction("RecruitFaculties");
        }

        public virtual IActionResult RecruitFaculties(int? filterByRecruitFacultyId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //prepare model
            var model = _recruitFacultyModelFactory.PrepareRecruitFacultyContentModel(new RecruitFacultyContentModel(), filterByRecruitFacultyId);

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RecruitFacultyList(RecruitFacultySearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _recruitFacultyModelFactory.PrepareRecruitFacultyListModel(searchModel);

            return Json(model);
        }

        public virtual IActionResult RecruitFacultyCreate()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //prepare model
            var model = _recruitFacultyModelFactory.PrepareRecruitFacultyModel(new RecruitFacultyModel(), null);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult RecruitFacultyCreate(RecruitFacultyModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {

                var item = model.ToEntity<RecruitFaculty>();
                item.CreatedOnUtc = DateTime.UtcNow;
                recruitFacultyService.InsertRecruitFaculty(item);

                if (!continueEditing)
                    return RedirectToAction("RecruitFaculties");

                return RedirectToAction("RecruitFacultyEdit", new { id = item.Id });
            }

            //prepare model
            model = _recruitFacultyModelFactory.PrepareRecruitFacultyModel(model, null, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult RecruitFacultyEdit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var item = recruitFacultyService.GetRecruitFacultyById(id);
            if (item == null)
                return RedirectToAction("RecruitFaculties");

            //prepare model
            var model = _recruitFacultyModelFactory.PrepareRecruitFacultyModel(null, item);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult RecruitFacultyEdit(RecruitFacultyModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var item = recruitFacultyService.GetRecruitFacultyById(model.Id);
            if (item == null)
                return RedirectToAction("RecruitFaculties");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                recruitFacultyService.UpdateRecruitFaculty(item);

                if (!continueEditing)
                    return RedirectToAction("RecruitFaculties");

                return RedirectToAction("RecruitFacultyEdit", new { id = item.Id });
            }

            //prepare model
            model = _recruitFacultyModelFactory.PrepareRecruitFacultyModel(model, item, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult RecruitFacultyDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var item = recruitFacultyService.GetRecruitFacultyById(id);
            if (item == null)
                return RedirectToAction("RecruitFaculties");

            recruitFacultyService.DeleteRecruitFaculty(item);

            return RedirectToAction("RecruitFaculties");
        }

        #endregion

        #region Job
        public virtual IActionResult Index()
        {
            return RedirectToAction("Jobs");
        }

        public virtual IActionResult Jobs(int? filterByJobId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //prepare model
            var model = _recruitFacultyModelFactory.PrepareJobSearchModel(new JobSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(JobSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedDataTablesJson();

            if (searchModel.SearchCategoryId == 0)
            {
                searchModel.SearchCategoryId = null;
            }
            //prepare model
            var model = _recruitFacultyModelFactory.PrepareJobListModel(searchModel);

            return Json(model);
        }

        public virtual IActionResult JobCreate()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //prepare model
            var model = _recruitFacultyModelFactory.PrepareJobModel(new JobModel(), null, true);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult JobCreate(JobModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var item = model.ToEntity<Job>();
                item.CreatedOnUtc = DateTime.UtcNow;
                // item.RecruitFacultyId =  model.SelectedCategoryIds[0];
                item.LanguageId = 2;
                _jobService.InsertJob(item);

                //search engine name
                var seName = _urlRecordService.ValidateSeName(item, model.SeName, model.Title, true);
                _urlRecordService.SaveSlug(item, seName, item.LanguageId);

                // //Stores
                // SaveStoreMappings(item, model);
                // //categories
                // SaveCategoryMappings(item, model);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.Jobs.Added"));

                if (!continueEditing)
                    return RedirectToAction("Jobs");

                return RedirectToAction("JobEdit", new { id = item.Id });
            }

            //prepare model
            model = _recruitFacultyModelFactory.PrepareJobModel(model, null, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult JobEdit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var item = _jobService.GetJobById(id);
            if (item == null)
                return RedirectToAction("Jobs");

            //prepare model
            var model = _recruitFacultyModelFactory.PrepareJobModel(null, item);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult JobEdit(JobModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var item = _jobService.GetJobById(model.Id);
            if (item == null)
                return RedirectToAction("Jobs");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                // item.RecruitFacultyId =  model.SelectedCategoryIds[0];
                item.LanguageId = 2;
                _jobService.UpdateJob(item);

                //activity log
                _customerActivityService.InsertActivity("EditNews",
                    string.Format(_localizationService.GetResource("ActivityLog.EditNews"), item.Id), item);

                //search engine name
                var seName = _urlRecordService.ValidateSeName(item, model.SeName, model.Title, true);
                _urlRecordService.SaveSlug(item, seName, item.LanguageId);

                // //stores
                // SaveStoreMappings(item, model);

                // //categories
                // SaveCategoryMappings(item, model);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.Jobs.Updated"));

                if (!continueEditing)
                    return RedirectToAction("Jobs");

                return RedirectToAction("JobEdit", new { id = item.Id });
            }

            //prepare model
            model = _recruitFacultyModelFactory.PrepareJobModel(model, item, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var item = _jobService.GetJobById(id);
            if (item == null)
                return RedirectToAction("Jobs");

            _jobService.DeleteJob(item);

            //activity log
            _customerActivityService.InsertActivity("DeleteJob",
                string.Format(_localizationService.GetResource("ActivityLog.DeleteJob"), item.Id), item);

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.Jobs.Deleted"));

            return RedirectToAction("Jobs");
        }
        #endregion
    
        #region Resume
        public virtual IActionResult Resume()
        {
            return RedirectToAction("Resumes");
        }

        public virtual IActionResult Resumes(int? filterByResumeId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //prepare model
            var model = _recruitFacultyModelFactory.PrepareResumeSearchModel(new ResumeSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult ResumeList(ResumeSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedDataTablesJson();

            if (searchModel.SearchCategoryId == 0)
            {
                searchModel.SearchCategoryId = null;
            }
            //prepare model
            var model = _recruitFacultyModelFactory.PrepareResumeListModel(searchModel);

            return Json(model);
        }

        public virtual IActionResult ResumeCreate()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //prepare model
            var model = _recruitFacultyModelFactory.PrepareResumeModel(new ResumeModel(), null, true);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult ResumeCreate(ResumeModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var item = model.ToEntity<Resume>();
                item.CreatedOnUtc = DateTime.UtcNow;
                // item.RecruitFacultyId =  model.SelectedCategoryIds[0];
                _resumeService.InsertResume(item);


                // //Stores
                // SaveStoreMappings(item, model);
                // //categories
                // SaveCategoryMappings(item, model);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.Resumes.Added"));

                if (!continueEditing)
                    return RedirectToAction("Resumes");

                return RedirectToAction("ResumeEdit", new { id = item.Id });
            }

            //prepare model
            model = _recruitFacultyModelFactory.PrepareResumeModel(model, null, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult ResumeEdit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var item = _resumeService.GetResumeById(id);
            if (item == null)
                return RedirectToAction("Resumes");

            //prepare model
            var model = _recruitFacultyModelFactory.PrepareResumeModel(null, item);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult ResumeEdit(ResumeModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var item = _resumeService.GetResumeById(model.Id);
            if (item == null)
                return RedirectToAction("Resumes");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                // item.RecruitFacultyId =  model.SelectedCategoryIds[0];
                _resumeService.UpdateResume(item);

                //activity log
                _customerActivityService.InsertActivity("EditNews",
                    string.Format(_localizationService.GetResource("ActivityLog.EditNews"), item.Id), item);

                // //stores
                // SaveStoreMappings(item, model);

                // //categories
                // SaveCategoryMappings(item, model);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.Resumes.Updated"));

                if (!continueEditing)
                    return RedirectToAction("Resumes");

                return RedirectToAction("ResumeEdit", new { id = item.Id });
            }

            //prepare model
            model = _recruitFacultyModelFactory.PrepareResumeModel(model, item, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult ResumeDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var item = _resumeService.GetResumeById(id);
            if (item == null)
                return RedirectToAction("Resumes");

            _resumeService.DeleteResume(item);

            //activity log
            _customerActivityService.InsertActivity("DeleteResume",
                string.Format(_localizationService.GetResource("ActivityLog.DeleteResume"), item.Id), item);

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.Resumes.Deleted"));

            return RedirectToAction("Resumes");
        }

        public virtual IActionResult ResumeInfo(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var item = _resumeService.GetResumeById(id);
            if (item == null)
                return RedirectToAction("Resumes");

            //prepare model
            var model = _recruitFacultyModelFactory.PrepareResumeModel(null, item);

            return View(model);
        }
        #endregion
    
    }
}