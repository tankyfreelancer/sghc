﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.News;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.News;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.News;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers
{
    public partial class NewsController : BaseAdminController
    {
        #region Fields

        private readonly ICustomerActivityService _customerActivityService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILocalizationService _localizationService;
        private readonly INewsModelFactory _newsModelFactory;
        private readonly INewsCategoryModelFactory _newsCategoryModelFactory;
        private readonly INewsService _newsService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IStoreService _storeService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IPictureService _pictureService;

        #endregion

        #region Ctor

        public NewsController(ICustomerActivityService customerActivityService,
            IEventPublisher eventPublisher,
            ILocalizationService localizationService,
            INewsModelFactory newsModelFactory,
            INewsService newsService,
            INewsCategoryModelFactory newsCategoryModelFactory,
            INotificationService notificationService,
            IPermissionService permissionService,
            IStoreMappingService storeMappingService,
            IStoreService storeService,
            IPictureService pictureService,
            IUrlRecordService urlRecordService)
        {
            _customerActivityService = customerActivityService;
            _eventPublisher = eventPublisher;
            _localizationService = localizationService;
            _newsModelFactory = newsModelFactory;
            _newsService = newsService;
            _newsCategoryModelFactory = newsCategoryModelFactory;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _storeMappingService = storeMappingService;
            _storeService = storeService;
            _urlRecordService = urlRecordService;
            _pictureService = pictureService;
        }

        #endregion

        #region Utilities

        protected virtual void SaveStoreMappings(NewsItem newsItem, NewsItemModel model)
        {
            newsItem.LimitedToStores = model.SelectedStoreIds.Any();

            var existingStoreMappings = _storeMappingService.GetStoreMappings(newsItem);
            var allStores = _storeService.GetAllStores();
            foreach (var store in allStores)
            {
                if (model.SelectedStoreIds.Contains(store.Id))
                {
                    //new store
                    if (existingStoreMappings.Count(sm => sm.StoreId == store.Id) == 0)
                        _storeMappingService.InsertStoreMapping(newsItem, store.Id);
                }
                else
                {
                    //remove store
                    var storeMappingToDelete = existingStoreMappings.FirstOrDefault(sm => sm.StoreId == store.Id);
                    if (storeMappingToDelete != null)
                        _storeMappingService.DeleteStoreMapping(storeMappingToDelete);
                }
            }
        }

        #endregion

        #region Methods        

        #region News category

        public virtual IActionResult NewsCategory()
        {
            return RedirectToAction("NewsCategories");
        }

        public virtual IActionResult NewsCategories(int? filterByNewsCategoryId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //prepare model
            var model = _newsCategoryModelFactory.PrepareNewsContentModel(new NewsContentModel(), filterByNewsCategoryId);

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult NewsCategoryList(NewsItemSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedDataTablesJson();

            if(searchModel.SearchCategoryId == 0){
                searchModel.SearchCategoryId = null;
            }
            //prepare model
            var model = _newsCategoryModelFactory.PrepareNewsCategoryListModel(searchModel);

            return Json(model);
        }

        public virtual IActionResult NewsCategoryCreate()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //prepare model
            var model = _newsCategoryModelFactory.PrepareNewsCategoryModel(new NewsCategoryModel(), null);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult NewsCategoryCreate(NewsCategoryModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(model.Full)){
                    return View(model);
                }
                if(model.CategoryId == 0){
                    model.CategoryId = null;
                }
                var newsItem = model.ToEntity<NewsCategory>();
                newsItem.CreatedOnUtc = DateTime.UtcNow;
                _newsService.InsertNewsCategory(newsItem);

                //activity log
                _customerActivityService.InsertActivity("AddNewNews",
                    string.Format(_localizationService.GetResource("ActivityLog.AddNewNews"), newsItem.Id), newsItem);

                //search engine name
                var seName = _urlRecordService.ValidateSeName(newsItem, model.SeName, model.Title, true);
                _urlRecordService.SaveSlug(newsItem, seName, newsItem.LanguageId);

                //Stores
                //SaveStoreMappings(newsItem, model);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.News.NewsCategories.Added"));

                if (!continueEditing)
                    return RedirectToAction("NewsCategories");

                return RedirectToAction("NewsCategoryEdit", new { id = newsItem.Id });
            }

            //prepare model
            model = _newsCategoryModelFactory.PrepareNewsCategoryModel(model, null, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult NewsCategoryEdit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var newsItem = _newsService.GetNewsCategoryById(id);
            if (newsItem == null)
                return RedirectToAction("NewsCategories");
            
            //prepare model
            var model = _newsCategoryModelFactory.PrepareNewsCategoryModel(null, newsItem);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult NewsCategoryEdit(NewsCategoryModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var newsItem = _newsService.GetNewsCategoryById(model.Id);
            if (newsItem == null)
                return RedirectToAction("NewsCategories");

            if (ModelState.IsValid)
            {
                if(model.CategoryId == 0){
                    model.CategoryId = null;
                }
                newsItem = model.ToEntity(newsItem);
                _newsService.UpdateNewsCategory(newsItem);

                //activity log
                _customerActivityService.InsertActivity("EditNews",
                    string.Format(_localizationService.GetResource("ActivityLog.EditNews"), newsItem.Id), newsItem);

                //search engine name
                var seName = _urlRecordService.ValidateSeName(newsItem, model.SeName, model.Title, true);
                _urlRecordService.SaveSlug(newsItem, seName, newsItem.LanguageId);

                //stores
                //SaveStoreMappings(newsItem, model);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.News.NewsCategories.Updated"));

                if (!continueEditing)
                    return RedirectToAction("NewsCategories");

                return RedirectToAction("NewsCategoryEdit", new { id = newsItem.Id });
            }

            //prepare model
            model = _newsCategoryModelFactory.PrepareNewsCategoryModel(model, newsItem, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult NewsCategoryDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var newsItem = _newsService.GetNewsCategoryById(id);
            if (newsItem == null)
                return RedirectToAction("NewsCategories");

            _newsService.DeleteNewsCategory(newsItem);

            //activity log
            _customerActivityService.InsertActivity("DeleteNews",
                string.Format(_localizationService.GetResource("ActivityLog.DeleteNews"), newsItem.Id), newsItem);

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.News.NewsCategories.Deleted"));

            return RedirectToAction("NewsCategories");
        }

        #endregion

        #region News items

        public virtual IActionResult Index()
        {
            return RedirectToAction("NewsItems");
        }

        public virtual IActionResult NewsItems(int? filterByNewsItemId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //prepare model
            var model = _newsModelFactory.PrepareNewsItemSearchModel(new NewsItemSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(NewsItemSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedDataTablesJson();

            if(searchModel.SearchCategoryId == 0){
                searchModel.SearchCategoryId = null;
            }
            //prepare model
            var model = _newsModelFactory.PrepareNewsItemListModel(searchModel);

            return Json(model);
        }

        public virtual IActionResult NewsItemCreate()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //prepare model
            var model = _newsModelFactory.PrepareNewsItemModel(new NewsItemModel(), null,true);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult NewsItemCreate(NewsItemModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var newsItem = model.ToEntity<NewsItem>();
                newsItem.CreatedOnUtc = DateTime.UtcNow;
                _newsService.InsertNews(newsItem);

                //activity log
                _customerActivityService.InsertActivity("AddNewNews",
                    string.Format(_localizationService.GetResource("ActivityLog.AddNewNews"), newsItem.Id), newsItem);

                //search engine name
                var seName = _urlRecordService.ValidateSeName(newsItem, model.SeName, model.Title, true);
                _urlRecordService.SaveSlug(newsItem, seName, newsItem.LanguageId);

                //Stores
                SaveStoreMappings(newsItem, model);
                //categories
                SaveCategoryMappings(newsItem, model);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.News.NewsItems.Added"));

                if (!continueEditing)
                    return RedirectToAction("NewsItems");

                return RedirectToAction("NewsItemEdit", new { id = newsItem.Id });
            }

            //prepare model
            model = _newsModelFactory.PrepareNewsItemModel(model, null, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult NewsItemEdit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var newsItem = _newsService.GetNewsById(id);
            if (newsItem == null)
                return RedirectToAction("NewsItems");

            //prepare model
            var model = _newsModelFactory.PrepareNewsItemModel(null, newsItem);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult NewsItemEdit(NewsItemModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var newsItem = _newsService.GetNewsById(model.Id);
            if (newsItem == null)
                return RedirectToAction("NewsItems");

            if (ModelState.IsValid)
            {
                newsItem = model.ToEntity(newsItem);
                _newsService.UpdateNews(newsItem);

                //activity log
                _customerActivityService.InsertActivity("EditNews",
                    string.Format(_localizationService.GetResource("ActivityLog.EditNews"), newsItem.Id), newsItem);

                //search engine name
                var seName = _urlRecordService.ValidateSeName(newsItem, model.SeName, model.Title, true);
                _urlRecordService.SaveSlug(newsItem, seName, newsItem.LanguageId);

                //stores
                SaveStoreMappings(newsItem, model);

                //categories
                SaveCategoryMappings(newsItem, model);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.News.NewsItems.Updated"));

                if (!continueEditing)
                    return RedirectToAction("NewsItems");

                return RedirectToAction("NewsItemEdit", new { id = newsItem.Id });
            }

            //prepare model
            model = _newsModelFactory.PrepareNewsItemModel(model, newsItem, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var newsItem = _newsService.GetNewsById(id);
            if (newsItem == null)
                return RedirectToAction("NewsItems");

            _newsService.DeleteNews(newsItem);

            //activity log
            _customerActivityService.InsertActivity("DeleteNews",
                string.Format(_localizationService.GetResource("ActivityLog.DeleteNews"), newsItem.Id), newsItem);

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.ContentManagement.News.NewsItems.Deleted"));

            return RedirectToAction("NewsItems");
        }

        #endregion

        #region Comments

        public virtual IActionResult NewsComments(int? filterByNewsItemId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news item with the specified id
            var newsItem = _newsService.GetNewsById(filterByNewsItemId ?? 0);
            if (newsItem == null && filterByNewsItemId.HasValue)
                return RedirectToAction("NewsComments");

            //prepare model
            var model = _newsModelFactory.PrepareNewsCommentSearchModel(new NewsCommentSearchModel(), newsItem);

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Comments(NewsCommentSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _newsModelFactory.PrepareNewsCommentListModel(searchModel, searchModel.NewsItemId);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult CommentUpdate(NewsCommentModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news comment with the specified id
            var comment = _newsService.GetNewsCommentById(model.Id)
                ?? throw new ArgumentException("No comment found with the specified id");

            var previousIsApproved = comment.IsApproved;

            //fill entity from model
            comment = model.ToEntity(comment);

            //activity log
            _customerActivityService.InsertActivity("EditNewsComment",
                string.Format(_localizationService.GetResource("ActivityLog.EditNewsComment"), comment.Id), comment);

            //raise event (only if it wasn't approved before and is approved now)
            if (!previousIsApproved && comment.IsApproved)
                _eventPublisher.Publish(new NewsCommentApprovedEvent(comment));

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult CommentDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news comment with the specified id
            var comment = _newsService.GetNewsCommentById(id)
                ?? throw new ArgumentException("No comment found with the specified id", nameof(id));

            _newsService.DeleteNewsComment(comment);

            //activity log
            _customerActivityService.InsertActivity("DeleteNewsComment",
                string.Format(_localizationService.GetResource("ActivityLog.DeleteNewsComment"), comment.Id), comment);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult DeleteSelectedComments(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            if (selectedIds == null)
                return Json(new { Result = true });

            var comments = _newsService.GetNewsCommentsByIds(selectedIds.ToArray());

            _newsService.DeleteNewsComments(comments);

            //activity log
            foreach (var newsComment in comments)
            {
                _customerActivityService.InsertActivity("DeleteNewsComment",
                    string.Format(_localizationService.GetResource("ActivityLog.DeleteNewsComment"), newsComment.Id), newsComment);
            }

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult ApproveSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            if (selectedIds == null)
                return Json(new { Result = true });

            //filter not approved comments
            var newsComments = _newsService.GetNewsCommentsByIds(selectedIds.ToArray()).Where(comment => !comment.IsApproved);

            foreach (var newsComment in newsComments)
            {
                newsComment.IsApproved = true;

                //raise event 
                _eventPublisher.Publish(new NewsCommentApprovedEvent(newsComment));

                //activity log
                _customerActivityService.InsertActivity("EditNewsComment",
                    string.Format(_localizationService.GetResource("ActivityLog.EditNewsComment"), newsComment.Id), newsComment);
            }

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult DisapproveSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            if (selectedIds == null)
                return Json(new { Result = true });

            //filter approved comments
            var newsComments = _newsService.GetNewsCommentsByIds(selectedIds.ToArray()).Where(comment => comment.IsApproved);

            foreach (var newsComment in newsComments)
            {
                newsComment.IsApproved = false;

                //activity log
                _customerActivityService.InsertActivity("EditNewsComment",
                    string.Format(_localizationService.GetResource("ActivityLog.EditNewsComment"), newsComment.Id), newsComment);
            }

            return Json(new { Result = true });
        }

        #endregion

        #region News pictures

        public virtual IActionResult NewsPictureAdd(int pictureId, int displayOrder,
            string overrideAltAttribute, string overrideTitleAttribute, int newsId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            if (pictureId == 0)
                throw new ArgumentException();

            //try to get a news with the specified id
            var news = _newsService.GetNewsById(newsId)
                ?? throw new ArgumentException("No news found with the specified id");


            if (_newsService.GetNewsPicturesByNewsId(newsId).Any(p => p.PictureId == pictureId))
                return Json(new { Result = false });

            //try to get a picture with the specified id
            var picture = _pictureService.GetPictureById(pictureId)
                ?? throw new ArgumentException("No picture found with the specified id");

            _pictureService.UpdatePicture(picture.Id,
                _pictureService.LoadPictureBinary(picture),
                picture.MimeType,
                picture.SeoFilename,
                overrideAltAttribute,
                overrideTitleAttribute);

            _pictureService.SetSeoFilename(pictureId, _pictureService.GetPictureSeName(news.Title));

            _newsService.InsertNewsPicture(new NewsPicture
            {
                PictureId = pictureId,
                NewsId = newsId,
                DisplayOrder = displayOrder
            });

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult NewsPictureList(NewsPictureSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedDataTablesJson();

            //try to get a news with the specified id
            var news = _newsService.GetNewsById(searchModel.NewsId)
                ?? throw new ArgumentException("No news found with the specified id");

            //prepare model
            var model = _newsModelFactory.PrepareNewsPictureListModel(searchModel, news);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult NewsPictureUpdate(NewsPictureModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news picture with the specified id
            var newsPicture = _newsService.GetNewsPictureById(model.Id)
                ?? throw new ArgumentException("No news picture found with the specified id");

          

            //try to get a picture with the specified id
            var picture = _pictureService.GetPictureById(newsPicture.PictureId)
                ?? throw new ArgumentException("No picture found with the specified id");

            _pictureService.UpdatePicture(picture.Id,
                _pictureService.LoadPictureBinary(picture),
                picture.MimeType,
                picture.SeoFilename,
                model.OverrideAltAttribute,
                model.OverrideTitleAttribute);

            newsPicture.DisplayOrder = model.DisplayOrder;
            _newsService.UpdateNewsPicture(newsPicture);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult NewsPictureDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news picture with the specified id
            var newsPicture = _newsService.GetNewsPictureById(id)
                ?? throw new ArgumentException("No news picture found with the specified id");

         

            var pictureId = newsPicture.PictureId;
            _newsService.DeleteNewsPicture(newsPicture);

            //try to get a picture with the specified id
            var picture = _pictureService.GetPictureById(pictureId)
                ?? throw new ArgumentException("No picture found with the specified id");

            _pictureService.DeletePicture(picture);

            return new NullJsonResult();
        }

        #endregion
        #endregion

        #region News Category pictures

        public virtual IActionResult NewsCategoryPictureAdd(int pictureId, int displayOrder,
            string overrideAltAttribute, string overrideTitleAttribute, int newsCategoryId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            if (pictureId == 0)
                throw new ArgumentException();

            //try to get a news with the specified id
            var news = _newsService.GetNewsCategoryById(newsCategoryId)
                ?? throw new ArgumentException("No news found with the specified id");


            if (_newsService.GetNewsCategoryPicturesByNewsCategoryId(newsCategoryId).Any(p => p.PictureId == pictureId))
                return Json(new { Result = false });

            //try to get a picture with the specified id
            var picture = _pictureService.GetPictureById(pictureId)
                ?? throw new ArgumentException("No picture found with the specified id");

            _pictureService.UpdatePicture(picture.Id,
                _pictureService.LoadPictureBinary(picture),
                picture.MimeType,
                picture.SeoFilename,
                overrideAltAttribute,
                overrideTitleAttribute);

            _pictureService.SetSeoFilename(pictureId, _pictureService.GetPictureSeName(news.Title));

            _newsService.InsertNewsCategoryPicture(new NewsCategoryPicture
            {
                PictureId = pictureId,
                NewsCategoryId = newsCategoryId,
                DisplayOrder = displayOrder
            });

            return Json(new { Result = true });
        }

        [HttpPost]
        public virtual IActionResult NewsCategoryPictureList(NewsCategoryPictureSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedDataTablesJson();

            //try to get a news with the specified id
            var news = _newsService.GetNewsCategoryById(searchModel.NewsCategoryId)
                ?? throw new ArgumentException("No news found with the specified id");

            //prepare model
            var model = _newsModelFactory.PrepareNewsCategoryPictureListModel(searchModel, news);

            return Json(model);
        }

        [HttpPost]
        public virtual IActionResult NewsCategoryPictureUpdate(NewsPictureModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news picture with the specified id
            var newsCategoryPicture = _newsService.GetNewsCategoryPictureById(model.Id)
                ?? throw new ArgumentException("No news picture found with the specified id");

          

            //try to get a picture with the specified id
            var picture = _pictureService.GetPictureById(newsCategoryPicture.PictureId)
                ?? throw new ArgumentException("No picture found with the specified id");

            _pictureService.UpdatePicture(picture.Id,
                _pictureService.LoadPictureBinary(picture),
                picture.MimeType,
                picture.SeoFilename,
                model.OverrideAltAttribute,
                model.OverrideTitleAttribute);

            newsCategoryPicture.DisplayOrder = model.DisplayOrder;
            _newsService.UpdateNewsCategoryPicture(newsCategoryPicture);

            return new NullJsonResult();
        }

        [HttpPost]
        public virtual IActionResult NewsCategoryPictureDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageNews))
                return AccessDeniedView();

            //try to get a news picture with the specified id
            var newsCategoryPicture = _newsService.GetNewsCategoryPictureById(id)
                ?? throw new ArgumentException("No news Category picture found with the specified id");

         

            var pictureId = newsCategoryPicture.PictureId;
            _newsService.DeleteNewsCategoryPicture(newsCategoryPicture);

            //try to get a picture with the specified id
            var picture = _pictureService.GetPictureById(pictureId)
                ?? throw new ArgumentException("No picture found with the specified id");

            _pictureService.DeletePicture(picture);

            return new NullJsonResult();
        }

        #endregion

        protected virtual void SaveCategoryMappings(NewsItem news, NewsItemModel model)
        {
            var existingProductCategories = _newsService.GetByNewsId(news.Id, true);

            //delete categories
            foreach (var existingProductCategory in existingProductCategories)
                if (!model.SelectedCategoryIds.Contains(existingProductCategory.CategoryId))
                    _newsService.DeleteNewsCategoryMap(existingProductCategory);

            //add categories
            foreach (var categoryId in model.SelectedCategoryIds)
            {
                if (_newsService.FindNewsCategoryMap(existingProductCategories, news.Id, categoryId) == null)
                {
                    //find next display order
                    var displayOrder = 1;
                    var existingCategoryMapping = _newsService.GetNewsByNewsCategoryId(categoryId, showHidden: true);
                    if (existingCategoryMapping.Any())
                        displayOrder = existingCategoryMapping.Max(x => x.DisplayOrder) + 1;
                    _newsService.InsertNewsCategoryMap(new NewsCategoryMap
                    {
                        NewsId = news.Id,
                        CategoryId = categoryId,
                        DisplayOrder = displayOrder
                    });
                }
            }
        }
    }
}