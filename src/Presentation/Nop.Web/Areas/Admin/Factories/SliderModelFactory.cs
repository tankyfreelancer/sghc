﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Services.Catalog;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Seo;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Areas.Admin.Models.Media;
using Nop.Web.Framework.Extensions;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the slider model factory implementation
    /// </summary>
    public partial class SliderModelFactory : ISliderModelFactory
    {
        #region Fields

        private readonly CatalogSettings _catalogSettings;
        private readonly IAclSupportedModelFactory _aclSupportedModelFactory;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ISliderService _sliderService;
        private readonly IDiscountService _discountService;
        private readonly IDiscountSupportedModelFactory _discountSupportedModelFactory;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedModelFactory _localizedModelFactory;
        private readonly IProductService _productService;
        private readonly IStoreMappingSupportedModelFactory _storeMappingSupportedModelFactory;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IPictureService _pictureService;

        #endregion

        #region Ctor

        public SliderModelFactory(CatalogSettings catalogSettings,
            IAclSupportedModelFactory aclSupportedModelFactory,
            IBaseAdminModelFactory baseAdminModelFactory,
            ISliderService sliderService,
            IDiscountService discountService,
            IDiscountSupportedModelFactory discountSupportedModelFactory,
            ILocalizationService localizationService,
            ILocalizedModelFactory localizedModelFactory,
            IProductService productService,
            IStoreMappingSupportedModelFactory storeMappingSupportedModelFactory,
            IPictureService pictureService,
            IUrlRecordService urlRecordService)
        {
            _catalogSettings = catalogSettings;
            _aclSupportedModelFactory = aclSupportedModelFactory;
            _baseAdminModelFactory = baseAdminModelFactory;
            _sliderService = sliderService;
            _discountService = discountService;
            _discountSupportedModelFactory = discountSupportedModelFactory;
            _localizationService = localizationService;
            _localizedModelFactory = localizedModelFactory;
            _productService = productService;
            _storeMappingSupportedModelFactory = storeMappingSupportedModelFactory;
            _urlRecordService = urlRecordService;
            _pictureService = pictureService;
        }

        public SliderListModel PrepareSliderListModel(SliderSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));


            //get sliders
            var sliders = _sliderService.GetAllSliders(showHidden: true,
                title: searchModel.Title,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare grid model
            var model = new SliderListModel().PrepareToGrid(searchModel, sliders, () =>
            {
                //fill in model values from the entity
                return sliders.Select(slider =>
                {
                    var sliderModel = slider.ToModel<SliderModel>();
                    var picture = _pictureService.GetPictureById(slider.PictureId);
                    if(picture != null){
                        sliderModel.PictureThumbnailUrl = _pictureService.GetPictureUrl(picture, 75);
                    }
                    return sliderModel;
                });
            });

            return model;
        }

        public SliderModel PrepareSliderModel(SliderModel model, Slider slider, bool excludeProperties = false)
        {
            //set default values for the new model
            if (slider == null)
            {
                model.PageSize = _catalogSettings.DefaultManufacturerPageSize;
                model.PageSizeOptions = _catalogSettings.DefaultManufacturerPageSizeOptions;
                model.AllowCustomersToSelectPageSize = true;

                return model;
            }
            //fill in model values from the entity
            if (model == null)
            {
                model = slider.ToModel<SliderModel>();
            }
            var picture = _pictureService.GetPictureById(model.PictureId);
            if(picture != null){
                model.PictureThumbnailUrl = _pictureService.GetPictureUrl(picture, 75);
            }
            return model;
        }

        public SliderSearchModel PrepareSliderSearchModel(SliderSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        #endregion


    }
}