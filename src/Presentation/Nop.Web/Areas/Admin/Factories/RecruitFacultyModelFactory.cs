﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Recruit;
using Nop.Core.Html;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Recruit;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Infrastructure.Cache;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Recruit;
using Nop.Web.Framework.Extensions;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the recruitFaculty model factory implementation
    /// </summary>
    public partial class RecruitFacultyModelFactory : IRecruitFacultyModelFactory
    {
        #region Fields

        private readonly CatalogSettings _catalogSettings;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ICustomerService _customerService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly IRecruitFacultyService _recruitFacultyService;
        private readonly IJobService _jobService;
        private readonly IResumeService _resumeService;
        private readonly IStoreMappingSupportedModelFactory _storeMappingSupportedModelFactory;
        private readonly IStoreService _storeService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IPictureService _pictureService;
        private readonly IStaticCacheManager _cacheManager;
        private readonly ICountryService _countryService;

        #endregion

        #region Ctor

        public RecruitFacultyModelFactory(CatalogSettings catalogSettings,
            IBaseAdminModelFactory baseAdminModelFactory,
            ICustomerService customerService,
            IDateTimeHelper dateTimeHelper,
            ILanguageService languageService,
            ILocalizationService localizationService,
            IRecruitFacultyService recruitFacultyService,
            IStoreMappingSupportedModelFactory storeMappingSupportedModelFactory,
            IStoreService storeService,
            IPictureService pictureService,
            IStaticCacheManager cacheManager,
            IJobService jobService,
            IResumeService resumeService,
            ICountryService countryService,
            IUrlRecordService urlRecordService)
        {
            _catalogSettings = catalogSettings;
            _customerService = customerService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _dateTimeHelper = dateTimeHelper;
            _languageService = languageService;
            _localizationService = localizationService;
            _recruitFacultyService = recruitFacultyService;
            _storeMappingSupportedModelFactory = storeMappingSupportedModelFactory;
            _storeService = storeService;
            _urlRecordService = urlRecordService;
            _pictureService = pictureService;
            _cacheManager = cacheManager;
            _jobService = jobService;
            _resumeService = resumeService;
            _countryService = countryService;
        }


        #endregion
        #region Recruit Faculty
        public RecruitFacultyContentModel PrepareRecruitFacultyContentModel(RecruitFacultyContentModel recruitFacultyContentModel, int? filterByRecruitFacultyId)
        {
            if (recruitFacultyContentModel == null)
                throw new ArgumentNullException(nameof(recruitFacultyContentModel));

            //prepare nested search models
            PrepareRecruitFacultySearchModel(recruitFacultyContentModel.Items);
            var item = _recruitFacultyService.GetRecruitFacultyById(filterByRecruitFacultyId ?? 0);


            return recruitFacultyContentModel;
        }

        public RecruitFacultyListModel PrepareRecruitFacultyListModel(RecruitFacultySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var item = _recruitFacultyService.GetPaging(searchModel.SearchName);

            //prepare list model
            var model = new RecruitFacultyListModel().PrepareToGrid(searchModel, item, () =>
            {
                return item.Select(x =>
                {
                    //fill in model values from the entity
                    var itemModel = x.ToModel<RecruitFacultyModel>();
                    return itemModel;
                });
            });
            return model;
        }

        public RecruitFacultyModel PrepareRecruitFacultyModel(RecruitFacultyModel model, RecruitFaculty recruitFacultyItem, bool excludeProperties = false)
        {
            //fill in model values from the entity
            if (recruitFacultyItem != null)
            {
                if (model == null)
                {
                    model = recruitFacultyItem.ToModel<RecruitFacultyModel>();
                }
            }

            return model;
        }

        public RecruitFacultySearchModel PrepareRecruitFacultySearchModel(RecruitFacultySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare available stores
            _baseAdminModelFactory.PrepareStores(searchModel.AvailableStores);

            searchModel.HideStoresList = _catalogSettings.IgnoreStoreLimitations || searchModel.AvailableStores.SelectionIsNotPossible();

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        #endregion
        #region Job
        public JobContentModel PrepareJobContentModel(JobContentModel jobContentModel, int? filterByJobId)
        {
            if (jobContentModel == null)
                throw new ArgumentNullException(nameof(jobContentModel));

            //prepare nested search models
            PrepareJobSearchModel(jobContentModel.Items);
            var item = _jobService.GetJobById(filterByJobId ?? 0);


            return jobContentModel;
        }

        public JobListModel PrepareJobListModel(JobSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var item = _jobService.GetPaging(searchModel.SearchName);

            //prepare list model
            var model = new JobListModel().PrepareToGrid(searchModel, item, () =>
            {
                return item.Select(x =>
                {
                    //fill in model values from the entity
                    var itemModel = x.ToModel<JobModel>();
                    itemModel.SeName = _urlRecordService.GetSeName(x, x.LanguageId, true, false);
                    var category = _recruitFacultyService.GetRecruitFacultyById(itemModel.RecruitFacultyId.GetValueOrDefault());
                    if (category != null)
                    {
                        itemModel.RecruitFacultyName = category.Name;
                    }
                    return itemModel;
                });
            });
            return model;
        }

        public JobModel PrepareJobModel(JobModel model, Job jobItem, bool excludeProperties = false)
        {
            if (jobItem != null)
            {
                if (model == null)
                {
                    model = jobItem.ToModel<JobModel>();
                    model.SelectedCategoryIds = new List<int> { jobItem.RecruitFacultyId.GetValueOrDefault() };
                    model.SeName = _urlRecordService.GetSeName(jobItem, jobItem.LanguageId, true, false);
                }
            }
            if (!excludeProperties)
            {
                var item = _jobService.GetJobById(jobItem.Id);
                if (item != null)
                {
                    model.SelectedCategoryIds = new List<int> { item.RecruitFacultyId.GetValueOrDefault() };
                }
            }
            PrepareCategories(model.AvailableCategories, false);
            foreach (var categoryItem in model.AvailableCategories)
            {
                categoryItem.Selected = int.TryParse(categoryItem.Value, out var categoryId)
                    && model.SelectedCategoryIds.Contains(categoryId);
            }

            return model;
        }

        public JobSearchModel PrepareJobSearchModel(JobSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare available stores
            _baseAdminModelFactory.PrepareStores(searchModel.AvailableStores);

            searchModel.HideStoresList = _catalogSettings.IgnoreStoreLimitations || searchModel.AvailableStores.SelectionIsNotPossible();

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }
        protected virtual void PrepareCategories(IList<SelectListItem> items, bool withSpecialDefaultItem = true, string defaultItemText = null, bool isJobList = false)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));

            //prepare available categories
            var availableCategoryItems = GetCategoryList(isJobList: isJobList);
            foreach (var categoryItem in availableCategoryItems)
            {
                items.Add(categoryItem);
            }

            //insert special item for the default value
            PrepareDefaultItem(items, withSpecialDefaultItem, defaultItemText);
        }
        protected virtual void PrepareDefaultItem(IList<SelectListItem> items, bool withSpecialDefaultItem, string defaultItemText = null)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));

            //whether to insert the first special item for the default value
            if (!withSpecialDefaultItem)
                return;

            //at now we use "0" as the default value
            const string value = "0";

            //prepare item text
            defaultItemText ??= _localizationService.GetResource("Admin.Common.All");

            //insert this default item at first
            items.Insert(0, new SelectListItem { Text = defaultItemText, Value = value });
        }
        #endregion
        #region Resume
        public ResumeContentModel PrepareResumeContentModel(ResumeContentModel jobContentModel, int? filterByResumeId)
        {
            if (jobContentModel == null)
                throw new ArgumentNullException(nameof(jobContentModel));

            //prepare nested search models
            PrepareResumeSearchModel(jobContentModel.Items);
            var item = _resumeService.GetResumeById(filterByResumeId ?? 0);


            return jobContentModel;
        }

        public ResumeListModel PrepareResumeListModel(ResumeSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var item = _resumeService.GetPaging(searchModel.SearchName);

            //prepare list model
            var model = new ResumeListModel().PrepareToGrid(searchModel, item, () =>
            {
                return item.Select(x =>
                {
                    //fill in model values from the entity
                    var itemModel = x.ToModel<ResumeModel>();
                    var job = _jobService.GetJobById(itemModel.JobId.GetValueOrDefault());
                    if (job != null)
                    {
                        var recruitFaculty = _recruitFacultyService.GetRecruitFacultyById(job.RecruitFacultyId.GetValueOrDefault());
                        if (recruitFaculty != null)
                        {
                            itemModel.RecruitFacultyName = recruitFaculty.Name;
                        }
                        itemModel.JobName = job.Title;
                    }
                    itemModel.CityName = _countryService.GetCity(x.CityId)?.Name;
                    itemModel.TempCityName = _countryService.GetCity(x.TempCityId)?.Name;
                    itemModel.DistrictName = _countryService.GetDistrict(x.DistrictId)?.Name;
                    itemModel.TempDistrictName = _countryService.GetDistrict(x.TempDistrictId)?.Name;
                    itemModel.WardName = _countryService.GetWard(x.WardId)?.Name;
                    itemModel.TempWardName = _countryService.GetWard(x.TempWardId)?.Name;

                    return itemModel;
                });
            });
            return model;
        }

        public ResumeModel PrepareResumeModel(ResumeModel model, Resume resumeItem, bool excludeProperties = false)
        {
            if (resumeItem != null)
            {
                if (model == null)
                {
                    model = resumeItem.ToModel<ResumeModel>();
                    var job = _jobService.GetJobById(model.JobId.GetValueOrDefault());
                    if (job != null)
                    {
                        var recruitFaculty = _recruitFacultyService.GetRecruitFacultyById(job.RecruitFacultyId.GetValueOrDefault());
                        if (recruitFaculty != null)
                        {
                            model.RecruitFacultyName = recruitFaculty.Name;
                        }
                        model.JobName = job.Title;
                    }
                    model.CityName = _countryService.GetCity(model.CityId)?.Name;
                    model.TempCityName = _countryService.GetCity(model.TempCityId)?.Name;
                    model.DistrictName = _countryService.GetDistrict(model.DistrictId)?.Name;
                    model.TempDistrictName = _countryService.GetDistrict(model.TempDistrictId)?.Name;
                    model.WardName = _countryService.GetWard(model.WardId)?.Name;
                    model.TempWardName = _countryService.GetWard(model.TempWardId)?.Name;
                    model.SelectedCategoryIds = new List<int> { resumeItem.JobId.GetValueOrDefault() };
                }
            }
            if (!excludeProperties)
            {
                var item = _resumeService.GetResumeById(resumeItem.Id);
                if (item != null)
                {
                    model.SelectedCategoryIds = new List<int> { item.JobId.GetValueOrDefault() };
                }
            }
            PrepareCategories(model.AvailableCategories, false,isJobList: true);
            foreach (var categoryItem in model.AvailableCategories)
            {
                categoryItem.Selected = int.TryParse(categoryItem.Value, out var categoryId)
                    && model.SelectedCategoryIds.Contains(categoryId);
            }

            return model;
        }

        public ResumeSearchModel PrepareResumeSearchModel(ResumeSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare available stores
            _baseAdminModelFactory.PrepareStores(searchModel.AvailableStores);

            searchModel.HideStoresList = _catalogSettings.IgnoreStoreLimitations || searchModel.AvailableStores.SelectionIsNotPossible();

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        protected virtual List<SelectListItem> GetCategoryList(bool showHidden = true, bool isJobList = true)
        {
            var listItems = new List<SelectListItem>();
            if (isJobList)
            {
                var categories = _jobService.GetAll();
                listItems = categories.Select(c => new SelectListItem
                {
                    Text = c.Title,
                    Value = c.Id.ToString()
                }).ToList();
            }
            else
            {
                var categories = _recruitFacultyService.GetAll(showHidden: showHidden);
                listItems = categories.Select(c => new SelectListItem
                {
                    Text = c.Name,
                    Value = c.Id.ToString()
                }).ToList();
            }


            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in listItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }
        #endregion

    }
}