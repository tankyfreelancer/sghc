﻿using Nop.Core.Domain.Recruit;
using Nop.Web.Areas.Admin.Models.Recruit;

namespace Nop.Web.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the recruitFaculty model factory
    /// </summary>
    public partial interface IRecruitFacultyModelFactory
    {
        /// <summary>
        /// Prepare recruitFaculty content model
        /// </summary>
        /// <param name="recruitFacultyContentModel">RecruitFaculty content model</param>
        /// <param name="filterByRecruitFacultyId">Filter by recruitFaculty item ID</param>
        /// <returns>RecruitFaculty content model</returns>
        RecruitFacultyContentModel PrepareRecruitFacultyContentModel(RecruitFacultyContentModel recruitFacultyContentModel, int? filterByRecruitFacultyId);

        /// <summary>
        /// Prepare recruitFaculty item search model
        /// </summary>
        /// <param name="searchModel">RecruitFaculty item search model</param>
        /// <returns>RecruitFaculty item search model</returns>
        RecruitFacultySearchModel PrepareRecruitFacultySearchModel(RecruitFacultySearchModel searchModel);

        /// <summary>
        /// Prepare paged recruitFaculty item list model
        /// </summary>
        /// <param name="searchModel">RecruitFaculty item search model</param>
        /// <returns>RecruitFaculty item list model</returns>
        RecruitFacultyListModel PrepareRecruitFacultyListModel(RecruitFacultySearchModel searchModel);

        /// <summary>
        /// Prepare recruitFaculty item model
        /// </summary>
        /// <param name="model">RecruitFaculty item model</param>
        /// <param name="recruitFacultyItem">RecruitFaculty item</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>RecruitFaculty item model</returns>
        RecruitFacultyModel PrepareRecruitFacultyModel(RecruitFacultyModel model, RecruitFaculty recruitFacultyItem, bool excludeProperties = false);

        #region Job
        /// <summary>
        /// Prepare job content model
        /// </summary>
        /// <param name="jobContentModel">Job content model</param>
        /// <param name="filterByJobId">Filter by job item ID</param>
        /// <returns>Job content model</returns>
        JobContentModel PrepareJobContentModel(JobContentModel jobContentModel, int? filterByJobId);

        /// <summary>
        /// Prepare job item search model
        /// </summary>
        /// <param name="searchModel">Job item search model</param>
        /// <returns>Job item search model</returns>
        JobSearchModel PrepareJobSearchModel(JobSearchModel searchModel);

        /// <summary>
        /// Prepare paged job item list model
        /// </summary>
        /// <param name="searchModel">Job item search model</param>
        /// <returns>Job item list model</returns>
        JobListModel PrepareJobListModel(JobSearchModel searchModel);

        /// <summary>
        /// Prepare job item model
        /// </summary>
        /// <param name="model">Job item model</param>
        /// <param name="jobItem">Job item</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Job item model</returns>
        JobModel PrepareJobModel(JobModel model, Job jobItem, bool excludeProperties = false);
        #endregion

        #region Resume
        /// <summary>
        /// Prepare resume content model
        /// </summary>
        /// <param name="resumeContentModel">Resume content model</param>
        /// <param name="filterByResumeId">Filter by resume item ID</param>
        /// <returns>Resume content model</returns>
        ResumeContentModel PrepareResumeContentModel(ResumeContentModel resumeContentModel, int? filterByResumeId);

        /// <summary>
        /// Prepare resume item search model
        /// </summary>
        /// <param name="searchModel">Resume item search model</param>
        /// <returns>Resume item search model</returns>
        ResumeSearchModel PrepareResumeSearchModel(ResumeSearchModel searchModel);

        /// <summary>
        /// Prepare paged resume item list model
        /// </summary>
        /// <param name="searchModel">Resume item search model</param>
        /// <returns>Resume item list model</returns>
        ResumeListModel PrepareResumeListModel(ResumeSearchModel searchModel);

        /// <summary>
        /// Prepare resume item model
        /// </summary>
        /// <param name="model">Resume item model</param>
        /// <param name="resumeItem">Resume item</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Resume item model</returns>
        ResumeModel PrepareResumeModel(ResumeModel model, Resume resumeItem, bool excludeProperties = false);
        #endregion
    }
}