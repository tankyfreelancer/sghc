﻿using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Settings
{
    /// <summary>
    /// Represents a general and common settings model
    /// </summary>
    public partial class GeneralCommonSettingsModel : BaseNopModel, ISettingsModel
    {
        #region Ctor

        public GeneralCommonSettingsModel()
        {
            StoreInformationSettings = new StoreInformationSettingsModel();
            SitemapSettings = new SitemapSettingsModel();
            SeoSettings = new SeoSettingsModel();
            SecuritySettings = new SecuritySettingsModel();
            CaptchaSettings = new CaptchaSettingsModel();
            PdfSettings = new PdfSettingsModel();
            LocalizationSettings = new LocalizationSettingsModel();
            FullTextSettings = new FullTextSettingsModel();
            DisplayDefaultMenuItemSettings = new DisplayDefaultMenuItemSettingsModel();
            DisplayDefaultFooterItemSettings = new DisplayDefaultFooterItemSettingsModel();
            AdminAreaSettings = new AdminAreaSettingsModel();
            MinificationSettings = new MinificationSettingsModel();
            HomepageModel = new Nop.Web.Models.Home.HomepageModel();
        }

        #endregion

        #region Properties

        public int ActiveStoreScopeConfiguration { get; set; }

        public StoreInformationSettingsModel StoreInformationSettings { get; set; }

        public SitemapSettingsModel SitemapSettings { get; set; }

        public SeoSettingsModel SeoSettings { get; set; }

        public SecuritySettingsModel SecuritySettings { get; set; }

        public CaptchaSettingsModel CaptchaSettings { get; set; }

        public PdfSettingsModel PdfSettings { get; set; }

        public LocalizationSettingsModel LocalizationSettings { get; set; }

        public FullTextSettingsModel FullTextSettings { get; set; }

        public DisplayDefaultMenuItemSettingsModel DisplayDefaultMenuItemSettings { get; set; }

        public DisplayDefaultFooterItemSettingsModel DisplayDefaultFooterItemSettings { get; set; }

        public AdminAreaSettingsModel AdminAreaSettings { get; set; }

        public MinificationSettingsModel MinificationSettings { get; set; }
        public HospitalSettingsModel HospitalSettings{ get; set; }

        public Nop.Web.Models.Home.HomepageModel HomepageModel { get; set; }
        [UIHint("Picture")]
        public int Rss1PictureId { get; set; }
        [UIHint("Picture")]
        public int Rss2PictureId { get; set; }
        [UIHint("Picture")]
        public int BannerAboutPageId { get; set; }
        [UIHint("Picture")]
        public int BannerRecruitPageId { get; set; }
        [UIHint("Picture")]
        public int BannerContactPageId { get; set; }
        [UIHint("Picture")]
        public int BannerSpecialistPageId { get; set; }
        [UIHint("Picture")]
        public int BannerDoctorPageId { get; set; }
        [UIHint("Picture")]
        public int BannerServicePageId { get; set; }
        [UIHint("Picture")]
        public int BannerPackagePageId { get; set; }
        [UIHint("Picture")]
        public int BannerUserManualPageId { get; set; }
        [UIHint("Picture")]
        public int BannerNewsPageId { get; set; }
        [UIHint("Picture")]
        public int BannerDefaultId { get; set; }
        [UIHint("Picture")]
        public int HomePageBookingPictureId { get; set; }
        [UIHint("Picture")]
        public int HomePageDoctorPictureId { get; set; }
        [UIHint("Picture")]
        public int HomePageVendorPictureId { get; set; }
        [UIHint("Picture")]
        public int HomePageDoctorMobilePictureId { get; set; }
        [UIHint("Picture")]
        public int HomePageVendorMobilePictureId { get; set; }
        
        #endregion
    }
}