﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Settings
{
    /// <summary>
    /// Represents a Hospital settings model
    /// </summary>
    public partial class HospitalSettingsModel : BaseNopModel, ISettingsModel
    {
        #region Properties

        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.GeneralCommon.PageTitleSeparator")]
        [NoTrim]
        public string PageTitleSeparator { get; set; }
        public bool PageTitleSeparator_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.GeneralCommon.PageTitleHospitalAdjustment")]
        public int PageTitleHospitalAdjustment { get; set; }
        public bool PageTitleHospitalAdjustment_OverrideForStore { get; set; }
        public SelectList PageTitleHospitalAdjustmentValues { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string VideoUrl { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [UIHint("Picture")]
        [NopResourceDisplayName("setting.homepage.bannerfront")]
        public int BannerFront { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [UIHint("Picture")]
        [NopResourceDisplayName("setting.homepage.bannerback")]
        public int BannerBehind { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string PhoneSuport { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string PhoneOnline { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string BrandShortDescription { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Fax { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Address { get; set; }
        
        [UIHint("Picture")]
        [NopResourceDisplayName("setting.aboutpage.slider01")]
        public int AboutSlider01 { get; set; }
        [UIHint("Picture")]
        [NopResourceDisplayName("setting.aboutpage.slider01")]
        public int AboutSlider02 { get; set; }
        [UIHint("Picture")]
        [NopResourceDisplayName("setting.aboutpage.slider03")]
        public int AboutSlider03 { get; set; }
        public string RssUrl { get; set; }
        public string AboutVision { get; set; }
        public string AboutMission { get; set; }
        public string AboutCoreValue { get; set; }
        public string AboutWhyChooseUs { get; set; }
        [UIHint("Picture")]
        [NopResourceDisplayName("setting.aboutpage.avatar")]
        public int AboutPresidentAvatarId { get; set; }
        public string AboutPresidentTitle { get; set; }
        public string AboutPresidentDescription { get; set; }
        public string AboutPresidentContent { get; set; }
        public string AboutVideoUrl { get; set; }
        public string DoctorVideoUrl { get; set; }
        [UIHint("Picture")]
        [NopResourceDisplayName("setting.aboutpage.picture01")]
        public int AboutPicture01Id { get; set; }
        [UIHint("Picture")]
        [NopResourceDisplayName("setting.aboutpage.picture02")]
        public int AboutPicture02Id { get; set; }
        [UIHint("Picture")]
        [NopResourceDisplayName("setting.aboutpage.picture03")]
        public int AboutPicture03Id { get; set; }
        [UIHint("Picture")]
        [NopResourceDisplayName("setting.aboutpage.picture04")]
        public int AboutPicture04Id { get; set; }

       
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.customeraddresses")]
        public string Address1 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.fields.phone")]
        public string PhoneNumber1 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.fields.fax")]
        public string Fax1 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Email1 { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.customeraddresses")]
        public string Address2 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.fields.phone")]
        public string PhoneNumber2 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.fields.fax")]
        public string Fax2 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.associatedexternalauth.email")]
        public string Email2 { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.customeraddresses")]
        public string Address3 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.fields.phone")]
        public string PhoneNumber3 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.fields.fax")]
        public string Fax3 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.associatedexternalauth.email")]
        public string Email3 { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.customeraddresses")]
        public string Address4 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.fields.phone")]
        public string PhoneNumber4 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.fields.fax")]
        public string Fax4 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.associatedexternalauth.email")]
        public string Email4 { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.customeraddresses")]
        public string Address5 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.fields.phone")]
        public string PhoneNumber5 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.fields.fax")]
        public string Fax5 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.associatedexternalauth.email")]
        public string Email5 { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.customeraddresses")]
        public string Address6 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.fields.phone")]
        public string PhoneNumber6 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.fields.fax")]
        public string Fax6 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        [NopResourceDisplayName("account.associatedexternalauth.email")]
        public string Email6 { get; set; }

        [NopResourceDisplayName("branch")]
        public string Branch1 { get; set; }
        [NopResourceDisplayName("branch")]
        public string Branch2 { get; set; }
        [NopResourceDisplayName("branch")]
        public string Branch3 { get; set; }
        [NopResourceDisplayName("branch")]
        public string Branch4 { get; set; }
        [NopResourceDisplayName("branch")]
        public string Branch5 { get; set; }
        [NopResourceDisplayName("branch")]
        public string Branch6 { get; set; }
// config for sghc
    // banner
        [NopResourceDisplayName("setting.homepage.banner.smalltitle.text")]
        public string SmallTitle { get; set; }
        [NopResourceDisplayName("setting.homepage.banner.title.text")]
        public string Title { get; set; }
        [NopResourceDisplayName("setting.homepage.banner.slogan.text")]
        public string Slogan { get; set; }
        [NopResourceDisplayName("setting.homepage.banner.imageurl.text")]
        [UIHint("Picture")]
        public int BannerImageUrl { get; set; }

        public string BannerVideoUrl { get; set; }
    // booking 
        [NopResourceDisplayName("setting.homepage.booking.desc.text")]
        public string BookingDescription { get; set; }
        [NopResourceDisplayName("homepage.timetowork")]
        public string WorkTime { get; set; }
        public string BookingWorkTime { get; set; }
        [NopResourceDisplayName("setting.morning")]
        public string BookingWorkTimeMorning { get; set; }
        [NopResourceDisplayName("setting.afternoon")]
        public string BookingWorkTimeAfternoon { get; set; }
    // doctor
        [NopResourceDisplayName("setting.homepage.docktor.title.text.text")]
        public string DoctorTitle { get; set; }
        [NopResourceDisplayName("setting.homepage.docktor.description.text.text")]
        public string DoctorDescription { get; set; }
        [NopResourceDisplayName("setting.homepage.doctor.image.text")]
        [UIHint("Picture")]
        public int DoctorImageUrl { get; set; }
        [UIHint("Picture")]
        public int Rss1PictureId { get; set; }
        [UIHint("Picture")]
        public int Rss2PictureId { get; set; }
        [UIHint("Picture")]
        public int BannerAboutPageId { get; set; }
        [UIHint("Picture")]
        public int BannerRecruitPageId { get; set; }
        [UIHint("Picture")]
        public int BannerContactPageId { get; set; }
        [UIHint("Picture")]
        public int BannerSpecialistPageId { get; set; }
        [UIHint("Picture")]
        public int BannerDoctorPageId { get; set; }
        [UIHint("Picture")]
        public int BannerServicePageId { get; set; }
        [UIHint("Picture")]
        public int BannerPackagePageId { get; set; }
        [UIHint("Picture")]
        public int BannerUserManualPageId { get; set; }
        [UIHint("Picture")]
        public int BannerNewsPageId { get; set; }
        [UIHint("Picture")]
        public int BannerDefaultId { get; set; }
        [UIHint("Picture")]
        public int HomePageBookingPictureId { get; set; }
        [UIHint("Picture")]
        public int HomePageDoctorPictureId { get; set; }
        [UIHint("Picture")]
        public int HomePageVendorPictureId { get; set; }
        [UIHint("Picture")]
        public int HomePageDoctorMobilePictureId { get; set; }
        [UIHint("Picture")]
        public int HomePageVendorMobilePictureId { get; set; }
        
        #endregion
    }
}