﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Recruit
{
    /// <summary>
    /// Represents a news content model
    /// </summary>
    public partial class ResumeContentModel : BaseNopModel
    {
        #region Ctor

        public ResumeContentModel()
        {
            Items = new ResumeSearchModel();
        }

        #endregion

        #region Properties

        public ResumeSearchModel Items { get; set; }


        #endregion
    }
}
