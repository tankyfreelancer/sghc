﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Recruit
{
    /// <summary>
    /// Represents a news content model
    /// </summary>
    public partial class JobContentModel : BaseNopModel
    {
        #region Ctor

        public JobContentModel()
        {
            Items = new JobSearchModel();
        }

        #endregion

        #region Properties

        public JobSearchModel Items { get; set; }


        #endregion
    }
}
