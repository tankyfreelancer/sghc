﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Recruit
{
    /// <summary>
    /// Represents a news item search model
    /// </summary>
    public partial class RecruitFacultySearchModel : BaseSearchModel
    {
        #region Ctor

        public RecruitFacultySearchModel()
        {
            AvailableCategories = new List<SelectListItem>();
            AvailableStores = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.ContentManagement.News.NewsItems.List.SearchStore")]
        public int SearchStoreId { get; set; }

        public IList<SelectListItem> AvailableStores { get; set; }

        public bool HideStoresList { get; set; }
        [NopResourceDisplayName("Admin.ContentManagement.News.NewsItems.List.SearchName")]
        public string SearchName { get; set; }
        [NopResourceDisplayName("Admin.ContentManagement.News.NewsItems.List.SearchCategoryId")]
        public int? SearchCategoryId { get; set; }

        public IList<SelectListItem> AvailableCategories { get; set; }
        #endregion
    }
}