﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Recruit
{
    /// <summary>
    /// Represents a news content model
    /// </summary>
    public partial class RecruitFacultyContentModel : BaseNopModel
    {
        #region Ctor

        public RecruitFacultyContentModel()
        {
            Items = new RecruitFacultySearchModel();
        }

        #endregion

        #region Properties

        public RecruitFacultySearchModel Items { get; set; }


        #endregion
    }
}
