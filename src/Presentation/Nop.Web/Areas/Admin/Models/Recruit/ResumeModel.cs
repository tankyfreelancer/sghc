﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Recruit
{
    /// <summary>
    /// Represents a news item model
    /// </summary>
    public partial class ResumeModel : BaseNopEntityModel, IStoreMappingSupportedModel
    {
        #region Ctor

        public ResumeModel()
        {
           AvailableLanguages = new List<SelectListItem>();

            SelectedStoreIds = new List<int>();
            AvailableStores = new List<SelectListItem>();

            SelectedCategoryIds = new List<int>();
            AvailableCategories = new List<SelectListItem>(); 
        }

        #endregion

        public string FullName { get; set; }

        public bool Gender { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string TempAddress { get; set; }

        public string Major { get; set; }

        public string College { get; set; }

        public string Expirence { get; set; }

        public string Bio { get; set; }

        public bool Active { get; set; }

        public bool Deleted { get; set; }
        public string AttachFile { get; set; }
        public int? JobId { get; set; }
        public int DisplayOrder { get; set; }

        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public int WardId { get; set; }
        public int TempCityId { get; set; }
        public int TempDistrictId { get; set; }
        public int TempWardId { get; set; }
        public string RecruitFacultyName { get; set; }
        public string JobName { get; set; }
        public string CityName { get; set; }
        public string TempCityName { get; set; }
        public string DistrictName { get; set; }
        public string TempDistrictName { get; set; }
        public string WardName { get; set; }
        public string TempWardName { get; set; }
        public DateTime CreatedOnUtc { get; set; }

        public IList<int> SelectedStoreIds { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
        public IList<SelectListItem> AvailableLanguages { get; set; }
         public IList<int> SelectedCategoryIds { get; set; }
        public IList<SelectListItem> AvailableCategories { get; set; }
    }
}