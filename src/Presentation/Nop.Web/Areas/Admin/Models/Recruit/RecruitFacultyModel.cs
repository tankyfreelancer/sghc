﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Recruit
{
    /// <summary>
    /// Represents a news item model
    /// </summary>
    public partial class RecruitFacultyModel : BaseNopEntityModel, IStoreMappingSupportedModel
    {
        #region Ctor

        public RecruitFacultyModel()
        {
        }

        #endregion


        public string Name { get; set; }

        public string Description { get; set; }

        public int DisplayOrder { get; set; }

        public bool Active { get; set; }

        public bool Deleted { get; set; }

        public DateTime CreatedOnUtc { get; set; }
        public IList<int> SelectedStoreIds { get; set; }

        public IList<SelectListItem> AvailableStores { get; set; }

    }
}