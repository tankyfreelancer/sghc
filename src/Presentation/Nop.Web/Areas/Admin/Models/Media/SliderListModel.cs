﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Media
{
    /// <summary>
    /// Represents a manufacturer list model
    /// </summary>
    public partial class SliderListModel : BasePagedListModel<SliderModel>
    {
    }
}