﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Media
{
    /// <summary>
    /// Represents a slider model
    /// </summary>
    public partial class SliderModel : BaseNopEntityModel
    {
        #region Ctor

        public SliderModel()
        {
            if (PageSize < 1)
            {
                PageSize = 5;
            }
            SliderSearchModel = new SliderSearchModel();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Sliders.Fields.Name")]
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Url { get; set; }
        public bool IsMobile { get; set; }
        public string PictureThumbnailUrl { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sliders.Fields.Description")]
        public string Description { get; set; }
        public string SeName { get; set; }

        [UIHint("Picture")]
        [NopResourceDisplayName("Admin.Catalog.Sliders.Fields.Picture")]
        public int PictureId { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsActive { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sliders.Fields.PageSize")]
        public int PageSize { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sliders.Fields.AllowCustomersToSelectPageSize")]
        public bool AllowCustomersToSelectPageSize { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sliders.Fields.PageSizeOptions")]
        public string PageSizeOptions { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sliders.Fields.PriceRanges")]
        public SliderSearchModel SliderSearchModel { get; set; }

        #endregion
    }
}