﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Media
{
    /// <summary>
    /// Represents a manufacturer product search model
    /// </summary>
    public partial class SliderSearchModel : BaseSearchModel
    {
        #region Properties
        public int Id { get; set; }
        public bool IsMobile { get; set; }
        public string Title { get; set; }
        #endregion
    }
}