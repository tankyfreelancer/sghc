﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Stores
{
    /// <summary>
    /// Represents a Branch model
    /// </summary>
public partial class BranchModel : BaseNopEntityModel
    {
        public BranchModel()
        {
        }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Hotline { get; set; }
        public string WorkTime { get; set; }
        public bool HeadOffice { get; set; }

    }
}