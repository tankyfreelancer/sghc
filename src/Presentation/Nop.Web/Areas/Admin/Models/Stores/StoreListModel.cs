﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Stores
{
    /// <summary>
    /// Represents a store list model
    /// </summary>
    public partial class StoreListModel : BasePagedListModel<StoreModel>
    {
    }
      public partial class BranchListModel : BasePagedListModel<BranchModel>
    {
    }
}