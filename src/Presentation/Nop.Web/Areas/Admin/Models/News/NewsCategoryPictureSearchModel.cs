﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.News
{
    /// <summary>
    /// Represents a news picture search model
    /// </summary>
    public partial class NewsCategoryPictureSearchModel : BaseSearchModel
    {
        #region Properties

        public int NewsCategoryId { get; set; }
        
        #endregion
    }
}