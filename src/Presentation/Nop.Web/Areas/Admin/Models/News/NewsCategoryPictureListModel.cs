﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.News
{
    /// <summary>
    /// Represents a news picture list model
    /// </summary>
    public partial class NewsCategoryPictureListModel : BasePagedListModel<NewsCategoryPictureModel>
    {
    }
}