﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Home
{
    /// <summary>
    /// Represents a project news model
    /// </summary>
    public partial class ProjectNewsModel : BaseNopModel
    {
        #region Ctor

        public ProjectNewsModel()
        {
            Items = new List<ProjectNewsDetailsModel>();
        }

        #endregion

        #region Properties

        public List<ProjectNewsDetailsModel> Items { get; set; }

        public bool HasNewItems { get; set; }

        public bool HideAdvertisements { get; set; }

        #endregion
    }
}