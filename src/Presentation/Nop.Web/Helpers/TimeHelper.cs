using System;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using Nop.Core.Domain.Customers;
using System.Collections.Generic;

namespace Nop.Web.Helpers
{
    public static class TimeHelper
    {
        public static DateTime? ConvertTime(string time)
        {
            if (string.IsNullOrEmpty(time))
            {
                return null;
            }
            var isDateTime = DateTime.TryParse(time,out DateTime dateTime);
            if (!isDateTime){
                // try parse dummies format "202005261300"
                if (time.Length != "202005261300".Length){
                    return null;
                }
                var year = int.Parse(time.Substring(0,4));
                var month = int.Parse(time.Substring(4,2));
                var day = int.Parse(time.Substring(6,2));
                var hour = int.Parse(time.Substring(8,2));
                var min = int.Parse(time.Substring(10,2));
                return new DateTime(year,month,day,hour,min,0);
            }
            return dateTime;
        }
         public static TimeSpan ConvertTimeSpan(string time)
        {
            if (string.IsNullOrEmpty(time))
            {
                return new TimeSpan();
            }
            var isDateTime = TimeSpan.TryParse(time,out TimeSpan timeSpan);
            if (!isDateTime){
                // try parse dummies format
                if (time.Length != "13:00".Length){
                    return new TimeSpan();
                }
                var hour = int.Parse(time.Substring(0,2));
                var min = int.Parse(time.Substring(3,2));
                return new TimeSpan(hour,min,0);
            }
            return timeSpan;
        }
        public static bool IsMatch(DateTime thatDay, string startTime, string endTime)
        {
            var start = ConvertTime(startTime);
            var end = ConvertTime(endTime);
            if (start == null || end == null)
            {
                return false;
            }
            var startDate = start.Value;
            var endDate = end.Value;
            if(    thatDay.Year != startDate.Year 
                || thatDay.Month != startDate.Month 
                || thatDay.Day != startDate.Day 
                // || thatDay.Year != endDate.Year 
                // || thatDay.Month != endDate.Month 
                // || thatDay.Day != endDate.Day 
            ){
                return false;
            }
            return true;
        }
    }
}