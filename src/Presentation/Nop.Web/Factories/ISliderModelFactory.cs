﻿using System.Collections.Generic;
using Nop.Core.Domain.Media;
using Nop.Web.Areas.Admin.Models.Media;

namespace Nop.Web.Factories
{
    /// <summary>
    /// Represents the slider model factory
    /// </summary>
    public partial interface ISliderModelFactory
    {
        /// <summary>
        /// Prepare slider search model
        /// </summary>
        /// <param name="searchModel">Slider search model</param>
        /// <returns>Slider search model</returns>
        SliderSearchModel PrepareSliderSearchModel(SliderSearchModel searchModel);

        /// <summary>
        /// Prepare paged slider list model
        /// </summary>
        /// <param name="searchModel">Slider search model</param>
        /// <returns>Slider list model</returns>
        List<SliderModel> PrepareSliderListModel(SliderSearchModel searchModel);

        /// <summary>
        /// Prepare slider model
        /// </summary>
        /// <param name="model">Slider model</param>
        /// <param name="slider">Slider</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Slider model</returns>
        SliderModel PrepareSliderModel(SliderModel model,
            Slider slider, bool excludeProperties = false);

        /// <summary>
        /// Prepare paged slider product list model
        /// </summary>
        /// <param name="searchModel">Slider product search model</param>
        /// <param name="slider">Slider</param>
        /// <returns>Slider product list model</returns>
    }
}