﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.Media;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Forums;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Framework.Extensions;
using Nop.Web.Models.Common;
using Nop.Web.Models.Customer;
using Nop.Web.Models.Profile;

namespace Nop.Web.Factories
{
    /// <summary>
    /// Represents the profile model factory
    /// </summary>
    public partial class ProfileModelFactory : IProfileModelFactory
    {
        #region Fields

        private readonly CustomerSettings _customerSettings;
        private readonly ForumSettings _forumSettings;
        private readonly ICountryService _countryService;
        private readonly ICustomerService _customerService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IForumService _forumService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IPictureService _pictureService;
        private readonly IWorkContext _workContext;
        private readonly MediaSettings _mediaSettings;
        private readonly HIS.IBusinessService _hisBusinessService;
        private readonly IPatientService _patientService;
        #endregion

        #region Ctor

        public ProfileModelFactory(CustomerSettings customerSettings,
            ForumSettings forumSettings,
            ICountryService countryService,
            ICustomerService customerService,
            IDateTimeHelper dateTimeHelper,
            IForumService forumService,
            IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            IWorkContext workContext,
            IPatientService patientService,
            HIS.IBusinessService hisBusinessService,
            MediaSettings mediaSettings)
        {
            _customerSettings = customerSettings;
            _forumSettings = forumSettings;
            _countryService = countryService;
            _customerService = customerService;
            _dateTimeHelper = dateTimeHelper;
            _forumService = forumService;
            _genericAttributeService = genericAttributeService;
            _localizationService = localizationService;
            _pictureService = pictureService;
            _workContext = workContext;
            _mediaSettings = mediaSettings;
            _patientService = patientService;
            _hisBusinessService = hisBusinessService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare the profile index model
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <param name="page">Number of posts page; pass null to disable paging</param>
        /// <returns>Profile index model</returns>
        public virtual ProfileIndexModel PrepareProfileIndexModel(Customer customer, int? page)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            var pagingPosts = false;
            var postsPage = 0;

            if (page.HasValue)
            {
                postsPage = page.Value;
                pagingPosts = true;
            }

            var name = _customerService.FormatUsername(customer);
            var title = string.Format(_localizationService.GetResource("Profile.ProfileOf"), name);

            var model = new ProfileIndexModel
            {
                ProfileTitle = title,
                PostsPage = postsPage,
                PagingPosts = pagingPosts,
                CustomerProfileId = customer.Id,
                ForumsEnabled = _forumSettings.ForumsEnabled,
                Customer = new CustomerInfoModel
                {
                    Email = _workContext.CurrentCustomer.Email,
                },
                CustomerInfo = new CustomerInfo
                {
                    Email = customer.Email,
                    Phone = customer.PhoneNumber,
                }
            };

            var patients = _patientService.GetByCustomer(_workContext.CurrentCustomer.Id);
            if (patients.Any())
            {
                var patient = patients.FirstOrDefault();
                model.Customer.Gender = patient.Sex == 1 ? "Nữ" : "Nam";
                model.Customer.FirstName = patient.Name;
                model.Customer.Phone = _workContext.CurrentCustomer.PhoneNumber;
                DateTime.TryParse(patient.BirthDate, out DateTime date);
                model.Customer.DateOfBirthDay = date.Day;
                model.Customer.DateOfBirthMonth = date.Month;
                model.Customer.DateOfBirthYear = date.Year;

                model.Patient = new PatientModel
                {
                    CityId = patient.CityId.ToString(),
                    Address = patient.Address,
                    Birthdate = patient.BirthDate,
                    Birthyear = patient.BirthYear,
                    CountryCode = patient.CountryCode,
                    Day = patient.BirthDate,
                    DistrictId = patient.DistrictId.ToString(),
                    Email = patient.Email,
                    HisCityId = patient.CityId.ToString(),
                    HisDistrictId = patient.DistrictId.ToString(),
                    HisWardId = patient.WardId.ToString(),
                    HisId = patient.HisId,
                    Id = patient.Id,
                    Insurance = patient.BHYTCode,
                    Mobile = patient.Mobile,
                    Passport = patient.Passport,
                    PatientId = patient.HisId,
                    Sex = patient.Sex,
                    SocialId = patient.Passport,
                    Name = patient.Name,
                    Surname = patient.SurName,
                };

                char[] delimiters = new char[] { ' ', '\r', '\n' };
                var popularSurName = new String[] { "Nguyễn", "Trần", "Lê", "Phạm", "Hoàng", "Vũ", "Phan", "Trương", "Bùi", "Đặng", "Đỗ", "Ngô", "Hồ", "Dương", "Đinh" };
                model.CustomerInfo.FirstName = patient.Name;
                model.CustomerInfo.LastName = patient.SurName;
                if (String.IsNullOrEmpty(patient.Name) && String.IsNullOrEmpty(patient.SurName))
                {
                    model.CustomerInfo.FirstName = "Chưa Đặt";
                    model.CustomerInfo.LastName = "Tên";
                }
                else
                {
                    if (String.IsNullOrEmpty(patient.Name))
                    {
                        model.CustomerInfo.FirstName = patient.SurName;
                    }
                    if (String.IsNullOrEmpty(patient.SurName))
                    {
                        model.CustomerInfo.LastName = patient.Name;
                    }
                    var firstNameLength = model.CustomerInfo.FirstName.Split(delimiters, StringSplitOptions.RemoveEmptyEntries).Length;
                    var lastNameLength = model.CustomerInfo.LastName.Split(delimiters, StringSplitOptions.RemoveEmptyEntries).Length;
                    if ((lastNameLength > 1 && firstNameLength == 1) || popularSurName.Contains(model.CustomerInfo.LastName))
                    {
                        var temp = model.CustomerInfo.FirstName;
                        model.CustomerInfo.FirstName = model.CustomerInfo.LastName;
                        model.CustomerInfo.LastName = temp;
                    }
                }

                model.CustomerInfo.Sex = patient.Sex == 1;
                model.CustomerInfo.DateOfBirthDay = patient.Day.GetValueOrDefault();
                model.CustomerInfo.DateOfBirthMonth = patient.Month.GetValueOrDefault();
                model.CustomerInfo.DateOfBirthYear = patient.Year.GetValueOrDefault();
                model.CustomerInfo.CityId = patient.CityId.GetValueOrDefault();
                model.CustomerInfo.DistrictId = patient.DistrictId.GetValueOrDefault();
                model.CustomerInfo.WardId = patient.WardId.GetValueOrDefault();
                model.CustomerInfo.Address = patient.Address;
            }
            // get billing address
            var address = _customerService.GetCustomerBillingAddress(_workContext.CurrentCustomer);
            if (address != null)
            {
                model.Address = address.ToModel<AddressModel>();
            }

            return model;
        }

        /// <summary>
        /// Prepare the profile info model
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <returns>Profile info model</returns>
        public virtual ProfileInfoModel PrepareProfileInfoModel(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            //avatar
            var avatarUrl = "";
            if (_customerSettings.AllowCustomersToUploadAvatars)
            {
                avatarUrl = _pictureService.GetPictureUrl(
                 _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.AvatarPictureIdAttribute),
                 _mediaSettings.AvatarPictureSize,
                 _customerSettings.DefaultAvatarEnabled,
                 defaultPictureType: PictureType.Avatar);
            }

            //location
            var locationEnabled = false;
            var location = string.Empty;
            if (_customerSettings.ShowCustomersLocation)
            {
                locationEnabled = true;

                var countryId = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.CountryIdAttribute);
                var country = _countryService.GetCountryById(countryId);
                if (country != null)
                {
                    location = _localizationService.GetLocalized(country, x => x.Name);
                }
                else
                {
                    locationEnabled = false;
                }
            }

            //private message
            var pmEnabled = _forumSettings.AllowPrivateMessages && !_customerService.IsGuest(customer);

            //total forum posts
            var totalPostsEnabled = false;
            var totalPosts = 0;
            if (_forumSettings.ForumsEnabled && _forumSettings.ShowCustomersPostCount)
            {
                totalPostsEnabled = true;
                totalPosts = _genericAttributeService.GetAttribute<int>(customer, NopCustomerDefaults.ForumPostCountAttribute);
            }

            //registration date
            var joinDateEnabled = false;
            var joinDate = string.Empty;

            if (_customerSettings.ShowCustomersJoinDate)
            {
                joinDateEnabled = true;
                joinDate = _dateTimeHelper.ConvertToUserTime(customer.CreatedOnUtc, DateTimeKind.Utc).ToString("f");
            }

            //birth date
            var dateOfBirthEnabled = false;
            var dateOfBirth = string.Empty;
            if (_customerSettings.DateOfBirthEnabled)
            {
                var dob = _genericAttributeService.GetAttribute<DateTime?>(customer, NopCustomerDefaults.DateOfBirthAttribute);
                if (dob.HasValue)
                {
                    dateOfBirthEnabled = true;
                    dateOfBirth = dob.Value.ToString("D");
                }
            }

            var model = new ProfileInfoModel
            {
                CustomerProfileId = customer.Id,
                AvatarUrl = avatarUrl,
                LocationEnabled = locationEnabled,
                Location = location,
                PMEnabled = pmEnabled,
                TotalPostsEnabled = totalPostsEnabled,
                TotalPosts = totalPosts.ToString(),
                JoinDateEnabled = joinDateEnabled,
                JoinDate = joinDate,
                DateOfBirthEnabled = dateOfBirthEnabled,
                DateOfBirth = dateOfBirth,
                IsAuthenticated = true,
            };

            return model;
        }

        /// <summary>
        /// Prepare the profile posts model
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <param name="page">Number of posts page</param>
        /// <returns>Profile posts model</returns>
        public virtual ProfilePostsModel PrepareProfilePostsModel(Customer customer, int page)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            if (page > 0)
            {
                page -= 1;
            }

            var pageSize = _forumSettings.LatestCustomerPostsPageSize;

            var list = _forumService.GetAllPosts(0, customer.Id, string.Empty, false, page, pageSize);

            var latestPosts = new List<PostsModel>();

            foreach (var forumPost in list)
            {
                var posted = string.Empty;
                if (_forumSettings.RelativeDateTimeFormattingEnabled)
                {
                    var languageCode = _workContext.WorkingLanguage.LanguageCulture;
                    var postedAgo = forumPost.CreatedOnUtc.RelativeFormat(languageCode);
                    posted = string.Format(_localizationService.GetResource("Common.RelativeDateTime.Past"), postedAgo);
                }
                else
                {
                    posted = _dateTimeHelper.ConvertToUserTime(forumPost.CreatedOnUtc, DateTimeKind.Utc).ToString("f");
                }

                var topic = _forumService.GetTopicById(forumPost.TopicId);

                latestPosts.Add(new PostsModel
                {
                    ForumTopicId = topic.Id,
                    ForumTopicTitle = topic.Subject,
                    ForumTopicSlug = _forumService.GetTopicSeName(topic),
                    ForumPostText = _forumService.FormatPostText(forumPost),
                    Posted = posted
                });
            }

            var pagerModel = new PagerModel
            {
                PageSize = list.PageSize,
                TotalRecords = list.TotalCount,
                PageIndex = list.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "CustomerProfilePaged",
                UseRouteLinks = true,
                RouteValues = new RouteValues { pageNumber = page, id = customer.Id }
            };

            var model = new ProfilePostsModel
            {
                PagerModel = pagerModel,
                Posts = latestPosts,
            };

            return model;
        }

        #endregion
    }
}