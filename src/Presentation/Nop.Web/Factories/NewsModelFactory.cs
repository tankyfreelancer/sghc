﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.News;
using Nop.Core.Domain.Security;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.News;
using Nop.Services.Seo;
using Nop.Web.Infrastructure.Cache;
using Nop.Web.Models.Media;
using Nop.Web.Models.News;

namespace Nop.Web.Factories
{
    /// <summary>
    /// Represents the news model factory
    /// </summary>
    public partial class NewsModelFactory : INewsModelFactory
    {
        #region Fields

        private readonly CaptchaSettings _captchaSettings;
        private readonly CustomerSettings _customerSettings;
        private readonly ICustomerService _customerService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly INewsService _newsService;
        private readonly IPictureService _pictureService;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IStoreContext _storeContext;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IWorkContext _workContext;
        private readonly MediaSettings _mediaSettings;
        private readonly NewsSettings _newsSettings;
        private readonly ILocalizationService _localizationService;
        private readonly IWebHelper _webHelper;

        #endregion

        #region Ctor

        public NewsModelFactory(CaptchaSettings captchaSettings,
            CustomerSettings customerSettings,
            ICustomerService customerService,
            IDateTimeHelper dateTimeHelper,
            IGenericAttributeService genericAttributeService,
            INewsService newsService,
            IPictureService pictureService,
            IStaticCacheManager cacheManager,
            IStoreContext storeContext,
            IUrlRecordService urlRecordService,
            IWorkContext workContext,
            MediaSettings mediaSettings,
            ILocalizationService localizationService,
            IWebHelper webHelper,
            NewsSettings newsSettings)
        {
            _captchaSettings = captchaSettings;
            _customerSettings = customerSettings;
            _customerService = customerService;
            _dateTimeHelper = dateTimeHelper;
            _genericAttributeService = genericAttributeService;
            _newsService = newsService;
            _pictureService = pictureService;
            _cacheManager = cacheManager;
            _storeContext = storeContext;
            _urlRecordService = urlRecordService;
            _workContext = workContext;
            _mediaSettings = mediaSettings;
            _newsSettings = newsSettings;
            _localizationService = localizationService;
            _webHelper = webHelper;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare the news comment model
        /// </summary>
        /// <param name="newsComment">News comment</param>
        /// <returns>News comment model</returns>
        public virtual NewsCommentModel PrepareNewsCommentModel(NewsComment newsComment)
        {
            if (newsComment == null)
                throw new ArgumentNullException(nameof(newsComment));

            var customer = _customerService.GetCustomerById(newsComment.CustomerId);

            var model = new NewsCommentModel
            {
                Id = newsComment.Id,
                CustomerId = newsComment.CustomerId,
                CustomerName = _customerService.FormatUsername(customer),
                CommentTitle = newsComment.CommentTitle,
                CommentText = newsComment.CommentText,
                CreatedOn = _dateTimeHelper.ConvertToUserTime(newsComment.CreatedOnUtc, DateTimeKind.Utc),
                AllowViewingProfiles = _customerSettings.AllowViewingProfiles && newsComment.CustomerId != 0 && !_customerService.IsGuest(customer),
            };

            if (_customerSettings.AllowCustomersToUploadAvatars)
            {
                model.CustomerAvatarUrl = _pictureService.GetPictureUrl(
                    _genericAttributeService.GetAttribute<Customer, int>(newsComment.CustomerId, NopCustomerDefaults.AvatarPictureIdAttribute),
                    _mediaSettings.AvatarPictureSize, _customerSettings.DefaultAvatarEnabled, defaultPictureType: PictureType.Avatar);
            }

            return model;
        }

        /// <summary>
        /// Prepare the news item model
        /// </summary>
        /// <param name="model">News item model</param>
        /// <param name="newsItem">News item</param>
        /// <param name="prepareComments">Whether to prepare news comment models</param>
        /// <returns>News item model</returns>
        public virtual NewsItemModel PrepareNewsItemModel(NewsItemModel model, NewsItem newsItem, bool prepareComments)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            if (newsItem == null)
                throw new ArgumentNullException(nameof(newsItem));

            model.Id = newsItem.Id;
            model.MetaTitle = newsItem.MetaTitle;
            model.MetaDescription = newsItem.MetaDescription;
            model.MetaKeywords = newsItem.MetaKeywords;
            model.SeName = _urlRecordService.GetSeName(newsItem, newsItem.LanguageId, ensureTwoPublishedLanguages: false);
            model.Title = newsItem.Title;
            model.Short = newsItem.Short;
            model.Full = newsItem.Full;
            model.AllowComments = newsItem.AllowComments;
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(newsItem.StartDateUtc ?? newsItem.CreatedOnUtc, DateTimeKind.Utc);
            model.AddNewComment.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnNewsCommentPage;
            model.Tags = newsItem.Tags;
            model.IncludeInTopMenu = newsItem.IncludeInTopMenu;
            model.ShowOnHomePage = newsItem.ShowOnHomePage;
            model.VideoUrl = newsItem.VideoUrl;
            model.DefaultPictureModel = PreparePictureModel(newsItem);

            //number of news comments
            var storeId = _newsSettings.ShowNewsCommentsPerStore ? _storeContext.CurrentStore.Id : 0;

            model.NumberOfComments = _newsService.GetNewsCommentsCount(newsItem, storeId, true);

            if (prepareComments)
            {
                var newsComments = _newsService.GetAllComments(
                    newsItemId: newsItem.Id,
                    approved: true,
                    storeId: _newsSettings.ShowNewsCommentsPerStore ? _storeContext.CurrentStore.Id : 0);

                foreach (var nc in newsComments.OrderBy(comment => comment.CreatedOnUtc))
                {
                    var commentModel = PrepareNewsCommentModel(nc);
                    model.Comments.Add(commentModel);
                }
            }
            // category
            model.Category.Name = "Tin tức";
            model.Category.SeName = "tin-tuc";
            var categoryMap = _newsService.GetByNewsId(newsItem.Id);
            if (categoryMap != null)
            {
                var ids = categoryMap.Select(x => x.CategoryId).ToArray();
                var categories = _newsService.GetNewsCategoryByIds(ids);
                if (categories != null && categories.Any())
                {
                    var category = categories.First();
                    model.Category.Name = category.Title;
                    model.Category.SeName = _urlRecordService.GetSeName(category, category.LanguageId, true, false);
                    model.Category.Route = category.Short;
                }
                //related news

                var relatedNews = _newsService.GetRelatedNews(newsItem.Id);
                model.NewsRelated = relatedNews.Select(x => new NewsRelated
                {
                    Title = x.Title,
                    CreatedOn = x.CreatedOnUtc,
                    MetaDescription = x.MetaDescription,
                    MetaKeywords = x.MetaKeywords,
                    MetaTitle = x.MetaTitle,
                    SeName = _urlRecordService.GetSeName(x, x.LanguageId, true, false),
                    Short = x.Short,
                    DefaultPictureModel = PreparePictureModel(x)
                }).ToList();
            }
            return model;
        }

        /// <summary>
        /// Prepare the home page news items model
        /// </summary>
        /// <returns>Home page news items model</returns>
        public virtual HomepageNewsItemsModel PrepareHomepageNewsItemsModel(bool showOnHomePage = true)
        {
            var cacheKey = NopModelCacheDefaults.HomepageNewsModelKey.FillCacheKey(_workContext.WorkingLanguage.Id, _storeContext.CurrentStore.Id);
            var cachedModel = _cacheManager.Get(cacheKey, () =>
            {
                var news = _newsService.GetAllNews(_workContext.WorkingLanguage.Id, _storeContext.CurrentStore.Id, 0, 100, showOnHomePage: showOnHomePage);

                // get truyen-thong 
                var commNews = _newsService.GetNewsByMenuSeName("truyen-thong");
                // get goi-kham
                var packNews = _newsService.GetNewsByMenuSeName("goi-kham");

                var newsHomepage = news.Where(x => commNews.All(a => a.Id != x.Id) && packNews.All(a => a.Id != x.Id));
                var hotNews = new NewsItemModel();
                if (newsHomepage.Any())
                {
                    PrepareNewsItemModel(hotNews, newsHomepage.FirstOrDefault(), false);
                    newsHomepage = newsHomepage.Skip(1).ToList();
                }
                var popupNews = new NewsItemModel();
                var popups = _newsService.GetNewsByMenuSeName("popup");
                if (popups.Any())
                {
                    // get current 
                    var popup = popups.FirstOrDefault(b =>
                    (!b.StartDateUtc.HasValue || b.StartDateUtc <= DateTime.UtcNow) &&
                    (!b.EndDateUtc.HasValue || b.EndDateUtc >= DateTime.UtcNow) &&
                    b.Published
                    );
                    if (popup != null)
                    {
                        popupNews = PrepareNewsItemModel(popupNews, popup, false);
                    }
                }
                return new HomepageNewsItemsModel
                {
                    WorkingLanguageId = _workContext.WorkingLanguage.Id,
                    HotNews = hotNews,
                    PopupNews = popupNews,
                    NewsItems = newsHomepage.Select(x =>
                        {
                            var newsModel = new NewsItemModel();
                            PrepareNewsItemModel(newsModel, x, false);
                            if (!string.IsNullOrEmpty(x.Short) && x.Short.Length > 210)
                            {
                                newsModel.Short = x.Short.Substring(0, x.Short.Substring(0, 200).LastIndexOf(' ')) + "...";
                            }
                            if (!string.IsNullOrEmpty(x.Title) && x.Title.Length > 90)
                            {
                                newsModel.Title = x.Title.Substring(0, x.Title.Substring(0, 80).LastIndexOf(' ')) + "...";
                            }
                            return newsModel;
                        }).OrderByDescending(x => x.CreatedOn)
                    .Take(10)
                    .ToList(),
                    Communications = commNews.Select(x =>
                        {
                            var newsModel = new NewsItemModel();
                            PrepareNewsItemModel(newsModel, x, false);
                            if (!string.IsNullOrEmpty(x.Short) && x.Short.Length > 210)
                            {
                                newsModel.Short = x.Short.Substring(0, x.Short.Substring(0, 200).LastIndexOf(' ')) + "...";
                            }
                            if (!string.IsNullOrEmpty(x.Title) && x.Title.Length > 90)
                            {
                                newsModel.Title = x.Title.Substring(0, x.Title.Substring(0, 80).LastIndexOf(' ')) + "...";
                            }
                            return newsModel;
                        }).OrderByDescending(x => x.CreatedOn)
                    .Take(10)
                    .ToList(),
                    Packages = packNews.Select(x =>
                        {
                            var newsModel = new NewsItemModel();
                            PrepareNewsItemModel(newsModel, x, false);
                            if (!string.IsNullOrEmpty(x.Short) && x.Short.Length > 210)
                            {
                                newsModel.Short = x.Short.Substring(0, x.Short.Substring(0, 200).LastIndexOf(' ')) + "...";
                            }
                            if (!string.IsNullOrEmpty(x.Title) && x.Title.Length > 90)
                            {
                                newsModel.Title = x.Title.Substring(0, x.Title.Substring(0, 80).LastIndexOf(' ')) + "...";
                            }
                            return newsModel;
                        }).OrderByDescending(x => x.CreatedOn)
                    .Take(10)
                    .ToList(),
                    NewsCategories = new List<NewsCategoryModel>()
                };
            });

            //"Comments" property of "NewsItemModel" object depends on the current customer.
            //Furthermore, we just don't need it for home page news. So let's reset it.
            //But first we need to clone the cached model (the updated one should not be cached)
            var model = (HomepageNewsItemsModel)cachedModel.Clone();
            foreach (var newsItemModel in model.NewsItems)
                newsItemModel.Comments.Clear();

            return model;
        }

        /// <summary>
        /// Prepare the news item list model
        /// </summary>
        /// <param name="command">News paging filtering model</param>
        /// <returns>News item list model</returns>
        public virtual NewsItemListModel PrepareNewsItemListModel(NewsPagingFilteringModel command)
        {
            var model = new NewsItemListModel
            {
                WorkingLanguageId = _workContext.WorkingLanguage.Id
            };

            if (command.PageSize <= 0)
                command.PageSize = _newsSettings.NewsArchivePageSize;
            if (command.PageNumber <= 0)
                command.PageNumber = 1;

            var newsItems = _newsService.GetAllNews(_workContext.WorkingLanguage.Id, _storeContext.CurrentStore.Id,
                command.PageNumber - 1, command.PageSize, false, command.IsNews, command.NewsCategoryId, command.Search);
            model.PagingFilteringContext.LoadPagedList(newsItems);

            model.NewsItems = newsItems
                .Select(x =>
                {
                    var newsModel = new NewsItemModel();
                    PrepareNewsItemModel(newsModel, x, false);
                    newsModel.DefaultPictureModel = PreparePictureModel(x);
                    return newsModel;
                }).ToList();

            return model;
        }
        /// <summary>
        /// Prepare the product overview picture model
        /// </summary>
        /// <param name="news">Product</param>
        /// <param name="thumbPictureSize">Product thumb picture size (longest side); pass null to use the default value of media settings</param>
        /// <returns>Picture model</returns>
        public virtual PictureModel PreparePictureModel(NewsItem news, int? thumbPictureSize = null)
        {
            if (news == null)
                throw new ArgumentNullException(nameof(news));

            var name = _localizationService.GetLocalized(news, x => x.Title);
            //If a size has been set in the view, we use it in priority
            var pictureSize = thumbPictureSize ?? _mediaSettings.ProductThumbPictureSize;

            //prepare picture model
            var cacheKey = NopModelCacheDefaults.NewsDefaultPictureModelKey.FillCacheKey(
                news.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(),
                _storeContext.CurrentStore.Id);

            var defaultPictureModel = _cacheManager.Get(cacheKey, () =>
            {
                var picture = _pictureService.GetPicturesByNewsId(news.Id, 1).FirstOrDefault();
                var pictureModel = new PictureModel
                {
                    ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                    FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                    //"title" attribute
                    Title = (picture != null && !string.IsNullOrEmpty(picture.TitleAttribute))
                        ? picture.TitleAttribute
                        : string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"),
                            name),
                    //"alt" attribute
                    AlternateText = (picture != null && !string.IsNullOrEmpty(picture.AltAttribute))
                        ? picture.AltAttribute
                        : string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"),
                            name)
                };

                return pictureModel;
            });

            return defaultPictureModel;
        }
        public virtual PictureModel PreparePictureModel(NewsCategory news, int? thumbPictureSize = null)
        {
            if (news == null)
                throw new ArgumentNullException(nameof(news));

            var name = _localizationService.GetLocalized(news, x => x.Title);
            //If a size has been set in the view, we use it in priority
            var pictureSize = thumbPictureSize ?? _mediaSettings.ProductThumbPictureSize;

            //prepare picture model
            var cacheKey = NopModelCacheDefaults.NewsCategoryDefaultPictureModelKey.FillCacheKey(
                news.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(),
                _storeContext.CurrentStore.Id);

            var defaultPictureModel = _cacheManager.Get(cacheKey, () =>
            {
                var picture = _pictureService.GetPicturesByNewsCategoryId(news.Id, 1).FirstOrDefault();
                var pictureModel = new PictureModel
                {
                    ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                    FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                    //"title" attribute
                    Title = (picture != null && !string.IsNullOrEmpty(picture.TitleAttribute))
                        ? picture.TitleAttribute
                        : string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"),
                            name),
                    //"alt" attribute
                    AlternateText = (picture != null && !string.IsNullOrEmpty(picture.AltAttribute))
                        ? picture.AltAttribute
                        : string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"),
                            name)
                };

                return pictureModel;
            });

            return defaultPictureModel;
        }
        #endregion
        public virtual NewsCategoryListModel PrepareNewsCategoryListModel(NewsPagingFilteringModel command, string menuSeName = null)
        {
            var model = new NewsCategoryListModel
            {
                WorkingLanguageId = _workContext.WorkingLanguage.Id
            };

            if (command.PageSize <= 0)
                command.PageSize = _newsSettings.NewsArchivePageSize;
            if (command.PageNumber <= 0)
                command.PageNumber = 1;
            IPagedList<NewsCategory> newsCategories;
            if (!String.IsNullOrEmpty(menuSeName))
            {
                newsCategories = _newsService.GetNewsCategoriesChildByMenuSeName(menuSeName, pageIndex: command.PageIndex, pageSize: command.PageSize);
            }
            else
            {
                newsCategories = _newsService.GetAllNewsCategory();
            }

            model.PagingFilteringContext.LoadPagedList(newsCategories);

            model.Items = newsCategories
                .Select(x =>
                {
                    var newsModel = new NewsCategorySummaryModel(x);
                    newsModel.SeName = _urlRecordService.GetSeName(x, newsModel.LanguageId, ensureTwoPublishedLanguages: false);
                    newsModel.DefaultPictureModel = PreparePictureModel(x);
                    return newsModel;
                }).ToList();

            return model;
        }
    }
}