﻿using Nop.Core.Domain.Recruit;
using Nop.Web.Areas.Admin.Models.Recruit;

namespace Nop.Web.Factories
{
    /// <summary>
    /// Represents the interface of the recruitFaculty model factory
    /// </summary>
    public partial interface IRecruitFacultyModelFactory
    {
        /// <summary>
        /// Prepare the recruitFaculty item model
        /// </summary>
        /// <param name="model">RecruitFaculty item model</param>
        /// <param name="recruitFaculty">RecruitFaculty item</param>
        /// <returns>RecruitFaculty item model</returns>
        RecruitFacultyModel PrepareRecruitFacultyModel(RecruitFacultyModel model, RecruitFaculty recruitFaculty);


        /// <summary>
        /// Prepare the recruitFaculty item list model
        /// </summary>
        /// <param name="command">RecruitFaculty paging filtering model</param>
        /// <returns>RecruitFaculty item list model</returns>

        // TODO:
        // RecruitFacultyListModel PrepareRecruitFacultyListModel(RecruitFacultyPagingFilteringModel command);
    }
}
