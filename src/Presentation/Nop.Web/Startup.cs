﻿using System.Net;
using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Nop.Core.Configuration;
using Nop.Core.Infrastructure;
using Nop.Web.Framework.Infrastructure.Extensions;

namespace Nop.Web
{
    /// <summary>
    /// Represents startup class of application
    /// </summary>
    public class Startup
    {
        #region Fields

        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private IEngine _engine;
        private NopConfig _nopConfig;

        #endregion

        #region Ctor

        public Startup(IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        {
            _configuration = configuration;
            _webHostEnvironment = webHostEnvironment;
        }

        #endregion

        /// <summary>
        /// Add services to the application and configure service provider
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        public void ConfigureServices(IServiceCollection services)
        {
            (_engine, _nopConfig) = services.ConfigureApplicationServices(_configuration, _webHostEnvironment);
            services.Configure<CookiePolicyOptions>(options =>
           {
               //    options.HttpOnly = HttpOnlyPolicy.Always;
               options.CheckConsentNeeded = context => true;
               options.Secure = CookieSecurePolicy.Always;
               options.MinimumSameSitePolicy = SameSiteMode.None;
               options.OnAppendCookie = cookieContext =>
                   CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
               options.OnDeleteCookie = cookieContext =>
                   CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
           });
             services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.All;
                // options.KnownNetworks.Add(new IPNetwork(IPAddress.Parse("127.0.0.1"), 24));
            });
            // services.AddAuthentication()
            // .AddFacebook(facebookOptions =>           
            // {
            //     facebookOptions.AppId = "1066507640823462";
            //     facebookOptions.AppSecret = "ed83e0230c1ecc5f983f63b8a8b4177f";
            // })
            //      .AddGoogle(options =>
            // {
            //     options.ClientId = "401073259157-0b0j7n4oeo9e2rcf269d0qkrqlt8aomt.apps.googleusercontent.com";
            //     options.ClientSecret = "1KiHI0Q3aa1StCul-q3TXTgD";
            // });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            _engine.RegisterDependencies(builder, _nopConfig);
        }

        /// <summary>
        /// Configure the application HTTP request pipeline
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        public void Configure(IApplicationBuilder application)
        {
            application.ConfigureRequestPipeline();

            application.UseMiddleware(typeof(VisitorCounterMiddleware));

            application.StartEngine();
            application.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404)
                {
                    context.Request.Path = "/";
                    await next();
                }
            });
            // application.UseCookiePolicy();
            application.UseHttpsRedirection();
        }
        private void CheckSameSite(HttpContext httpContext, CookieOptions options)
        {
            if (options.SameSite == SameSiteMode.None)
            {
                var userAgent = httpContext.Request.Headers["User-Agent"].ToString();
                if (SameSite.BrowserDetection.DisallowsSameSiteNone(userAgent))
                {
                    options.SameSite = SameSiteMode.Unspecified;
                }
            }
        }
    }
}