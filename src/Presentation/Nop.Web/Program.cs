﻿using System.Security.Cryptography.X509Certificates;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Nop.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        .UseIISIntegration()
                        .UseStartup<Startup>();

                    // use cert from args
                    // webBuilder.UseKestrel(options =>
                    // {
                    //      // HTTPS 443
                    //     options.ListenAnyIP(443, builder =>
                    //     {
                    //         builder.UseHttps(cert);
                    //     });
                    // });
                });
        }
    }
}
