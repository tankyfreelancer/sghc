﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Vendors;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Factories;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Catalog;
using System.Threading.Tasks;
using Nop.Web.Models.Common;
using Nop.Services.Customers;
using Nop.Core.Domain.Common;
using Nop.Services.Media;
using System;

namespace Nop.Web.Controllers
{
    public partial class DoctorController : BasePublicController
    {
        #region Fields

        private readonly CatalogSettings _catalogSettings;
        private readonly IAclService _aclService;
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly ICategoryService _categoryService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IPermissionService _permissionService;
        private readonly IProductModelFactory _productModelFactory;
        private readonly IProductService _productService;
        private readonly IProductTagService _productTagService;
        private readonly IStoreContext _storeContext;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IVendorService _vendorService;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly MediaSettings _mediaSettings;
        private readonly VendorSettings _vendorSettings;
        private readonly HIS.ICategoryService _hisCategoryService;
        private readonly IPatientService _patientService;
        private readonly IPictureService _pictureService;
        private readonly HomepageSettings _homeSettings;

        #endregion

        #region Ctor

        public DoctorController(CatalogSettings catalogSettings,
            IAclService aclService,
            ICatalogModelFactory catalogModelFactory,
            ICategoryService categoryService,
            ICustomerActivityService customerActivityService,
            IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            IManufacturerService manufacturerService,
            IPermissionService permissionService,
            IProductModelFactory productModelFactory,
            IProductService productService,
            IProductTagService productTagService,
            IStoreContext storeContext,
            IStoreMappingService storeMappingService,
            IVendorService vendorService,
            IWebHelper webHelper,
            IWorkContext workContext,
            MediaSettings mediaSettings,
            VendorSettings vendorSettings,
            HIS.ICategoryService hisCategoryService,
            HomepageSettings homeSettings,
            IPictureService pictureService,
            IPatientService patientService)
        {
            _catalogSettings = catalogSettings;
            _aclService = aclService;
            _catalogModelFactory = catalogModelFactory;
            _categoryService = categoryService;
            _customerActivityService = customerActivityService;
            _genericAttributeService = genericAttributeService;
            _localizationService = localizationService;
            _manufacturerService = manufacturerService;
            _permissionService = permissionService;
            _productModelFactory = productModelFactory;
            _productService = productService;
            _productTagService = productTagService;
            _storeContext = storeContext;
            _storeMappingService = storeMappingService;
            _vendorService = vendorService;
            _webHelper = webHelper;
            _workContext = workContext;
            _mediaSettings = mediaSettings;
            _vendorSettings = vendorSettings;
            _hisCategoryService = hisCategoryService;
            _patientService = patientService;
            _homeSettings = homeSettings;
            _pictureService = pictureService;
        }

        #endregion

        #region Categories
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> Index(SearchModel model, CatalogPagingFilteringModel command)
        {
            var defaultResponse = new SearchModel();
            try
            {
                await SyncDoctor();
                if (model == null)
                    model = new SearchModel();

                model.ProductType = ProductType.SimpleProduct;
                model.PagingFilteringContext.PageSize = 10;
                command.PageSize = 10;
                model = _catalogModelFactory.PrepareSearchModel(model, command);
                model.BannerUrl = _pictureService
                            .GetPictureUrl(_homeSettings.BannerDoctorPageId,
                            showDefaultPicture: false);
                ViewBag.DoctorVideoUrl = _homeSettings.DoctorVideoUrl;
                return View(model);


                // var pageSize = 6;
                // var (total, products) = _productService.GetProducts(pageNumber * pageSize, pageSize);

                // if (model == null)
                //     model = new SearchModel();
                // model.ProductType = ProductType.GroupedProduct;
                // model = _catalogModelFactory.PrepareSearchModel(model, command);
                // return View(model);
                // var result = new ProductPaging
                // {
                //     Total = total,
                //     PageIndex = pageNumber,
                //     PageSize = pageSize,
                //     TotalPage = total / pageSize,
                //     Products = products.Select(x => _productModelFactory.PrepareProductDetailsModel(x)).ToList(),
                //     PagerModel = new PagerModel
                //     {
                //         PageSize = pageSize,
                //         TotalRecords = total,
                //         ShowTotalSummary = true,
                //         RouteActionName = "Doctor",
                //         UseRouteLinks = true,
                //         RouteValues = new DoctorRouteValues { pageNumber = pageNumber }
                //     }
                // };

                // return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> Index2(int? categoryId, CatalogPagingFilteringModel command)
        {
            var defaultResponse = new List<DoctorFacultyModel>();
            try
            {
                await SyncDoctor();
                await SyncFaculty();

                var faculties = _categoryService.GetAllCategories();
                var faculty = faculties.FirstOrDefault(x => !x.Deleted);
                if (categoryId.HasValue)
                {
                    faculty = faculties.FirstOrDefault(x => x.Id == categoryId && !x.Deleted);
                }
                if (faculty == null)
                    return InvokeHttp404();

                var model = _catalogModelFactory.PrepareCategoryModel(faculty, command);

                var falcultyModel = faculties.Select(x => new DoctorFacultyModel
                {
                    AllowCustomersToSelectPageSize = x.AllowCustomersToSelectPageSize,
                    CategoryTemplateId = x.CategoryTemplateId,
                    CreatedOnUtc = x.CreatedOnUtc,
                    Deleted = x.Deleted,
                    Description = x.Description,
                    DisplayOrder = x.DisplayOrder,
                    Id = x.Id,
                    IncludeInTopMenu = x.IncludeInTopMenu,
                    LimitedToStores = x.LimitedToStores,
                    MetaDescription = x.MetaDescription,
                    MetaKeywords = x.MetaKeywords,
                    MetaTitle = x.MetaTitle,
                    Name = x.Name,
                    PageSize = x.PageSize,
                    PageSizeOptions = x.PageSizeOptions,
                    ParentCategoryId = x.ParentCategoryId,
                    PictureId = x.PictureId,
                    PriceRanges = x.PriceRanges,
                    Published = x.Published,
                    ShowOnHomepage = x.ShowOnHomepage,
                    SubjectToAcl = x.SubjectToAcl,
                    UpdatedOnUtc = x.UpdatedOnUtc,
                    CategoryModel = model
                });
                //model
                return View(falcultyModel.ToList());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }

        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult Category(int categoryId, CatalogPagingFilteringModel command)
        {
            try
            {
                var category = _categoryService.GetCategoryById(categoryId);
                if (category == null || category.Deleted)
                    return InvokeHttp404();

                var notAvailable =
                    //published?
                    !category.Published ||
                    //ACL (access control list) 
                    !_aclService.Authorize(category) ||
                    //Store mapping
                    !_storeMappingService.Authorize(category);
                //Check whether the current user has a "Manage categories" permission (usually a store owner)
                //We should allows him (her) to use "Preview" functionality
                var hasAdminAccess = _permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel) && _permissionService.Authorize(StandardPermissionProvider.ManageCategories);
                if (notAvailable && !hasAdminAccess)
                    return InvokeHttp404();

                //'Continue shopping' URL
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    NopCustomerDefaults.LastContinueShoppingPageAttribute,
                    _webHelper.GetThisPageUrl(false),
                    _storeContext.CurrentStore.Id);

                //display "edit" (manage) link
                if (_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel) && _permissionService.Authorize(StandardPermissionProvider.ManageCategories))
                    DisplayEditLink(Url.Action("Edit", "Category", new { id = category.Id, area = AreaNames.Admin }));

                //activity log
                _customerActivityService.InsertActivity("PublicStore.ViewCategory",
                    string.Format(_localizationService.GetResource("ActivityLog.PublicStore.ViewCategory"), category.Name), category);

                //model
                var model = _catalogModelFactory.PrepareCategoryModel(category, command);

                //template
                var templateViewPath = _catalogModelFactory.PrepareCategoryTemplateViewPath(category.CategoryTemplateId);
                return View(templateViewPath, model);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public virtual IActionResult GetCatalogRoot()
        {
            var model = _catalogModelFactory.PrepareRootCategories();

            return Json(model);
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public virtual IActionResult GetCatalogSubCategories(int id)
        {
            var model = _catalogModelFactory.PrepareSubCategories(id);

            return Json(model);
        }

        #endregion

        #region Manufacturers

        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult Manufacturer(int manufacturerId, CatalogPagingFilteringModel command)
        {
            try
            {

                var manufacturer = _manufacturerService.GetManufacturerById(manufacturerId);
                if (manufacturer == null || manufacturer.Deleted)
                    return InvokeHttp404();

                var notAvailable =
                    //published?
                    !manufacturer.Published ||
                    //ACL (access control list) 
                    !_aclService.Authorize(manufacturer) ||
                    //Store mapping
                    !_storeMappingService.Authorize(manufacturer);
                //Check whether the current user has a "Manage categories" permission (usually a store owner)
                //We should allows him (her) to use "Preview" functionality
                var hasAdminAccess = _permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel) && _permissionService.Authorize(StandardPermissionProvider.ManageManufacturers);
                if (notAvailable && !hasAdminAccess)
                    return InvokeHttp404();

                //'Continue shopping' URL
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    NopCustomerDefaults.LastContinueShoppingPageAttribute,
                    _webHelper.GetThisPageUrl(false),
                    _storeContext.CurrentStore.Id);

                //display "edit" (manage) link
                if (_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel) && _permissionService.Authorize(StandardPermissionProvider.ManageManufacturers))
                    DisplayEditLink(Url.Action("Edit", "Manufacturer", new { id = manufacturer.Id, area = AreaNames.Admin }));

                //activity log
                _customerActivityService.InsertActivity("PublicStore.ViewManufacturer",
                    string.Format(_localizationService.GetResource("ActivityLog.PublicStore.ViewManufacturer"), manufacturer.Name), manufacturer);

                //model
                var model = _catalogModelFactory.PrepareManufacturerModel(manufacturer, command);

                //template
                var templateViewPath = _catalogModelFactory.PrepareManufacturerTemplateViewPath(manufacturer.ManufacturerTemplateId);
                return View(templateViewPath, model);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult ManufacturerAll()
        {
            var defaultResponse = new List<ManufacturerModel>();
            try
            {
                var model = _catalogModelFactory.PrepareManufacturerAllModels();
                return View(model);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }

        #endregion

        #region Vendors

        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult Vendor(int vendorId, CatalogPagingFilteringModel command)
        {
            var defaultResponse = new VendorModel();
            try
            {
                var vendor = _vendorService.GetVendorById(vendorId);
                if (vendor == null || vendor.Deleted || !vendor.Active)
                    return InvokeHttp404();

                //'Continue shopping' URL
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    NopCustomerDefaults.LastContinueShoppingPageAttribute,
                    _webHelper.GetThisPageUrl(false),
                    _storeContext.CurrentStore.Id);

                //display "edit" (manage) link
                if (_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel) && _permissionService.Authorize(StandardPermissionProvider.ManageVendors))
                    DisplayEditLink(Url.Action("Edit", "Vendor", new { id = vendor.Id, area = AreaNames.Admin }));

                //model
                var model = _catalogModelFactory.PrepareVendorModel(vendor, command);

                return View(model);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }

        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult VendorAll()
        {
            var defaultResponse = new List<VendorModel>();
            try
            {
                //we don't allow viewing of vendors if "vendors" block is hidden
                if (_vendorSettings.VendorsBlockItemsToDisplay == 0)
                    return RedirectToRoute("Homepage");

                var model = _catalogModelFactory.PrepareVendorAllModels();
                return View(model);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }

        #endregion

        #region Product tags

        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult ProductsByTag(int productTagId, CatalogPagingFilteringModel command)
        {
            var defaultResponse = new ProductsByTagModel();
            try
            {
                var productTag = _productTagService.GetProductTagById(productTagId);
                if (productTag == null)
                    return InvokeHttp404();

                var model = _catalogModelFactory.PrepareProductsByTagModel(productTag, command);
                return View(model);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }

        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult ProductTagsAll()
        {
            var model = _catalogModelFactory.PrepareProductTagsAllModel();
            return View(model);
        }

        #endregion

        #region Searching

        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult Search(SearchModel model, CatalogPagingFilteringModel command)
        {
            var defaultResponse = new SearchModel();
            try
            {
                //'Continue shopping' URL
                _genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
                    NopCustomerDefaults.LastContinueShoppingPageAttribute,
                    _webHelper.GetThisPageUrl(true),
                    _storeContext.CurrentStore.Id);

                if (model == null)
                    model = new SearchModel();

                model = _catalogModelFactory.PrepareSearchModel(model, command);
                return View(model);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }

        public virtual IActionResult SearchTermAutoComplete(string term)
        {
            if (string.IsNullOrWhiteSpace(term) || term.Length < _catalogSettings.ProductSearchTermMinimumLength)
                return Content("");

            //products
            var productNumber = _catalogSettings.ProductSearchAutoCompleteNumberOfProducts > 0 ?
                _catalogSettings.ProductSearchAutoCompleteNumberOfProducts : 10;

            var products = _productService.SearchProducts(
                storeId: _storeContext.CurrentStore.Id,
                keywords: term,
                languageId: _workContext.WorkingLanguage.Id,
                visibleIndividuallyOnly: true,
                pageSize: productNumber);

            var showLinkToResultSearch = _catalogSettings.ShowLinkToAllResultInSearchAutoComplete && (products.TotalCount > productNumber);

            var models = _productModelFactory.PrepareProductOverviewModels(products, false, _catalogSettings.ShowProductImagesInSearchAutoComplete, _mediaSettings.AutoCompleteSearchThumbPictureSize).ToList();
            var result = (from p in models
                          select new
                          {
                              label = p.Name,
                              producturl = Url.RouteUrl("Product", new { SeName = p.SeName }),
                              productpictureurl = p.DefaultPictureModel.ImageUrl,
                              showlinktoresultsearch = showLinkToResultSearch
                          })
                .ToList();
            return Json(result);
        }

        #endregion
        public async Task<bool> SyncDoctor()
        {
            // get doctor from his
            var hisDoctors = await _hisCategoryService.GetDoctors();
            // get doctor from db
            var doctors = _productService.GetSummary();

            if (hisDoctors == null || !hisDoctors.Any())
            {
                return true;
            }

            if (!doctors.Any())
            {
                // create doctors
                foreach (var doctor in hisDoctors)
                {
                    var _doctor = new Product
                    {
                        ProductTypeId = (int)ProductType.SimpleProduct,
                        ParentGroupedProductId = 0,
                        VisibleIndividually = true,
                        Name = doctor.Fullname,
                        ShortDescription = doctor.Fullname,
                        FullDescription = doctor.Fullname,
                        ProductTemplateId = 1,
                        ShowOnHomepage = false,
                        MetaKeywords = doctor.Fullname,
                        MetaDescription = doctor.Fullname,
                        MetaTitle = doctor.Fullname,
                        Sku = doctor.Id,
                        HisId = int.Parse(doctor.Id),
                        Published = true,
                        Sex = doctor.Sex,
                        Birthyear = doctor.Birthyear,
                        Role = doctor.Role
                    };
                    _productService.InsertProduct(_doctor);
                }
                return true;
            }
            var newDoctors = hisDoctors.Where(x => doctors.All(a => a.HisId.ToString() != x.Id));
            if (newDoctors.Any())
            {
                foreach (var doctor in newDoctors)
                {
                    var _doctor = new Product
                    {
                        ProductTypeId = (int)ProductType.SimpleProduct,
                        ParentGroupedProductId = 0,
                        VisibleIndividually = true,
                        Name = doctor.Fullname,
                        ShortDescription = doctor.Fullname,
                        FullDescription = doctor.Fullname,
                        ProductTemplateId = 1,
                        ShowOnHomepage = false,
                        MetaKeywords = doctor.Fullname,
                        MetaDescription = doctor.Fullname,
                        MetaTitle = doctor.Fullname,
                        Sku = doctor.Id,
                        HisId = int.Parse(doctor.Id),
                        Published = true,
                        Sex = doctor.Sex,
                        Birthyear = doctor.Birthyear,
                        Role = doctor.Role
                    };
                    _productService.InsertProduct(_doctor);
                }
            }
            // var oldDoctors = doctors.Where(x => hisDoctors.All(a => a.Id != x.HisId.ToString()));
            // if (oldDoctors.Any())
            // {
            //     var _doctors = _productService.GetProductsByIds(oldDoctors.Select(x => x.Id).ToArray());
            //     foreach (var doctor in _doctors)
            //     {
            //         _productService.DeleteProduct(doctor);
            //     }
            // }
            return true;
        }
        public async Task<bool> SyncFaculty()
        {
            // get  from his
            var hisFalculties = await _hisCategoryService.GetSubjects();
            // get  from db
            var faculties = _categoryService.GetAllCategories();

            if (hisFalculties == null || !hisFalculties.Any())
            {
                return true;
            }

            if (!faculties.Any())
            {
                // create 
                foreach (var item in hisFalculties)
                {
                    var _item = new Category
                    {
                        Name = item.Name,
                        Description = item.Description,
                        CategoryTemplateId = 1,
                        MetaKeywords = item.Name,
                        MetaDescription = item.Description,
                        MetaTitle = item.Name,
                        ParentCategoryId = 0,
                        Published = true,
                        HisId = item.Id,
                        DefaultPrice = item.DefaultPrice,
                    };
                    _categoryService.InsertCategory(_item);
                }
                return true;
            }
            var newFalculties = hisFalculties.Where(x => faculties.All(a => a.HisId != x.Id));
            if (newFalculties.Any())
            {
                foreach (var item in newFalculties)
                {
                    var _item = new Category
                    {
                        Name = item.Name,
                        Description = item.Description,
                        CategoryTemplateId = 1,
                        MetaKeywords = item.Name,
                        MetaDescription = item.Description,
                        MetaTitle = item.Name,
                        ParentCategoryId = 0,
                        Published = true,
                        HisId = item.Id,
                        DefaultPrice = item.DefaultPrice,
                    };
                    _categoryService.InsertCategory(_item);
                }
            }
            // var old = faculties.Where(x => hisFalculties.All(a => a.Id != x.HisId));
            // if (old.Any())
            // {
            //     var _old = _categoryService.GetCategoriesByIds(old.Select(x => x.Id).ToArray());
            //     foreach (var item in _old)
            //     {
            //         _categoryService.DeleteCategory(item);
            //     }
            // }
            return true;
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult BMI()
        {
            return View();
        }

    }
}