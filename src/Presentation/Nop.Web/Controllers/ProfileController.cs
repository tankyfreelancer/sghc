﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Factories;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Profile;

namespace Nop.Web.Controllers
{
    [HttpsRequirement(SslRequirement.No)]
    public partial class ProfileController : BasePublicController
    {
        private readonly CustomerSettings _customerSettings;
        private readonly ICustomerService _customerService;
        private readonly IPermissionService _permissionService;
        private readonly IProfileModelFactory _profileModelFactory;
        private readonly IWorkContext _workContext;
        private readonly IOrderService _orderService;
        private readonly IPatientService _patientService;
        private readonly IAddressService _addressService;
        private readonly ICustomerRegistrationService _customerRegistrationService;

        public ProfileController(CustomerSettings customerSettings,
            ICustomerService customerService,
            IPermissionService permissionService,
            IWorkContext workContext,
            IPatientService patientService,
            IOrderService orderService,
            ICustomerRegistrationService customerRegistrationService,
            IAddressService addressService,
            IProfileModelFactory profileModelFactory)
        {
            _customerSettings = customerSettings;
            _customerService = customerService;
            _permissionService = permissionService;
            _profileModelFactory = profileModelFactory;
            _workContext = workContext;
            _orderService = orderService;
            _patientService = patientService;
            _addressService = addressService;
            _customerRegistrationService = customerRegistrationService;
        }

        public virtual IActionResult Index(int? id, int? pageNumber)
        {
            if (!_customerSettings.AllowViewingProfiles)
            {
                return RedirectToRoute("Homepage");
            }

            var customerId = _workContext.CurrentCustomer.Id;
            if (id.HasValue)
            {
                customerId = id.Value;
            }

            var customer = _customerService.GetCustomerById(customerId);
            if (customer == null || _customerService.IsGuest(customer))
            {
                return RedirectToRoute("Homepage");
            }

            //display "edit" (manage) link
            if (_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel) && _permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                DisplayEditLink(Url.Action("Edit", "Customer", new { id = customer.Id, area = AreaNames.Admin }));

            var model = _profileModelFactory.PrepareProfileIndexModel(customer, pageNumber);
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Index(ProfileIndexModel model)
        {

            if (!_customerService.IsRegistered(_workContext.CurrentCustomer))
                return Challenge();

            var customer = _workContext.CurrentCustomer;

            if (_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel) && _permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
                DisplayEditLink(Url.Action("Edit", "Customer", new { id = customer.Id, area = AreaNames.Admin }));
            if (_customerService.CheckPhoneNumber(_workContext.CurrentCustomer.Id, model.CustomerInfo.Phone))
            {
                var error = "SDT đã tồn tại";
                return RedirectToAction("PageError", "Booking", new { error });
            }
            

            _workContext.CurrentCustomer.PhoneNumber = model.CustomerInfo.Phone;
            _customerService.UpdateCustomer(_workContext.CurrentCustomer);

            var patients = _patientService.GetByCustomer(_workContext.CurrentCustomer.Id);
            if (patients.Any())
            {
                var patient = patients.FirstOrDefault();
                patient.Name = model.CustomerInfo.FirstName;
                patient.SurName = model.CustomerInfo.LastName;
                patient.Mobile = model.CustomerInfo.Phone;
                patient.Sex = model.CustomerInfo.Sex ? 1 : 0;
                patient.Day = model.CustomerInfo.DateOfBirthDay;
                patient.Month = model.CustomerInfo.DateOfBirthMonth;
                patient.Year = model.CustomerInfo.DateOfBirthYear;
                patient.BirthDate = $"{model.CustomerInfo.DateOfBirthDay}/{model.CustomerInfo.DateOfBirthMonth}/{model.CustomerInfo.DateOfBirthYear}";
                patient.BirthYear = model.CustomerInfo.DateOfBirthYear;
                patient.CityId = model.CustomerInfo.CityId;
                patient.DistrictId = model.CustomerInfo.DistrictId;
                patient.WardId = model.CustomerInfo.WardId;
                patient.Address = model.CustomerInfo.Address;
                _patientService.UpdatePatient(patient);
            }
            var address = _customerService.GetCustomerBillingAddress(_workContext.CurrentCustomer);
            if (address != null)
            {
                address = model.Address.ToEntity<Address>();
                _addressService.UpdateAddress(address);
            }

            if (model.IsPasswordChanged)
            {
                var changePasswordRequest = new ChangePasswordRequest(customer.Email,
                                    true, _customerSettings.DefaultPasswordFormat, model.PasswordModel.NewPassword, model.PasswordModel.OldPassword);
                var changePasswordResult = _customerRegistrationService.ChangePassword(changePasswordRequest);
                if (!changePasswordResult.Success)
                {
                    var error = string.Join("; ", changePasswordResult.Errors);
                    return RedirectToAction("PageError", "Booking", new { error });
                }
            }
            var data = _profileModelFactory.PrepareProfileIndexModel(customer, 0);
            data.Success = true;
            return View(data);
        }
    }
}