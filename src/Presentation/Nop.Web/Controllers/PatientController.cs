﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Vendors;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Factories;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Catalog;
using System.Threading.Tasks;
using Nop.Services.Customers;
using Nop.Web.Models.Customer;
using System;
using Nop.Services.Directory;
using Nop.Core.Domain.Directory;
using System.Globalization;
using Nop.Web.Helpers;
using Nop.Services.Defaults;

namespace Nop.Web.Controllers
{
    public partial class PatientController : BasePublicController
    {
        #region Fields

        private readonly CatalogSettings _catalogSettings;
        private readonly IAclService _aclService;
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly ICategoryService _categoryService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IPermissionService _permissionService;
        private readonly IProductModelFactory _productModelFactory;
        private readonly IProductService _productService;
        private readonly IProductTagService _productTagService;
        private readonly IStoreContext _storeContext;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IVendorService _vendorService;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly MediaSettings _mediaSettings;
        private readonly VendorSettings _vendorSettings;
        private readonly HIS.ICategoryService _hisCategoryService;
        private readonly HIS.IBusinessService _hisBusinessService;
        private readonly IPatientService _patientService;
        private readonly ICountryService _countryService;
        private readonly ICustomerService _customerService;
        private readonly IEncryptionService _encryptionService;
        private readonly CustomerSettings _customerSettings;

        #endregion

        #region Ctor

        public PatientController(CatalogSettings catalogSettings,
            IAclService aclService,
            CustomerSettings customerSettings,
            ICatalogModelFactory catalogModelFactory,
            ICategoryService categoryService,
            ICustomerActivityService customerActivityService,
            IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            IManufacturerService manufacturerService,
            IPermissionService permissionService,
            IProductModelFactory productModelFactory,
            IProductService productService,
            IProductTagService productTagService,
            IStoreContext storeContext,
            IStoreMappingService storeMappingService,
            IVendorService vendorService,
            IWebHelper webHelper,
            IWorkContext workContext,
            MediaSettings mediaSettings,
            VendorSettings vendorSettings,
            HIS.ICategoryService hisCategoryService,
            IEncryptionService encryptionService,
            IPatientService patientService,
            HIS.IBusinessService hisBusinessService,
            ICustomerService customerService,
            ICountryService countryService)
        {
            _catalogSettings = catalogSettings;
            _aclService = aclService;
            _catalogModelFactory = catalogModelFactory;
            _categoryService = categoryService;
            _customerActivityService = customerActivityService;
            _genericAttributeService = genericAttributeService;
            _localizationService = localizationService;
            _manufacturerService = manufacturerService;
            _permissionService = permissionService;
            _productModelFactory = productModelFactory;
            _productService = productService;
            _productTagService = productTagService;
            _customerSettings = customerSettings;
            _storeContext = storeContext;
            _storeMappingService = storeMappingService;
            _encryptionService = encryptionService;
            _vendorService = vendorService;
            _webHelper = webHelper;
            _workContext = workContext;
            _mediaSettings = mediaSettings;
            _vendorSettings = vendorSettings;
            _hisCategoryService = hisCategoryService;
            _patientService = patientService;
            _hisBusinessService = hisBusinessService;
            _countryService = countryService;
            _customerService = customerService;
        }

        #endregion
        // Profile
        [HttpsRequirement(SslRequirement.No)]
        public IActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Customer");
            };

            // get patients by customer
            var patients = _patientService.GetByCustomer(_workContext.CurrentCustomer.Id);
            var model = new PatientModel();
            var results = patients.Select(x => model.Map(x));
            return View(results.ToList());
        }
        [HttpPost]
        [HttpsRequirement(SslRequirement.No)]
        public async Task<IActionResult> UpdatePhoneNumber(int patientId, string phoneNumber)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Customer");
            };
            // get patients by customer
            var result = new PatientModel();
            var patients = _patientService.GetByCustomer(_workContext.CurrentCustomer.Id);
            if (!patients.Any())
            {
                return View(result);
            }
            var patient = patients.FirstOrDefault(x => x.Id == patientId);
            if (patient == null)
            {
                return View(result);
            }
            patient.Mobile = phoneNumber;
            var res = await _hisBusinessService.PostPatientsPhone(patient.HisId.ToString(), phoneNumber);
            if (!res)
            {
                return View(result);
            }
            _patientService.UpdatePatient(patient);

            return View(result.Map(patient));
        }
        [HttpPost]
        [HttpsRequirement(SslRequirement.No)]
        public async Task<IActionResult> CheckInsurance(int patientId, string date)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Customer");
            };
            // get patients by customer
            var patients = _patientService.GetByCustomer(_workContext.CurrentCustomer.Id);
            if (!patients.Any())
            {
                return View(null);
            }
            var patient = patients.FirstOrDefault(x => x.Id == patientId);
            if (patient == null)
            {
                return View(null);
            }
            var res = await _hisBusinessService.PostPatientsInsurance(patient.HisId.ToString(), date);

            return Json(res);
        }
        [HttpsRequirement(SslRequirement.No)]
        public async Task<IActionResult> GetEncounter(string patientCode, List<string> date)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Customer");
            };
            // get patients by customer
            var patients = _patientService.GetByCustomer(_workContext.CurrentCustomer.Id);
            if (!patients.Any())
            {
                return View(null);
            }
            var patient = patients.FirstOrDefault(x => x.Code == patientCode);
            if (patient == null)
            {
                return View(null);
            }
            var res = await _hisBusinessService.GetEncounter(patientCode, date);

            return View(res);
        }
        [HttpsRequirement(SslRequirement.No)]
        public async Task<IActionResult> GetTestResults(string patientCode, string EncounterCode)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Customer");
            };
            // get patients by customer
            var patients = _patientService.GetByCustomer(_workContext.CurrentCustomer.Id);
            if (!patients.Any())
            {
                return View(null);
            }
            var patient = patients.FirstOrDefault(x => x.Code == patientCode);
            if (patient == null)
            {
                return View(null);
            }
            var res = await _hisBusinessService.GetTestResults(patientCode, EncounterCode);

            return View(res);
        }
        [HttpsRequirement(SslRequirement.No)]
        public async Task<IActionResult> GetImageDiagnosticResults(string patientCode, string EncounterCode)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Customer");
            };
            // get patients by customer
            var patients = _patientService.GetByCustomer(_workContext.CurrentCustomer.Id);
            if (!patients.Any())
            {
                return View(null);
            }
            var patient = patients.FirstOrDefault(x => x.Code == patientCode);
            if (patient == null)
            {
                return View(null);
            }
            var res = await _hisBusinessService.GetImageDiagnosticResults(patientCode, EncounterCode);

            return View(res);
        }

        [HttpsRequirement(SslRequirement.No)]
        public async Task<IActionResult> GetPrescription(string patientCode, string EncounterCode)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Customer");
            };
            // get patients by customer
            var patients = _patientService.GetByCustomer(_workContext.CurrentCustomer.Id);
            if (!patients.Any())
            {
                return View(null);
            }
            var patient = patients.FirstOrDefault(x => x.Code == patientCode);
            if (patient == null)
            {
                return View(null);
            }
            var res = await _hisBusinessService.GetPrescription(patientCode, EncounterCode);

            return View(res);
        }
        public ActionResult<dynamic> AddCustomer([FromBody] AddCustomerModel model)
        {
            if (model == null)
            {
                return BadRequest("Model is empty");
            }
            if (string.IsNullOrEmpty(model.Token))
            {
                return BadRequest("Token is required");
            }
            if (model.Token != "SuyZGEha3sqJZCwqpsGZanWLL6HSTtqm")
            {
                return BadRequest("Token is invalid");
            }
            if (!ModelState.IsValid)
            {
                string messages = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                return BadRequest(messages);
            }
            var errors = new List<string>();
            if (model.Gender == null)
            {
                model.Gender = "Nam";
            }
            model.Email = model.Email.Trim();
            model.Username = model.Email;
            if (String.IsNullOrEmpty(model.CountryCode))
            {
                model.CountryCode = "VN";
            }
            var month = $"{model.DateOfBirthMonth}";
            if (model.DateOfBirthMonth < 10)
            {
                month = $"0{model.DateOfBirthMonth}";
            }
            var day = $"{model.DateOfBirthDay}";
            if (model.DateOfBirthDay < 10)
            {
                day = $"0{model.DateOfBirthDay}";
            }
            var city = _countryService.GetCityByHisId(model.CityId);
            var district = _countryService.GetDistrictByHisId(model.DistrictId);
            var ward = _countryService.GetWardByHisId(model.WardId);
            if (city == null || district == null || ward == null)
            {
                var error = "Thông tin địa chỉ (thành phồ, huyện, xã) không đúng";
                Console.WriteLine(error);
            }
            var emailExisted = _customerService.GetCustomerByEmail(model.Email) != null;
            var _patient = _patientService.GetByHisId(model.Id);
            if (_patient != null)
            {
                return UpdateCustomer(model);
            }
            if (_customerService.GetCustomerByPhone(model.Phone) != null)
            {
                return BadRequest("PhoneNumber already exist");
            }
            if (emailExisted)
            {
                return BadRequest("Email already exist");
            }

            var customer = new Customer
            {
                Active = true,
                Email = model.Email,
                PhoneNumber = model.Phone,
                Username = model.Email,
                CustomerGuid = Guid.NewGuid(),
                CreatedOnUtc = DateTime.UtcNow,
                LastActivityDateUtc = DateTime.UtcNow,
                RegisteredInStoreId = _storeContext.CurrentStore.Id,
            };
            _customerService.InsertCustomer(customer);
            customer = _customerService.GetCustomerByEmail(model.Email);
            if (customer == null)
            {
                return BadRequest("Customer insert fail");
            }

            var patient = new Patient
            {
                HisId = model.Id,
                Name = model.FirstName,
                SurName = model.LastName,
                Sex = model.Gender == "M" ? 0 : 1,
                BirthDate = $"{model.DateOfBirthYear}{month}{day}",
                BirthYear = model.DateOfBirthYear.GetValueOrDefault(),
                Mobile = model.Phone,
                SocialId = int.Parse(model.Passport),
                CountryId = model.CountryId,
                CountryCode = model.CountryCode,
                CityId = model.CityId,
                DistrictId = model.DistrictId,
                WardId = model.WardId,
                Address = model.Address,
                Code = model.Id.ToString(),
                Passport = model.Passport,
                Email = model.Email,
                CustomerId = customer.Id,
                BHYTCode = model.Insurance,
                Day = model.DateOfBirthDay,
                Month = model.DateOfBirthMonth,
                Year = model.DateOfBirthYear
            };
            _patientService.InsertPatient(patient);
            //validate unique user

            var customerPassword = new CustomerPassword
            {
                CustomerId = customer.Id,
                PasswordFormat = PasswordFormat.Hashed,
                CreatedOnUtc = DateTime.UtcNow
            };
            var saltKey = _encryptionService.CreateSaltKey(NopCustomerServiceDefaults.PasswordSaltKeySize);
            customerPassword.PasswordSalt = saltKey;
            customerPassword.Password = _encryptionService.CreatePasswordHash(model.Password, saltKey, _customerSettings.HashedPasswordFormat);
            _customerService.InsertCustomerPassword(customerPassword);
            //add to 'Registered' role
            var registeredRole = _customerService.GetCustomerRoleBySystemName(NopCustomerDefaults.RegisteredRoleName);
            if (registeredRole == null)
                return BadRequest("'Registered' role could not be loaded");

            _customerService.AddCustomerRoleMapping(new CustomerCustomerRoleMapping { CustomerId = customer.Id, CustomerRoleId = registeredRole.Id });

            return Ok(patient);
        }
        public ActionResult<dynamic> UpdateCustomer([FromBody] AddCustomerModel model)
        {
            var patient = _patientService.GetByHisId(model.Id);
            if (patient == null || !patient.CustomerId.HasValue)
            {
                return BadRequest("Update customer failed. Patient does not exist");
            }
            var customer = _customerService.GetCustomerById(patient.CustomerId.Value);
            if (customer == null)
            {
                return BadRequest("Update customer failed. Customer doses not exist");
            }
            var isPhoneExist = _customerService.CheckPhoneNumber(customer.Id, model.Phone);
            if (isPhoneExist)
            {
                return BadRequest("Update customer failed. Phone number already exist");
            }
            customer.PhoneNumber = model.Phone;
            _customerService.UpdateCustomer(customer);
            patient.Mobile = model.Phone;
            if (!string.IsNullOrEmpty(model.FirstName))
            {
                patient.Name = model.FirstName;
            }
            if (!String.IsNullOrEmpty(model.LastName))
            {
                patient.SurName = model.LastName;
            }
            if (model.DateOfBirthDay.HasValue)
            {
                patient.Day = model.DateOfBirthDay.Value;
            }
            if (model.DateOfBirthMonth.HasValue)
            {
                patient.Month = model.DateOfBirthMonth.Value;
            }
            if (model.DateOfBirthYear.HasValue)
            {
                patient.Year = model.DateOfBirthYear.Value;
            }
            if (!String.IsNullOrEmpty(model.Address))
            {
                patient.Address = model.Address;
            }
            if (model.CityId > 0)
            {
                patient.CityId = model.CityId;
            }
            if (model.DistrictId > 0)
            {
                patient.DistrictId = model.DistrictId;
            }
            if (model.WardId > 0)
            {
                patient.WardId = model.WardId;
            }
            if (!String.IsNullOrEmpty(model.Passport))
            {
                patient.Passport = model.Passport;
            }
            if (!String.IsNullOrEmpty(model.Insurance))
            {
                patient.BHYTCode = model.Insurance;
            }
            _patientService.UpdatePatient(patient);
            return Ok(patient);
        }
    }
}