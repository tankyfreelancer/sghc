﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.News;
using Nop.Core.Domain.Security;
using Nop.Core.Rss;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.News;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Factories;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Models.News;
using Nop.Services.Catalog;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Common;
using Nop.Core.Domain.Common;
using Nop.Services.Media;

namespace Nop.Web.Controllers
{
    [HttpsRequirement(SslRequirement.No)]
    public partial class NewsController : BasePublicController
    {
        #region Fields

        private readonly CaptchaSettings _captchaSettings;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ICustomerService _customerService;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILocalizationService _localizationService;
        private readonly INewsModelFactory _newsModelFactory;
        private readonly INewsService _newsService;
        private readonly IPermissionService _permissionService;
        private readonly IStoreContext _storeContext;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly NewsSettings _newsSettings;
        private readonly IProductService _productService;
        private readonly IProductModelFactory _productModelFactory;
        private readonly IPictureService _pictureService;
        private readonly HomepageSettings _homeSettings;

        #endregion

        #region Ctor

        public NewsController(CaptchaSettings captchaSettings,
            ICustomerActivityService customerActivityService,
            ICustomerService customerService,
            IEventPublisher eventPublisher,
            ILocalizationService localizationService,
            INewsModelFactory newsModelFactory,
            INewsService newsService,
            IPermissionService permissionService,
            IStoreContext storeContext,
            IProductService productService,
            IStoreMappingService storeMappingService,
            IUrlRecordService urlRecordService,
            IWebHelper webHelper,
            IWorkContext workContext,
            IWorkflowMessageService workflowMessageService,
            LocalizationSettings localizationSettings,
            IProductModelFactory productModelFactory,
            HomepageSettings homeSettings,
            IPictureService pictureService,
            NewsSettings newsSettings)
        {
            _captchaSettings = captchaSettings;
            _customerActivityService = customerActivityService;
            _customerService = customerService;
            _eventPublisher = eventPublisher;
            _localizationService = localizationService;
            _newsModelFactory = newsModelFactory;
            _newsService = newsService;
            _permissionService = permissionService;
            _storeContext = storeContext;
            _storeMappingService = storeMappingService;
            _urlRecordService = urlRecordService;
            _webHelper = webHelper;
            _workContext = workContext;
            _workflowMessageService = workflowMessageService;
            _localizationSettings = localizationSettings;
            _newsSettings = newsSettings;
            _productService = productService;
            _productModelFactory = productModelFactory;
            _homeSettings = homeSettings;
            _pictureService = pictureService;
        }

        #endregion

        #region Methods

        public virtual IActionResult List(NewsPagingFilteringModel command)
        {
            if (!_newsSettings.Enabled)
                return RedirectToRoute("Homepage");
            var newsCategories = _newsService.GetAllNewsCategory(isNews: true);
            if (!newsCategories.Any())
            {
                return RedirectToRoute("Homepage");
            }
            var newsCategory = newsCategories.FirstOrDefault();
            if (command.NewsCategoryId.HasValue)
            {
                newsCategory = newsCategories.FirstOrDefault(x => x.Id == command.NewsCategoryId);
            }
            if (newsCategory == null)
            {
                return RedirectToRoute("Homepage");
            }

            command.IsNews = true;
            var newsItemListModel = _newsModelFactory.PrepareNewsItemListModel(command);
            var news = _newsModelFactory.PrepareHomepageNewsItemsModel();
            var communications = news.NewsItems.Where(x => x.Category.Name == "Truyền thông").OrderByDescending(x => x.CreatedOn).ToList();
            var model = new NewsCategoryModel
            {
                Categories = newsCategories.Select(x => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem { Text = x.Title, Value = x.Id.ToString() }).ToList(),
                CurrentCategoryId = newsCategory.Id,
                CurrentCategoryName = newsCategory.Title,
                NewsItemListModel = newsItemListModel,
                Communications = communications,
                BannerUrl = _pictureService
                        .GetPictureUrl(_homeSettings.BannerNewsPageId,
                        showDefaultPicture: false)
            };
            return View(model);
        }

        public virtual IActionResult UserManual(NewsPagingFilteringModel command)
        {
            if (!_newsSettings.Enabled)
                return RedirectToRoute("Homepage");
            var newsCategories = _newsService.GetAllNewsCategory(isNews: true);
            if (!newsCategories.Any())
            {
                return RedirectToRoute("Homepage");
            }
            var newsCategory = newsCategories.FirstOrDefault(x => x.MenuSeName == "huong-dan-khach-hang");
            if (newsCategory == null)
            {
                return RedirectToRoute("Homepage");
            }

            command.IsNews = true;
            command.NewsCategoryId = newsCategory.Id;
            var newsItemListModel = _newsModelFactory.PrepareNewsItemListModel(command);
            var news = _newsModelFactory.PrepareHomepageNewsItemsModel();
            var model = new NewsCategoryModel
            {
                Categories = newsCategories.Select(x => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem { Text = x.Title, Value = x.Id.ToString() }).ToList(),
                CurrentCategoryId = newsCategory.Id,
                CurrentCategoryName = newsCategory.Title,
                NewsItemListModel = newsItemListModel,
                BannerUrl = _pictureService
                        .GetPictureUrl(_homeSettings.BannerUserManualPageId,
                        showDefaultPicture: false)
            };
            return View(model);
        }
        public virtual IActionResult NewsCategory(int newsCategoryId, NewsPagingFilteringModel command, CatalogPagingFilteringModel command2)
        {
            var newsCategory = _newsService.GetRoot(newsCategoryId);
            if (newsCategory == null)
            {
                return RedirectToRoute("Homepage");
            }
            switch (newsCategory.MenuSeName)
            {
                case "dich-vu":
                    return Service(newsCategoryId, command);
                case "chuyen-khoa":
                    return Specialist(newsCategoryId, command);
                case "huong-dan-khach-hang":
                    return UserManual(newsCategoryId, command);
                default:
                    return RedirectToRoute("Homepage");
            }
        }
        public virtual IActionResult Service(int? newsCategoryId, NewsPagingFilteringModel command)
        {
            var categories = _newsService.GetServices();
            if (!categories.Any())
            {
                return RedirectToRoute("Homepage");
            }

            command.PageSize = 10;
            var serviceCategory = categories.FirstOrDefault();
            if (newsCategoryId.HasValue)
            {
                serviceCategory = categories.FirstOrDefault(x => x.Id == newsCategoryId.Value);
            }
            if (serviceCategory == null)
            {
                return RedirectToRoute("Homepage");
            }
            command.NewsCategoryId = serviceCategory.Id;
            var newsItemListModel = _newsModelFactory.PrepareNewsItemListModel(command);
            var model = new ServiceModel
            {
                Categories = categories,
                News = newsItemListModel,
                BannerUrl = _pictureService
                        .GetPictureUrl(_homeSettings.BannerServicePageId,
                        showDefaultPicture: false)
            };

            return View("Service", model);
        }
        public virtual IActionResult SpecialistDoctors(int newsCategoryId, int pageSize = 3, int pageNumber = 1)
        {
            var result = new List<ProductDetailsModel>();
            var category = _newsService.GetNewsCategoryById(newsCategoryId);
            if (category == null)
            {
                return Json(new
                {
                    total = 0,
                    result
                });
            }
            if (pageNumber <= 0)
            {
                pageNumber = 1;
            }
            var (total, products) = _productService.GetProducts((pageNumber - 1) * pageSize, pageSize, category.SpecialistCategoryId);

            result = products.Select(x => _productModelFactory.PrepareProductDetailsModel(x)).ToList();
            return Json(new
            {
                total,
                result
            });
        }
        public virtual IActionResult SpecialistNews(int newsCategoryId, int pageSize = 5, int pageNumber = 1)
        {
            var result = new NewsItemListModel();
            var category = _newsService.GetNewsCategoryById(newsCategoryId);
            if (category == null)
            {
                return Json(result);
            }
            if (pageNumber <= 0)
            {
                pageNumber = 1;
            }
            var command = new NewsPagingFilteringModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                NewsCategoryId = category.Id
            };
            result = _newsModelFactory.PrepareNewsItemListModel(command);
            return Json(result);
        }
        public virtual IActionResult Specialist(int newsCategoryId, NewsPagingFilteringModel command)
        {
            var category = _newsService.GetNewsCategoryById(newsCategoryId);
            if (category == null)
            {
                return RedirectToRoute("Homepage");
            }
            command.PageSize = 4;
            command.NewsCategoryId = category.Id;

            var newsItemListModel = _newsModelFactory.PrepareNewsItemListModel(command);

            var model = new SpecialistModel
            {
                Category = category,
                News = newsItemListModel,
                Doctors = new ProductPaging(),
                BannerUrl = _pictureService
                        .GetPictureUrl(_homeSettings.BannerSpecialistPageId,
                        showDefaultPicture: false)
            };

            return View("Specialist", model);
        }
        public virtual IActionResult UserManual(int newsCategoryId, NewsPagingFilteringModel command)
        {
            var category = _newsService.GetNewsCategoryById(newsCategoryId);
            if (category == null)
            {
                return RedirectToRoute("Homepage");
            }
            command.PageSize = 9;
            command.NewsCategoryId = category.Id;
            var newsItemListModel = _newsModelFactory.PrepareNewsItemListModel(command);
            var news = _newsModelFactory.PrepareHomepageNewsItemsModel();
            var model = new NewsCategoryModel
            {
                Categories = new List<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem>(),
                CurrentCategoryId = newsCategoryId,
                CurrentCategoryName = category.Title,
                NewsItemListModel = newsItemListModel,
                BannerUrl = _pictureService
                        .GetPictureUrl(_homeSettings.BannerUserManualPageId,
                        showDefaultPicture: false)
            };
            return View("UserManual", model);
        }
        public virtual IActionResult ListRss(int languageId)
        {
            var feed = new RssFeed(
                $"{_localizationService.GetLocalized(_storeContext.CurrentStore, x => x.Name)}: News",
                "News",
                new Uri(_webHelper.GetStoreLocation()),
                DateTime.UtcNow);

            if (!_newsSettings.Enabled)
                return new RssActionResult(feed, _webHelper.GetThisPageUrl(false));

            var items = new List<RssItem>();
            var newsItems = _newsService.GetAllNews(languageId, _storeContext.CurrentStore.Id);
            foreach (var n in newsItems)
            {
                var newsUrl = Url.RouteUrl("NewsItem", new { SeName = _urlRecordService.GetSeName(n, n.LanguageId, ensureTwoPublishedLanguages: false) }, _webHelper.CurrentRequestProtocol);
                items.Add(new RssItem(n.Title, n.Short, new Uri(newsUrl), $"urn:store:{_storeContext.CurrentStore.Id}:news:blog:{n.Id}", n.CreatedOnUtc));
            }
            feed.Items = items;
            return new RssActionResult(feed, _webHelper.GetThisPageUrl(false));
        }

        public virtual IActionResult NewsItem(int newsItemId)
        {
            if (!_newsSettings.Enabled)
                return RedirectToRoute("Homepage");

            var newsItem = _newsService.GetNewsById(newsItemId);
            if (newsItem == null)
                return InvokeHttp404();

            var notAvailable =
                //published?
                !newsItem.Published ||
                //availability dates
                !_newsService.IsNewsAvailable(newsItem) ||
                //Store mapping
                !_storeMappingService.Authorize(newsItem);
            //Check whether the current user has a "Manage news" permission (usually a store owner)
            //We should allows him (her) to use "Preview" functionality
            var hasAdminAccess = _permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel) && _permissionService.Authorize(StandardPermissionProvider.ManageNews);
            if (notAvailable && !hasAdminAccess)
                return InvokeHttp404();

            var model = new NewsItemModel();
            model = _newsModelFactory.PrepareNewsItemModel(model, newsItem, true);
            model.BannerUrl = _pictureService
                        .GetPictureUrl(_homeSettings.BannerNewsPageId,
                        showDefaultPicture: false);

            //display "edit" (manage) link
            if (hasAdminAccess)
                DisplayEditLink(Url.Action("NewsItemEdit", "News", new { id = newsItem.Id, area = AreaNames.Admin }));

            return View(model);
        }

        [HttpPost, ActionName("NewsItem")]
        [AutoValidateAntiforgeryToken]
        [FormValueRequired("add-comment")]
        [ValidateCaptcha]
        public virtual IActionResult NewsCommentAdd(int newsItemId, NewsItemModel model, bool captchaValid)
        {
            if (!_newsSettings.Enabled)
                return RedirectToRoute("Homepage");

            var newsItem = _newsService.GetNewsById(newsItemId);
            if (newsItem == null || !newsItem.Published || !newsItem.AllowComments)
                return RedirectToRoute("Homepage");

            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnNewsCommentPage && !captchaValid)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptchaMessage"));
            }

            if (_customerService.IsGuest(_workContext.CurrentCustomer) && !_newsSettings.AllowNotRegisteredUsersToLeaveComments)
            {
                ModelState.AddModelError("", _localizationService.GetResource("News.Comments.OnlyRegisteredUsersLeaveComments"));
            }

            if (ModelState.IsValid)
            {
                var comment = new NewsComment
                {
                    NewsItemId = newsItem.Id,
                    CustomerId = _workContext.CurrentCustomer.Id,
                    CommentTitle = model.AddNewComment.CommentTitle,
                    CommentText = model.AddNewComment.CommentText,
                    IsApproved = !_newsSettings.NewsCommentsMustBeApproved,
                    StoreId = _storeContext.CurrentStore.Id,
                    CreatedOnUtc = DateTime.UtcNow,
                };

                _newsService.InsertNewsComment(comment);

                //notify a store owner;
                if (_newsSettings.NotifyAboutNewNewsComments)
                    _workflowMessageService.SendNewsCommentNotificationMessage(comment, _localizationSettings.DefaultAdminLanguageId);

                //activity log
                _customerActivityService.InsertActivity("PublicStore.AddNewsComment",
                    _localizationService.GetResource("ActivityLog.PublicStore.AddNewsComment"), comment);

                //raise event
                if (comment.IsApproved)
                    _eventPublisher.Publish(new NewsCommentApprovedEvent(comment));

                //The text boxes should be cleared after a comment has been posted
                //That' why we reload the page
                TempData["nop.news.addcomment.result"] = comment.IsApproved
                    ? _localizationService.GetResource("News.Comments.SuccessfullyAdded")
                    : _localizationService.GetResource("News.Comments.SeeAfterApproving");

                return RedirectToRoute("NewsItem", new { SeName = _urlRecordService.GetSeName(newsItem, newsItem.LanguageId, ensureTwoPublishedLanguages: false) });
            }

            //If we got this far, something failed, redisplay form
            model = _newsModelFactory.PrepareNewsItemModel(model, newsItem, true);
            return View(model);
        }

        #endregion
    }
}