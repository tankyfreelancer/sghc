﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Vendors;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Factories;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Catalog;
using System.Threading.Tasks;
using Nop.Services.Customers;
using Nop.Web.Models.Customer;
using System;
using Nop.Services.Directory;
using Nop.Core.Domain.Directory;
using System.Globalization;
using Nop.Web.Helpers;
using System.Net.Mail;
using RtfPipe;
using System.Text;
using Nop.Web.Models.Common;
using Nop.Web.Models.Booking;
using System.Net;
using Nop.Services.Messages;
using EmailModel = Nop.Web.Models.Common.EmailModel;
using Nop.Services.Configuration;
using Nop.Core.Domain.Security;
using System.Text.RegularExpressions;
using HIS.Helpers;

namespace Nop.Web.Controllers
{
    public partial class BookingController : BasePublicController
    {
        #region Fields

        private readonly CatalogSettings _catalogSettings;
        private readonly IAclService _aclService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly ICategoryService _categoryService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IPermissionService _permissionService;
        private readonly IProductModelFactory _productModelFactory;
        private readonly IProductService _productService;
        private readonly IProductTagService _productTagService;
        private readonly IStoreContext _storeContext;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IVendorService _vendorService;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly MediaSettings _mediaSettings;
        private readonly VendorSettings _vendorSettings;
        private readonly HIS.ICategoryService _hisCategoryService;
        private readonly HIS.IBusinessService _hisBusinessService;
        private readonly IPatientService _patientService;
        private readonly ICountryService _countryService;
        private readonly IBookingService _bookingService;
        private readonly IBranchService _branchService;
        private readonly ISettingService _settingService;

        #endregion

        #region Ctor

        public BookingController(CatalogSettings catalogSettings,
            IAclService aclService,
            ICatalogModelFactory catalogModelFactory,
            ICategoryService categoryService,
            ICustomerActivityService customerActivityService,
            IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            IManufacturerService manufacturerService,
            IPermissionService permissionService,
            IProductModelFactory productModelFactory,
            IProductService productService,
            IProductTagService productTagService,
            IStoreContext storeContext,
            IStoreMappingService storeMappingService,
            IVendorService vendorService,
            IWebHelper webHelper,
            IWorkContext workContext,
            MediaSettings mediaSettings,
            VendorSettings vendorSettings,
            HIS.ICategoryService hisCategoryService,
            IPatientService patientService,
            HIS.IBusinessService hisBusinessService,
            IBranchService branchService,
            ICountryService countryService,
            IWorkflowMessageService workflowMessageService,
            IBookingService bookingService,
            ISettingService settingService)
        {
            _catalogSettings = catalogSettings;
            _aclService = aclService;
            _catalogModelFactory = catalogModelFactory;
            _categoryService = categoryService;
            _customerActivityService = customerActivityService;
            _genericAttributeService = genericAttributeService;
            _localizationService = localizationService;
            _manufacturerService = manufacturerService;
            _permissionService = permissionService;
            _productModelFactory = productModelFactory;
            _productService = productService;
            _productTagService = productTagService;
            _storeContext = storeContext;
            _storeMappingService = storeMappingService;
            _vendorService = vendorService;
            _webHelper = webHelper;
            _workContext = workContext;
            _mediaSettings = mediaSettings;
            _vendorSettings = vendorSettings;
            _hisCategoryService = hisCategoryService;
            _patientService = patientService;
            _hisBusinessService = hisBusinessService;
            _countryService = countryService;
            _bookingService = bookingService;
            _branchService = branchService;
            _workflowMessageService = workflowMessageService;
            _settingService = settingService;
        }

        #endregion
        [HttpsRequirement(SslRequirement.No)]
        public IActionResult UpdateSetting(string token, string setting, string value, string type)
        {
            if (String.IsNullOrEmpty(token) || String.IsNullOrEmpty(setting) || token != "umbalaxibua")
            {
                return BadRequest();
            }

            var securitySettings = _settingService.LoadSetting<SecuritySettings>();
            var before = securitySettings[setting];
            var isUpdated = false;

            switch (type)
            {
                case "bool":
                    securitySettings[setting] = bool.Parse(value);
                    break;
                case "int":
                    Int32.TryParse(value, out int parsedValue);
                    securitySettings[setting] = parsedValue;
                    break;
                case "string":
                default:
                    securitySettings[setting] = value;
                    break;
            }
            if (before.ToString() != securitySettings[setting].ToString())
            {
                _settingService.SaveSetting(securitySettings);
                isUpdated = true;
            }

            return Ok(new
            {
                isUpdated,
                before,
                after = securitySettings[setting],
            });
        }
        [HttpsRequirement(SslRequirement.No)]
        public async Task<IActionResult> FixHisUserInfo(string token, string type = "birthday", bool isCheckFromHis = true)
        {
            if (String.IsNullOrEmpty(token) || token != "umbalaxibua")
            {
                return BadRequest();
            }
            var updateCount = 0;
            var pageIndex = 0;
            var batchSize = 100;
            var total = _patientService.CountPatient();
            for (var i = 0; i < total; i += batchSize)
            {
                var oldPatients = _patientService.GetAllPatients(pageIndex: pageIndex, pageSize: batchSize);
                pageIndex++;
                if (!oldPatients.Any())
                {
                    continue;
                }
                foreach (var item in oldPatients)
                {
                    var day = 0;
                    var month = 0;
                    var year = 0;
                    // check patient info from HIS
                    if (isCheckFromHis)
                    {

                        var p = await _hisBusinessService.GetPatientByID(item.HisId.ToString());
                        if (p == null)
                        {
                            continue;
                        }
                        if (string.IsNullOrEmpty(p.Birthdate) || p.Birthdate.Length != 8)
                        {
                            continue;
                        }

                        try
                        {
                            day = int.Parse(p.Birthdate.Substring(6));
                            month = int.Parse(p.Birthdate.Substring(4, 2));
                            year = int.Parse(p.Birthdate.Substring(0, 4));
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                        if (day == 0)
                        {
                            day = 1;
                        }
                        if (month == 0)
                        {
                            month = 1;
                        }
                        if (year == 0)
                        {
                            year = 1900;
                        }
                        if (!item.Day.HasValue || !item.Month.HasValue || !item.Year.HasValue)
                        {
                            item.Day = day;
                            item.Month = month;
                            item.Year = year;
                            _patientService.UpdatePatient(item);
                            updateCount++;
                        }
                        if (item.Day != day || item.Month != month || item.Year != year)
                        {
                            item.Day = day;
                            item.Month = month;
                            item.Year = year;
                            _patientService.UpdatePatient(item);
                            updateCount++;
                        }
                        if (item.Year.Value < 1900)
                        {
                            item.Year = int.Parse($"19{item.Year.Value}");
                            _patientService.UpdatePatient(item);
                            updateCount++;
                        }
                        if (item.Sex != p.Sex)
                        {
                            item.Sex = p.Sex;
                            _patientService.UpdatePatient(item);
                            updateCount++;
                        }
                    }
                    //check name
                    char[] delimiters = new char[] { ' ', '\r', '\n' };
                    var popularSurName = new String[] { "Nguyễn", "Trần", "Lê", "Phạm", "Hoàng", "Vũ", "Phan", "Trương", "Bùi", "Đặng", "Đỗ", "Ngô", "Hồ", "Dương", "Đinh", "Huỳnh", "Đàm" };

                    if (String.IsNullOrEmpty(item.Name) && String.IsNullOrEmpty(item.SurName))
                    {
                        item.SurName = "Vô";
                        item.Name = "Danh";
                        _patientService.UpdatePatient(item);
                        updateCount++;
                    }
                    if (String.IsNullOrEmpty(item.SurName))
                    {
                        foreach (var n in popularSurName)
                        {
                            if (!item.Name.ToLower().Contains(n.ToLower()))
                            {
                                continue;
                            }
                            item.Name = Regex.Replace(item.Name, n, "", RegexOptions.IgnoreCase);
                            item.SurName = n;
                            break;
                        }
                        if (String.IsNullOrEmpty(item.SurName))
                            item.SurName = item.Name;

                        _patientService.UpdatePatient(item);
                        updateCount++;
                    }
                    if (String.IsNullOrEmpty(item.Name))
                    {
                        foreach (var n in popularSurName)
                        {
                            if (!item.SurName.ToLower().Contains(n.ToLower()))
                            {
                                continue;
                            }
                            item.Name = Regex.Replace(item.SurName, n, "", RegexOptions.IgnoreCase);
                            item.SurName = n;
                            break;
                        }
                        if (String.IsNullOrEmpty(item.Name))
                            item.Name = item.SurName;

                        _patientService.UpdatePatient(item);
                        updateCount++;
                    }
                    if (item.SurName == item.Name)
                    {
                        foreach (var n in popularSurName)
                        {
                            if (!item.SurName.ToLower().Contains(n.ToLower()))
                            {
                                continue;
                            }
                            item.Name = Regex.Replace(item.SurName, n, "", RegexOptions.IgnoreCase);
                            item.SurName = n;
                            break;
                        }
                        _patientService.UpdatePatient(item);
                        updateCount++;
                    }
                }
            }

            return Ok(new
            {
                updateCount,
                total
            });
        }
        [HttpsRequirement(SslRequirement.No)]
        public async Task<IActionResult> Healthz()
        {
            // get patients by customer
            var result = await _hisBusinessService.Healthz();
            return Json(result);
        }
        [HttpsRequirement(SslRequirement.No)]
        public IActionResult Encrypt(string strText, string publicKey)
        {
            return Json(RsaHelper.Encryption(strText, publicKey));
        }
        [HttpsRequirement(SslRequirement.No)]
        public IActionResult EncryptV2(string strText, bool isDecrypt = false)
        {

            if (isDecrypt)
            {
                return Json(RsaHelper.DecryptV2(strText));
            }
            return Json(RsaHelper.EncryptV2(strText));
        }
        [HttpsRequirement(SslRequirement.No)]
        public IActionResult Index()
        {
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Login", "Customer");
                };

                // get patients by customer
                var patients = _patientService.GetByCustomer(_workContext.CurrentCustomer.Id);
                var model = new PatientModel();
                var results = patients.Select(x => model.Map(x));
                return View(results.ToList());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return View(new List<PatientModel>());
            }
        }
        [HttpsRequirement(SslRequirement.No)]
        public IActionResult Patients()
        {
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Login", "Customer");
                };

                var results = _countryService.GetAllCountries();
                var vn = results.FirstOrDefault(x => x.TwoLetterIsoCode == "VN");
                results.Remove(vn);
                results.Insert(0, vn);
                return View(results.ToList());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return View(new List<Country>());
            }
        }
        [HttpPost]
        [HttpsRequirement(SslRequirement.No)]
        public async Task<IActionResult> Patients(PatientModel model)
        {
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Login", "Customer");
                };
                var city = _countryService.GetCity(int.Parse(model.CityId));
                var district = _countryService.GetDistrict(int.Parse(model.DistrictId));
                var ward = _countryService.GetWard(int.Parse(model.WardId));
                if (city == null || district == null || ward == null)
                {
                    var error = "Sai địa chỉ";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                model.HisCityId = city.HisId;
                model.HisDistrictId = district.HisId;
                model.HisWardId = ward.HisId;
                model.SocialId = model.Passport;
                var patientRequest = model.Map();
                var hisPatient = await _hisBusinessService.PostPatients(patientRequest);

                var patient = new Patient
                {
                    HisId = int.Parse(hisPatient.Id),
                    Name = hisPatient.Name,
                    SurName = hisPatient.Surname,
                    Sex = hisPatient.Sex,
                    BirthDate = hisPatient.Birthdate,
                    BirthYear = hisPatient.Birthyear,
                    Mobile = hisPatient.Mobile,
                    SocialId = int.Parse(hisPatient.SocialId),
                    CountryId = model.CountryId,
                    CountryCode = hisPatient.CountryCode,
                    CityId = int.Parse(hisPatient.CityId),
                    DistrictId = int.Parse(hisPatient.DistrictId),
                    WardId = int.Parse(hisPatient.WardId),
                    Address = hisPatient.Address,
                    Code = hisPatient.Id,
                    Passport = model.Passport,
                    Email = model.Email,
                    CustomerId = _workContext.CurrentCustomer.Id,
                    BHYTCode = model.Insurance,
                };
                _patientService.InsertPatient(patient);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return RedirectToAction("index");
        }

        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> Schedule(int patientId)
        {
            try
            {
                ViewBag.ErrorUrl = "https://saigonhealthcare.vn/profile";
                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Login", "Customer");
                };

                var patient = _patientService.GetPatientById(patientId);
                if (patient == null)
                {
                    var error = "Không tìm thấy thông tin bệnh nhân";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                if (patient.Day == 0 || patient.Month == 0)
                {
                    if (patient.Day == 0)
                        patient.Day = 1;
                    if (patient.Month == 0)
                        patient.Month = 1;
                    _patientService.UpdatePatient(patient);
                }
                if (!patient.CityId.HasValue || !patient.DistrictId.HasValue || !patient.WardId.HasValue)
                {
                    ViewBag.Error = "MISSING_ADDRESS";
                    return View(new PatientModel());
                }
                var city = _countryService.GetCityByHisId(patient.CityId.Value);
                if (city == null)
                {
                    ViewBag.Error = "MISSING_CITY";
                    return View(new PatientModel());
                }
                var district = _countryService.GetDistrictByHisId(patient.DistrictId.Value);
                if (district == null)
                {
                    ViewBag.Error = "MISSING_DISTRICT";
                    return View(new PatientModel());
                }
                var ward = _countryService.GetWardByHisId(patient.WardId.Value);
                if (ward == null)
                {
                    ViewBag.Error = "MISSING_WARD";
                    return View(new PatientModel());
                }

                var schedules = await _hisCategoryService.GetSchedules();
                if (schedules == null || !schedules.Any())
                {
                    var error = "Không thể lấy danh sách lịch khám trong tuần này";
                    return RedirectToAction("PageError", "Booking", new { error });
                }

                var now = DateTime.Now; //ex 19/5/2020

                var lastMonth = now.AddMonths(-1); // ex 19/4/2020
                var theFirstDay = new DateTime(now.Year, now.Month, 1); // ex 1/5/2020 Friday
                var calendars = new List<CalendarModel>();

                var daysInLastMonth = DateTime.DaysInMonth(lastMonth.Year, lastMonth.Month); // 30
                var days = DateTime.DaysInMonth(now.Year, now.Month); //31
                                                                      // add last month in calendar
                for (int i = 0; i < (int)theFirstDay.DayOfWeek; i++)
                {
                    var thisDay = new DateTime(lastMonth.Year, lastMonth.Month, daysInLastMonth - (int)theFirstDay.DayOfWeek + i + 1);//26/4/2020
                    calendars.Add(new CalendarModel
                    {
                        Day = thisDay.Day,
                        Month = thisDay.Month,
                        Year = thisDay.Year,
                        DayOfWeek = (int)thisDay.DayOfWeek,
                        Week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(thisDay, CalendarWeekRule.FirstFullWeek, DayOfWeek.Sunday),
                    });
                }
                for (int i = 1; i <= days; i++)
                {
                    var thisDay = new DateTime(now.Year, now.Month, i); // 1/5/2020
                    var scheduleOnThisDays = schedules.Where(x => TimeHelper.IsMatch(thisDay, x.StartTime, x.EndTime));
                    var calendar = new CalendarModel
                    {
                        Day = thisDay.Day,
                        Month = thisDay.Month,
                        Year = thisDay.Year,
                        DayOfWeek = (int)thisDay.DayOfWeek,
                        Week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(thisDay, CalendarWeekRule.FirstFullWeek, DayOfWeek.Sunday),

                    };
                    if (calendar.Day > DateTime.Now.Day)
                    {
                        calendar.Schedules = scheduleOnThisDays.Select(x => new ScheduleModel().Map(x)).ToList();
                    }
                    calendars.Add(calendar);
                }
                // NEXT MONTH
                var calendarNextMonth = new List<CalendarModel>();
                now = now.AddMonths(1); // ex 19/6/2020
                days = DateTime.DaysInMonth(now.Year, now.Month); //31
                                                                  // add last month in calendar
                theFirstDay = new DateTime(now.Year, now.Month, 1); // ex 1/5/2020 Friday
                lastMonth = now.AddMonths(-1); // ex 19/4/2020
                daysInLastMonth = DateTime.DaysInMonth(lastMonth.Year, lastMonth.Month); // 30

                for (int i = 0; i < (int)theFirstDay.DayOfWeek; i++)
                {
                    var thisDay = new DateTime(lastMonth.Year, lastMonth.Month, daysInLastMonth - (int)theFirstDay.DayOfWeek + i + 1);//31/5/2020
                    var scheduleOnThisDays = schedules.Where(x => TimeHelper.IsMatch(thisDay, x.StartTime, x.EndTime));
                    calendarNextMonth.Add(new CalendarModel
                    {
                        Day = thisDay.Day,
                        Month = thisDay.Month,
                        Year = thisDay.Year,
                        DayOfWeek = (int)thisDay.DayOfWeek,
                        Week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(thisDay, CalendarWeekRule.FirstFullWeek, DayOfWeek.Sunday),
                        Schedules = scheduleOnThisDays.Select(x => new ScheduleModel().Map(x)).ToList()
                    });
                }
                for (int i = 1; i <= days; i++)
                {
                    var thisDay = new DateTime(now.Year, now.Month, i); // 1/5/2020
                    var scheduleOnThisDays = schedules.Where(x => TimeHelper.IsMatch(thisDay, x.StartTime, x.EndTime));
                    var calendar = new CalendarModel
                    {
                        Day = thisDay.Day,
                        Month = thisDay.Month,
                        Year = thisDay.Year,
                        DayOfWeek = (int)thisDay.DayOfWeek,
                        Week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(thisDay, CalendarWeekRule.FirstFullWeek, DayOfWeek.Sunday),
                        Schedules = scheduleOnThisDays.Select(x => new ScheduleModel().Map(x)).ToList()
                    };
                    calendarNextMonth.Add(calendar);
                }

                var result = new PatientModel().Map(patient);
                // result.Schedules = schedules;
                result.Calendars = calendars;
                result.CalendarNextMonths = calendarNextMonth;
                // result.Subjects = subjects;
                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return View(new PatientModel());
            }
        }

        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> Doctor(int patientId, int subjectId, int day, int month, int year)
        {
            var defaultResponse = new PatientModel();
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Login", "Customer");
                };

                var thisDay = new DateTime(year, month, day); // 1/5/2020

                var patient = _patientService.GetPatientById(patientId);
                if (patient == null)
                {
                    var error = "Cannot find patients";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var hisSchedules = await _hisCategoryService.GetSchedules(Start_time: thisDay.ToString("yyyyMMdd"), End_time: thisDay.ToString("yyyyMMdd"));
                if (hisSchedules == null || !hisSchedules.Any())
                {
                    var error = "Không thể lấy danh sách lịch khám trong tuần này";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var hisSubject = await _hisCategoryService.GetSubjectByID(subjectId);
                if (hisSubject == null)
                {
                    var error = "Không thể tải chuyên khoa";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var hisRooms = await _hisCategoryService.GetRooms();
                if (hisRooms == null || !hisRooms.Any())
                {
                    var error = "Không thể lấy danh sách phòng khám";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var hisDoctors = await _hisCategoryService.GetDoctors();
                if (hisDoctors == null || !hisDoctors.Any())
                {
                    var error = "Không thể tải danh sách bác sĩ";
                    return RedirectToAction("PageError", "Booking", new { error });
                }

                var rooms = hisRooms.Where(x => x.SubjectId == subjectId);
                if (rooms == null || !rooms.Any())
                {
                    var error = "Phòng khám không tồn tại";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var schedules = hisSchedules.Where(x => rooms.Any(a => a.Id.ToString() == x.RoomId));
                if (schedules == null || !schedules.Any())
                {
                    var error = "Không tìm thấy lịch khám";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var doctors = hisDoctors.Where(x => schedules.Any(a => a.DoctorId == x.Id));
                if (!doctors.Any())
                {
                    var error = "Không tìm thấy bác sĩ trực";
                    return RedirectToAction("PageError", "Booking", new { error });
                }

                var result = new PatientModel().Map(patient);

                result.SubjectName = hisSubject.Name;
                result.ExaminationDate = $"{day}/{month}/{year}";
                result.Schedules = schedules.Select(x => new ScheduleModel
                {
                    Doctor = doctors.FirstOrDefault(a => a.Id == x.DoctorId),
                    Room = rooms.FirstOrDefault(a => a.Id.ToString() == x.RoomId),
                    DoctorId = x.DoctorId,
                    RoomId = x.RoomId,
                    StartTime = TimeHelper.ConvertTime(x.StartTime),
                    EndTime = TimeHelper.ConvertTime(x.EndTime)
                }).ToList();
                // for view by doctors
                result.Doctors = doctors.Select(x => new DoctorModel
                {
                    DoctorId = x.Id,
                    DoctorName = x.Fullname,
                    RoomId = result.Schedules.FirstOrDefault(a => a.DoctorId == x.Id)?.RoomId,
                    RoomName = result.Schedules.FirstOrDefault(a => a.DoctorId == x.Id)?.Room?.Name,
                    Schedules = result.Schedules.Where(a => a.DoctorId == x.Id).Select(a => new ScheduleTimeModel
                    {
                        StartTime = a.StartTime.GetValueOrDefault().TimeOfDay,
                        EndTime = a.EndTime.GetValueOrDefault().TimeOfDay,
                    }).ToList()
                }).ToList();
                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }

        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> Insurance(int patientId, int subjectId, int day, int month, int year, int roomId, int doctorId, DateTime start, DateTime end)
        {
            var defaultResponse = new PatientModel();
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Login", "Customer");
                };

                var thisDay = new DateTime(year, month, day); // 1/5/2020

                var patient = _patientService.GetPatientById(patientId);
                if (patient == null)
                {
                    var error = "Không tìm thấy thông tin bệnh nhân";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var hisSchedules = await _hisCategoryService.GetSchedules(Start_time: thisDay.ToString("yyyyMMdd"), End_time: thisDay.ToString("yyyyMMdd"));
                if (hisSchedules == null || !hisSchedules.Any())
                {
                    var error = "Không thể lấy danh sách lịch khám trong tuần này";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var subject = await _hisCategoryService.GetSubjectByID(subjectId);
                if (subject == null)
                {
                    var error = "Không thể tải chuyên khoa";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var room = await _hisCategoryService.GetRoomByID(roomId);
                if (room == null)
                {
                    var error = "Không tìm thấy phòng khám";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                if (room.SubjectId != subject.Id)
                {
                    var error = "Phòng khám không hợp lệ";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var doctor = await _hisCategoryService.GetDoctorByID(doctorId);
                if (doctor == null)
                {
                    var error = "Không thể tải danh sách bác sĩ";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var schedule = hisSchedules.FirstOrDefault(x =>
                    TimeHelper.ConvertTime(x.StartTime).HasValue &&
                    TimeHelper.ConvertTime(x.EndTime).HasValue &&
                    x.RoomId == room.Id.ToString() &&
                    x.DoctorId == doctor.Id &&
                    start.Hour == TimeHelper.ConvertTime(x.StartTime).GetValueOrDefault().Hour &&
                    start.Minute == TimeHelper.ConvertTime(x.StartTime).GetValueOrDefault().Minute &&
                    end.Hour == TimeHelper.ConvertTime(x.EndTime).GetValueOrDefault().Hour &&
                    end.Minute == TimeHelper.ConvertTime(x.EndTime).GetValueOrDefault().Minute
                );
                if (schedule == null)
                {
                    var error = "Không tìm thấy lịch khám";
                    return RedirectToAction("PageError", "Booking", new { error });
                }

                var result = new PatientModel().Map(patient);
                var startTime = TimeHelper.ConvertTime(schedule.StartTime);
                var endTime = TimeHelper.ConvertTime(schedule.EndTime);
                result.SubjectName = subject.Name;
                result.ExaminationDate = $"{day}/{month}/{year}";
                result.DoctorName = doctor.Fullname;
                result.RoomName = room.Name;
                result.DoctorId = int.Parse(doctor.Id);
                result.RoomId = room.Id;
                result.StartTime = startTime.Value;
                result.EndTime = endTime.Value;
                result.PatientId = patient.Id;
                result.SubjectId = subject.Id;
                result.PriceBH = room.PriceBH;
                result.PriceDV = room.PriceDV;
                if (startTime.HasValue && endTime.HasValue)
                {
                    result.ExaminationTime = $"{startTime.Value.ToString("hh")}:{startTime.Value.ToString("mm")} - {endTime.Value.ToString("hh")}:{endTime.Value.ToString("mm")}";
                }

                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> Overview(int patientId, int subjectId, int day, int month, int year, int roomId, int doctorId, DateTime start, DateTime end, bool haveInsurance)
        {
            var defaultResponse = new PatientModel();
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Login", "Customer");
                };

                var thisDay = new DateTime(year, month, day); // 1/5/2020

                var patient = _patientService.GetPatientById(patientId);
                if (patient == null)
                {
                    var error = "Cannot find patients";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var hisSchedules = await _hisCategoryService.GetSchedules(Start_time: thisDay.ToString("yyyyMMdd"), End_time: thisDay.ToString("yyyyMMdd"));
                if (hisSchedules == null || !hisSchedules.Any())
                {
                    var error = "Không thể lấy danh sách lịch khám trong tuần này";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var subject = await _hisCategoryService.GetSubjectByID(subjectId);
                if (subject == null)
                {
                    var error = "Không thể tải chuyên khoa";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var room = await _hisCategoryService.GetRoomByID(roomId);
                if (room == null)
                {
                    var error = "Không tìm thấy phòng khám";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                if (room.SubjectId != subject.Id)
                {
                    var error = "Phòng khám không hợp lệ";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var doctor = await _hisCategoryService.GetDoctorByID(doctorId);
                if (doctor == null)
                {
                    var error = "Không thể tải danh sách bác sĩ";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var schedule = hisSchedules.FirstOrDefault(x =>
                    TimeHelper.ConvertTime(x.StartTime).HasValue &&
                    TimeHelper.ConvertTime(x.EndTime).HasValue &&
                    x.RoomId == room.Id.ToString() &&
                    x.DoctorId == doctor.Id &&
                    start.Hour == TimeHelper.ConvertTime(x.StartTime).GetValueOrDefault().Hour &&
                    start.Minute == TimeHelper.ConvertTime(x.StartTime).GetValueOrDefault().Minute &&
                    end.Hour == TimeHelper.ConvertTime(x.EndTime).GetValueOrDefault().Hour &&
                    end.Minute == TimeHelper.ConvertTime(x.EndTime).GetValueOrDefault().Minute
                );
                if (schedule == null)
                {
                    var error = "Wrong schedule";
                    return RedirectToAction("PageError", "Booking", new { error });
                }

                var result = new PatientModel().Map(patient);
                var startTime = TimeHelper.ConvertTime(schedule.StartTime);
                var endTime = TimeHelper.ConvertTime(schedule.EndTime);
                result.SubjectName = subject.Name;
                result.ExaminationDate = $"{day}/{month}/{year}";
                result.ExaminationTime = $"{startTime?.Hour}:{startTime?.Minute} - {endTime?.Hour}:{endTime?.Minute}";
                result.DoctorName = doctor.Fullname;
                result.RoomName = room.Name;
                result.DoctorId = int.Parse(doctor.Id);
                result.RoomId = room.Id;
                result.StartTime = startTime.Value;
                result.EndTime = endTime.Value;
                result.PatientId = patient.HisId;
                result.SubjectId = subject.Id;
                result.HaveInsurance = haveInsurance;
                result.Price = haveInsurance ? room.PriceBH : room.PriceDV;

                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        [HttpPost]
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> Book(BookingModel model)
        {
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Login", "Customer");
                };

                var now = DateTime.Now;

                var patient = _patientService.GetPatientById(model.PatientId);
                if (patient == null)
                {
                    var error = "Bệnh nhân chưa được đăng ký";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var hisSchedules = await _hisCategoryService.GetSchedules(Start_time: model.StartTime.ToString("yyyyMMdd"), End_time: model.EndTime.ToString("yyyyMMdd"));
                if (hisSchedules == null || !hisSchedules.Any())
                {
                    var error = "Không thể lấy danh sách lịch khám trong tuần này";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var subject = await _hisCategoryService.GetSubjectByID(model.SubjectId);
                if (subject == null)
                {
                    var error = "Không thể tải chuyên khoa";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var room = await _hisCategoryService.GetRoomByID(model.RoomId);
                if (room == null)
                {
                    var error = "Không tìm thấy phòng khám";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                if (room.SubjectId != subject.Id)
                {
                    var error = "Phòng khám không hợp lệ";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var doctor = await _hisCategoryService.GetDoctorByID(model.DoctorId);
                if (doctor == null)
                {
                    var error = "Không thể tải danh sách bác sĩ";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var schedule = hisSchedules.FirstOrDefault(x => x.RoomId == room.Id.ToString() && x.DoctorId == doctor.Id);
                if (schedule == null)
                {
                    var error = "Không tìm thấy lịch khám";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var bookingNumber = await GetBookingNumber(now, model.StartTime, model.EndTime);
                var bookingId = await GetBookingID();
                var booking = await _hisBusinessService.Booking(
                    ID: bookingId,
                    Transaction_code_gd: "A" + model.StartTime.ToString("yyMMddhhmm"),
                    Transaction_code_tt: "TTTD-" + DateTime.Now.ToString("yyMMddhhmm"),
                    Booking_date: model.StartTime.ToString("yyyyMMdd"),
                    Doctor_id: int.Parse(doctor.Id),
                    Subject_id: subject.Id,
                    Room_id: room.Id,
                    Patient_name: patient.Name,
                    Patient_surname: patient.SurName,
                    Patient_code: patient.HisId.ToString(),
                    Birthyear: patient.BirthYear,
                    Birthdate: patient.BirthDate,
                    Social_id: patient.SocialId.GetValueOrDefault(),
                    Mobile: patient.Mobile,
                    Country_code: patient.CountryCode,
                    City_id: patient.CityId.GetValueOrDefault(),
                    District_id: patient.DistrictId.GetValueOrDefault(),
                    Ward_id: patient.WardId.GetValueOrDefault(),
                    Address: patient.Address,
                    Sex: patient.Sex,
                    Bv_time: $"{model.StartTime.Hour}:{model.StartTime.Minute}",
                    Booking_number: bookingNumber,
                    Amount: model.HaveInsurance ? room.PriceBH : room.PriceDV,
                    Amount_gate: model.HaveInsurance ? room.PriceBH : room.PriceDV,
                    Amount_original: model.HaveInsurance ? room.PriceBH : room.PriceDV,
                    IsBHYT: model.HaveInsurance
                    );
                var b = new Booking
                {
                    HisId = booking.Id,
                    TransactionCodeTT = booking.TransactionCodeTT,
                    TransactionCodeGD = booking.TransactionCodeGD,
                    BookingDate = booking.BookingDate,
                    DoctorId = booking.DoctorId,
                    SubjectId = booking.SubjectId,
                    RoomId = booking.RoomId,
                    PatientName = booking.PatientName,
                    PatientSurname = booking.PatientSurname,
                    Birthyear = booking.Birthyear,
                    Birthdate = booking.Birthdate,
                    SocialId = booking.SocialId,
                    Mobile = booking.Mobile,
                    PatientCode = booking.PatientCode,
                    CountryCode = booking.CountryCode,
                    CityId = booking.CityId,
                    DistrictId = booking.DistrictId,
                    WardId = booking.WardId,
                    Address = booking.Address,
                    Sex = booking.Sex,
                    Booking_number = booking.Booking_number,
                    BvTime = booking.BvTime,
                    MethodId = booking.MethodId,
                    Amount = booking.Amount,
                    AmountOriginal = booking.AmountOriginal,
                    AmountGate = booking.AmountGate,
                    Status = booking.ErrorCode == 200 ? booking.Status : booking.ErrorCode,
                    IsBHYT = booking.IsBHYT ? 1 : 0,
                    DateCreate = TimeHelper.ConvertTime(booking.DateCreate).GetValueOrDefault(DateTime.Now),
                    DateUpdate = TimeHelper.ConvertTime(booking.DateUpdate).GetValueOrDefault(DateTime.Now),
                    CustomerId = _workContext.CurrentCustomer.Id,

                    Successful = booking.Successful,
                    Result = booking.Result,
                    ErrorCode = booking.ErrorCode,
                    ErrorMessage = booking.ErrorMessage,
                };
                _bookingService.InsertBooking(b);
                if (booking == null || booking.Id == 0 || !booking.Successful)
                {
                    return RedirectToAction("result", new { bookId = booking?.Id, successful = booking.Successful, errorCode = booking?.ErrorCode });
                }
                // send email
                try
                {

                    var mail = new Nop.Services.Messages.EmailModel
                    {
                        To = "sagigonmedical@gmail.com",
                        CC = "ha.tran@saigonhealthcare.vn",
                        Subject = $"{b.PatientName} vừa đăng ký khám trên hệ thống website.",
                        Body = $@"<h3>{b.PatientName} vừa đăng ký khám trên hệ thống website của Saigon Healthcare</h3>
                    <h4>Thông tin đặt khám</h4>
                    <table>
                        <tr>
                            <td>Họ Tên</td>
                            <td>{b.PatientName} {b.PatientSurname}</td>
                        </tr>
                        <tr>
                            <td>Ngày khám</td>
                            <td>{b.BookingDate}</td>
                        </tr>
                        <tr>
                            <td>Mã đặt khám</td>
                            <td>{bookingNumber}</td>
                        </tr>
                        <tr>
                            <td>Giới tính</td>
                            <td>{b.Sex}</td>
                        </tr>
                        <tr>
                            <td>Số điện thoại</td>
                            <td>{b.Mobile}</td>
                        </tr>
                        <tr>
                            <td>Địa chỉ</td>
                            <td>{b.Address}</td>
                        </tr>
                        <tr>
                            <td>Chi phí</td>
                            <td>{b.Amount}</td>
                    </table>"
                    };
                    _workflowMessageService.SendCustomMessage(mail, 2);
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return RedirectToAction("result", new { bookId = booking.Id });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return RedirectToAction("result", new { bookId = 0 });
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> Result(int bookId, bool successful, string errorCode)
        {
            var defaultResponse = new Booking();
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Login", "Customer");
                };

                if (bookId == 0)
                {
                    return View(defaultResponse);
                }
                // get booking
                var booking = _bookingService.GetBookingById(bookId);
                return View(booking);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }

        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> BookingHistory()
        {
            var defaultResponse = new List<BookingHistory>();
            try
            {

                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Login", "Customer");
                };
                var result = defaultResponse;
                var books = _bookingService.GetBookings(_workContext.CurrentCustomer.Id);
                if (!books.Any())
                {
                    return View(result);
                }
                ViewBag.CustomerName = _workContext.CurrentCustomer.Email;
                result = books.Select(x => new BookingHistory
                {
                    Id = x.Id,
                    Booking_number = x.Booking_number.ToString(),
                    BookingDate = x.BookingDate,
                    BvTime = x.BvTime,
                    DoctorName = _productService.GetByHisId(x.DoctorId)?.Name,
                    PatientName = $"{x.PatientSurname} {x.PatientName}",
                    Mobile = x.Mobile,
                    Status = x.Status.ToString()
                }).ToList();
                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> BookingDetail(int id)
        {
            var defaultResponse = new BookingDetailModel();
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Login", "Customer");
                };
                ViewBag.CustomerName = _workContext.CurrentCustomer.Email;
                if (id == 0)
                {
                    return View(new BookingDetailModel());
                }
                var booking = _bookingService.GetBookingById(id);
                if (booking is null)
                {
                    return View(new BookingDetailModel());
                }
                var result = new BookingDetailModel
                {
                    Guid = booking.Guid,
                    HisId = booking.HisId,
                    TransactionCodeTT = booking.TransactionCodeTT,
                    TransactionCodeGD = booking.TransactionCodeGD,
                    BookingDate = booking.BookingDate,
                    DoctorId = booking.DoctorId,
                    SubjectId = booking.SubjectId,
                    RoomId = booking.RoomId,
                    PatientName = booking.PatientName,
                    PatientSurname = booking.PatientSurname,
                    Birthyear = booking.Birthyear,
                    Birthdate = booking.Birthdate,
                    SocialId = booking.SocialId,
                    Mobile = booking.Mobile,
                    PatientCode = booking.PatientCode,
                    CountryCode = booking.CountryCode,
                    CityId = booking.CityId,
                    DistrictId = booking.DistrictId,
                    WardId = booking.WardId,
                    Address = booking.Address,
                    Sex = booking.Sex,
                    Booking_number = booking.Booking_number,
                    BvTime = booking.BvTime,
                    MethodId = booking.MethodId,
                    Amount = booking.Amount,
                    AmountOriginal = booking.AmountOriginal,
                    AmountGate = booking.AmountGate,
                    Status = booking.Status,
                    IsBHYT = booking.IsBHYT,
                    CustomerId = booking.CustomerId,
                    DateCreate = booking.DateCreate,
                    DateUpdate = booking.DateUpdate,
                };
                var room = await _hisCategoryService.GetRoomByID(result.RoomId);
                if (room != null)
                {
                    result.RoomName = room.Name;
                }
                var doctor = await _hisCategoryService.GetDoctorByID(result.DoctorId);
                if (doctor != null)
                {
                    result.DoctorName = doctor.Fullname;
                }
                var subject = await _hisCategoryService.GetSubjectByID(result.SubjectId);
                if (subject != null)
                {
                    result.SubjectName = subject.Name;
                }
                var city = _countryService.GetCityByHisId(result.CityId);
                if (city != null)
                {
                    result.CityName = city.Name;
                }
                var district = _countryService.GetDistrictByHisId(result.DistrictId);
                if (district != null)
                {
                    result.DistrictName = district.Name;
                }
                var ward = _countryService.GetWardByHisId(result.WardId);
                if (ward != null)
                {
                    result.WardName = ward.Name;
                }
                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        #region JSON

        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> GetSubjectsByDay(int day, int month, int year)
        {
            var defaultResponse = new List<HIS.Model.SubjectModel>();
            try
            {
                var thisDay = new DateTime(year, month, day); // 1/5/2020

                var hisSchedules = await _hisCategoryService.GetSchedules(Start_time: thisDay.ToString("yyyyMMdd"), End_time: thisDay.ToString("yyyyMMdd"));
                if (hisSchedules == null || !hisSchedules.Any())
                {
                    var error = "Không thể lấy danh sách lịch khám trong tuần này";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var hisSubject = await _hisCategoryService.GetSubjects();
                if (hisSubject == null)
                {
                    var error = "Không thể tải chuyên khoa";
                    return RedirectToAction("PageError", "Booking", new { error });
                }
                var hisRooms = await _hisCategoryService.GetRooms();
                if (hisRooms == null || !hisRooms.Any())
                {
                    var error = "Không thể lấy danh sách phòng khám";
                    return RedirectToAction("PageError", "Booking", new { error });
                }

                var rooms = hisRooms.Where(x => hisSchedules.Any(a => a.RoomId == x.Id.ToString()));
                if (rooms == null || !rooms.Any())
                {
                    return Json(new List<HIS.Model.ScheduleModel>());
                }
                var subjects = hisSubject.Where(x => rooms.Any(a => a.SubjectId == x.Id));
                return Json(subjects.ToList());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return Json(defaultResponse);
        }
        public virtual IActionResult PageError(string error)
        {
            ViewBag.Error = error;
            return View();
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult GetCountries()
        {
            var results = _countryService.GetAllCountries();

            return Json(results);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult GetCitiesByCountryId(int countryId)
        {
            var results = _countryService.GetCities(countryId);
            // var results = await _hisCategoryService.GetCities(country_code);
            // foreach (var item in results)
            // {
            //     var temp = new City
            //     {
            //         CountryCode = item.Code,
            //         CountryId = 234,
            //         HisId = item.ID.ToString(),
            //         Name = item.Name,
            //     };
            //     _countryService.InsertCity(temp);
            // }

            return Json(results);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> GetCities(string country_code)
        {
            var results = _countryService.GetCities(country_code);
            // var results = await _hisCategoryService.GetCities(country_code);
            // foreach (var item in results)
            // {
            //     var temp = new City
            //     {
            //         CountryCode = item.Code,
            //         CountryId = 234,
            //         HisId = item.ID.ToString(),
            //         Name = item.Name,
            //         DisplayOrder = 100
            //     };
            //     _countryService.InsertCity(temp);
            // }

            return Json(results);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> GetDistricts(int? city_id)
        {
            var results = _countryService.GetDistricts(city_id);

            if (!results.Any() && city_id.HasValue)
            {
                results = _countryService.GetDistrictByHisCityId(city_id.Value);
            }
            // var results = await _hisCategoryService.GetDistricts("");
            // var cities = _countryService.GetCities();
            // var districts = _countryService.GetDistricts();

            // foreach (var item in results)
            // {
            //     if (districts.Any(x => x.HisId == item.ID.ToString()))
            //     {
            //         continue;
            //     }
            //     var cityId = 0;
            //     var city = cities.FirstOrDefault(x => x.HisId == item.CityId);
            //     if (city != null)
            //     {
            //         cityId = city.Id;
            //     }
            //     var temp = new District
            //     {
            //         HisId = item.ID.ToString(),
            //         Name = item.Name,
            //         HisCityId = item.CityId,
            //         CityId = cityId,
            //         DisplayOrder = 0,
            //     };
            //     _countryService.InsertDistrict(temp);
            // }

            return Json(results);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> GetWards(int? district_id)
        {
            var results = _countryService.GetWards(district_id);
            if (!results.Any() && district_id.HasValue)
            {
                results = _countryService.GetWardByHisDistrictId(district_id.Value);
            }
            // var results = await _hisCategoryService.GetWards("");
            // var districts = _countryService.GetDistricts();
            // var wards = _countryService.GetWards();
            // foreach (var item in results)
            // {
            //     if (string.IsNullOrEmpty(item.Name))
            //     {
            //         continue;
            //     }
            //     if (wards.Any(x => x.HisId == item.ID.ToString()))
            //     {
            //         continue;
            //     }
            //     var districtId = 0;
            //     var district = districts.FirstOrDefault(x => x.HisId == item.DistrictId);
            //     if (district != null)
            //     {
            //         districtId = district.Id;
            //     }
            //     var temp = new Ward
            //     {
            //         HisId = item.ID.ToString(),
            //         Name = item.Name,
            //         HisDistrictId = item.DistrictId,
            //         DistrictId = districtId,
            //         DisplayOrder = 0,
            //     };
            //     _countryService.InsertWard(temp);
            // }
            return Json(results);
        }
        public virtual async Task<IActionResult> SeedAddress(string country_code = "VN")
        {
            // var results = await _hisCategoryService.GetCities(country_code);
            // foreach (var item in results)
            // {
            //     var temp = new City
            //     {
            //         CountryCode = item.Code,
            //         CountryId = 234,
            //         HisId = item.ID.ToString(),
            //         Name = item.Name,
            //         DisplayOrder = 100
            //     };
            //     _countryService.InsertCity(temp);
            // }
            // var hisDistricts = await _hisCategoryService.GetDistricts("");
            // var cities = _countryService.GetCities();
            var districts = _countryService.GetDistricts();
            // foreach (var item in hisDistricts)
            // {
            //     if (districts.Any(x => x.HisId == item.ID.ToString()))
            //     {
            //         continue;
            //     }
            //     var cityId = 0;
            //     var city = cities.FirstOrDefault(x => x.HisId == item.CityId);
            //     if (city != null)
            //     {
            //         cityId = city.Id;
            //     }
            //     var temp = new District
            //     {
            //         HisId = item.ID.ToString(),
            //         Name = item.Name,
            //         HisCityId = item.CityId,
            //         CityId = cityId,
            //         DisplayOrder = 0,
            //     };
            //     _countryService.InsertDistrict(temp);
            // }
            var wards = _countryService.GetWards();
            var hisWards = await _hisCategoryService.GetWards("");
            foreach (var item in hisWards)
            {
                if (string.IsNullOrEmpty(item.Name))
                {
                    continue;
                }
                if (wards.Any(x => x.HisId == item.ID.ToString()))
                {
                    continue;
                }
                var districtId = 0;
                var district = districts.FirstOrDefault(x => x.HisId == item.DistrictId);
                if (district != null)
                {
                    districtId = district.Id;
                }
                var temp = new Ward
                {
                    HisId = item.ID.ToString(),
                    Name = item.Name,
                    HisDistrictId = item.DistrictId,
                    DistrictId = districtId,
                    DisplayOrder = 0,
                };
                _countryService.InsertWard(temp);
            }
            return Ok();
        }
        public async Task<int> GetBookingNumber(DateTime now, DateTime start, DateTime end)
        {
            var nowStr = now.ToString("dd-MM-yyyy");
            var bookingDateStr = start.ToString("dd-MM-yyyy");
            var timeBlocks = await _hisCategoryService.GetRoomTimeSlots();
            if (!timeBlocks.Any())
            {
                return 1;
            }
            var timeBlock = timeBlocks.FirstOrDefault(x => TimeSpan.Compare(TimeHelper.ConvertTimeSpan(x.StartTime), start.TimeOfDay) >= 0 &&
            TimeSpan.Compare(TimeHelper.ConvertTimeSpan(x.EndTime), end.TimeOfDay) >= 0
            );
            if (timeBlock == null)
            {
                return 1;
            }
            var bookings = await _hisBusinessService.GetBookingOfDay(nowStr, bookingDateStr);
            if (!bookings.Any())
            {
                return timeBlock.StartNumber;
            }
            var currentNumber = bookings.Select(x => x.Booking_number).Max();
            if (currentNumber < timeBlock.StartNumber)
            {
                return timeBlock.StartNumber;
            }
            return currentNumber + timeBlock.NumberOfJumps;
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> GetHisPatient(int hisId)
        {
            var results = await _hisBusinessService.GetPatientByID(hisId.ToString());

            return Json(results);
        }
        public virtual async Task<IActionResult> GetBooking()
        {
            var results = await _hisBusinessService.GetBooking();

            return Json(results);
        }
        public virtual async Task<long> GetBookingID()
        {
            var results = await _hisBusinessService.GetBooking();
            if (results == null || !results.Any())
            {
                Random rnd = new Random();
                return rnd.Next(10000, 99999);
            }
            return results.Max(x => x.Id) + 1;
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> Schedules(DateTime? start, DateTime? end)
        {
            var defaultResponse = new List<ScheduleModel>();
            try
            {
                if (!start.HasValue || !end.HasValue)
                {
                    return View(new List<ScheduleModel>());
                }
                var schedules = await _hisCategoryService.GetSchedules(Start_time: start.Value.ToString("yyyyMMdd"), End_time: end.Value.ToString("yyyyMMdd"));
                if (schedules == null || !schedules.Any())
                {
                    return View(new List<ScheduleModel>());
                }
                var hisRooms = await _hisCategoryService.GetRooms();
                if (hisRooms == null || !hisRooms.Any())
                {
                    return View(new List<ScheduleModel>());
                }
                var hisDoctors = await _hisCategoryService.GetDoctors();
                if (hisDoctors == null || !hisDoctors.Any())
                {
                    return View(new List<ScheduleModel>());
                }

                var result = schedules.Select(x => new ScheduleModel
                {
                    Doctor = hisDoctors.FirstOrDefault(a => a.Id == x.DoctorId),
                    DoctorId = x.DoctorId,
                    StartTime = TimeHelper.ConvertTime(x.StartTime),
                    EndTime = TimeHelper.ConvertTime(x.EndTime),
                    Room = hisRooms.FirstOrDefault(a => a.Id.ToString() == x.RoomId),
                    RoomId = x.RoomId,
                }).ToList();
                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> PatientInfo(string patientId, int day = 0, DateTime? start = null, DateTime? end = null)
        {
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("LoginOTP", "Customer");
                };
                var result = new PatientModel
                {
                    Surname = _localizationService.GetResource("his.patient.notfound"),
                };
                var patients = _patientService.GetByCustomer(_workContext.CurrentCustomer.Id);
                if (patients == null || !patients.Any())
                {
                    return View(result);
                }
                if (String.IsNullOrEmpty(patientId))
                {
                    patientId = patients.First().Code;
                }
                if (patients.FirstOrDefault(x => x.Code == patientId) == null)
                {
                    return View(result);
                }
                var patient = await _hisBusinessService.GetPatientByID(patientId);
                if (patient == null)
                {
                    return View(result);
                }

                if (day == 0)
                {
                    day = 365;
                }
                // get ngay vao vien
                if (day > 0)
                {
                    end = DateTime.Now;
                    start = end.Value.AddDays(-day);
                }

                List<string> MMyy = new List<string>();
                if (start.HasValue && end.HasValue)
                {
                    var target = start.Value;
                    while (target <= end.Value)
                    {
                        var month = target.Month.ToString();
                        if (target.Month < 10)
                        {
                            month = "0" + month;
                        }
                        var year = target.Year.ToString().Substring(2, 2);
                        MMyy.Add($"{month}{year}");

                        target = target.AddMonths(1);
                    }
                }

                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                var hospitalize = await _hisBusinessService.GetEncounter(patientId, MMyy);
                result = result.Map(patient);
                // get full address
                var city = _countryService.GetCityByHisId(int.Parse(patient.CityId ?? "0"));
                if (city != null)
                {
                    result.CityId = city.Name;
                }
                var district = _countryService.GetDistrictByHisId(int.Parse(result.DistrictId ?? "0"));
                if (district != null)
                {
                    result.DistrictId = district.Name;
                }
                var ward = _countryService.GetWardByHisId(int.Parse(result.WardId ?? "0"));
                if (ward != null)
                {
                    result.WardId = ward.Name;
                }
                result.Hospitalize = hospitalize.OrderByDescending(x => x.CreateDate).ToList();
                Int32.TryParse(patientId, out int temp);
                result.PatientId = temp;
                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return View(new PatientModel());
            }

        }

        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> PatientDetail(string patientId, string encounterCode, int day = 0, DateTime? start = null, DateTime? end = null)
        {
            var defaultResponse = new PatientModel();
            try
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("LoginOTP", "Customer");
                };
                var result = new PatientModel
                {
                    Surname = _localizationService.GetResource("his.patient.notfound"),
                };

                var patients = _patientService.GetByCustomer(_workContext.CurrentCustomer.Id);
                if (patients == null || !patients.Any())
                {
                    return View(result);
                }
                if (String.IsNullOrEmpty(patientId))
                {
                    patientId = patients.First().Code;
                }
                if (patients.FirstOrDefault(x => x.Code == patientId) == null)
                {
                    return View(result);
                }
                var patient = await _hisBusinessService.GetPatientByID(patientId);
                if (patient == null)
                {
                    return View(result);
                }

                if (string.IsNullOrEmpty(encounterCode))
                {
                    result.Surname = _localizationService.GetResource("his.encounterCode.invalid");
                    return View(result);
                }

                // get ngay vao vien
                if (day > 0)
                {
                    end = DateTime.Now;
                    if (start == null)
                    {
                        start = end;
                    }
                    start = start.Value.AddDays(-day);
                }

                List<string> MMyy = new List<string>();
                if (start.HasValue && end.HasValue)
                {
                    var target = start.Value;
                    while (target <= end.Value)
                    {
                        var month = target.Month.ToString();
                        if (target.Month < 10)
                        {
                            month = "0" + month;
                        }
                        var year = target.Year.ToString().Substring(2, 2);
                        MMyy.Add($"{month}{year}");

                        target = target.AddMonths(1);
                    }
                }

                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                var hospitalize = await _hisBusinessService.GetEncounter(patientId, MMyy);
                var generalInfo = hospitalize.FirstOrDefault(x => x.EncounterCode == encounterCode);
                if (generalInfo == null)
                {
                    generalInfo = new HIS.Model.EncounterModel();
                }
                result = result.Map(patient);
                result.Hospitalize = new List<HIS.Model.EncounterModel> { generalInfo };
                // result.General = booking;
                var prescription = await _hisBusinessService.GetPrescription(patientId, encounterCode, lstMMyy: string.Join("+", MMyy.ToArray()));
                var testResult = await _hisBusinessService.GetTestResults(patientId, encounterCode);
                // get chuan doan hinh anh
                var ids = await _hisBusinessService.GetImageDiagnosticResults(patientId, encounterCode);

                // get toa thuốc  && remove ids duplication
                for (int i = 0; i < ids.Count; i++)
                {
                    if (!string.IsNullOrEmpty(ids[i].Description))
                    {
                        ids[i].Description = Rtf.ToHtml(ids[i].Description);
                    }
                    if (!string.IsNullOrEmpty(ids[i].Conclusion))
                    {
                        ids[i].Conclusion = Rtf.ToHtml(ids[i].Conclusion);
                    }
                }
                for (int i = 0; i < prescription.Count; i++)
                {
                    if (!String.IsNullOrEmpty(prescription[i].Dosage))
                    {
                        var dosage = prescription[i].Dosage.Split("^");
                        if (dosage.Count() == 4)
                        {
                            prescription[i].Morning = dosage[0];
                            prescription[i].Noon = dosage[1];
                            prescription[i].AfterNoon = dosage[2];
                            prescription[i].Evening = dosage[3];
                            prescription[i].DosageText = "";
                            if (int.Parse(dosage[0]) > 0)
                            {
                                prescription[i].DosageText = $"Sáng: {dosage[0]}";
                            }
                            if (int.Parse(dosage[1]) > 0)
                            {
                                prescription[i].DosageText = $"{prescription[i].DosageText}, Trưa: {dosage[1]}";
                            }
                            if (int.Parse(dosage[2]) > 0)
                            {
                                prescription[i].DosageText = $"{prescription[i].DosageText}, Chiều: {dosage[2]}";
                            }
                            if (int.Parse(dosage[3]) > 0)
                            {
                                prescription[i].DosageText = $"{prescription[i].DosageText}, Tối: {dosage[3]}";
                            }
                        }
                    }
                }


                result.Prescription = prescription;
                result.TestResults = testResult;
                result.IDS = ids;

                result.Prescription = result.Prescription.OrderByDescending(x => x.CreateDate).ToList();
                result.TestResults = result.TestResults.OrderByDescending(x => x.GettingSampleDate).ToList();
                result.IDS = result.IDS.OrderByDescending(x => x.CreateDate).ToList();
                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> TestResult(string patientId, string encouterCode = "")
        {
            var defaultResponse = new List<HIS.Model.TestResultModel>();
            try
            {
                var result = new List<HIS.Model.TestResultModel>();
                var patient = await _hisBusinessService.GetPatientByID(patientId);
                if (patient == null)
                {
                    return View(result);
                }
                result = await _hisBusinessService.GetTestResults(patientId, encouterCode);
                // if (!result.Any())
                // {
                //     result = new List<HIS.Model.TestResultModel>
                //     { new HIS.Model.TestResultModel{
                //         GettingSampleDate = "test",
                //         ReturnDate = "test",
                //         TypeOrderNumber = "test",
                //         TypeName = "test",
                //         SymptonOrderNumber = "test",
                //         SymptonID = "test",
                //         SymptonName = "test",
                //         ResultOrderNumber = "test",
                //         ResultID = "test",
                //         ResultName = "test",
                //         Result = "test",
                //         Unit = "test",
                //         UsualIndex = "test",
                //         Note = "test",
                //         Base64String = "test",
                //     }};
                // }
                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> CDHA(string patientId, string encouterCode = "")
        {
            var defaultResponse = new List<HIS.Model.ImageDiagnosticResultModel>();
            try
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                var result = new List<HIS.Model.ImageDiagnosticResultModel>();
                var patient = await _hisBusinessService.GetPatientByID(patientId);
                if (patient == null)
                {
                    return View(result);
                }
                result = await _hisBusinessService.GetImageDiagnosticResults(patientId, encouterCode);
                for (int i = 0; i < result.Count; i++)
                {
                    if (!string.IsNullOrEmpty(result[i].Description))
                    {
                        result[i].Description = Rtf.ToHtml(result[i].Description);
                    }
                    if (!string.IsNullOrEmpty(result[i].Conclusion))
                    {
                        result[i].Conclusion = Rtf.ToHtml(result[i].Conclusion);
                    }
                }
                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual async Task<IActionResult> Hospitalize(string patientId, DateTime? start = null, DateTime? end = null)
        {
            var defaultResponse = new List<HIS.Model.EncounterModel>();
            try
            {
                var result = new List<HIS.Model.EncounterModel>();
                var patient = await _hisBusinessService.GetPatientByID(patientId);
                if (patient == null)
                {
                    return View(result);
                }
                List<string> MMyy = new List<string>();
                if (start.HasValue && end.HasValue)
                {
                    var target = start.Value;
                    while (target <= end.Value)
                    {
                        var month = target.Month.ToString();
                        if (target.Month < 10)
                        {
                            month = "0" + month;
                        }
                        var year = target.Year.ToString().Substring(2, 2);
                        MMyy.Add($"{month}{year}");

                        target = target.AddMonths(1);
                    }
                }
                result = await _hisBusinessService.GetEncounter(patientId, MMyy);
                // if (!result.Any())
                // {
                //     result = new List<HIS.Model.EncounterModel>{
                //         new HIS.Model.EncounterModel{
                //             RoomName= "kito",
                //             DoctorName = "kibo",
                //             EncounterCode = "123"
                //         }
                //     };
                // }
                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        [HttpPost]
        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult SendEmail(EmailModel model)
        {
            try
            {

                var name = model.Gender ? "Bà" : "Ông";
                var branch = _branchService.GetBranchById(model.BranchId);

                var mail = new Nop.Services.Messages.EmailModel
                {
                    To = "sagigonmedical@gmail.com",
                    CC = "ha.tran@saigonhealthcare.vn",
                    Subject = $"{name} {model.FullName} vừa đăng ký nhận tin.",
                    Body = $"{name} {model.FullName} có SDT {model.PhoneNumber} tại địa chỉ email {model.Email} vừa đăng ký nhận tin tại cơ sở {branch?.Name}.Nội dung: {model.Message}",
                };
                if (String.IsNullOrEmpty(model.FullName))
                {
                    mail.Subject = $"SDT {model.PhoneNumber} vừa đăng ký nhận tin.";
                    mail.Body = $"Bệnh nhân có SDT {model.PhoneNumber} vừa đăng ký nhận tin với khói khám {model.ProductName}";
                }
                _workflowMessageService.SendCustomMessage(mail, 2);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            // using (MailMessage mail = new MailMessage())
            // {
            //     mail.From = new MailAddress("Cskh@saigonhealthcare.vn", "Website bệnh viện Saigon Health Care");
            //     mail.To.Add(new MailAddress("sagigonmedical@gmail.com"));
            //     mail.CC.Add(new MailAddress("Cskh@saigonhealthcare.vn"));
            //     mail.CC.Add(new MailAddress("ha.tran@saigonhealthcare.vn"));
            //     mail.Bcc.Add(new MailAddress("dtpham258@gmail.com"));
            //     mail.Subject = $"{name} {model.FullName} vừa đăng ký nhận tin.";
            //     mail.Body = $"{name} {model.FullName} có SDT {model.PhoneNumber} tại địa chỉ email {model.Email} vừa đăng ký nhận tin tại cơ sở {branch?.Name}.Nội dung: {model.Message}";
            //     if (String.IsNullOrEmpty(model.FullName))
            //     {
            //         mail.Subject = $"SDT {model.PhoneNumber} vừa đăng ký nhận tin.";
            //         mail.Body = $"Bệnh nhân có SDT {model.PhoneNumber} vừa đăng ký nhận tin với khói khám {model.ProductName}";
            //     }
            //     mail.IsBodyHtml = true;

            //     using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
            //     {
            //         // smtp.Credentials = new NetworkCredential("Cskh@saigonhealthcare.vn", "younztsftzsqbeub");
            //         smtp.Credentials = new NetworkCredential("nguyencatpham258@gmail.com", "xvjekuuwuzikswwx");
            //         // smtpClient.UseDefaultCredentials = true; // uncomment if you don't want to use the network credentials
            //         smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            //         smtp.EnableSsl = true;
            //         smtp.Send(mail);
            //     }
            // }
            if (String.IsNullOrEmpty(model.ReturnUrl))
            {
                return RedirectToAction("Contact", "Home", new { result = "success" });
            }
            return Redirect($"{model.ReturnUrl}?result=success");
        }
        #endregion
    }
}