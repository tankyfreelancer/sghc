﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Services.Caching.CachingDefaults;
using Nop.Services.Media;
using Nop.Services.News;
using Nop.Services.Recruit;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Factories;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Home;
using Nop.Web.Models.News;
using Nop.Core.Infrastructure;
using Nop.Core.Domain.Recruit;
using System.Net.Mail;
using System.Net;
using System.IO;
using Nop.Services.Messages;

namespace Nop.Web.Controllers
{
    public partial class HomeController : BasePublicController
    {
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly INewsModelFactory _newsModelFactory;

        private readonly ICatalogModelFactory _catalogModelFactory;

        private readonly HomepageSettings _homeSettings;

        private readonly IRecruitFacultyService _recruitFacultyService;

        private readonly IJobService _jobService;

        private readonly IPictureService _pictureService;
        private readonly IUrlRecordService _urlRecordService;

        private readonly IBranchService _branchService;

        private readonly Nop.Web.Areas.Admin.Factories.IManufacturerModelFactory
            _manufacturerModelFactory;

        private readonly IStaticCacheManager _cacheManager;
        private readonly IDownloadService _downloadService;
        private readonly INopFileProvider _fileProvider;
        private readonly IResumeService _resumeService;

        private readonly Nop.Services.Common.NopHttpClient _nopHttpClient;
        ISliderService _sliderService;
        ISliderModelFactory _sliderModelFactory;
        INewsService _newsService;

        public HomeController(
            IStaticCacheManager cacheManager,
            IMemoryCache manager,
            INewsModelFactory newsModelFactory,
            ICatalogModelFactory catalogModelFactory,
            HomepageSettings homeSettings,
            IPictureService pictureService,
            IBranchService branchService,
            Nop.Web.Areas.Admin.Factories.IManufacturerModelFactory
            manufacturerModelFactory,
            INopFileProvider fileProvider,
            Nop.Services.Common.NopHttpClient nopHttpClient,
            IRecruitFacultyService recruitFacultyService,
            IJobService jobService,
            ISliderModelFactory sliderModelFactory,
            INewsService newsService,
            IUrlRecordService urlRecordService,
            IWorkflowMessageService workflowMessageService,
            IDownloadService downloadService,
            IResumeService resumeService
        )
        {
            _newsModelFactory = newsModelFactory;
            _catalogModelFactory = catalogModelFactory;
            _homeSettings = homeSettings;
            _pictureService = pictureService;
            _branchService = branchService;
            _manufacturerModelFactory = manufacturerModelFactory;
            _cacheManager = cacheManager;
            _nopHttpClient = nopHttpClient;
            _fileProvider = fileProvider;
            _recruitFacultyService = recruitFacultyService;
            _jobService = jobService;
            _sliderModelFactory = sliderModelFactory;
            _newsService = newsService;
            _urlRecordService = urlRecordService;
            _downloadService = downloadService;
            _resumeService = resumeService;
            _workflowMessageService = workflowMessageService;

            var c1 =
                NopBlogsCachingDefaults
                    .BlogCommentsNumberCacheKey
                    .FillCacheKey(1, 2, 3);
            var c2 =
                NopBlogsCachingDefaults
                    .BlogCommentsNumberCacheKey
                    .FillCacheKey(4, 5, 6);
        }

        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult Index()
        {
            var defaultResponse = new HomepageModel();
            try
            {
                var setting = new Setting();
                var rssData = new Core.Rss.RssFeed();
                var mainRss = new Core.Rss.RssFeed();
                var newsModel = _newsModelFactory.PrepareHomepageNewsItemsModel();

                try
                {
                    rssData = _nopHttpClient.GetNewsRssAsync(_homeSettings.RssUrl).Result;
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine(ex);
                }
                try
                {
                    mainRss = _nopHttpClient.GetNewsRssAsync("https://www.medinet.gov.vn/rssphongyte.aspx?kt=th").Result;
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine(ex);
                }

                var newsFaculties = _newsModelFactory.PrepareNewsCategoryListModel(new NewsPagingFilteringModel
                {
                    PageSize = 10,
                }, menuSeName: "chuyen-khoa");
                var vendor =
                    _manufacturerModelFactory
                        .PrepareManufacturerListModel(new Nop.Web.Areas.Admin.Models.Catalog.ManufacturerSearchModel());

                var packageModel = new SearchModel
                {
                    ProductType = ProductType.GroupedProduct,
                    ShowOnHomePage = true
                };
                var command = new CatalogPagingFilteringModel
                {
                    PageSize = 100
                };
                var package = _catalogModelFactory.PrepareSearchModel(packageModel, command);
                var sliders = _sliderModelFactory.PrepareSliderListModel(new Areas.Admin.Models.Media.SliderSearchModel());
                var sliderMobiles = _sliderModelFactory.PrepareSliderListModel(new Areas.Admin.Models.Media.SliderSearchModel { IsMobile = true });
                var doctors = _catalogModelFactory.PrepareDoctorsHomePage();
                var faculties = _catalogModelFactory.PrepareHomepageCategoryModels();
                var mainRss1Url = _pictureService.GetPictureUrl(_homeSettings.Rss1PictureId, showDefaultPicture: false);
                var mainRss2Url = _pictureService.GetPictureUrl(_homeSettings.Rss2PictureId, showDefaultPicture: false);
                var bookPictureUrl = _pictureService.GetPictureUrl(_homeSettings.HomePageBookingPictureId, showDefaultPicture: false);
                var doctorPictureUrl = _pictureService.GetPictureUrl(_homeSettings.HomePageDoctorPictureId, showDefaultPicture: false);
                var vendorPictureUrl = _pictureService.GetPictureUrl(_homeSettings.HomePageVendorPictureId, showDefaultPicture: false);
                var doctorMobilePictureUrl = _pictureService.GetPictureUrl(_homeSettings.HomePageDoctorMobilePictureId, showDefaultPicture: false);
                var vendorMobilePictureUrl = _pictureService.GetPictureUrl(_homeSettings.HomePageVendorMobilePictureId, showDefaultPicture: false);
                var result = new HomepageModel
                {
                    Sliders = sliders,
                    SliderMobiles = sliderMobiles,
                    Doctors = doctors,
                    HomeSetting = setting,
                    RssFeed = rssData,
                    MainRss = mainRss,
                    Vendor = vendor,
                    PopupNews = newsModel.PopupNews,
                    HotNews = newsModel.HotNews,
                    Newses = newsModel.NewsItems.ToList(),
                    Communications = newsModel.Communications.ToList(),
                    Packages = package.Products.ToList(),

                    Faculties = faculties,
                    NewsFaculties = newsFaculties,
                    OtherNewses = new List<NewsItemModel>(),
                    MainRss1Url = mainRss1Url,
                    MainRss2Url = mainRss2Url,
                    BookPictureUrl = bookPictureUrl,
                    DoctorPictureUrl = doctorPictureUrl,
                    VendorPictureUrl = vendorPictureUrl,
                    DoctorMobilePictureUrl = doctorMobilePictureUrl,
                    VendorMobilePictureUrl = vendorMobilePictureUrl,
                };
                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult Sitemap()
        {
            var defaultResponse = new Models.Catalog.TopMenuModel();
            try
            {
                var model = _catalogModelFactory.PrepareTopMenuModel();

                return View(model);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult About()
        {
            var defaultResponse = new AboutPageModel();
            try
            {
                var news = _newsModelFactory.PrepareHomepageNewsItemsModel(false);
                var relatedNews =
                    news
                        .NewsItems
                        .Where(x =>
                            !string.IsNullOrEmpty(x.Tags) &&
                            (x.Tags.ToLower().Contains("về chúng tôi") || x.Tags.ToLower().Contains("ve chung toi") || x.Tags.ToLower().Contains("ve-chung-toi") || x.Tags.ToLower().Contains("về-chúng-tôi"))
                            )
                        .OrderByDescending(x => x.CreatedOn);

                var about =
                    new AboutPageModel
                    {
                        AboutSlider01 =
                            _pictureService
                                .GetPictureUrl(_homeSettings.AboutSlider01,
                                showDefaultPicture: false),
                        AboutSlider02 =
                            _pictureService
                                .GetPictureUrl(_homeSettings.AboutSlider02,
                                showDefaultPicture: false),
                        AboutSlider03 =
                            _pictureService
                                .GetPictureUrl(_homeSettings.AboutSlider03,
                                showDefaultPicture: false),
                        BannerUrl =
                            _pictureService
                                .GetPictureUrl(_homeSettings.BannerAboutPageId,
                                showDefaultPicture: false),
                        AboutVision = _homeSettings.AboutVision,
                        AboutMission = _homeSettings.AboutMission,
                        AboutCoreValue = _homeSettings.AboutCoreValue,
                        AboutWhyChooseUs = _homeSettings.AboutWhyChooseUs,
                        AboutVideoUrl = _homeSettings.AboutVideoUrl,
                        News = relatedNews.ToList()
                    };

                return View(about);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }

        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult Contact(string result = null)
        {
            var defaultResponse = new List<Core.Domain.Stores.Branch>();
            try
            {

                var contacts = _branchService.GetAllBranchs();
                ViewBag.BannerUrl =
                    _pictureService
                        .GetPictureUrl(_homeSettings.BannerContactPageId,
                        showDefaultPicture: false);
                ViewBag.Result = result;
                return View(contacts);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }


        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult AboutRecruit(RecruitPagingFilteringModel command)
        {
            var defaultResponse = new AboutRecruitModel();
            try
            {
                var paging = new RecruitPagingFilteringModel();
                if (command.PageSize <= 0)
                    command.PageSize = 6;
                if (command.PageNumber <= 0)
                    command.PageNumber = 1;

                var recruits = _recruitFacultyService.GetAll();
                if (!command.RecruitFacultyId.HasValue && recruits.Count() > 0)
                {
                    command.RecruitFacultyId = recruits[0].Id;
                }
                var jobs = _jobService.GetPaging(
                    search: command.Search,
                    pageIndex: command.PageIndex,
                    pageSize: command.PageSize,
                    recruitFacultyId: command.RecruitFacultyId
                    );
                paging.LoadPagedList(jobs);
                var recruit = recruits.FirstOrDefault(x => x.Id == command.RecruitFacultyId);
                var result = new AboutRecruitModel
                {
                    RecruitFaculties =
                            recruits
                                .Select(x => new RecruitFacultyModel(x))
                                .ToList(),
                    Jobs = jobs.Select(x =>
                    {
                        var result = new JobModel(x, _urlRecordService, _pictureService, _homeSettings);
                        result.RecruitFacultyName = recruit.Name;
                        return result;
                    }).ToList(),
                    BannerUrl =
                        _pictureService
                            .GetPictureUrl(_homeSettings.BannerRecruitPageId,
                            showDefaultPicture: false),
                    PagingFilteringContext = paging
                };
                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult AboutJob(int id)
        {
            var job = _jobService.GetJobById(id);
            if (job == null)
            {
                return RedirectToRoute("AboutRecruit");
            }
            var defaultResponse = new JobModel(job, _urlRecordService, _pictureService, _homeSettings);
            try
            {

                var recruit = _recruitFacultyService.GetRecruitFacultyById(job.RecruitFacultyId.GetValueOrDefault());
                var result = new JobModel(job, _urlRecordService, _pictureService, _homeSettings);
                if (recruit != null)
                {
                    result.RecruitFacultyName = recruit.Name;
                }

                result.BannerUrl =
                    _pictureService
                        .GetPictureUrl(_homeSettings.BannerRecruitPageId,
                        showDefaultPicture: false);
                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult AboutResume(int jobId, bool success = false)
        {
            var job = _jobService.GetJobById(jobId);
            if (job == null)
            {
                return RedirectToRoute("AboutRecruit");
            }
            var defaultResponse = new JobModel(job, _urlRecordService, _pictureService, _homeSettings);
            try
            {

                var recruit = _recruitFacultyService.GetRecruitFacultyById(job.RecruitFacultyId.GetValueOrDefault());
                var result = new JobModel(job, _urlRecordService, _pictureService, _homeSettings);
                if (recruit != null)
                {
                    result.RecruitFacultyName = recruit.Name;
                }
                result.BannerUrl =
                    _pictureService
                        .GetPictureUrl(_homeSettings.BannerRecruitPageId,
                        showDefaultPicture: false);
                ViewBag.Success = success;
                return View(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return View(defaultResponse);
        }
        [HttpPost]
        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult UploadCV(ResumeModel model)
        {
            try
            {
                var httpPostedFile = Request.Form.Files.FirstOrDefault();
                string attachFile = null;

                if (httpPostedFile != null)
                {
                    var fileName = httpPostedFile.FileName;
                    var contentType = httpPostedFile.ContentType;
                    var fileUploaded = _pictureService.InsertPicture(_downloadService.GetDownloadBits(httpPostedFile),
                    contentType, _fileProvider.GetFileNameWithoutExtension(fileName), validateBinary: false);
                    attachFile = _pictureService.GetPictureUrl(fileUploaded.Id, showDefaultPicture: false);
                }

                var job = _jobService.GetJobById(model.JobId);
                if (job == null)
                {
                    return RedirectToRoute("AboutRecruit");
                }
                var recruit = _recruitFacultyService.GetRecruitFacultyById(job.RecruitFacultyId.GetValueOrDefault());
                var result = new JobModel(job, _urlRecordService, _pictureService, _homeSettings);
                if (recruit != null)
                {
                    result.RecruitFacultyName = recruit.Name;
                }
                result.BannerUrl =
                    _pictureService
                        .GetPictureUrl(_homeSettings.BannerRecruitPageId,
                        showDefaultPicture: false);

                if (!ModelState.IsValid)
                {

                    var errors = String.Join(",", ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList());
                    ViewBag.Success = false;
                    ViewBag.Errors = errors;
                    return RedirectToRoute(new
                    {
                        controller = "Home",
                        action = "AboutResume",
                        jobId = model.JobId,
                        success = false,
                        errors
                    });
                }
                _resumeService.InsertResume(new Resume
                {
                    FullName = model.FullName,
                    Gender = model.Gender,
                    PhoneNumber = model.PhoneNumber,
                    Email = model.Email,
                    Address = model.Address,
                    TempAddress = model.TempAddress,
                    Major = model.Major,
                    College = model.College,
                    Expirence = model.Expirence,
                    Bio = model.Bio,
                    Active = true,
                    Deleted = false,
                    AttachFile = attachFile,
                    JobId = model.JobId,
                    CityId = model.CityId,
                    DistrictId = model.DistrictId,
                    WardId = model.WardId,
                    TempCityId = model.TempCityId,
                    TempDistrictId = model.TempDistrictId,
                    TempWardId = model.TempWardId,
                    CreatedOnUtc = DateTime.UtcNow,
                });
                var resume = _resumeService.GetResumeByEmail(model.Email);
                // send email to admin and ha.tran@saigonhealthcare.vn
                try
                {

                    var name = model.Gender ? "Bà" : "Ông";
                    var mail = new Nop.Services.Messages.EmailModel
                    {
                        To = "sagigonmedical@gmail.com",
                        CC = " cskh@saigonhealthcare.vn",
                        Subject = $"[Tuyển dụng] Ứng viên {model.FullName} đăng ký ứng tuyển vị trí {job.Title}",
                        Body = $@"<h3>{name} {model.FullName} đăng ký ứng tuyển vị trí {job.Title}</h3>
                    <h4>Thông tin ứng viên</h4>
                    <table>
                        <tr>
                            <td>Họ Tên</td>
                            <td>{model.FullName}</td>
                        </tr>
                        <tr>
                            <td>Vị trí ứng tuyển</td>
                            <td>{job.Title}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{model.Email}</td>
                        </tr>
                        <tr>
                            <td>Giới tính</td>
                            <td>{model.Gender}</td>
                        </tr>
                        <tr>
                            <td>Địa chỉ</td>
                            <td>{model.Address}</td>
                        </tr>
                        <tr>
                            <td>Chuyên môn</td>
                            <td>{model.Major}</td>
                        </tr>
                        <tr>
                            <td>Học vấn</td>
                            <td>{model.College}</td>
                        </tr>
                        <tr>
                            <td>Kinh nghiệm</td>
                            <td>{model.Expirence}</td>
                        </tr>
                        <tr>
                            <td>Giới thiệu về bản thân</td>
                            <td>{model.Bio}</td>
                        </tr>
                    </table>"
                    };

                    if (resume != null)
                    {
                        mail.Body += $"<p> Xem thêm thông tin chi tiết tại <a href='https://saigonhealthcare.vn/Admin/Recruit/ResumeInfo/{resume.Id}'>đây</a></p>";
                    }
                    _workflowMessageService.SendCustomMessage(mail, 2);
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return RedirectToRoute(new
                {
                    controller = "Home",
                    action = "AboutJob",
                    id = model.JobId,
                    success = true
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return RedirectToRoute(new
            {
                controller = "Home",
                action = "AboutJob",
                id = model.JobId,
                success = false
            });
        }
    }
}
