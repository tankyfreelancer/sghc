﻿using LinqToDB.Mapping;
using Nop.Core.Domain.News;

namespace Nop.Data.Mapping.News
{
    /// <summary>
    /// Represents a product picture mapping configuration
    /// </summary>
    public partial class NewsPictureMap : NopEntityTypeConfiguration<NewsPicture>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityMappingBuilder<NewsPicture> builder)
        {
            builder.HasTableName(NopMappingDefaults.NewsPictureTable);

            builder.Property(productpicture => productpicture.NewsId);
            builder.Property(productpicture => productpicture.PictureId);
            builder.Property(productpicture => productpicture.DisplayOrder);
        }

        #endregion
    }
}