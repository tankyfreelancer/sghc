﻿using LinqToDB.Mapping;
using Nop.Core.Domain.News;

namespace Nop.Data.Mapping.News
{
    /// <summary>
    /// Represents a news mapping configuration
    /// </summary>
    public partial class NewsCategoryMap : NopEntityTypeConfiguration<NewsCategory>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityMappingBuilder<NewsCategory> builder)
        {
            builder.HasTableName(NopMappingDefaults.NewsCategoryTable);

        }

        #endregion
    }
}