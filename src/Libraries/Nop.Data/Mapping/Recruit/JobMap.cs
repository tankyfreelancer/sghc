﻿using LinqToDB.Mapping;
using Nop.Core.Domain.Recruit;

namespace Nop.Data.Mapping.Recruit
{
    /// <summary>
    /// Represents a news mapping configuration
    /// </summary>
    public partial class JobMap : NopEntityTypeConfiguration<Job>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityMappingBuilder<Job> builder)
        {

        }

        #endregion
    }
}