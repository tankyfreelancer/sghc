﻿using LinqToDB.Mapping;
using Nop.Core.Domain.Directory;

namespace Nop.Data.Mapping.Directory
{
    /// <summary>
    /// Represents a Ward mapping configuration
    /// </summary>
    public partial class WardMap : NopEntityTypeConfiguration<Ward>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityMappingBuilder<Ward> builder)
        {
            builder.HasTableName(nameof(Ward));

            builder.Property(x => x.Name).HasLength(100).IsNullable(false);
        }

        #endregion
    }
}