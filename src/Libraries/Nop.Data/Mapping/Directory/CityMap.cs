﻿using LinqToDB.Mapping;
using Nop.Core.Domain.Directory;

namespace Nop.Data.Mapping.Directory
{
    /// <summary>
    /// Represents a City mapping configuration
    /// </summary>
    public partial class CityMap : NopEntityTypeConfiguration<City>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityMappingBuilder<City> builder)
        {
            builder.HasTableName(nameof(City));

            builder.Property(x => x.Name).HasLength(100).IsNullable(false);
        }

        #endregion
    }
}