﻿using LinqToDB.Mapping;
using Nop.Core.Domain.Directory;

namespace Nop.Data.Mapping.Directory
{
    /// <summary>
    /// Represents a District mapping configuration
    /// </summary>
    public partial class DistrictMap : NopEntityTypeConfiguration<District>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityMappingBuilder<District> builder)
        {
            builder.HasTableName(nameof(District));

            builder.Property(x => x.Name).HasLength(100).IsNullable(false);
        }

        #endregion
    }
}