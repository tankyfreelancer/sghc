﻿using LinqToDB.Mapping;
using Nop.Core.Domain.Stores;

namespace Nop.Data.Mapping.Stores
{
    /// <summary>
    /// Represents a Branch mapping configuration
    /// </summary>
    public partial class BranchMap : NopEntityTypeConfiguration<Branch>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityMappingBuilder<Branch> builder)
        {
            builder.HasTableName(nameof(Branch));
        }

        #endregion
    }
}