﻿using LinqToDB.Mapping;
using Nop.Core.Domain.Media;

namespace Nop.Data.Mapping.Media
{
    /// <summary>
    /// Represents a picture mapping configuration
    /// </summary>
    public partial class SliderMap : NopEntityTypeConfiguration<Slider>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityMappingBuilder<Slider> builder)
        {
            builder.HasTableName(nameof(Slider));
        }

        #endregion
    }
}