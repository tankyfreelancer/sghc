﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Services.News
{
   public class TopMenuModel
    {
        public TopMenuModel()
        {
            SubMenus = new List<TopMenuModel>();
        }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string SeName { get; set; }
        public List<TopMenuModel> SubMenus { get; set; }
    }
}
