﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.News;
using Nop.Core.Domain.Stores;
using Nop.Data;
using Nop.Services.Caching.CachingDefaults;
using Nop.Services.Caching.Extensions;
using Nop.Services.Events;
using Nop.Services.Seo;
using Nop.Services.Localization;
using Nop.Core.Domain.Common;
using Nop.Services.Security;
using Nop.Services.Customers;
using Nop.Core.Caching;
using Nop.Core.Domain.Blogs;

namespace Nop.Services.News
{
    /// <summary>
    /// News service
    /// </summary>
    public partial class NewsService : INewsService
    {
        #region Fields

        private readonly IAclService _aclService;
        private readonly CommonSettings _commonSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<NewsComment> _newsCommentRepository;
        private readonly IRepository<NewsItem> _newsItemRepository;
        private readonly IRepository<NewsPicture> _newsPictureRepository;
        private readonly IRepository<NewsCategoryPicture> _newsCategoryPictureRepository;
        private readonly IRepository<NewsCategory> _newsCategoryRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly IRepository<NewsCategoryMap> _newsCategoryMapRepository;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ICustomerService _customerService;
        private readonly IStaticCacheManager _cacheManager;

        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IStaticCacheManager _staticCacheManager;

        #endregion

        #region Ctor

        public NewsService(CatalogSettings catalogSettings,
            CommonSettings commonSettings,
            IEventPublisher eventPublisher,
            IAclService aclService,
            IRepository<NewsComment> newsCommentRepository,
            IRepository<NewsItem> newsItemRepository,
            IRepository<NewsCategory> newsCategoryRepository,
            IRepository<NewsPicture> newsPictureRepository,
            IRepository<NewsCategoryMap> newsCategoryMapRepository,
            IRepository<StoreMapping> storeMappingRepository,
            ICustomerService customerService,
            IUrlRecordService urlRecordService,
            IStaticCacheManager staticCacheManager,
            ILocalizationService localizationService,
            IWorkContext workContext,
            IRepository<NewsCategoryPicture> newsCategoryPictureRepository,
            IStaticCacheManager cacheManager)
        {
            _commonSettings = commonSettings;
            _customerService = customerService;
            _catalogSettings = catalogSettings;
            _eventPublisher = eventPublisher;
            _newsCommentRepository = newsCommentRepository;
            _newsItemRepository = newsItemRepository;
            _newsCategoryRepository = newsCategoryRepository;
            _storeMappingRepository = storeMappingRepository;
            _newsPictureRepository = newsPictureRepository;
            _newsCategoryMapRepository = newsCategoryMapRepository;
            _urlRecordService = urlRecordService;
            _localizationService = localizationService;
            _aclService = aclService;
            _staticCacheManager = staticCacheManager;
            _newsCategoryRepository = newsCategoryRepository;
            _newsCategoryPictureRepository = newsCategoryPictureRepository;
            _workContext = workContext;
            _cacheManager = cacheManager;
        }

        #endregion

        #region Methods

        public virtual IPagedList<NewsCategory> GetAllCategories(string categoryName, int storeId = 0,
                  int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            //don't use a stored procedure. Use LINQ
            var query = _newsCategoryRepository.Table;
            if (!showHidden)
                query = query.Where(c => c.Published);
            if (!string.IsNullOrWhiteSpace(categoryName))
                query = query.Where(c => c.Title.Contains(categoryName));
            query = query.OrderBy(c => c.CategoryId).ThenBy(c => c.Published).ThenBy(c => c.Id);

            if ((storeId > 0 && !_catalogSettings.IgnoreStoreLimitations) || (!showHidden && !_catalogSettings.IgnoreAcl))
            {

                if (storeId > 0 && !_catalogSettings.IgnoreStoreLimitations)
                {
                    //Store mapping
                    query = from c in query
                            join sm in _storeMappingRepository.Table
                                on new { c1 = c.Id, c2 = nameof(Category) } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into c_sm
                            from sm in c_sm.DefaultIfEmpty()
                            where !c.LimitedToStores || storeId == sm.StoreId
                            select c;
                }

                query = query.Distinct().OrderBy(c => c.DisplayOrder).ThenBy(c => c.Published).ThenBy(c => c.Id);
            }

            var unsortedCategories = query.ToList();

            //paging
            return new PagedList<NewsCategory>(unsortedCategories, pageIndex, pageSize);
        }
        /// <summary>
        /// Get category breadcrumb 
        /// </summary>
        /// <param name="category">Category</param>
        /// <param name="allCategories">All categories</param>
        /// <param name="showHidden">A value indicating whether to load hidden records</param>
        /// <returns>Category breadcrumb </returns>
        public virtual IList<NewsCategory> GetCategoryBreadCrumb(NewsCategory category, IList<NewsCategory> allCategories = null, bool showHidden = false)
        {
            if (category == null)
                throw new ArgumentNullException(nameof(category));

            var result = new List<NewsCategory>();

            //used to prevent circular references
            var alreadyProcessedCategoryIds = new List<int>();

            while (category != null && //not null
                   (showHidden || category.Published) && //published
                                                         //    (showHidden || _aclService.Authorize(category)) && //ACL
                   !alreadyProcessedCategoryIds.Contains(category.Id)) //prevent circular references
            {
                result.Add(category);

                alreadyProcessedCategoryIds.Add(category.Id);

                category = allCategories != null
                    ? allCategories.FirstOrDefault(c => c.Id == category.CategoryId)
                    : GetCategoryById(category.Id);
            }

            result.Reverse();

            return result;
        }
        public virtual NewsCategory GetCategoryById(int categoryId)
        {
            if (categoryId == 0)
                return null;

            return _newsCategoryRepository.ToCachedGetById(categoryId);
        }
        #region NewsCategory

        /// <summary>
        /// Deletes a news
        /// </summary>
        /// <param name="newsItem">NewsCategory item</param>
        public virtual void DeleteNewsCategory(NewsCategory newsItem)
        {
            if (newsItem == null)
                throw new ArgumentNullException(nameof(newsItem));

            _newsCategoryRepository.Delete(newsItem);

            //event notification
            _eventPublisher.EntityDeleted(newsItem);
        }

        /// <summary>
        /// Gets a news
        /// </summary>
        /// <param name="newsId">The news identifier</param>
        /// <returns>NewsCategory</returns>
        public virtual NewsCategory GetNewsCategoryById(int newsId)
        {
            if (newsId == 0)
                return null;

            return _newsCategoryRepository.ToCachedGetById(newsId);
        }

        /// <summary>
        /// Gets news
        /// </summary>
        /// <param name="newsIds">The news identifiers</param>
        /// <returns>NewsCategory</returns>
        public virtual IList<NewsCategory> GetNewsCategoryByIds(int[] newsIds)
        {
            var query = _newsCategoryRepository.Table;
            return query.Where(p => newsIds.Contains(p.Id)).OrderBy(x=>x.DisplayOrder).ThenByDescending(x=>x.StartDateUtc ?? x.CreatedOnUtc).ToList();
        }

        /// <summary>
        /// Gets all news
        /// </summary>
        /// <param name="languageId">Language identifier; 0 if you want to get all records</param>
        /// <param name="storeId">Store identifier; 0 if you want to get all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>NewsCategory items</returns>
        public virtual IPagedList<NewsCategory> GetAllNewsCategory(int languageId = 0, int storeId = 0,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, bool isNews = false, int? parentId = null, string search = null)
        {
            var query = _newsCategoryRepository.Table;
            if (languageId > 0)
                query = query.Where(n => languageId == n.LanguageId);

            if (isNews)
            {
                query = query.Where(x => x.Tags == null);
            }
            if (parentId.HasValue)
            {
                query = query.Where(x => x.CategoryId == parentId.Value || x.Id == parentId.Value);
            }
            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.Title.Contains(search) || x.Short.Contains(search));
            }
            query = query.OrderBy(x=>x.DisplayOrder).ThenByDescending(x=>x.StartDateUtc ?? x.CreatedOnUtc);

            //Store mapping
            if (storeId > 0 && !_catalogSettings.IgnoreStoreLimitations)
            {
                query = from n in query
                        join sm in _storeMappingRepository.Table
                        on new { c1 = n.Id, c2 = nameof(NewsCategory) } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into n_sm
                        from sm in n_sm.DefaultIfEmpty()
                        where !n.LimitedToStores || storeId == sm.StoreId
                        select n;

                query = query.Distinct().OrderBy(x=>x.DisplayOrder).ThenByDescending(x=>x.StartDateUtc ?? x.CreatedOnUtc);
            }

            var news = new PagedList<NewsCategory>(query, pageIndex, pageSize);
            return news;
        }

        /// <summary>
        /// Inserts a news item
        /// </summary>
        /// <param name="news">NewsCategory item</param>
        public virtual void InsertNewsCategory(NewsCategory news)
        {
            if (news == null)
                throw new ArgumentNullException(nameof(news));

            _newsCategoryRepository.Insert(news);

            //event notification
            _eventPublisher.EntityInserted(news);
        }

        /// <summary>
        /// Updates the news item
        /// </summary>
        /// <param name="news">NewsCategory item</param>
        public virtual void UpdateNewsCategory(NewsCategory news)
        {
            if (news == null)
                throw new ArgumentNullException(nameof(news));

            _newsCategoryRepository.Update(news);

            //event notification
            _eventPublisher.EntityUpdated(news);
        }

        /// <summary>
        /// Get a value indicating whether a news item is available now (availability dates)
        /// </summary>
        /// <param name="newsItem">NewsCategory item</param>
        /// <param name="dateTime">Datetime to check; pass null to use current date</param>
        /// <returns>Result</returns>
        public virtual bool IsNewsCategoryAvailable(NewsCategory newsItem, DateTime? dateTime = null)
        {

            return true;
        }
        #endregion

        #region News

        /// <summary>
        /// Deletes a news
        /// </summary>
        /// <param name="newsItem">News item</param>
        public virtual void DeleteNews(NewsItem newsItem)
        {
            if (newsItem == null)
                throw new ArgumentNullException(nameof(newsItem));

            _newsItemRepository.Delete(newsItem);

            //event notification
            _eventPublisher.EntityDeleted(newsItem);
        }

        /// <summary>
        /// Gets a news
        /// </summary>
        /// <param name="newsId">The news identifier</param>
        /// <returns>News</returns>
        public virtual NewsItem GetNewsById(int newsId)
        {
            if (newsId == 0)
                return null;

            return _newsItemRepository.ToCachedGetById(newsId);
        }

        /// <summary>
        /// Gets news
        /// </summary>
        /// <param name="newsIds">The news identifiers</param>
        /// <returns>News</returns>
        public virtual IList<NewsItem> GetNewsByIds(int[] newsIds)
        {
            var query = _newsItemRepository.Table;
            return query.Where(p => newsIds.Contains(p.Id)).OrderBy(x=>x.DisplayOrder).ThenByDescending(x=>x.StartDateUtc ?? x.CreatedOnUtc).ToList();
        }

        /// <summary>
        /// Gets all news
        /// </summary>
        /// <param name="languageId">Language identifier; 0 if you want to get all records</param>
        /// <param name="storeId">Store identifier; 0 if you want to get all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>News items</returns>
        public virtual IPagedList<NewsItem> GetAllNews(int languageId = 0, int storeId = 0,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, bool isNews = false, int? categoryId = null, string search = null, bool showOnHomePage = false)
        {
            var query = _newsItemRepository.Table;
            if (languageId > 0)
                query = query.Where(n => languageId == n.LanguageId);
            if (!showHidden)
            {
                var utcNow = DateTime.UtcNow;
                query = query.Where(n => n.Published);
                query = query.Where(n => !n.StartDateUtc.HasValue || n.StartDateUtc <= utcNow);
                query = query.Where(n => !n.EndDateUtc.HasValue || n.EndDateUtc >= utcNow);
            }
            if (isNews)
            {
                query = query.Where(x => x.Tags == "" || x.Tags == null);
            }
            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.Title.Contains(search) || x.Short.Contains(search));
            }
            if (showOnHomePage)
            {
                query = query.Where(x => x.ShowOnHomePage);
            }
            if (categoryId.HasValue)
            {
                query = from n in query
                        join m in _newsCategoryMapRepository.Table on n.Id equals m.NewsId into gj
                        from x in gj.DefaultIfEmpty()
                        where x != null && x.CategoryId == categoryId
                        select n;
            }
            query = query.OrderBy(x=>x.DisplayOrder).ThenByDescending(x=>x.StartDateUtc ?? x.CreatedOnUtc);

            //Store mapping
            if (storeId > 0 && !_catalogSettings.IgnoreStoreLimitations)
            {
                query = from n in query
                        join sm in _storeMappingRepository.Table
                        on new { c1 = n.Id, c2 = nameof(NewsItem) } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into n_sm
                        from sm in n_sm.DefaultIfEmpty()
                        where !n.LimitedToStores || storeId == sm.StoreId
                        select n;

                query = query.Distinct().OrderBy(x=>x.DisplayOrder).ThenByDescending(x=>x.StartDateUtc ?? x.CreatedOnUtc);
            }

            var news = new PagedList<NewsItem>(query, pageIndex, pageSize);
            return news;
        }

        /// <summary>
        /// Inserts a news item
        /// </summary>
        /// <param name="news">News item</param>
        public virtual void InsertNews(NewsItem news)
        {
            if (news == null)
                throw new ArgumentNullException(nameof(news));

            _newsItemRepository.Insert(news);

            //event notification
            _eventPublisher.EntityInserted(news);
        }

        /// <summary>
        /// Updates the news item
        /// </summary>
        /// <param name="news">News item</param>
        public virtual void UpdateNews(NewsItem news)
        {
            if (news == null)
                throw new ArgumentNullException(nameof(news));

            _newsItemRepository.Update(news);

            //event notification
            _eventPublisher.EntityUpdated(news);
        }

        /// <summary>
        /// Get a value indicating whether a news item is available now (availability dates)
        /// </summary>
        /// <param name="newsItem">News item</param>
        /// <param name="dateTime">Datetime to check; pass null to use current date</param>
        /// <returns>Result</returns>
        public virtual bool IsNewsAvailable(NewsItem newsItem, DateTime? dateTime = null)
        {
            if (newsItem == null)
                throw new ArgumentNullException(nameof(newsItem));

            if (newsItem.StartDateUtc.HasValue && newsItem.StartDateUtc.Value >= dateTime)
                return false;

            if (newsItem.EndDateUtc.HasValue && newsItem.EndDateUtc.Value <= dateTime)
                return false;

            return true;
        }
        #endregion

        #region News comments

        /// <summary>
        /// Gets all comments
        /// </summary>
        /// <param name="customerId">Customer identifier; 0 to load all records</param>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <param name="newsItemId">News item ID; 0 or null to load all records</param>
        /// <param name="approved">A value indicating whether to content is approved; null to load all records</param> 
        /// <param name="fromUtc">Item creation from; null to load all records</param>
        /// <param name="toUtc">Item creation to; null to load all records</param>
        /// <param name="commentText">Search comment text; null to load all records</param>
        /// <returns>Comments</returns>
        public virtual IList<NewsComment> GetAllComments(int customerId = 0, int storeId = 0, int? newsItemId = null,
            bool? approved = null, DateTime? fromUtc = null, DateTime? toUtc = null, string commentText = null)
        {
            var query = _newsCommentRepository.Table;

            if (approved.HasValue)
                query = query.Where(comment => comment.IsApproved == approved);

            if (newsItemId > 0)
                query = query.Where(comment => comment.NewsItemId == newsItemId);

            if (customerId > 0)
                query = query.Where(comment => comment.CustomerId == customerId);

            if (storeId > 0)
                query = query.Where(comment => comment.StoreId == storeId);

            if (fromUtc.HasValue)
                query = query.Where(comment => fromUtc.Value <= comment.CreatedOnUtc);

            if (toUtc.HasValue)
                query = query.Where(comment => toUtc.Value >= comment.CreatedOnUtc);

            if (!string.IsNullOrEmpty(commentText))
                query = query.Where(c => c.CommentText.Contains(commentText) || c.CommentTitle.Contains(commentText));

            query = query.OrderBy(nc => nc.CreatedOnUtc);

            return query.ToList();
        }

        /// <summary>
        /// Gets a news comment
        /// </summary>
        /// <param name="newsCommentId">News comment identifier</param>
        /// <returns>News comment</returns>
        public virtual NewsComment GetNewsCommentById(int newsCommentId)
        {
            if (newsCommentId == 0)
                return null;

            return _newsCommentRepository.ToCachedGetById(newsCommentId);
        }

        /// <summary>
        /// Get news comments by identifiers
        /// </summary>
        /// <param name="commentIds">News comment identifiers</param>
        /// <returns>News comments</returns>
        public virtual IList<NewsComment> GetNewsCommentsByIds(int[] commentIds)
        {
            if (commentIds == null || commentIds.Length == 0)
                return new List<NewsComment>();

            var query = from nc in _newsCommentRepository.Table
                        where commentIds.Contains(nc.Id)
                        select nc;
            var comments = query.ToList();
            //sort by passed identifiers
            var sortedComments = new List<NewsComment>();
            foreach (var id in commentIds)
            {
                var comment = comments.Find(x => x.Id == id);
                if (comment != null)
                    sortedComments.Add(comment);
            }

            return sortedComments;
        }

        /// <summary>
        /// Get the count of news comments
        /// </summary>
        /// <param name="newsItem">News item</param>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <param name="isApproved">A value indicating whether to count only approved or not approved comments; pass null to get number of all comments</param>
        /// <returns>Number of news comments</returns>
        public virtual int GetNewsCommentsCount(NewsItem newsItem, int storeId = 0, bool? isApproved = null)
        {
            var query = _newsCommentRepository.Table.Where(comment => comment.NewsItemId == newsItem.Id);

            if (storeId > 0)
                query = query.Where(comment => comment.StoreId == storeId);

            if (isApproved.HasValue)
                query = query.Where(comment => comment.IsApproved == isApproved.Value);

            var cacheKey = NopNewsCachingDefaults.NewsCommentsNumberCacheKey.FillCacheKey(newsItem, storeId, isApproved);

            return query.ToCachedCount(cacheKey);
        }

        /// <summary>
        /// Deletes a news comment
        /// </summary>
        /// <param name="newsComment">News comment</param>
        public virtual void DeleteNewsComment(NewsComment newsComment)
        {
            if (newsComment == null)
                throw new ArgumentNullException(nameof(newsComment));

            _newsCommentRepository.Delete(newsComment);

            //event notification
            _eventPublisher.EntityDeleted(newsComment);
        }

        /// <summary>
        /// Deletes a news comments
        /// </summary>
        /// <param name="newsComments">News comments</param>
        public virtual void DeleteNewsComments(IList<NewsComment> newsComments)
        {
            if (newsComments == null)
                throw new ArgumentNullException(nameof(newsComments));

            foreach (var newsComment in newsComments)
            {
                DeleteNewsComment(newsComment);
            }
        }

        /// <summary>
        /// Inserts a news comment
        /// </summary>
        /// <param name="comment">News comment</param>
        public virtual void InsertNewsComment(NewsComment comment)
        {
            if (comment == null)
                throw new ArgumentNullException(nameof(comment));

            _newsCommentRepository.Insert(comment);

            //event notification
            _eventPublisher.EntityInserted(comment);
        }

        #endregion

        #region News pictures

        /// <summary>
        /// Deletes a news picture
        /// </summary>
        /// <param name="newsPicture">News picture</param>
        public virtual void DeleteNewsPicture(NewsPicture newsPicture)
        {
            if (newsPicture == null)
                throw new ArgumentNullException(nameof(newsPicture));

            _newsPictureRepository.Delete(newsPicture);

            //event notification
            _eventPublisher.EntityDeleted(newsPicture);
        }

        /// <summary>
        /// Gets a news pictures by news identifier
        /// </summary>
        /// <param name="newsId">The news identifier</param>
        /// <returns>News pictures</returns>
        public virtual IList<NewsPicture> GetNewsPicturesByNewsId(int newsId)
        {
            var query = from pp in _newsPictureRepository.Table
                        where pp.NewsId == newsId
                        orderby pp.DisplayOrder, pp.Id
                        select pp;

            var newsPictures = query.ToList();

            return newsPictures;
        }

        /// <summary>
        /// Gets a news picture
        /// </summary>
        /// <param name="newsPictureId">News picture identifier</param>
        /// <returns>News picture</returns>
        public virtual NewsPicture GetNewsPictureById(int newsPictureId)
        {
            if (newsPictureId == 0)
                return null;

            return _newsPictureRepository.ToCachedGetById(newsPictureId);
        }

        /// <summary>
        /// Inserts a news picture
        /// </summary>
        /// <param name="newsPicture">News picture</param>
        public virtual void InsertNewsPicture(NewsPicture newsPicture)
        {
            if (newsPicture == null)
                throw new ArgumentNullException(nameof(newsPicture));

            _newsPictureRepository.Insert(newsPicture);

            //event notification
            _eventPublisher.EntityInserted(newsPicture);
        }

        /// <summary>
        /// Updates a news picture
        /// </summary>
        /// <param name="newsPicture">News picture</param>
        public virtual void UpdateNewsPicture(NewsPicture newsPicture)
        {
            if (newsPicture == null)
                throw new ArgumentNullException(nameof(newsPicture));

            _newsPictureRepository.Update(newsPicture);

            //event notification
            _eventPublisher.EntityUpdated(newsPicture);
        }

        /// <summary>
        /// Get the IDs of all news images 
        /// </summary>
        /// <param name="newssIds">Newss IDs</param>
        /// <returns>All picture identifiers grouped by news ID</returns>
        public IDictionary<int, int[]> GetNewssImagesIds(int[] newssIds)
        {
            var newsPictures = _newsPictureRepository.Table.Where(p => newssIds.Contains(p.NewsId)).ToList();

            return newsPictures.GroupBy(p => p.NewsId).ToDictionary(p => p.Key, p => p.Select(p1 => p1.PictureId).ToArray());
        }
        #endregion

        #region news categories mapping
        public IList<NewsCategoryMap> GetNewsByNewsCategoryId(int categoryId, bool showHidden = false)
        {
            if (categoryId == 0)
                return new List<NewsCategoryMap>();

            var query = from cm in _newsCategoryMapRepository.Table
                        join n in _newsCategoryRepository.Table on cm.CategoryId equals n.Id
                        where cm.CategoryId == categoryId
                        select cm;
            return query.OrderBy(x=>x.DisplayOrder).ToList();
        }
        public IPagedList<NewsItem> GetNewsByNewsCategoryId(int categoryId, int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            var query = from m in _newsCategoryMapRepository.Table
                        join n in _newsItemRepository.Table on m.NewsId equals n.Id
                        join c in _newsCategoryRepository.Table on m.CategoryId equals c.Id
                        where c.Id == categoryId
                        orderby n.CreatedOnUtc descending
                        select n;

            var result = new PagedList<NewsItem>(query, pageIndex, pageSize);
            return result;
        }

        public IList<NewsCategoryMap> GetNewsByNewsCategoryId(int categoryId, int storeId, bool showHidden = false)
        {
            return GetNewsByNewsCategoryId(categoryId, false);
        }

        public NewsCategoryMap GetNewsCategoryMapById(int newsCategoryMapId)
        {
            if (newsCategoryMapId == 0)
                return null;

            return _newsCategoryMapRepository.ToCachedGetById(newsCategoryMapId);
        }

        public void InsertNewsCategoryMap(NewsCategoryMap newsCategoryMap)
        {
            if (newsCategoryMap == null)
                throw new ArgumentNullException(nameof(newsCategoryMap));

            _newsCategoryMapRepository.Insert(newsCategoryMap);

            //event notification
            _eventPublisher.EntityInserted(newsCategoryMap);
        }

        public void UpdateNewsCategoryMap(NewsCategoryMap newsCategoryMap)
        {
            if (newsCategoryMap == null)
                throw new ArgumentNullException(nameof(newsCategoryMap));

            _newsCategoryMapRepository.Update(newsCategoryMap);

            //event notification
            _eventPublisher.EntityUpdated(newsCategoryMap);
        }

        public IList<NewsCategoryMap> GetByNewsId(int newsId, bool showHidden = false)
        {
            if (newsId == 0)
                return new List<NewsCategoryMap>();

            var query = from cm in _newsCategoryMapRepository.Table
                        where cm.NewsId == newsId
                        select cm;
            return query.ToList();
        }

        public void DeleteNewsCategoryMap(NewsCategoryMap newsCategoryMap)
        {
            if (newsCategoryMap == null)
                throw new ArgumentNullException(nameof(newsCategoryMap));


            _newsCategoryMapRepository.Delete(newsCategoryMap);

            //event notification
            _eventPublisher.EntityDeleted(newsCategoryMap);
        }

        public NewsCategoryMap FindNewsCategoryMap(IList<NewsCategoryMap> source, int newsId, int categoryId)
        {
            foreach (var item in source)
                if (item.NewsId == newsId && item.CategoryId == categoryId)
                    return item;

            return null;
        }

        public List<TopMenuModel> GetTopMenu()
        {
            var menu = new List<TopMenuModel>
            {
                // SAIGON HEALTHCARE
                 new TopMenuModel
                {
                    Name =  "SAIGON HEALTHCARE",
                    SeName = "trang-chu",
                    SubMenus = new List<TopMenuModel>{
                        new TopMenuModel{
                            Name = _localizationService.GetResource("menu.aboutus"),
                            SeName = "ve-chung-toi",
                        },
                        new TopMenuModel{
                            Name = _localizationService.GetResource("menu.recruit"),
                            SeName="thong-tin-tuyen-dung"
                        },
                        new TopMenuModel{
                            Name = _localizationService.GetResource("menu.contact"),
                            SeName="lien-he"
                        },
                    }
                },
                // CHUYÊN KHOA
                 new TopMenuModel
                {
                    Name =  _localizationService.GetResource("menu.faculty"),
                    SeName = "chuyen-khoa",
                },
                // CHUYÊN GIA - BÁC SĨ
                   new TopMenuModel
                {
                    Name =  _localizationService.GetResource("menu.specialist.doctor"),
                    SeName = "bac-si",
                },
                // DỊCH VỤ
                   new TopMenuModel
                {
                    Name =  _localizationService.GetResource("menu.service"),
                    SeName = "dich-vu",
                },
                // GÓI KHÁM
                   new TopMenuModel
                {
                    Name =  _localizationService.GetResource("menu.package"),
                    SeName = "goi-kham",
                },
                // HƯỚNG DẪN KHÁCH HÀNG
                   new TopMenuModel
                {
                    Name =  _localizationService.GetResource("menu.faq"),
                    SeName = "huong-dan-khach-hang",
                },
                // TIN TỨC
                new TopMenuModel
                {
                    Name = _localizationService.GetResource("menu.news"),
                    SeName = "tin-tuc"
                }
            };
            // get list category in top menu
            var categories = from c in _newsCategoryRepository.Table
                             where c.Published && c.IncludeInTopMenu
                             orderby c.DisplayOrder,c.CreatedOnUtc ascending
                             select c;
            if (categories == null || !categories.Any())
            {
                return menu;
            }
            foreach (var item in menu)
            {
                var menuItem = categories.FirstOrDefault(x => x.MenuSeName == item.SeName);
                if (menuItem == null)
                {
                    continue;
                }
                if (item.SeName == "chuyen-khoa" || item.SeName == "huong-dan-khach-hang")
                {
                    foreach (var subCate in categories.Where(x => x.CategoryId == menuItem.Id))
                    {
                        item.SubMenus.Add(new TopMenuModel
                        {
                            CategoryId = subCate.Id,
                            Name = subCate.Title,
                            SeName = _urlRecordService.GetSeName(subCate, subCate.LanguageId, true, false)
                        });
                    }
                }

                item.CategoryId = menuItem.Id;
            }

            // get list news belong to above category have in top menu
            var news = from n in _newsItemRepository.Table
                       join m in _newsCategoryMapRepository.Table on n.Id equals m.NewsId
                       join c in categories on m.CategoryId equals c.Id
                       where n.Published && n.IncludeInTopMenu
                       orderby n.DisplayOrder, n.CreatedOnUtc ascending
                       select new { n, m, c };
            if (news != null && news.Any())
            {
                foreach (var newsItem in news)
                {

                    if (menu.Any(x => x.CategoryId == newsItem.c.Id))
                    {
                        menu.First(x => x.CategoryId == newsItem.c.Id).SubMenus.Add(new TopMenuModel
                        {
                            Name = newsItem.n.Title,
                            SeName = _urlRecordService.GetSeName(newsItem.n, newsItem.n.LanguageId, true, false)
                        });
                    }
                }
            }
            return menu;
        }
        public List<NewsItem> GetRelatedNews(int newsId)
        {
            var categories = from m in _newsCategoryMapRepository.Table
                             join n in _newsItemRepository.Table on m.NewsId equals n.Id
                             join c in _newsCategoryRepository.Table on m.CategoryId equals c.Id
                             where n.Id == newsId
                             select c;
            if (categories == null || !categories.Any())
            {
                return new List<NewsItem>();
            }
            var relatedNews = from m in _newsCategoryMapRepository.Table
                              join n in _newsItemRepository.Table on m.NewsId equals n.Id
                              join c in categories on m.CategoryId equals c.Id
                              where n.Id != newsId
                              orderby n.CreatedOnUtc descending
                              select n;
            if (relatedNews == null || !relatedNews.Any())
            {
                return new List<NewsItem>();
            }
            return relatedNews.OrderBy(x=>x.DisplayOrder).ThenByDescending(x=>x.StartDateUtc ?? x.CreatedOnUtc).Take(5).ToList();
        }

        public string GetFormattedBreadCrumb(NewsCategory category, IList<NewsCategory> allCategories = null, string separator = ">>", int languageId = 0)
        {
            var result = string.Empty;

            var breadcrumb = GetCategoryBreadCrumb(category, allCategories, true);
            for (var i = 0; i <= breadcrumb.Count - 1; i++)
            {
                var categoryName = _localizationService.GetLocalized(breadcrumb[i], x => x.Title, languageId);
                result = string.IsNullOrEmpty(result) ? categoryName : $"{result} {separator} {categoryName}";
            }

            return result;
        }

        public IList<NewsCategory> GetAllCategories(int storeId = 0, bool showHidden = false)
        {
            var key = NopCatalogCachingDefaults.NewsCategoriesAllCacheKey.FillCacheKey(
                storeId,
                string.Join(",", _customerService.GetCustomerRoleIds(_workContext.CurrentCustomer)),
                showHidden);

            var categories = _staticCacheManager.Get(key, () => GetAllCategories(string.Empty, storeId, showHidden: showHidden));

            return categories;
        }

        public void DeleteNewsCategoryPicture(NewsCategoryPicture newsCategoryPicture)
        {
            if (newsCategoryPicture == null)
                throw new ArgumentNullException(nameof(newsCategoryPicture));

            _newsCategoryPictureRepository.Delete(newsCategoryPicture);

            //event notification
            _eventPublisher.EntityDeleted(newsCategoryPicture);
        }

        public IList<NewsCategoryPicture> GetNewsCategoryPicturesByNewsCategoryId(int newsCategoryId)
        {
            var query = from pp in _newsCategoryPictureRepository.Table
                        where pp.NewsCategoryId == newsCategoryId
                        orderby pp.DisplayOrder, pp.Id
                        select pp;

            return query.ToList();
        }

        public NewsCategoryPicture GetNewsCategoryPictureById(int newsCategoryPictureId)
        {
            if (newsCategoryPictureId == 0)
                return null;

            return _newsCategoryPictureRepository.ToCachedGetById(newsCategoryPictureId);
        }

        public void InsertNewsCategoryPicture(NewsCategoryPicture newsCategoryPicture)
        {
            if (newsCategoryPicture == null)
                throw new ArgumentNullException(nameof(newsCategoryPicture));

            _newsCategoryPictureRepository.Insert(newsCategoryPicture);

            //event notification
            _eventPublisher.EntityInserted(newsCategoryPicture);
        }

        public void UpdateNewsCategoryPicture(NewsCategoryPicture newsCategoryPicture)
        {
            if (newsCategoryPicture == null)
                throw new ArgumentNullException(nameof(newsCategoryPicture));

            _newsCategoryPictureRepository.Update(newsCategoryPicture);

            //event notification
            _eventPublisher.EntityUpdated(newsCategoryPicture);
        }
        public IList<NewsCategory> GetServices(bool showHidden = false)
        {
            var query = _newsCategoryRepository.Table;
            var service = query.OrderBy(x => x.CreatedOnUtc).FirstOrDefault(x => x.MenuSeName == "dich-vu");
            if (service == null)
            {
                return null;
            }
            if(!showHidden){
                query = query.Where(x => x.Published);
            }

            return query.Where(x => x.CategoryId == service.Id).OrderBy(x=>x.DisplayOrder).ThenByDescending(x=>x.StartDateUtc ?? x.CreatedOnUtc).ToList();
        }
        public NewsCategory GetByParentId(int categoryId)
        {
            var query = _newsCategoryRepository.Table;
            return query.FirstOrDefault(x => x.CategoryId == categoryId);
        }
        public NewsCategory GetRoot(int childId)
        {
            var query = _newsCategoryRepository.Table;
            for (int i = 0; i < 10; i++) // maximum 10 calls
            {
                var child = query.FirstOrDefault(x => x.Id == childId);
                if (child == null)
                {
                    return null;
                }

                if (child.CategoryId == null)
                {
                    // found root
                    return child;
                }

                var parent = query.FirstOrDefault(x => x.Id == child.CategoryId);
                if (parent == null)
                {
                    // workaround?
                    return child;
                }
                childId = parent.Id;
            }
            // not found
            return null;
        }

        public List<NewsCategory> GetNewsCategoriesChildByMenuSeName(string menuSeName)
        {
            if (string.IsNullOrEmpty(menuSeName))
            {
                return new List<NewsCategory>();
            }
            var root = _newsCategoryRepository.Table.FirstOrDefault(x => x.MenuSeName == menuSeName);
            if (root == null)
            {
                return new List<NewsCategory>();
            }
            var children = _newsCategoryRepository.Table.Where(x => x.CategoryId == root.Id).OrderBy(x=>x.DisplayOrder).ThenByDescending(x=>x.StartDateUtc ?? x.CreatedOnUtc);
            return children.ToList();
        }

        public IPagedList<NewsCategory> GetNewsCategoriesChildByMenuSeName(string menuSeName, int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            var query = _newsCategoryRepository.Table;
            if (string.IsNullOrEmpty(menuSeName))
            {
                return new PagedList<NewsCategory>(query.Where(x => x.Id == 0), pageIndex, pageSize);
            }
            var root = _newsCategoryRepository.Table.FirstOrDefault(x => x.MenuSeName == menuSeName);
            if (root == null)
            {
                return new PagedList<NewsCategory>(query.Where(x => x.Id == 0).OrderBy(x=>x.DisplayOrder).ThenByDescending(x=>x.StartDateUtc ?? x.CreatedOnUtc), pageIndex, pageSize);
            }
            query = query.Where(x => x.CategoryId == root.Id).OrderBy(x=>x.DisplayOrder).ThenByDescending(x=>x.StartDateUtc ?? x.CreatedOnUtc);
            var result = new PagedList<NewsCategory>(query, pageIndex, pageSize);
            return result;
        }
        public NewsCategory GetNewsCategoryByMenuSeName(string menuSeName)
        {
            if (string.IsNullOrEmpty(menuSeName))
            {
                return null;
            }
            var entity = _newsCategoryRepository.Table.FirstOrDefault(x => x.MenuSeName == menuSeName);
            return entity;
        }
        public List<NewsItem> GetNewsByMenuSeName(string menuSeName, bool showOnHomePage = false)
        {
            if (string.IsNullOrEmpty(menuSeName))
            {
                return new List<NewsItem>();
            }

            var query = _newsCategoryRepository.Table;
            var root = _newsCategoryRepository.Table.FirstOrDefault(x => x.MenuSeName == menuSeName);
            if (root == null)
            {
                return new List<NewsItem>();
            }
            var categories = query.Where(x => x.CategoryId == root.Id).Select(x => x.Id).ToList();
            categories.Add(root.Id);

            var news = from m in _newsCategoryMapRepository.Table
                       join n in _newsItemRepository.Table on m.NewsId equals n.Id
                       where categories.Contains(m.CategoryId)
                       orderby n.DisplayOrder ascending, n.CreatedOnUtc descending
                       select n;
            if(showOnHomePage){
                news = news.Where(x => x.ShowOnHomePage);
            }
            return news.ToList();
        }

        public bool CheckNewsInMenuSeName(string menuSeName, int newsId)
        {
            if (string.IsNullOrEmpty(menuSeName) || newsId == 0)
            {
                return false;
            }

            var query = _newsCategoryRepository.Table;
            var root = _newsCategoryRepository.Table.FirstOrDefault(x => x.MenuSeName == menuSeName);
            if (root == null)
            {
                return false;
            }
            var categories = query.Where(x => x.CategoryId == root.Id).Select(x => x.Id).ToList();
            categories.Add(root.Id);

            var news = from m in _newsCategoryMapRepository.Table
                       join n in _newsItemRepository.Table on m.NewsId equals n.Id
                       where n.Id == newsId && categories.Contains(m.CategoryId) 
                       orderby n.DisplayOrder ascending, n.CreatedOnUtc descending
                       select n;
            return news.FirstOrDefault() != null;
        }
        public virtual IList<BlogPostTag> GetAllBlogPostTags(int storeId, int languageId, bool showHidden = false)
        {
            var cacheKey = NopBlogsCachingDefaults.BlogTagsModelCacheKey.FillCacheKey(languageId, storeId);

            var blogPostTags = _cacheManager.Get(cacheKey, () =>
            {
                var rezBlogPostTags = new List<BlogPostTag>();

                var blogPosts = GetAllNews(storeId, languageId, showHidden: showHidden);

                foreach (var blogPost in blogPosts)
                {
                    var tags = ParseTags(blogPost);
                    foreach (var tag in tags)
                    {
                        var foundBlogPostTag = rezBlogPostTags.Find(bpt =>
                            bpt.Name.Equals(tag, StringComparison.InvariantCultureIgnoreCase));
                        if (foundBlogPostTag == null)
                        {
                            foundBlogPostTag = new BlogPostTag
                            {
                                Name = tag,
                                BlogPostCount = 1
                            };
                            rezBlogPostTags.Add(foundBlogPostTag);
                        }
                        else
                            foundBlogPostTag.BlogPostCount++;
                    }
                }

                return rezBlogPostTags;
            });

            return blogPostTags;
        }
        public virtual IList<string> ParseTags(NewsItem blogPost) 
        {
            if (blogPost == null)
                throw new ArgumentNullException(nameof(blogPost));

            if (blogPost.Tags == null)
                return new List<string>();

            var tags = blogPost.Tags.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(tag => tag.Trim())
                .Where(tag => !string.IsNullOrEmpty(tag)).ToList();

            return tags;
        }
        #endregion
        #endregion
    }
}