﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Blogs;
using Nop.Core.Domain.News;

namespace Nop.Services.News
{
    /// <summary>
    /// News service interface
    /// </summary>
    public partial interface INewsService
    {
        /// <summary>
        /// Get formatted category breadcrumb 
        /// Note: ACL and store mapping is ignored
        /// </summary>
        /// <param name="category">Category</param>
        /// <param name="allCategories">All categories</param>
        /// <param name="separator">Separator</param>
        /// <param name="languageId">Language identifier for localization</param>
        /// <returns>Formatted breadcrumb</returns>
        string GetFormattedBreadCrumb(NewsCategory category, IList<NewsCategory> allCategories = null,
            string separator = ">>", int languageId = 0);
        IList<NewsCategory> GetAllCategories(int storeId = 0, bool showHidden = false);
        #region News categories

        /// <summary>
        /// Deletes a news
        /// </summary>
        /// <param name="NewsCategory">News item</param>
        void DeleteNewsCategory(NewsCategory newsCategory);

        /// <summary>
        /// Gets a news
        /// </summary>
        /// <param name="newsId">The news identifier</param>
        /// <returns>News</returns>
        NewsCategory GetNewsCategoryById(int newsId);

        /// <summary>
        /// Gets news
        /// </summary>
        /// <param name="newsIds">The news identifiers</param>
        /// <returns>News</returns>
        IList<NewsCategory> GetNewsCategoryByIds(int[] newsIds);

        /// <summary>
        /// Gets all news
        /// </summary>
        /// <param name="languageId">Language identifier; 0 if you want to get all records</param>
        /// <param name="storeId">Store identifier; 0 if you want to get all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>News items</returns>
        IPagedList<NewsCategory> GetAllNewsCategory(int languageId = 0, int storeId = 0,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, bool isNews = false, int? parentId = null, string search = null);

        /// <summary>
        /// Inserts a news item
        /// </summary>
        /// <param name="news">News item</param>
        void InsertNewsCategory(NewsCategory news);

        /// <summary>
        /// Updates the news item
        /// </summary>
        /// <param name="news">News item</param>
        void UpdateNewsCategory(NewsCategory news);

        /// <summary>
        /// Get a value indicating whether a news item is available now (availability dates)
        /// </summary>
        /// <param name="NewsCategory">News item</param>
        /// <param name="dateTime">Datetime to check; pass null to use current date</param>
        /// <returns>Result</returns>
        bool IsNewsCategoryAvailable(NewsCategory newsCategory, DateTime? dateTime = null);

        #endregion

        #region News

        /// <summary>
        /// Deletes a news
        /// </summary>
        /// <param name="newsItem">News item</param>
        void DeleteNews(NewsItem newsItem);

        /// <summary>
        /// Gets a news
        /// </summary>
        /// <param name="newsId">The news identifier</param>
        /// <returns>News</returns>
        NewsItem GetNewsById(int newsId);

        /// <summary>
        /// Gets news
        /// </summary>
        /// <param name="newsIds">The news identifiers</param>
        /// <returns>News</returns>
        IList<NewsItem> GetNewsByIds(int[] newsIds);
        IList<NewsCategory> GetServices(bool showHidden = false);

        /// <summary>
        /// Gets all news
        /// </summary>
        /// <param name="languageId">Language identifier; 0 if you want to get all records</param>
        /// <param name="storeId">Store identifier; 0 if you want to get all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>News items</returns>
        IPagedList<NewsItem> GetAllNews(int languageId = 0, int storeId = 0,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, bool isNews = false, int? categoryId = null, string search = null, bool showOnHomePage = false);

        /// <summary>
        /// Inserts a news item
        /// </summary>
        /// <param name="news">News item</param>
        void InsertNews(NewsItem news);

        /// <summary>
        /// Updates the news item
        /// </summary>
        /// <param name="news">News item</param>
        void UpdateNews(NewsItem news);

        /// <summary>
        /// Get a value indicating whether a news item is available now (availability dates)
        /// </summary>
        /// <param name="newsItem">News item</param>
        /// <param name="dateTime">Datetime to check; pass null to use current date</param>
        /// <returns>Result</returns>
        bool IsNewsAvailable(NewsItem newsItem, DateTime? dateTime = null);

        #endregion

        #region News comments

        /// <summary>
        /// Gets all comments
        /// </summary>
        /// <param name="customerId">Customer identifier; 0 to load all records</param>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <param name="newsItemId">News item ID; 0 or null to load all records</param>
        /// <param name="approved">A value indicating whether to content is approved; null to load all records</param> 
        /// <param name="fromUtc">Item creation from; null to load all records</param>
        /// <param name="toUtc">Item creation to; null to load all records</param>
        /// <param name="commentText">Search comment text; null to load all records</param>
        /// <returns>Comments</returns>
        IList<NewsComment> GetAllComments(int customerId = 0, int storeId = 0, int? newsItemId = null,
            bool? approved = null, DateTime? fromUtc = null, DateTime? toUtc = null, string commentText = null);

        /// <summary>
        /// Gets a news comment
        /// </summary>
        /// <param name="newsCommentId">News comment identifier</param>
        /// <returns>News comment</returns>
        NewsComment GetNewsCommentById(int newsCommentId);

        /// <summary>
        /// Get news comments by identifiers
        /// </summary>
        /// <param name="commentIds">News comment identifiers</param>
        /// <returns>News comments</returns>
        IList<NewsComment> GetNewsCommentsByIds(int[] commentIds);

        /// <summary>
        /// Get the count of news comments
        /// </summary>
        /// <param name="newsItem">News item</param>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <param name="isApproved">A value indicating whether to count only approved or not approved comments; pass null to get number of all comments</param>
        /// <returns>Number of news comments</returns>
        int GetNewsCommentsCount(NewsItem newsItem, int storeId = 0, bool? isApproved = null);

        /// <summary>
        /// Deletes a news comment
        /// </summary>
        /// <param name="newsComment">News comment</param>
        void DeleteNewsComment(NewsComment newsComment);

        /// <summary>
        /// Deletes a news comments
        /// </summary>
        /// <param name="newsComments">News comments</param>
        void DeleteNewsComments(IList<NewsComment> newsComments);

        /// <summary>
        /// Inserts a news comment
        /// </summary>
        /// <param name="comment">News comment</param>
        void InsertNewsComment(NewsComment comment);

        #endregion

        #region News pictures

        /// <summary>
        /// Deletes a news picture
        /// </summary>
        /// <param name="newsPicture">News picture</param>
        void DeleteNewsPicture(NewsPicture newsPicture);
        void DeleteNewsCategoryPicture(NewsCategoryPicture newsCategoryPicture);

        /// <summary>
        /// Gets a news pictures by news identifier
        /// </summary>
        /// <param name="newsId">The news identifier</param>
        /// <returns>News pictures</returns>
        IList<NewsPicture> GetNewsPicturesByNewsId(int newsId);
        IList<NewsCategoryPicture> GetNewsCategoryPicturesByNewsCategoryId(int newsCategoryId);

        /// <summary>
        /// Gets a news picture
        /// </summary>
        /// <param name="newsPictureId">News picture identifier</param>
        /// <returns>News picture</returns>
        NewsPicture GetNewsPictureById(int newsPictureId);
        NewsCategoryPicture GetNewsCategoryPictureById(int newsCategoryPictureId);

        /// <summary>
        /// Inserts a news picture
        /// </summary>
        /// <param name="newsPicture">News picture</param>
        void InsertNewsPicture(NewsPicture newsPicture);
        void InsertNewsCategoryPicture(NewsCategoryPicture newsCategoryPicture);

        /// <summary>
        /// Updates a news picture
        /// </summary>
        /// <param name="newsPicture">News picture</param>
        void UpdateNewsPicture(NewsPicture newsPicture);
        void UpdateNewsCategoryPicture(NewsCategoryPicture newsCategoryPicture);

        /// <summary>
        /// Get the IDs of all news images 
        /// </summary>
        /// <param name="newssIds">Newss IDs</param>
        /// <returns>All picture identifiers grouped by news ID</returns>
        IDictionary<int, int[]> GetNewssImagesIds(int[] newssIds);

        #endregion

        #region news category mapping
        /// <summary>
        /// Gets product category mapping collection
        /// </summary>
        /// <param name="categoryId">Category identifier</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Product a category mapping collection</returns>
        IPagedList<NewsItem> GetNewsByNewsCategoryId(int categoryId, int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        /// <summary>
        /// Gets a product category mapping collection
        /// </summary>
        /// <param name="categoryId">Product identifier</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Product category mapping collection</returns>
        IList<NewsCategoryMap> GetNewsByNewsCategoryId(int categoryId, bool showHidden = false);
        /// <summary>
        /// Gets a product category mapping collection
        /// </summary>
        /// <param name="newsId">Product identifier</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Product category mapping collection</returns>
        IList<NewsCategoryMap> GetByNewsId(int newsId, bool showHidden = false);
        /// <summary>
        /// Gets a product category mapping collection
        /// </summary>
        /// <param name="categoryId">Product identifier</param>
        /// <param name="storeId">Store identifier (used in multi-store environment). "showHidden" parameter should also be "true"</param>
        /// <param name="showHidden"> A value indicating whether to show hidden records</param>
        /// <returns> Product category mapping collection</returns>
        IList<NewsCategoryMap> GetNewsByNewsCategoryId(int categoryId, int storeId, bool showHidden = false);

        /// <summary>
        /// Gets a product category mapping 
        /// </summary>
        /// <param name="newsCategoryMapId">Product category mapping identifier</param>
        /// <returns>Product category mapping</returns>
        NewsCategoryMap GetNewsCategoryMapById(int newsCategoryMapId);
        /// <summary>
        /// Returns a ProductCategory that has the specified values
        /// </summary>
        /// <param name="source">Source</param>
        /// <param name="newsId">Product identifier</param>
        /// <param name="categoryId">Category identifier</param>
        /// <returns>A ProductCategory that has the specified values; otherwise null</returns>
        NewsCategoryMap FindNewsCategoryMap(IList<NewsCategoryMap> source, int newsId, int categoryId);
        /// <summary>
        /// Inserts a product category mapping
        /// </summary>
        /// <param name="newsCategoryMap">>Product category mapping</param>
        void InsertNewsCategoryMap(NewsCategoryMap newsCategoryMap);

        /// <summary>
        /// Updates the product category mapping 
        /// </summary>
        /// <param name="newsCategoryMap">>Product category mapping</param>
        void UpdateNewsCategoryMap(NewsCategoryMap newsCategoryMap);
        /// <summary>
        /// Deletes a product category mapping
        /// </summary>
        /// <param name="newsCategoryMap">Product category</param>
        void DeleteNewsCategoryMap(NewsCategoryMap newsCategoryMap);
        #endregion


        /// <summary>
        /// Gets a news
        /// </summary>
        /// <returns>News</returns>
        List<TopMenuModel> GetTopMenu();
        /// <summary>
        /// Gets related news
        /// </summary>
        /// <returns>News</returns>
        List<NewsItem> GetRelatedNews(int newsId);
        NewsCategory GetNewsCategoryByMenuSeName(string menuSeName);
        NewsCategory GetByParentId(int categoryId);
        NewsCategory GetRoot(int childId);
        IPagedList<NewsCategory> GetNewsCategoriesChildByMenuSeName(string menuSeName, int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);
        List<NewsCategory> GetNewsCategoriesChildByMenuSeName(string menuSeName);
        List<NewsItem> GetNewsByMenuSeName(string menuSeName, bool showOnHomePage = false);
        IList<BlogPostTag> GetAllBlogPostTags(int storeId, int languageId, bool showHidden = false);
        bool CheckNewsInMenuSeName(string menuSeName, int newsId);

    }
}