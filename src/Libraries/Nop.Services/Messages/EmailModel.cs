﻿using System.ComponentModel.DataAnnotations;

namespace Nop.Services.Messages
{
    public partial class EmailModel
    {
        public EmailModel()
        {
        }

        public string To { get; set; }
        public string CC { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public string Gender { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string BranchName { get; set; }
        public string Message { get; set; }
        public string ProductName { get; set; }


    }
}