﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Stores;
using Nop.Data;
using Nop.Services.Events;

namespace Nop.Services.Stores
{
    /// <summary>
    /// Branch service
    /// </summary>
    public partial class BranchService : IBranchService
    {
        #region Fields

        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<Branch> _branchRepository;

        #endregion

        #region Ctor

        public BranchService(IEventPublisher eventPublisher,
            IRepository<Branch> branchRepository)
        {
            _eventPublisher = eventPublisher;
            _branchRepository = branchRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes a branch
        /// </summary>
        /// <param name="branch">Branch</param>
        public virtual void DeleteBranch(Branch branch)
        {
            if (branch == null)
                throw new ArgumentNullException(nameof(branch));

            var allBranchs = GetAllBranchs();
            if (allBranchs.Count == 1)
                throw new Exception("You cannot delete the only configured branch");

            _branchRepository.Delete(branch);

            //event notification
            _eventPublisher.EntityDeleted(branch);
        }

        /// <summary>
        /// Gets all branchs
        /// </summary>
        /// <returns>Branchs</returns>
        public virtual IList<Branch> GetAllBranchs()
        {
            var query = from s in _branchRepository.Table orderby s.Id select s;

            return query.ToList();
        }

        /// <summary>
        /// Gets a branch 
        /// </summary>
        /// <param name="branchId">Branch identifier</param>
        /// <returns>Branch</returns>
        public virtual Branch GetBranchById(int branchId)
        {
            if (branchId == 0)
                return null;

            var branch = _branchRepository.GetById(branchId);

            return branch;
        }

        /// <summary>
        /// Inserts a branch
        /// </summary>
        /// <param name="branch">Branch</param>
        public virtual void InsertBranch(Branch branch)
        {
            if (branch == null)
                throw new ArgumentNullException(nameof(branch));

            _branchRepository.Insert(branch);

            //event notification
            _eventPublisher.EntityInserted(branch);
        }

        /// <summary>
        /// Updates the branch
        /// </summary>
        /// <param name="branch">Branch</param>
        public virtual void UpdateBranch(Branch branch)
        {
            if (branch == null)
                throw new ArgumentNullException(nameof(branch));
            
            _branchRepository.Update(branch);

            //event notification
            _eventPublisher.EntityUpdated(branch);
        }

        #endregion
    }
}