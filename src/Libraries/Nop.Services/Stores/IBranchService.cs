﻿using System.Collections.Generic;
using Nop.Core.Domain.Stores;

namespace Nop.Services.Stores
{
    /// <summary>
    /// Branch service interface
    /// </summary>
    public partial interface IBranchService
    {
        /// <summary>
        /// Deletes a branch
        /// </summary>
        /// <param name="branch">Branch</param>
        void DeleteBranch(Branch branch);

        /// <summary>
        /// Gets all branchs
        /// </summary>
        /// <returns>Branchs</returns>
        IList<Branch> GetAllBranchs();

        /// <summary>
        /// Gets a branch 
        /// </summary>
        /// <param name="branchId">Branch identifier</param>
        /// <returns>Branch</returns>
        Branch GetBranchById(int branchId);

        /// <summary>
        /// Inserts a branch
        /// </summary>
        /// <param name="branch">Branch</param>
        void InsertBranch(Branch branch);

        /// <summary>
        /// Updates the branch
        /// </summary>
        /// <param name="branch">Branch</param>
        void UpdateBranch(Branch branch);
    }
}