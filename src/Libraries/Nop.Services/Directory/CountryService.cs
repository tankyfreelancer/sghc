﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Stores;
using Nop.Data;
using Nop.Services.Caching.CachingDefaults;
using Nop.Services.Caching.Extensions;
using Nop.Services.Events;
using Nop.Services.Localization;

namespace Nop.Services.Directory
{
    /// <summary>
    /// Country service
    /// </summary>
    public partial class CountryService : ICountryService
    {
        #region Fields

        private readonly CatalogSettings _catalogSettings;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILocalizationService _localizationService;
        private readonly IRepository<Country> _countryRepository;
        private readonly IRepository<City> _cityRepository;
        private readonly IRepository<District> _districtRepository;
        private readonly IRepository<Ward> _wardRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly IStoreContext _storeContext;

        #endregion

        #region Ctor

        public CountryService(CatalogSettings catalogSettings,
            IStaticCacheManager cacheManager,
            IEventPublisher eventPublisher,
            ILocalizationService localizationService,
            IRepository<Country> countryRepository,
            IRepository<City> cityRepository,
            IRepository<District> districtRepository,
            IRepository<Ward> wardRepository,
            IRepository<StoreMapping> storeMappingRepository,
            IStoreContext storeContext)
        {
            _catalogSettings = catalogSettings;
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _localizationService = localizationService;
            _countryRepository = countryRepository;
            _cityRepository = cityRepository;
            _districtRepository = districtRepository;
            _wardRepository = wardRepository;
            _storeMappingRepository = storeMappingRepository;
            _storeContext = storeContext;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes a country
        /// </summary>
        /// <param name="country">Country</param>
        public virtual void DeleteCountry(Country country)
        {
            if (country == null)
                throw new ArgumentNullException(nameof(country));

            _countryRepository.Delete(country);

            //event notification
            _eventPublisher.EntityDeleted(country);
        }

        /// <summary>
        /// Gets all countries
        /// </summary>
        /// <param name="languageId">Language identifier. It's used to sort countries by localized names (if specified); pass 0 to skip it</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Countries</returns>
        public virtual IList<Country> GetAllCountries(int languageId = 0, bool showHidden = false)
        {
            var key = NopDirectoryCachingDefaults.CountriesAllCacheKey.FillCacheKey(languageId, showHidden);
            return _cacheManager.Get(key, () =>
            {
                var query = _countryRepository.Table;
                if (!showHidden)
                    query = query.Where(c => c.Published);
                query = query.OrderBy(c => c.DisplayOrder).ThenBy(c => c.Name);

                if (!showHidden && !_catalogSettings.IgnoreStoreLimitations)
                {
                    //Store mapping
                    var currentStoreId = _storeContext.CurrentStore.Id;
                    query = from c in query
                            join sc in _storeMappingRepository.Table
                            on new { c1 = c.Id, c2 = nameof(Country) } equals new { c1 = sc.EntityId, c2 = sc.EntityName } into c_sc
                            from sc in c_sc.DefaultIfEmpty()
                            where !c.LimitedToStores || currentStoreId == sc.StoreId
                            select c;

                    query = query.Distinct().OrderBy(c => c.DisplayOrder).ThenBy(c => c.Name);
                }

                var countries = query.ToList();

                if (languageId > 0)
                {
                    //we should sort countries by localized names when they have the same display order
                    countries = countries
                        .OrderBy(c => c.DisplayOrder)
                        .ThenBy(c => _localizationService.GetLocalized(c, x => x.Name, languageId))
                        .ToList();
                }

                return countries;
            });
        }

        /// <summary>
        /// Gets all countries that allow billing
        /// </summary>
        /// <param name="languageId">Language identifier. It's used to sort countries by localized names (if specified); pass 0 to skip it</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Countries</returns>
        public virtual IList<Country> GetAllCountriesForBilling(int languageId = 0, bool showHidden = false)
        {
            return GetAllCountries(languageId, showHidden).Where(c => c.AllowsBilling).ToList();
        }

        /// <summary>
        /// Gets all countries that allow shipping
        /// </summary>
        /// <param name="languageId">Language identifier. It's used to sort countries by localized names (if specified); pass 0 to skip it</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Countries</returns>
        public virtual IList<Country> GetAllCountriesForShipping(int languageId = 0, bool showHidden = false)
        {
            return GetAllCountries(languageId, showHidden).Where(c => c.AllowsShipping).ToList();
        }

        /// <summary>
        /// Gets a country by address 
        /// </summary>
        /// <param name="address">Address</param>
        /// <returns>Country</returns>
        public virtual Country GetCountryByAddress(Address address)
        {
            return GetCountryById(address?.CountryId ?? 0);
        }

        /// <summary>
        /// Gets a country 
        /// </summary>
        /// <param name="countryId">Country identifier</param>
        /// <returns>Country</returns>
        public virtual Country GetCountryById(int countryId)
        {
            if (countryId == 0)
                return null;

            return _countryRepository.ToCachedGetById(countryId);
        }

        /// <summary>
        /// Get countries by identifiers
        /// </summary>
        /// <param name="countryIds">Country identifiers</param>
        /// <returns>Countries</returns>
        public virtual IList<Country> GetCountriesByIds(int[] countryIds)
        {
            if (countryIds == null || countryIds.Length == 0)
                return new List<Country>();

            var query = from c in _countryRepository.Table
                        where countryIds.Contains(c.Id)
                        select c;
            var countries = query.ToList();
            //sort by passed identifiers
            var sortedCountries = new List<Country>();
            foreach (var id in countryIds)
            {
                var country = countries.Find(x => x.Id == id);
                if (country != null)
                    sortedCountries.Add(country);
            }

            return sortedCountries;
        }

        /// <summary>
        /// Gets a country by two letter ISO code
        /// </summary>
        /// <param name="twoLetterIsoCode">Country two letter ISO code</param>
        /// <returns>Country</returns>
        public virtual Country GetCountryByTwoLetterIsoCode(string twoLetterIsoCode)
        {
            if (string.IsNullOrEmpty(twoLetterIsoCode))
                return null;

            var key = NopDirectoryCachingDefaults.CountriesByTwoLetterCodeCacheKey.FillCacheKey(twoLetterIsoCode);

            var query = from c in _countryRepository.Table
                        where c.TwoLetterIsoCode == twoLetterIsoCode
                        select c;

            return query.ToCachedFirstOrDefault(key);
        }

        /// <summary>
        /// Gets a country by three letter ISO code
        /// </summary>
        /// <param name="threeLetterIsoCode">Country three letter ISO code</param>
        /// <returns>Country</returns>
        public virtual Country GetCountryByThreeLetterIsoCode(string threeLetterIsoCode)
        {
            if (string.IsNullOrEmpty(threeLetterIsoCode))
                return null;

            var key = NopDirectoryCachingDefaults.CountriesByThreeLetterCodeCacheKey.FillCacheKey(threeLetterIsoCode);

            var query = from c in _countryRepository.Table
                        where c.ThreeLetterIsoCode == threeLetterIsoCode
                        select c;

            return query.ToCachedFirstOrDefault(key);
        }

        /// <summary>
        /// Inserts a country
        /// </summary>
        /// <param name="country">Country</param>
        public virtual void InsertCountry(Country country)
        {
            if (country == null)
                throw new ArgumentNullException(nameof(country));

            _countryRepository.Insert(country);

            //event notification
            _eventPublisher.EntityInserted(country);
        }

        /// <summary>
        /// Updates the country
        /// </summary>
        /// <param name="country">Country</param>
        public virtual void UpdateCountry(Country country)
        {
            if (country == null)
                throw new ArgumentNullException(nameof(country));

            _countryRepository.Update(country);

            //event notification
            _eventPublisher.EntityUpdated(country);
        }

        public void InsertCity(City city)
        {
            if (city == null)
                throw new ArgumentNullException(nameof(city));

            _cityRepository.Insert(city);

            //event notification
            _eventPublisher.EntityInserted(city);
        }

        public void InsertDistrict(District district)
        {
            if (district == null)
                throw new ArgumentNullException(nameof(district));

            _districtRepository.Insert(district);

            //event notification
            _eventPublisher.EntityInserted(district);
        }

        public void InsertWard(Ward ward)
        {
            if (ward == null)
                throw new ArgumentNullException(nameof(ward));
            try
            {
                _wardRepository.Insert(ward);

                //event notification
                _eventPublisher.EntityInserted(ward);
            }
            catch (System.Exception err)
            {
                Console.WriteLine(err);
            }

        }

        public IList<City> GetCities(string countryCode = "VN")
        {
            var query = from c in _cityRepository.Table
                        where c.CountryCode == countryCode
                        orderby c.DisplayOrder, c.Name
                        select c;
            return query.ToList(); ;
        }
        public IList<City> GetCities(int countryId)
        {
            var query = from c in _cityRepository.Table
                        where c.CountryId == countryId
                        orderby c.DisplayOrder, c.Name
                        select c;
            return query.ToList(); ;
        }

        public IList<District> GetDistricts(int? cityId = null)
        {
            var query = from c in _districtRepository.Table
                        select c;
            if (cityId.HasValue)
            {
                query = query.Where(x => x.CityId == cityId);
            }

            return query.OrderBy(x => x.DisplayOrder).ThenBy(x => x.Name).ToList();
        }

        public IList<Ward> GetWards(int? districtId = null)
        {
            var query = from c in _wardRepository.Table
                        select c;
            if (districtId.HasValue)
            {
                query = query.Where(x => x.DistrictId == districtId);
            }

            return query.OrderBy(x => x.DisplayOrder).ThenBy(x => x.Name).ToList();
        }

        public City GetCity(int id)
        {
            var query = from c in _cityRepository.Table
                        where c.Id == id
                        select c;

            return query.FirstOrDefault();
        }

        public District GetDistrict(int id)
        {
            var query = from c in _districtRepository.Table
                        where c.Id == id
                        select c;

            return query.FirstOrDefault();
        }

        public Ward GetWard(int id)
        {
            var query = from c in _wardRepository.Table
                        where c.Id == id
                        select c;

            return query.FirstOrDefault();
        }

        public City GetCityByHisId(int hisId)
        {
            var query = from c in _cityRepository.Table
                        where c.HisId == hisId.ToString() || c.Id == hisId
                        select c;

            return query.FirstOrDefault();
        }

        public District GetDistrictByHisId(int hisId)
        {
           var query = from c in _districtRepository.Table
                        where c.HisId == hisId.ToString() || c.Id == hisId
                        select c;
            return query.FirstOrDefault();
        }
        public IList<District> GetDistrictByHisCityId(int hisCityId)
        {
           var query = from c in _districtRepository.Table
                        where c.HisCityId == hisCityId.ToString()
                        select c;
            return query.ToList();
        }

        public Ward GetWardByHisId(int hisId)
        {
           var query = from c in _wardRepository.Table
                        where c.HisId == hisId.ToString() || c.Id == hisId
                        select c;
            return query.FirstOrDefault();
        }
        public IList<Ward> GetWardByHisDistrictId(int hisDistrictId)
        {
           var query = from c in _wardRepository.Table
                        where c.HisDistrictId == hisDistrictId.ToString()
                        select c;
            return query.ToList();
        }

        #endregion
    }
}