﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Tax;

namespace Nop.Services.Customers
{
    /// <summary>
    /// Patient service interface
    /// </summary>
    public partial interface IPatientService
    {
        #region Patients

        /// <summary>
        /// Gets all patients
        /// </summary>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="affiliateId">Affiliate identifier</param>
        /// <param name="vendorId">Vendor identifier</param>
        /// <param name="patientRoleIds">A list of patient role identifiers to filter by (at least one match); pass null or empty list in order to load all patients; </param>
        /// <param name="email">Email; null to load all patients</param>
        /// <param name="username">Username; null to load all patients</param>
        /// <param name="firstName">First name; null to load all patients</param>
        /// <param name="lastName">Last name; null to load all patients</param>
        /// <param name="dayOfBirth">Day of birth; 0 to load all patients</param>
        /// <param name="monthOfBirth">Month of birth; 0 to load all patients</param>
        /// <param name="company">Company; null to load all patients</param>
        /// <param name="phone">Phone; null to load all patients</param>
        /// <param name="zipPostalCode">Phone; null to load all patients</param>
        /// <param name="ipAddress">IP address; null to load all patients</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="getOnlyTotalCount">A value in indicating whether you want to load only total number of records. Set to "true" if you don't want to load data from database</param>
        /// <returns>Patients</returns>
        IPagedList<Patient> GetAllPatients(DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            int affiliateId = 0, int vendorId = 0, int[] patientRoleIds = null,
            string email = null, string username = null, string firstName = null, string lastName = null,
            int dayOfBirth = 0, int monthOfBirth = 0,
            string company = null, string phone = null, string zipPostalCode = null, string ipAddress = null,
            int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false);
        /// <summary>
        /// Gets all patients
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Patients</returns>
        int CountPatient();
        IPagedList<Patient> GetByCustomer(int customerId,int pageIndex = 0, int pageSize = int.MaxValue);
        List<Patient> GetByCustomer(int customerId);
        Patient GetByHisId(int hisId);

        /// <summary>
        /// Gets online patients
        /// </summary>
        /// <param name="lastActivityFromUtc">Patient last activity date (from)</param>
        /// <param name="patientRoleIds">A list of patient role identifiers to filter by (at least one match); pass null or empty list in order to load all patients; </param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Patients</returns>
        IPagedList<Patient> GetOnlinePatients(DateTime lastActivityFromUtc,
            int[] patientRoleIds, int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets patients with shopping carts
        /// </summary>
        /// <param name="shoppingCartType">Shopping cart type; pass null to load all records</param>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <param name="productId">Product identifier; pass null to load all records</param>
        /// <param name="createdFromUtc">Created date from (UTC); pass null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); pass null to load all records</param>
        /// <param name="countryId">Billing country identifier; pass null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Patients</returns>
        IPagedList<Patient> GetPatientsWithShoppingCarts(ShoppingCartType? shoppingCartType = null,
            int storeId = 0, int? productId = null,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null, int? countryId = null,
            int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets patient for shopping cart
        /// </summary>
        /// <param name="shoppingCart">Shopping cart</param>
        /// <returns>Result</returns>
        Patient GetShoppingCartPatient(IList<ShoppingCartItem> shoppingCart);

        /// <summary>
        /// Delete a patient
        /// </summary>
        /// <param name="patient">Patient</param>
        void DeletePatient(Patient patient, bool isHardDelete = true);

        /// <summary>
        /// Gets a patient
        /// </summary>
        /// <param name="patientId">Patient identifier</param>
        /// <returns>A patient</returns>
        Patient GetPatientById(int patientId);

        /// <summary>
        /// Get patients by identifiers
        /// </summary>
        /// <param name="patientIds">Patient identifiers</param>
        /// <returns>Patients</returns>
        IList<Patient> GetPatientsByIds(int[] patientIds);

        /// <summary>
        /// Gets a patient by GUID
        /// </summary>
        /// <param name="patientGuid">Patient GUID</param>
        /// <returns>A patient</returns>
        Patient GetPatientByGuid(Guid patientGuid);

        /// <summary>
        /// Get patient by email
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>Patient</returns>
        Patient GetPatientByEmail(string email);

        /// <summary>
        /// Get patient by system role
        /// </summary>
        /// <param name="systemName">System name</param>
        /// <returns>Patient</returns>
        Patient GetPatientBySystemName(string systemName);

        /// <summary>
        /// Get patient by code
        /// </summary>
        /// <param name="code">Username</param>
        /// <returns>Patient</returns>
        Patient GetPatientByCode(string code);

        /// <summary>
        /// Insert a guest patient
        /// </summary>
        /// <returns>Patient</returns>
        Patient InsertGuestPatient();

        /// <summary>
        /// Insert a patient
        /// </summary>
        /// <param name="patient">Patient</param>
        void InsertPatient(Patient patient);

        /// <summary>
        /// Updates the patient
        /// </summary>
        /// <param name="patient">Patient</param>
        void UpdatePatient(Patient patient);

        /// <summary>
        /// Reset data required for checkout
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="clearCouponCodes">A value indicating whether to clear coupon code</param>
        /// <param name="clearCheckoutAttributes">A value indicating whether to clear selected checkout attributes</param>
        /// <param name="clearRewardPoints">A value indicating whether to clear "Use reward points" flag</param>
        /// <param name="clearShippingMethod">A value indicating whether to clear selected shipping method</param>
        /// <param name="clearPaymentMethod">A value indicating whether to clear selected payment method</param>
        void ResetCheckoutData(Patient patient, int storeId,
            bool clearCouponCodes = false, bool clearCheckoutAttributes = false,
            bool clearRewardPoints = true, bool clearShippingMethod = true,
            bool clearPaymentMethod = true);

        /// <summary>
        /// Delete guest patient records
        /// </summary>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="onlyWithoutShoppingCart">A value indicating whether to delete patients only without shopping cart</param>
        /// <returns>Number of deleted patients</returns>
        int DeleteGuestPatients(DateTime? createdFromUtc, DateTime? createdToUtc, bool onlyWithoutShoppingCart);
        /// <summary>
        /// Get full name
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <returns>Patient full name</returns>
        string GetPatientFullName(Patient patient);

        /// <summary>
        /// Formats the patient name
        /// </summary>
        /// <param name="patient">Source</param>
        /// <param name="stripTooLong">Strip too long patient name</param>
        /// <param name="maxLength">Maximum patient name length</param>
        /// <returns>Formatted text</returns>
        string FormatUsername(Patient patient, bool stripTooLong = false, int maxLength = 0);

        /// <summary>
        /// Gets coupon codes
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <returns>Coupon codes</returns>
        string[] ParseAppliedDiscountCouponCodes(Patient patient);

        /// <summary>
        /// Adds a coupon code
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <param name="couponCode">Coupon code</param>
        /// <returns>New coupon codes document</returns>
        void ApplyDiscountCouponCode(Patient patient, string couponCode);

        /// <summary>
        /// Removes a coupon code
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <param name="couponCode">Coupon code to remove</param>
        /// <returns>New coupon codes document</returns>
        void RemoveDiscountCouponCode(Patient patient, string couponCode);

        /// <summary>
        /// Gets coupon codes
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <returns>Coupon codes</returns>
        string[] ParseAppliedGiftCardCouponCodes(Patient patient);

        /// <summary>
        /// Adds a coupon code
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <param name="couponCode">Coupon code</param>
        /// <returns>New coupon codes document</returns>
        void ApplyGiftCardCouponCode(Patient patient, string couponCode);

        /// <summary>
        /// Removes a coupon code
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <param name="couponCode">Coupon code to remove</param>
        /// <returns>New coupon codes document</returns>
        void RemoveGiftCardCouponCode(Patient patient, string couponCode);

        #endregion

        }
}