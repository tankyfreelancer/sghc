﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Core.Infrastructure;
using Nop.Data;
using Nop.Services.Caching.CachingDefaults;
using Nop.Services.Caching.Extensions;
using Nop.Services.Common;
using Nop.Services.Events;
using Nop.Services.Localization;

namespace Nop.Services.Customers
{
    /// <summary>
    /// Patient service
    /// </summary>
    public partial class PatientService : IPatientService
    {
        #region Fields

        private readonly ICacheManager _cacheManager;
        private readonly IDataProvider _dataProvider;
        private readonly IEventPublisher _eventPublisher;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IRepository<Address> _patientAddressRepository;
        private readonly IRepository<Patient> _patientRepository;
        private readonly IRepository<GenericAttribute> _gaRepository;
        private readonly IRepository<ShoppingCartItem> _shoppingCartRepository;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly ShoppingCartSettings _shoppingCartSettings;

        #endregion

        #region Ctor

        public PatientService(
            ICacheManager cacheManager,
            IDataProvider dataProvider,
            IEventPublisher eventPublisher,
            IGenericAttributeService genericAttributeService,
            IRepository<Address> patientAddressRepository,
            IRepository<Patient> patientRepository,
            IRepository<GenericAttribute> gaRepository,
            IRepository<ShoppingCartItem> shoppingCartRepository,
            IStaticCacheManager staticCacheManager,
            ShoppingCartSettings shoppingCartSettings)
        {
            _cacheManager = cacheManager;
            _dataProvider = dataProvider;
            _eventPublisher = eventPublisher;
            _genericAttributeService = genericAttributeService;
            _patientAddressRepository = patientAddressRepository;
            _patientRepository = patientRepository;
            _gaRepository = gaRepository;
            _shoppingCartRepository = shoppingCartRepository;
            _staticCacheManager = staticCacheManager;
            _shoppingCartSettings = shoppingCartSettings;
        }

        #endregion

        #region Methods

        #region Patients

        /// <summary>
        /// Gets all patients
        /// </summary>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="affiliateId">Affiliate identifier</param>
        /// <param name="vendorId">Vendor identifier</param>
        /// <param name="patientRoleIds">A list of patient role identifiers to filter by (at least one match); pass null or empty list in order to load all patients; </param>
        /// <param name="email">Email; null to load all patients</param>
        /// <param name="username">Username; null to load all patients</param>
        /// <param name="firstName">First name; null to load all patients</param>
        /// <param name="lastName">Last name; null to load all patients</param>
        /// <param name="dayOfBirth">Day of birth; 0 to load all patients</param>
        /// <param name="monthOfBirth">Month of birth; 0 to load all patients</param>
        /// <param name="company">Company; null to load all patients</param>
        /// <param name="phone">Phone; null to load all patients</param>
        /// <param name="zipPostalCode">Phone; null to load all patients</param>
        /// <param name="ipAddress">IP address; null to load all patients</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="getOnlyTotalCount">A value in indicating whether you want to load only total number of records. Set to "true" if you don't want to load data from database</param>
        /// <returns>Patients</returns>
        public virtual IPagedList<Patient> GetAllPatients(DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            int affiliateId = 0, int vendorId = 0, int[] patientRoleIds = null,
            string email = null, string username = null, string firstName = null, string lastName = null,
            int dayOfBirth = 0, int monthOfBirth = 0,
            string company = null, string phone = null, string zipPostalCode = null, string ipAddress = null,
            int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false)
        {
            var query = _patientRepository.Table;
            if (createdFromUtc.HasValue)
                query = query.Where(c => createdFromUtc.Value <= c.CreatedOnUtc);
            if (createdToUtc.HasValue)
                query = query.Where(c => createdToUtc.Value >= c.CreatedOnUtc);

            query = query.Where(c => !c.Deleted);


            if (!string.IsNullOrWhiteSpace(email))
                query = query.Where(c => c.Email.Contains(email));
            if (!string.IsNullOrWhiteSpace(firstName))
            {
                query = query
                    .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Patient = x, Attribute = y })
                    .Where(z => z.Attribute.KeyGroup == nameof(Patient) &&
                                z.Attribute.Value.Contains(firstName))
                    .Select(z => z.Patient);
            }

            if (!string.IsNullOrWhiteSpace(lastName))
            {
                query = query
                    .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Patient = x, Attribute = y })
                    .Where(z => z.Attribute.KeyGroup == nameof(Patient) &&
                                z.Attribute.Value.Contains(lastName))
                    .Select(z => z.Patient);
            }

            //date of birth is stored as a string into database.
            //we also know that date of birth is stored in the following format YYYY-MM-DD (for example, 1983-02-18).
            //so let's search it as a string
            if (dayOfBirth > 0 && monthOfBirth > 0)
            {
                //both are specified
                var dateOfBirthStr = monthOfBirth.ToString("00", CultureInfo.InvariantCulture) + "-" + dayOfBirth.ToString("00", CultureInfo.InvariantCulture);

                //z.Attribute.Value.Length - dateOfBirthStr.Length = 5
                //dateOfBirthStr.Length = 5
                query = query
                    .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Patient = x, Attribute = y })
                    .Where(z => z.Attribute.KeyGroup == nameof(Patient) &&
                                z.Attribute.Value.Substring(5, 5) == dateOfBirthStr)
                    .Select(z => z.Patient);
            }
            else if (dayOfBirth > 0)
            {
                //only day is specified
                var dateOfBirthStr = dayOfBirth.ToString("00", CultureInfo.InvariantCulture);

                //z.Attribute.Value.Length - dateOfBirthStr.Length = 8
                //dateOfBirthStr.Length = 2
                query = query
                    .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Patient = x, Attribute = y })
                    .Where(z => z.Attribute.KeyGroup == nameof(Patient) &&
                                z.Attribute.Value.Substring(8, 2) == dateOfBirthStr)
                    .Select(z => z.Patient);
            }
            else if (monthOfBirth > 0)
            {
                //only month is specified
                var dateOfBirthStr = "-" + monthOfBirth.ToString("00", CultureInfo.InvariantCulture) + "-";
                query = query
                    .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Patient = x, Attribute = y })
                    .Where(z => z.Attribute.KeyGroup == nameof(Patient) &&
                                z.Attribute.Value.Contains(dateOfBirthStr))
                    .Select(z => z.Patient);
            }
            //search by company
            if (!string.IsNullOrWhiteSpace(company))
            {
                query = query
                    .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Patient = x, Attribute = y })
                    .Where(z => z.Attribute.KeyGroup == nameof(Patient) &&
                                z.Attribute.Value.Contains(company))
                    .Select(z => z.Patient);
            }
            //search by phone
            if (!string.IsNullOrWhiteSpace(phone))
            {
                query = query
                    .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Patient = x, Attribute = y })
                    .Where(z => z.Attribute.KeyGroup == nameof(Patient) &&
                                z.Attribute.Value.Contains(phone))
                    .Select(z => z.Patient);
            }
            //search by zip
            if (!string.IsNullOrWhiteSpace(zipPostalCode))
            {
                query = query
                    .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Patient = x, Attribute = y })
                    .Where(z => z.Attribute.KeyGroup == nameof(Patient) &&
                                z.Attribute.Value.Contains(zipPostalCode))
                    .Select(z => z.Patient);
            }



            query = query.OrderByDescending(c => c.CreatedOnUtc);

            var patients = new PagedList<Patient>(query, pageIndex, pageSize, getOnlyTotalCount);
            return patients;
        }
        public IPagedList<Patient> GetByCustomer(int customerId, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _patientRepository.Table;
            query = query.Where(c => !c.Deleted);
            query = query.Where(x => x.CustomerId == customerId);


            query = query.OrderByDescending(c => c.CreatedOnUtc);
            var patients = new PagedList<Patient>(query, pageIndex, pageSize);

            return patients;
        }
        public List<Patient> GetByCustomer(int customerId)
        {
            var query = _patientRepository.Table;
            query = query.Where(c => !c.Deleted);
            query = query.Where(x => x.CustomerId == customerId);


            query = query.OrderByDescending(c => c.CreatedOnUtc);

            return query.ToList();
        }
        /// <summary>
        /// Gets online patients
        /// </summary>
        /// <param name="lastActivityFromUtc">Patient last activity date (from)</param>
        /// <param name="patientRoleIds">A list of patient role identifiers to filter by (at least one match); pass null or empty list in order to load all patients; </param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Patients</returns>
        public virtual IPagedList<Patient> GetOnlinePatients(DateTime lastActivityFromUtc,
            int[] patientRoleIds, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _patientRepository.Table;
            query = query.Where(c => !c.Deleted);


            query = query.OrderByDescending(c => c.CreatedOnUtc);
            var patients = new PagedList<Patient>(query, pageIndex, pageSize);

            return patients;
        }

        /// <summary>
        /// Gets patients with shopping carts
        /// </summary>
        /// <param name="shoppingCartType">Shopping cart type; pass null to load all records</param>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <param name="productId">Product identifier; pass null to load all records</param>
        /// <param name="createdFromUtc">Created date from (UTC); pass null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); pass null to load all records</param>
        /// <param name="countryId">Billing country identifier; pass null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Patients</returns>
        public virtual IPagedList<Patient> GetPatientsWithShoppingCarts(ShoppingCartType? shoppingCartType = null,
            int storeId = 0, int? productId = null,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null, int? countryId = null,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            //get all shopping cart items
            var items = _shoppingCartRepository.Table;

            //filter by type
            if (shoppingCartType.HasValue)
                items = items.Where(item => item.ShoppingCartTypeId == (int)shoppingCartType.Value);

            //filter shopping cart items by store
            if (storeId > 0 && !_shoppingCartSettings.CartsSharedBetweenStores)
                items = items.Where(item => item.StoreId == storeId);

            //filter shopping cart items by product
            if (productId > 0)
                items = items.Where(item => item.ProductId == productId);

            //filter shopping cart items by date
            if (createdFromUtc.HasValue)
                items = items.Where(item => createdFromUtc.Value <= item.CreatedOnUtc);
            if (createdToUtc.HasValue)
                items = items.Where(item => createdToUtc.Value >= item.CreatedOnUtc);

            //get all active patients
            var patients = _patientRepository.Table.Where(patient => patient.Active && !patient.Deleted);



            var patientsWithCarts = from c in patients
                                    orderby c.Id
                                    select c;

            return new PagedList<Patient>(patientsWithCarts.Distinct(), pageIndex, pageSize);
        }

        /// <summary>
        /// Gets patient for shopping cart
        /// </summary>
        /// <param name="shoppingCart">Shopping cart</param>
        /// <returns>Result</returns>
        public virtual Patient GetShoppingCartPatient(IList<ShoppingCartItem> shoppingCart)
        {
            return null;
        }

        /// <summary>
        /// Delete a patient
        /// </summary>
        /// <param name="patient">Patient</param>
        public virtual void DeletePatient(Patient patient, bool isHardDelete = true)
        {
            if (patient == null)
                throw new ArgumentNullException(nameof(patient));

            if (isHardDelete)
            {
                _patientRepository.Delete(patient);
                return;
            }

            patient.Deleted = true;


            UpdatePatient(patient);

            //event notification
            _eventPublisher.EntityDeleted(patient);
        }

        /// <summary>
        /// Gets a patient
        /// </summary>
        /// <param name="patientId">Patient identifier</param>
        /// <returns>A patient</returns>
        public virtual Patient GetPatientById(int patientId)
        {
            if (patientId == 0)
                return null;
            var patient = _patientRepository.Table.FirstOrDefault(patient => patient.Active && !patient.Deleted && (patient.Id == patientId || patient.HisId == patientId));
            return patient;
        }

        /// <summary>
        /// Get patients by identifiers
        /// </summary>
        /// <param name="patientIds">Patient identifiers</param>
        /// <returns>Patients</returns>
        public virtual IList<Patient> GetPatientsByIds(int[] patientIds)
        {
            if (patientIds == null || patientIds.Length == 0)
                return new List<Patient>();

            var query = from c in _patientRepository.Table
                        where patientIds.Contains(c.Id) && !c.Deleted
                        select c;
            var patients = query.ToList();
            //sort by passed identifiers
            var sortedPatients = new List<Patient>();
            foreach (var id in patientIds)
            {
                var patient = patients.Find(x => x.Id == id);
                if (patient != null)
                    sortedPatients.Add(patient);
            }

            return sortedPatients;
        }

        /// <summary>
        /// Gets a patient by GUID
        /// </summary>
        /// <param name="patientGuid">Patient GUID</param>
        /// <returns>A patient</returns>
        public virtual Patient GetPatientByGuid(Guid patientGuid)
        {
            if (patientGuid == Guid.Empty)
                return null;

            var query = from c in _patientRepository.Table
                        where c.Guid == patientGuid
                        orderby c.Id
                        select c;
            var patient = query.FirstOrDefault();
            return patient;
        }

        /// <summary>
        /// Get patient by email
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>Patient</returns>
        public virtual Patient GetPatientByEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return null;

            var query = from c in _patientRepository.Table
                        orderby c.Id
                        where c.Email == email
                        select c;
            var patient = query.FirstOrDefault();
            return patient;
        }

        /// <summary>
        /// Get patient by system name
        /// </summary>
        /// <param name="systemName">System name</param>
        /// <returns>Patient</returns>
        public virtual Patient GetPatientBySystemName(string systemName)
        {
            if (string.IsNullOrWhiteSpace(systemName))
                return null;

            var query = from c in _patientRepository.Table
                        orderby c.Id
                        select c;
            var patient = query.FirstOrDefault();
            return patient;
        }

        /// <summary>
        /// Get patient by username
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>Patient</returns>
        public virtual Patient GetPatientByCode(string code)
        {
            if (string.IsNullOrWhiteSpace(code))
                return null;

            var query = from c in _patientRepository.Table
                        orderby c.Id
                        where c.Code == code
                        select c;
            var patient = query.FirstOrDefault();
            return patient;
        }

        /// <summary>
        /// Insert a guest patient
        /// </summary>
        /// <returns>Patient</returns>
        public virtual Patient InsertGuestPatient()
        {
            var patient = new Patient
            {
                Guid = Guid.NewGuid(),
                Active = true,
                CreatedOnUtc = DateTime.UtcNow,
            };

            return patient;
        }

        /// <summary>
        /// Insert a patient
        /// </summary>
        /// <param name="patient">Patient</param>
        public virtual void InsertPatient(Patient patient)
        {
            if (patient == null)
                throw new ArgumentNullException(nameof(patient));

            _patientRepository.Insert(patient);

            //event notification
            _eventPublisher.EntityInserted(patient);
        }

        /// <summary>
        /// Updates the patient
        /// </summary>
        /// <param name="patient">Patient</param>
        public virtual void UpdatePatient(Patient patient)
        {
            if (patient == null)
                throw new ArgumentNullException(nameof(patient));

            _patientRepository.Update(patient);

            //event notification
            _eventPublisher.EntityUpdated(patient);
        }

        /// <summary>
        /// Reset data required for checkout
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="clearCouponCodes">A value indicating whether to clear coupon code</param>
        /// <param name="clearCheckoutAttributes">A value indicating whether to clear selected checkout attributes</param>
        /// <param name="clearRewardPoints">A value indicating whether to clear "Use reward points" flag</param>
        /// <param name="clearShippingMethod">A value indicating whether to clear selected shipping method</param>
        /// <param name="clearPaymentMethod">A value indicating whether to clear selected payment method</param>
        public virtual void ResetCheckoutData(Patient patient, int storeId,
            bool clearCouponCodes = false, bool clearCheckoutAttributes = false,
            bool clearRewardPoints = true, bool clearShippingMethod = true,
            bool clearPaymentMethod = true)
        {
            if (patient == null)
                throw new ArgumentNullException();

            UpdatePatient(patient);
        }

        /// <summary>
        /// Delete guest patient records
        /// </summary>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="onlyWithoutShoppingCart">A value indicating whether to delete patients only without shopping cart</param>
        /// <returns>Number of deleted patients</returns>
        public virtual int DeleteGuestPatients(DateTime? createdFromUtc, DateTime? createdToUtc, bool onlyWithoutShoppingCart)
        {
            //prepare parameters
            var pOnlyWithoutShoppingCart = SqlParameterHelper.GetBooleanParameter("OnlyWithoutShoppingCart", onlyWithoutShoppingCart);
            var pCreatedFromUtc = SqlParameterHelper.GetDateTimeParameter("CreatedFromUtc", createdFromUtc);
            var pCreatedToUtc = SqlParameterHelper.GetDateTimeParameter("CreatedToUtc", createdToUtc);
            var pTotalRecordsDeleted = SqlParameterHelper.GetOutputInt32Parameter("TotalRecordsDeleted");

            //invoke stored procedure
            _dataProvider.Query<object>("EXEC [DeleteGuests] @OnlyWithoutShoppingCart, @CreatedFromUtc, @CreatedToUtc, @TotalRecordsDeleted OUTPUT",
                pOnlyWithoutShoppingCart,
                pCreatedFromUtc,
                pCreatedToUtc,
                pTotalRecordsDeleted);

            var totalRecordsDeleted = pTotalRecordsDeleted.Value != DBNull.Value ? Convert.ToInt32(pTotalRecordsDeleted.Value) : 0;
            return totalRecordsDeleted;
        }


        /// <summary>
        /// Get full name
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <returns>Patient full name</returns>
        public virtual string GetPatientFullName(Patient patient)
        {
            if (patient == null)
                throw new ArgumentNullException(nameof(patient));
            var fullName = string.Empty;
            if (!string.IsNullOrWhiteSpace(patient.Name) && !string.IsNullOrWhiteSpace(patient.SurName))
                fullName = $"{patient.Name} {patient.SurName}";
            else
            {
                if (!string.IsNullOrWhiteSpace(patient.Name))
                    fullName = patient.Name;

                if (!string.IsNullOrWhiteSpace(patient.SurName))
                    fullName = patient.SurName;
            }

            return fullName;
        }

        /// <summary>
        /// Formats the patient name
        /// </summary>
        /// <param name="patient">Source</param>
        /// <param name="stripTooLong">Strip too long patient name</param>
        /// <param name="maxLength">Maximum patient name length</param>
        /// <returns>Formatted text</returns>
        public virtual string FormatUsername(Patient patient, bool stripTooLong = false, int maxLength = 0)
        {
            if (patient == null)
                return string.Empty;

            var result = string.Empty;
            if (stripTooLong && maxLength > 0)
                result = CommonHelper.EnsureMaximumLength(result, maxLength);

            return result;
        }

        /// <summary>
        /// Gets coupon codes
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <returns>Coupon codes</returns>
        public virtual string[] ParseAppliedDiscountCouponCodes(Patient patient)
        {
            if (patient == null)
                throw new ArgumentNullException(nameof(patient));


            var couponCodes = new List<string>();

            try
            {
                var xmlDoc = new XmlDocument();

                var nodeList1 = xmlDoc.SelectNodes(@"//DiscountCouponCodes/CouponCode");
                foreach (XmlNode node1 in nodeList1)
                {
                    if (node1.Attributes?["Code"] == null)
                        continue;
                    var code = node1.Attributes["Code"].InnerText.Trim();
                    couponCodes.Add(code);
                }
            }
            catch
            {
                // ignored
            }

            return couponCodes.ToArray();
        }

        /// <summary>
        /// Adds a coupon code
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <param name="couponCode">Coupon code</param>
        /// <returns>New coupon codes document</returns>
        public virtual void ApplyDiscountCouponCode(Patient patient, string couponCode)
        {
            if (patient == null)
                throw new ArgumentNullException(nameof(patient));

            var result = string.Empty;
            try
            {

                couponCode = couponCode.Trim().ToLower();

                var xmlDoc = new XmlDocument();

                var rootElement = (XmlElement)xmlDoc.SelectSingleNode(@"//DiscountCouponCodes");

                XmlElement gcElement = null;
                //find existing
                var nodeList1 = xmlDoc.SelectNodes(@"//DiscountCouponCodes/CouponCode");
                foreach (XmlNode node1 in nodeList1)
                {
                    if (node1.Attributes?["Code"] == null)
                        continue;

                    var couponCodeAttribute = node1.Attributes["Code"].InnerText.Trim();

                    if (couponCodeAttribute.ToLower() != couponCode.ToLower())
                        continue;

                    gcElement = (XmlElement)node1;
                    break;
                }

                //create new one if not found
                if (gcElement == null)
                {
                    gcElement = xmlDoc.CreateElement("CouponCode");
                    gcElement.SetAttribute("Code", couponCode);
                    rootElement.AppendChild(gcElement);
                }

                result = xmlDoc.OuterXml;
            }
            catch
            {
                // ignored
            }

        }

        /// <summary>
        /// Removes a coupon code
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <param name="couponCode">Coupon code to remove</param>
        /// <returns>New coupon codes document</returns>
        public virtual void RemoveDiscountCouponCode(Patient patient, string couponCode)
        {
            if (patient == null)
                throw new ArgumentNullException(nameof(patient));

            //get applied coupon codes
            var existingCouponCodes = ParseAppliedDiscountCouponCodes(patient);

            //save again except removed one
            foreach (var existingCouponCode in existingCouponCodes)
                if (!existingCouponCode.Equals(couponCode, StringComparison.InvariantCultureIgnoreCase))
                    ApplyDiscountCouponCode(patient, existingCouponCode);
        }

        /// <summary>
        /// Gets coupon codes
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <returns>Coupon codes</returns>
        public virtual string[] ParseAppliedGiftCardCouponCodes(Patient patient)
        {
            if (patient == null)
                throw new ArgumentNullException(nameof(patient));


            var couponCodes = new List<string>();
            return couponCodes.ToArray();
        }

        /// <summary>
        /// Adds a coupon code
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <param name="couponCode">Coupon code</param>
        /// <returns>New coupon codes document</returns>
        public virtual void ApplyGiftCardCouponCode(Patient patient, string couponCode)
        {
            if (patient == null)
                throw new ArgumentNullException(nameof(patient));

            var result = string.Empty;
            try
            {

                couponCode = couponCode.Trim().ToLower();

                var xmlDoc = new XmlDocument();

                var rootElement = (XmlElement)xmlDoc.SelectSingleNode(@"//GiftCardCouponCodes");

                XmlElement gcElement = null;
                //find existing
                var nodeList1 = xmlDoc.SelectNodes(@"//GiftCardCouponCodes/CouponCode");
                foreach (XmlNode node1 in nodeList1)
                {
                    if (node1.Attributes?["Code"] == null)
                        continue;

                    var couponCodeAttribute = node1.Attributes["Code"].InnerText.Trim();
                    if (couponCodeAttribute.ToLower() != couponCode.ToLower())
                        continue;

                    gcElement = (XmlElement)node1;
                    break;
                }

                //create new one if not found
                if (gcElement == null)
                {
                    gcElement = xmlDoc.CreateElement("CouponCode");
                    gcElement.SetAttribute("Code", couponCode);
                    rootElement.AppendChild(gcElement);
                }

                result = xmlDoc.OuterXml;
            }
            catch
            {
                // ignored
            }
        }

        /// <summary>
        /// Removes a coupon code
        /// </summary>
        /// <param name="patient">Patient</param>
        /// <param name="couponCode">Coupon code to remove</param>
        /// <returns>New coupon codes document</returns>
        public virtual void RemoveGiftCardCouponCode(Patient patient, string couponCode)
        {
            if (patient == null)
                throw new ArgumentNullException(nameof(patient));

            //get applied coupon codes
            var existingCouponCodes = ParseAppliedGiftCardCouponCodes(patient);

            //clear them

            //save again except removed one
            foreach (var existingCouponCode in existingCouponCodes)
                if (!existingCouponCode.Equals(couponCode, StringComparison.InvariantCultureIgnoreCase))
                    ApplyGiftCardCouponCode(patient, existingCouponCode);
        }
        public virtual Patient GetByHisId(int hisId)
        {
            var query = from c in _patientRepository.Table
                        where c.HisId == hisId
                        orderby c.Id
                        select c;
            var patient = query.FirstOrDefault();
            return patient;
        }

        public int CountPatient()
        {
            var query = from c in _patientRepository.Table
												where !c.Deleted
                        select c;
            return query.Count();
        }


        #endregion

        #endregion
    }
}