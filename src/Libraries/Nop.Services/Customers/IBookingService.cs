﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Tax;

namespace Nop.Services.Customers
{
    /// <summary>
    /// Booking service interface
    /// </summary>
    public partial interface IBookingService
    {
        #region Bookings

        /// <summary>
        /// Gets all Bookings
        /// </summary>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="affiliateId">Affiliate identifier</param>
        /// <param name="vendorId">Vendor identifier</param>
        /// <param name="BookingRoleIds">A list of Booking role identifiers to filter by (at least one match); pass null or empty list in order to load all Bookings; </param>
        /// <param name="email">Email; null to load all Bookings</param>
        /// <param name="username">Username; null to load all Bookings</param>
        /// <param name="firstName">First name; null to load all Bookings</param>
        /// <param name="lastName">Last name; null to load all Bookings</param>
        /// <param name="dayOfBirth">Day of birth; 0 to load all Bookings</param>
        /// <param name="monthOfBirth">Month of birth; 0 to load all Bookings</param>
        /// <param name="company">Company; null to load all Bookings</param>
        /// <param name="phone">Phone; null to load all Bookings</param>
        /// <param name="zipPostalCode">Phone; null to load all Bookings</param>
        /// <param name="ipAddress">IP address; null to load all Bookings</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="getOnlyTotalCount">A value in indicating whether you want to load only total number of records. Set to "true" if you don't want to load data from database</param>
        /// <returns>Bookings</returns>
        IPagedList<Booking> GetAllBookings(DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            int affiliateId = 0, int vendorId = 0, int[] BookingRoleIds = null,
            string email = null, string username = null, string firstName = null, string lastName = null,
            int dayOfBirth = 0, int monthOfBirth = 0,
            string company = null, string phone = null, string zipPostalCode = null, string ipAddress = null,
            int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false);
    

        /// <summary>
        /// Gets online Bookings
        /// </summary>
        /// <param name="lastActivityFromUtc">Booking last activity date (from)</param>
        /// <param name="BookingRoleIds">A list of Booking role identifiers to filter by (at least one match); pass null or empty list in order to load all Bookings; </param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Bookings</returns>
        IPagedList<Booking> GetOnlineBookings(DateTime lastActivityFromUtc,
            int[] BookingRoleIds, int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets Bookings with shopping carts
        /// </summary>
        /// <param name="shoppingCartType">Shopping cart type; pass null to load all records</param>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <param name="productId">Product identifier; pass null to load all records</param>
        /// <param name="createdFromUtc">Created date from (UTC); pass null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); pass null to load all records</param>
        /// <param name="countryId">Billing country identifier; pass null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Bookings</returns>
        IPagedList<Booking> GetBookingsWithShoppingCarts(ShoppingCartType? shoppingCartType = null,
            int storeId = 0, int? productId = null,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null, int? countryId = null,
            int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets Booking for shopping cart
        /// </summary>
        /// <param name="shoppingCart">Shopping cart</param>
        /// <returns>Result</returns>
        Booking GetShoppingCartBooking(IList<ShoppingCartItem> shoppingCart);

        /// <summary>
        /// Delete a Booking
        /// </summary>
        /// <param name="Booking">Booking</param>
        void DeleteBooking(Booking Booking);

        /// <summary>
        /// Gets a Booking
        /// </summary>
        /// <param name="BookingId">Booking identifier</param>
        /// <returns>A Booking</returns>
        Booking GetBookingById(int BookingId);

        /// <summary>
        /// Get Bookings by identifiers
        /// </summary>
        /// <param name="BookingIds">Booking identifiers</param>
        /// <returns>Bookings</returns>
        IList<Booking> GetBookingsByIds(int[] BookingIds);

        /// <summary>
        /// Gets a Booking by GUID
        /// </summary>
        /// <param name="BookingGuid">Booking GUID</param>
        /// <returns>A Booking</returns>
        Booking GetBookingByGuid(Guid BookingGuid);

      

        /// <summary>
        /// Get Booking by system role
        /// </summary>
        /// <param name="systemName">System name</param>
        /// <returns>Booking</returns>
        Booking GetBookingBySystemName(string systemName);


        /// <summary>
        /// Insert a Booking
        /// </summary>
        /// <param name="Booking">Booking</param>
        void InsertBooking(Booking Booking);

        /// <summary>
        /// Updates the Booking
        /// </summary>
        /// <param name="Booking">Booking</param>
        void UpdateBooking(Booking Booking);

        /// <summary>
        /// Reset data required for checkout
        /// </summary>
        /// <param name="Booking">Booking</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="clearCouponCodes">A value indicating whether to clear coupon code</param>
        /// <param name="clearCheckoutAttributes">A value indicating whether to clear selected checkout attributes</param>
        /// <param name="clearRewardPoints">A value indicating whether to clear "Use reward points" flag</param>
        /// <param name="clearShippingMethod">A value indicating whether to clear selected shipping method</param>
        /// <param name="clearPaymentMethod">A value indicating whether to clear selected payment method</param>
        void ResetCheckoutData(Booking Booking, int storeId,
            bool clearCouponCodes = false, bool clearCheckoutAttributes = false,
            bool clearRewardPoints = true, bool clearShippingMethod = true,
            bool clearPaymentMethod = true);

        /// <summary>
        /// Delete guest Booking records
        /// </summary>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="onlyWithoutShoppingCart">A value indicating whether to delete Bookings only without shopping cart</param>
        /// <returns>Number of deleted Bookings</returns>
        int DeleteGuestBookings(DateTime? createdFromUtc, DateTime? createdToUtc, bool onlyWithoutShoppingCart);
        

        /// <summary>
        /// Formats the Booking name
        /// </summary>
        /// <param name="Booking">Source</param>
        /// <param name="stripTooLong">Strip too long Booking name</param>
        /// <param name="maxLength">Maximum Booking name length</param>
        /// <returns>Formatted text</returns>
        string FormatUsername(Booking Booking, bool stripTooLong = false, int maxLength = 0);

        /// <summary>
        /// Gets coupon codes
        /// </summary>
        /// <param name="Booking">Booking</param>
        /// <returns>Coupon codes</returns>
        string[] ParseAppliedDiscountCouponCodes(Booking Booking);

        /// <summary>
        /// Adds a coupon code
        /// </summary>
        /// <param name="Booking">Booking</param>
        /// <param name="couponCode">Coupon code</param>
        /// <returns>New coupon codes document</returns>
        void ApplyDiscountCouponCode(Booking Booking, string couponCode);

        /// <summary>
        /// Removes a coupon code
        /// </summary>
        /// <param name="Booking">Booking</param>
        /// <param name="couponCode">Coupon code to remove</param>
        /// <returns>New coupon codes document</returns>
        void RemoveDiscountCouponCode(Booking Booking, string couponCode);

        /// <summary>
        /// Gets coupon codes
        /// </summary>
        /// <param name="Booking">Booking</param>
        /// <returns>Coupon codes</returns>
        string[] ParseAppliedGiftCardCouponCodes(Booking Booking);

        /// <summary>
        /// Adds a coupon code
        /// </summary>
        /// <param name="Booking">Booking</param>
        /// <param name="couponCode">Coupon code</param>
        /// <returns>New coupon codes document</returns>
        void ApplyGiftCardCouponCode(Booking Booking, string couponCode);

        /// <summary>
        /// Removes a coupon code
        /// </summary>
        /// <param name="Booking">Booking</param>
        /// <param name="couponCode">Coupon code to remove</param>
        /// <returns>New coupon codes document</returns>
        void RemoveGiftCardCouponCode(Booking Booking, string couponCode);
        List<Booking> GetBookings(int customerId);
        #endregion

        }
}