﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Core.Infrastructure;
using Nop.Data;
using Nop.Services.Caching.CachingDefaults;
using Nop.Services.Caching.Extensions;
using Nop.Services.Common;
using Nop.Services.Events;
using Nop.Services.Localization;

namespace Nop.Services.Customers
{
    /// <summary>
    /// Booking service
    /// </summary>
    public partial class BookingService : IBookingService
    {
        #region Fields

        private readonly ICacheManager _cacheManager;
        private readonly IDataProvider _dataProvider;
        private readonly IEventPublisher _eventPublisher;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IRepository<Address> _BookingAddressRepository;
        private readonly IRepository<Booking> _BookingRepository;
        private readonly IRepository<GenericAttribute> _gaRepository;
        private readonly IRepository<ShoppingCartItem> _shoppingCartRepository;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly ShoppingCartSettings _shoppingCartSettings;

        #endregion

        #region Ctor

        public BookingService(
            ICacheManager cacheManager,
            IDataProvider dataProvider,
            IEventPublisher eventPublisher,
            IGenericAttributeService genericAttributeService,
            IRepository<Address> BookingAddressRepository,
            IRepository<Booking> BookingRepository,
            IRepository<GenericAttribute> gaRepository,
            IRepository<ShoppingCartItem> shoppingCartRepository,
            IStaticCacheManager staticCacheManager,
            ShoppingCartSettings shoppingCartSettings)
        {
            _cacheManager = cacheManager;
            _dataProvider = dataProvider;
            _eventPublisher = eventPublisher;
            _genericAttributeService = genericAttributeService;
            _BookingAddressRepository = BookingAddressRepository;
            _BookingRepository = BookingRepository;
            _gaRepository = gaRepository;
            _shoppingCartRepository = shoppingCartRepository;
            _staticCacheManager = staticCacheManager;
            _shoppingCartSettings = shoppingCartSettings;
        }

        #endregion

        #region Methods

        #region Bookings

        /// <summary>
        /// Gets all Bookings
        /// </summary>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="affiliateId">Affiliate identifier</param>
        /// <param name="vendorId">Vendor identifier</param>
        /// <param name="BookingRoleIds">A list of Booking role identifiers to filter by (at least one match); pass null or empty list in order to load all Bookings; </param>
        /// <param name="email">Email; null to load all Bookings</param>
        /// <param name="username">Username; null to load all Bookings</param>
        /// <param name="firstName">First name; null to load all Bookings</param>
        /// <param name="lastName">Last name; null to load all Bookings</param>
        /// <param name="dayOfBirth">Day of birth; 0 to load all Bookings</param>
        /// <param name="monthOfBirth">Month of birth; 0 to load all Bookings</param>
        /// <param name="company">Company; null to load all Bookings</param>
        /// <param name="phone">Phone; null to load all Bookings</param>
        /// <param name="zipPostalCode">Phone; null to load all Bookings</param>
        /// <param name="ipAddress">IP address; null to load all Bookings</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="getOnlyTotalCount">A value in indicating whether you want to load only total number of records. Set to "true" if you don't want to load data from database</param>
        /// <returns>Bookings</returns>
        public virtual IPagedList<Booking> GetAllBookings(DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            int affiliateId = 0, int vendorId = 0, int[] BookingRoleIds = null,
            string email = null, string username = null, string firstName = null, string lastName = null,
            int dayOfBirth = 0, int monthOfBirth = 0,
            string company = null, string phone = null, string zipPostalCode = null, string ipAddress = null,
            int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false)
        {
            var query = _BookingRepository.Table;


            //date of birth is stored as a string into database.
            //we also know that date of birth is stored in the following format YYYY-MM-DD (for example, 1983-02-18).
            //so let's search it as a string


            query = query.OrderByDescending(c => c.DateCreate);

            var Bookings = new PagedList<Booking>(query, pageIndex, pageSize, getOnlyTotalCount);
            return Bookings;
        }


        /// <summary>
        /// Gets online Bookings
        /// </summary>
        /// <param name="lastActivityFromUtc">Booking last activity date (from)</param>
        /// <param name="BookingRoleIds">A list of Booking role identifiers to filter by (at least one match); pass null or empty list in order to load all Bookings; </param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Bookings</returns>
        public virtual IPagedList<Booking> GetOnlineBookings(DateTime lastActivityFromUtc,
            int[] BookingRoleIds, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _BookingRepository.Table;


            query = query.OrderByDescending(c => c.DateCreate);
            var Bookings = new PagedList<Booking>(query, pageIndex, pageSize);

            return Bookings;
        }

        /// <summary>
        /// Gets Bookings with shopping carts
        /// </summary>
        /// <param name="shoppingCartType">Shopping cart type; pass null to load all records</param>
        /// <param name="storeId">Store identifier; pass 0 to load all records</param>
        /// <param name="productId">Product identifier; pass null to load all records</param>
        /// <param name="createdFromUtc">Created date from (UTC); pass null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); pass null to load all records</param>
        /// <param name="countryId">Billing country identifier; pass null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Bookings</returns>
        public virtual IPagedList<Booking> GetBookingsWithShoppingCarts(ShoppingCartType? shoppingCartType = null,
            int storeId = 0, int? productId = null,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null, int? countryId = null,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            //get all shopping cart items
            var items = _shoppingCartRepository.Table;

            //filter by type
            if (shoppingCartType.HasValue)
                items = items.Where(item => item.ShoppingCartTypeId == (int)shoppingCartType.Value);

            //filter shopping cart items by store
            if (storeId > 0 && !_shoppingCartSettings.CartsSharedBetweenStores)
                items = items.Where(item => item.StoreId == storeId);

            //filter shopping cart items by product
            if (productId > 0)
                items = items.Where(item => item.ProductId == productId);

            //filter shopping cart items by date
            if (createdFromUtc.HasValue)
                items = items.Where(item => createdFromUtc.Value <= item.CreatedOnUtc);
            if (createdToUtc.HasValue)
                items = items.Where(item => createdToUtc.Value >= item.CreatedOnUtc);

            //get all active Bookings
            var Bookings = _BookingRepository.Table;



            var BookingsWithCarts = from c in Bookings
                                    orderby c.Id
                                    select c;

            return new PagedList<Booking>(BookingsWithCarts.Distinct(), pageIndex, pageSize);
        }

        /// <summary>
        /// Gets Booking for shopping cart
        /// </summary>
        /// <param name="shoppingCart">Shopping cart</param>
        /// <returns>Result</returns>
        public virtual Booking GetShoppingCartBooking(IList<ShoppingCartItem> shoppingCart)
        {
            return null;
        }

        /// <summary>
        /// Delete a Booking
        /// </summary>
        /// <param name="Booking">Booking</param>
        public virtual void DeleteBooking(Booking Booking)
        {
            if (Booking == null)
                throw new ArgumentNullException(nameof(Booking));
            _BookingRepository.Delete(Booking);

            //event notification
            _eventPublisher.EntityDeleted(Booking);
        }

        /// <summary>
        /// Gets a Booking
        /// </summary>
        /// <param name="BookingId">Booking identifier</param>
        /// <returns>A Booking</returns>
        public virtual Booking GetBookingById(int BookingId)
        {
            if (BookingId == 0)
                return null;

            var query = from c in _BookingRepository.Table
                        where c.HisId == BookingId
                        orderby c.Id
                        select c;
            var Booking = query.FirstOrDefault();
            return Booking;
        }

        /// <summary>
        /// Get Bookings by identifiers
        /// </summary>
        /// <param name="BookingIds">Booking identifiers</param>
        /// <returns>Bookings</returns>
        public virtual IList<Booking> GetBookingsByIds(int[] BookingIds)
        {
            if (BookingIds == null || BookingIds.Length == 0)
                return new List<Booking>();

            var query = from c in _BookingRepository.Table
                        where BookingIds.Contains(c.Id)
                        select c;
            var Bookings = query.ToList();
            //sort by passed identifiers
            var sortedBookings = new List<Booking>();
            foreach (var id in BookingIds)
            {
                var Booking = Bookings.Find(x => x.Id == id);
                if (Booking != null)
                    sortedBookings.Add(Booking);
            }

            return sortedBookings;
        }

        /// <summary>
        /// Gets a Booking by GUID
        /// </summary>
        /// <param name="BookingGuid">Booking GUID</param>
        /// <returns>A Booking</returns>
        public virtual Booking GetBookingByGuid(Guid BookingGuid)
        {
            if (BookingGuid == Guid.Empty)
                return null;

            var query = from c in _BookingRepository.Table
                        where c.Guid == BookingGuid
                        orderby c.Id
                        select c;
            var Booking = query.FirstOrDefault();
            return Booking;
        }



        /// <summary>
        /// Get Booking by system name
        /// </summary>
        /// <param name="systemName">System name</param>
        /// <returns>Booking</returns>
        public virtual Booking GetBookingBySystemName(string systemName)
        {
            if (string.IsNullOrWhiteSpace(systemName))
                return null;

            var query = from c in _BookingRepository.Table
                        orderby c.Id
                        select c;
            var Booking = query.FirstOrDefault();
            return Booking;
        }



        /// <summary>
        /// Insert a Booking
        /// </summary>
        /// <param name="Booking">Booking</param>
        public virtual void InsertBooking(Booking Booking)
        {
            if (Booking == null)
                throw new ArgumentNullException(nameof(Booking));

            _BookingRepository.Insert(Booking);

            //event notification
            _eventPublisher.EntityInserted(Booking);
        }

        /// <summary>
        /// Updates the Booking
        /// </summary>
        /// <param name="Booking">Booking</param>
        public virtual void UpdateBooking(Booking Booking)
        {
            if (Booking == null)
                throw new ArgumentNullException(nameof(Booking));

            _BookingRepository.Update(Booking);

            //event notification
            _eventPublisher.EntityUpdated(Booking);
        }

        /// <summary>
        /// Reset data required for checkout
        /// </summary>
        /// <param name="Booking">Booking</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="clearCouponCodes">A value indicating whether to clear coupon code</param>
        /// <param name="clearCheckoutAttributes">A value indicating whether to clear selected checkout attributes</param>
        /// <param name="clearRewardPoints">A value indicating whether to clear "Use reward points" flag</param>
        /// <param name="clearShippingMethod">A value indicating whether to clear selected shipping method</param>
        /// <param name="clearPaymentMethod">A value indicating whether to clear selected payment method</param>
        public virtual void ResetCheckoutData(Booking Booking, int storeId,
            bool clearCouponCodes = false, bool clearCheckoutAttributes = false,
            bool clearRewardPoints = true, bool clearShippingMethod = true,
            bool clearPaymentMethod = true)
        {
            if (Booking == null)
                throw new ArgumentNullException();

            UpdateBooking(Booking);
        }

        /// <summary>
        /// Delete guest Booking records
        /// </summary>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="onlyWithoutShoppingCart">A value indicating whether to delete Bookings only without shopping cart</param>
        /// <returns>Number of deleted Bookings</returns>
        public virtual int DeleteGuestBookings(DateTime? createdFromUtc, DateTime? createdToUtc, bool onlyWithoutShoppingCart)
        {
            //prepare parameters
            var pOnlyWithoutShoppingCart = SqlParameterHelper.GetBooleanParameter("OnlyWithoutShoppingCart", onlyWithoutShoppingCart);
            var pCreatedFromUtc = SqlParameterHelper.GetDateTimeParameter("CreatedFromUtc", createdFromUtc);
            var pCreatedToUtc = SqlParameterHelper.GetDateTimeParameter("CreatedToUtc", createdToUtc);
            var pTotalRecordsDeleted = SqlParameterHelper.GetOutputInt32Parameter("TotalRecordsDeleted");

            //invoke stored procedure
            _dataProvider.Query<object>("EXEC [DeleteGuests] @OnlyWithoutShoppingCart, @CreatedFromUtc, @CreatedToUtc, @TotalRecordsDeleted OUTPUT",
                pOnlyWithoutShoppingCart,
                pCreatedFromUtc,
                pCreatedToUtc,
                pTotalRecordsDeleted);

            var totalRecordsDeleted = pTotalRecordsDeleted.Value != DBNull.Value ? Convert.ToInt32(pTotalRecordsDeleted.Value) : 0;
            return totalRecordsDeleted;
        }




        /// <summary>
        /// Formats the Booking name
        /// </summary>
        /// <param name="Booking">Source</param>
        /// <param name="stripTooLong">Strip too long Booking name</param>
        /// <param name="maxLength">Maximum Booking name length</param>
        /// <returns>Formatted text</returns>
        public virtual string FormatUsername(Booking Booking, bool stripTooLong = false, int maxLength = 0)
        {
            if (Booking == null)
                return string.Empty;

            var result = string.Empty;
            if (stripTooLong && maxLength > 0)
                result = CommonHelper.EnsureMaximumLength(result, maxLength);

            return result;
        }

        /// <summary>
        /// Gets coupon codes
        /// </summary>
        /// <param name="Booking">Booking</param>
        /// <returns>Coupon codes</returns>
        public virtual string[] ParseAppliedDiscountCouponCodes(Booking Booking)
        {
            if (Booking == null)
                throw new ArgumentNullException(nameof(Booking));


            var couponCodes = new List<string>();

            try
            {
                var xmlDoc = new XmlDocument();

                var nodeList1 = xmlDoc.SelectNodes(@"//DiscountCouponCodes/CouponCode");
                foreach (XmlNode node1 in nodeList1)
                {
                    if (node1.Attributes?["Code"] == null)
                        continue;
                    var code = node1.Attributes["Code"].InnerText.Trim();
                    couponCodes.Add(code);
                }
            }
            catch
            {
                // ignored
            }

            return couponCodes.ToArray();
        }

        /// <summary>
        /// Adds a coupon code
        /// </summary>
        /// <param name="Booking">Booking</param>
        /// <param name="couponCode">Coupon code</param>
        /// <returns>New coupon codes document</returns>
        public virtual void ApplyDiscountCouponCode(Booking Booking, string couponCode)
        {
            if (Booking == null)
                throw new ArgumentNullException(nameof(Booking));

            var result = string.Empty;
            try
            {

                couponCode = couponCode.Trim().ToLower();

                var xmlDoc = new XmlDocument();

                var rootElement = (XmlElement)xmlDoc.SelectSingleNode(@"//DiscountCouponCodes");

                XmlElement gcElement = null;
                //find existing
                var nodeList1 = xmlDoc.SelectNodes(@"//DiscountCouponCodes/CouponCode");
                foreach (XmlNode node1 in nodeList1)
                {
                    if (node1.Attributes?["Code"] == null)
                        continue;

                    var couponCodeAttribute = node1.Attributes["Code"].InnerText.Trim();

                    if (couponCodeAttribute.ToLower() != couponCode.ToLower())
                        continue;

                    gcElement = (XmlElement)node1;
                    break;
                }

                //create new one if not found
                if (gcElement == null)
                {
                    gcElement = xmlDoc.CreateElement("CouponCode");
                    gcElement.SetAttribute("Code", couponCode);
                    rootElement.AppendChild(gcElement);
                }

                result = xmlDoc.OuterXml;
            }
            catch
            {
                // ignored
            }

        }

        /// <summary>
        /// Removes a coupon code
        /// </summary>
        /// <param name="Booking">Booking</param>
        /// <param name="couponCode">Coupon code to remove</param>
        /// <returns>New coupon codes document</returns>
        public virtual void RemoveDiscountCouponCode(Booking Booking, string couponCode)
        {
            if (Booking == null)
                throw new ArgumentNullException(nameof(Booking));

            //get applied coupon codes
            var existingCouponCodes = ParseAppliedDiscountCouponCodes(Booking);

            //save again except removed one
            foreach (var existingCouponCode in existingCouponCodes)
                if (!existingCouponCode.Equals(couponCode, StringComparison.InvariantCultureIgnoreCase))
                    ApplyDiscountCouponCode(Booking, existingCouponCode);
        }

        /// <summary>
        /// Gets coupon codes
        /// </summary>
        /// <param name="Booking">Booking</param>
        /// <returns>Coupon codes</returns>
        public virtual string[] ParseAppliedGiftCardCouponCodes(Booking Booking)
        {
            if (Booking == null)
                throw new ArgumentNullException(nameof(Booking));


            var couponCodes = new List<string>();
            return couponCodes.ToArray();
        }

        /// <summary>
        /// Adds a coupon code
        /// </summary>
        /// <param name="Booking">Booking</param>
        /// <param name="couponCode">Coupon code</param>
        /// <returns>New coupon codes document</returns>
        public virtual void ApplyGiftCardCouponCode(Booking Booking, string couponCode)
        {
            if (Booking == null)
                throw new ArgumentNullException(nameof(Booking));

            var result = string.Empty;
            try
            {

                couponCode = couponCode.Trim().ToLower();

                var xmlDoc = new XmlDocument();

                var rootElement = (XmlElement)xmlDoc.SelectSingleNode(@"//GiftCardCouponCodes");

                XmlElement gcElement = null;
                //find existing
                var nodeList1 = xmlDoc.SelectNodes(@"//GiftCardCouponCodes/CouponCode");
                foreach (XmlNode node1 in nodeList1)
                {
                    if (node1.Attributes?["Code"] == null)
                        continue;

                    var couponCodeAttribute = node1.Attributes["Code"].InnerText.Trim();
                    if (couponCodeAttribute.ToLower() != couponCode.ToLower())
                        continue;

                    gcElement = (XmlElement)node1;
                    break;
                }

                //create new one if not found
                if (gcElement == null)
                {
                    gcElement = xmlDoc.CreateElement("CouponCode");
                    gcElement.SetAttribute("Code", couponCode);
                    rootElement.AppendChild(gcElement);
                }

                result = xmlDoc.OuterXml;
            }
            catch
            {
                // ignored
            }
        }

        /// <summary>
        /// Removes a coupon code
        /// </summary>
        /// <param name="Booking">Booking</param>
        /// <param name="couponCode">Coupon code to remove</param>
        /// <returns>New coupon codes document</returns>
        public virtual void RemoveGiftCardCouponCode(Booking Booking, string couponCode)
        {
            if (Booking == null)
                throw new ArgumentNullException(nameof(Booking));

            //get applied coupon codes
            var existingCouponCodes = ParseAppliedGiftCardCouponCodes(Booking);

            //clear them

            //save again except removed one
            foreach (var existingCouponCode in existingCouponCodes)
                if (!existingCouponCode.Equals(couponCode, StringComparison.InvariantCultureIgnoreCase))
                    ApplyGiftCardCouponCode(Booking, existingCouponCode);
        }
        public virtual List<Booking> GetBookings(int customerId)
        {
            if (customerId == 0)
            {
                return new List<Booking>();
            }
            var query = _BookingRepository.Table;
            query = query.Where(x => x.CustomerId == customerId);
            query = query.OrderByDescending(x => x.PatientCode).ThenByDescending(x => x.BookingDate);
            return query.ToList();
        }


        #endregion

        #endregion
    }
}