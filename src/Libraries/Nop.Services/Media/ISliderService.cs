﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Media;

namespace Nop.Services.Media
{
    /// <summary>
    /// Slider service interface
    /// </summary>
    public partial interface ISliderService
    {
        /// <summary>
        /// Deletes a slider
        /// </summary>
        /// <param name="slider">Slider</param>
        void DeleteSlider(Slider slider);

        /// <summary>
        /// Gets all sliders
        /// </summary>
        /// <param name="languageId">Language identifier. It's used to sort sliders by localized names (if specified); pass 0 to skip it</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sliders</returns>
        IList<Slider> GetAllSliders(int languageId = 0, bool showHidden = false, string title = null, bool isMobile = false);
        IPagedList<Slider> GetAllSliders(string title = "",
                   int pageIndex = 0,
                   int pageSize = int.MaxValue,
                   bool showHidden = false);
        /// <summary>
        /// Gets a slider 
        /// </summary>
        /// <param name="sliderId">Slider identifier</param>
        /// <returns>Slider</returns>
        Slider GetSliderById(int sliderId);

        /// <summary>
        /// Get sliders by identifiers
        /// </summary>
        /// <param name="sliderIds">Slider identifiers</param>
        /// <returns>Sliders</returns>
        IList<Slider> GetSlidersByIds(int[] sliderIds);

        /// <summary>
        /// Inserts a slider
        /// </summary>
        /// <param name="slider">Slider</param>
        void InsertSlider(Slider slider);

        /// <summary>
        /// Updates the slider
        /// </summary>
        /// <param name="slider">Slider</param>
        void UpdateSlider(Slider slider);
    }
}