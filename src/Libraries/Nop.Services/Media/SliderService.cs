﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Stores;
using Nop.Data;
using Nop.Services.Caching.CachingDefaults;
using Nop.Services.Caching.Extensions;
using Nop.Services.Events;
using Nop.Services.Localization;

namespace Nop.Services.Media
{
    /// <summary>
    /// Slider service
    /// </summary>
    public partial class SliderService : ISliderService
    {
        #region Fields

        private readonly CatalogSettings _catalogSettings;
        private readonly IStaticCacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly ILocalizationService _localizationService;
        private readonly IRepository<Slider> _sliderRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        private readonly IStoreContext _storeContext;

        #endregion

        #region Ctor

        public SliderService(CatalogSettings catalogSettings,
            IStaticCacheManager cacheManager,
            IEventPublisher eventPublisher,
            ILocalizationService localizationService,
            IRepository<Slider> sliderRepository,
            IRepository<StoreMapping> storeMappingRepository,
            IStoreContext storeContext)
        {
            _catalogSettings = catalogSettings;
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _localizationService = localizationService;
            _sliderRepository = sliderRepository;
            _storeMappingRepository = storeMappingRepository;
            _storeContext = storeContext;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes a slider
        /// </summary>
        /// <param name="slider">Slider</param>
        public virtual void DeleteSlider(Slider slider)
        {
            if (slider == null)
                throw new ArgumentNullException(nameof(slider));

            _sliderRepository.Delete(slider);

            //event notification
            _eventPublisher.EntityDeleted(slider);
        }

        /// <summary>
        /// Gets all sliders
        /// </summary>
        /// <param name="languageId">Language identifier. It's used to sort sliders by localized names (if specified); pass 0 to skip it</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sliders</returns>
        public virtual IList<Slider> GetAllSliders(int languageId = 0, bool showHidden = false, string title = null, bool isMobile = false)
        {

            var query = _sliderRepository.Table;
            if (!String.IsNullOrEmpty(title))
            {
                query = from c in query
                        where c.Title.Contains(title)
                        select c;
            }
            if (!showHidden)
            {
                query = query.Where(x => x.IsActive);
                if (isMobile)
                {
                    query = query.Where(x => x.IsMobile);
                }
                else
                {
                    query = query.Where(x => !x.IsMobile);
                }
            }
            query = query.Distinct().OrderByDescending(x => x.DisplayOrder).ThenBy(c => c.Id);

            return query.ToList();
        }

        public IPagedList<Slider> GetAllSliders(string title = "", int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            var query = _sliderRepository.Table;
            if (!String.IsNullOrEmpty(title))
            {
                query = from c in query
                        where c.Title.Contains(title)
                        select c;
            }
            if (!showHidden)
            {
                query = query.Where(x => x.IsActive);
            }
            query = query.Distinct().OrderByDescending(x => x.DisplayOrder).ThenBy(c => c.Id);

            return new PagedList<Slider>(query, pageIndex, pageSize);
        }

        public virtual Slider GetSliderById(int sliderId)
        {
            if (sliderId == 0)
                return null;

            return _sliderRepository.ToCachedGetById(sliderId);
        }

        /// <summary>
        /// Get sliders by identifiers
        /// </summary>
        /// <param name="sliderIds">Slider identifiers</param>
        /// <returns>Sliders</returns>
        public virtual IList<Slider> GetSlidersByIds(int[] sliderIds)
        {
            if (sliderIds == null || sliderIds.Length == 0)
                return new List<Slider>();

            var query = from c in _sliderRepository.Table
                        where sliderIds.Contains(c.Id)
                        select c;
            var sliders = query.ToList();
            //sort by passed identifiers
            var sortedSliders = new List<Slider>();
            foreach (var id in sliderIds)
            {
                var slider = sliders.Find(x => x.Id == id);
                if (slider != null)
                    sortedSliders.Add(slider);
            }

            return sortedSliders;
        }


        /// <summary>
        /// Inserts a slider
        /// </summary>
        /// <param name="slider">Slider</param>
        public virtual void InsertSlider(Slider slider)
        {
            if (slider == null)
                throw new ArgumentNullException(nameof(slider));

            _sliderRepository.Insert(slider);

            //event notification
            _eventPublisher.EntityInserted(slider);
        }

        /// <summary>
        /// Updates the slider
        /// </summary>
        /// <param name="slider">Slider</param>
        public virtual void UpdateSlider(Slider slider)
        {
            if (slider == null)
                throw new ArgumentNullException(nameof(slider));

            _sliderRepository.Update(slider);

            //event notification
            _eventPublisher.EntityUpdated(slider);
        }

        #endregion
    }
}