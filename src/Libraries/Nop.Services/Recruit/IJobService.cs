﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Recruit;

namespace Nop.Services.Recruit
{
    /// <summary>
    /// Job service interface
    /// </summary>
    public partial interface IJobService
    {
        IList<Job> GetAll(int? recruitFacultyId = null, string search = null);
        IPagedList<Job> GetPaging(string search = null,int languageId = 0, int storeId = 0, int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false,int? recruitFacultyId = null);

        /// <summary>
        /// Gets a job
        /// </summary>
        /// <param name="jobId">Job identifier</param>
        /// <returns>Job</returns>
        Job GetJobById(int jobId);

        /// <summary>
        /// Inserts a job
        /// </summary>
        /// <param name="job">Job</param>
        void InsertJob(Job job);

        /// <summary>
        /// Updates the job
        /// </summary>
        /// <param name="job">Job</param>
        void UpdateJob(Job job);

        /// <summary>
        /// Deletes a job
        /// </summary>
        /// <param name="job">Job</param>
        void DeleteJob(Job job);
    }
}
