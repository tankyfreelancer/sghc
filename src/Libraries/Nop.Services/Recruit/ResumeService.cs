﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Recruit;
using Nop.Core.Domain.Stores;
using Nop.Data;
using Nop.Services.Caching.CachingDefaults;
using Nop.Services.Caching.Extensions;
using Nop.Services.Events;
using Nop.Services.Localization;

namespace Nop.Services.Recruit
{
    /// <summary>
    /// Resume service
    /// </summary>
    public partial class ResumeService : IResumeService
    {
        #region Fields

        private readonly CatalogSettings _catalogSettings;

        private readonly IStaticCacheManager _cacheManager;

        private readonly IEventPublisher _eventPublisher;

        private readonly ILocalizationService _localizationService;

        private readonly IRepository<Resume> _resumeRepository;

        private readonly IStoreContext _storeContext;


        #endregion



        #region Ctor
        public ResumeService(
            CatalogSettings catalogSettings,
            IStaticCacheManager cacheManager,
            IEventPublisher eventPublisher,
            ILocalizationService localizationService,
            IRepository<Resume> resumeRepository,
            IStoreContext storeContext
        )
        {
            _catalogSettings = catalogSettings;
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _localizationService = localizationService;
            _resumeRepository = resumeRepository;
            _storeContext = storeContext;
        }


        #endregion



        #region Methods

        public IList<Resume> GetAll(string search = null)
        {
            var query = _resumeRepository.Table.Where(x => !x.Deleted);
            if (!String.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.FullName.Contains(search));
            }
            query =
                query
                    .Distinct()
                    .OrderBy(c => c.DisplayOrder)
                    .ThenBy(c => c.FullName);

            var resumes = query.ToList();
            return resumes;
        }

        /// <summary>
        /// Gets a resume

        /// </summary>
        /// <param name="resumeId">Resume identifier</param>
        /// <returns>Resume</returns>
        public virtual Resume GetResumeById(int resumeId)
        {
            if (resumeId == 0)
                return null;

            return _resumeRepository.ToCachedGetById(resumeId);
        }
        public virtual Resume GetResumeByEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return null;
            }
            return _resumeRepository.Table.OrderByDescending(x=>x.CreatedOnUtc).FirstOrDefault(x=>x.Email==email);
        }

        /// <summary>
        /// Inserts a resume
        /// </summary>
        /// <param name="resume">Resume</param>
        public virtual void InsertResume(Resume resume)
        {
            if (resume == null)
                throw new ArgumentNullException(nameof(resume));

            _resumeRepository.Insert(resume);

            //event notification
            _eventPublisher.EntityInserted(resume);
        }

        /// <summary>
        /// Updates the resume
        /// </summary>
        /// <param name="resume">Resume</param>
        public virtual void UpdateResume(Resume resume)
        {
            if (resume == null)
                throw new ArgumentNullException(nameof(resume));

            _resumeRepository.Update(resume);

            //event notification
            _eventPublisher.EntityUpdated(resume);
        }

        /// <summary>
        /// Deletes a resume
        /// </summary>
        /// <param name="resume">Resume</param>
        public virtual void DeleteResume(Resume resume)
        {
            if (resume == null)
                throw new ArgumentNullException(nameof(resume));

            resume.Deleted = true;
            _resumeRepository.Update(resume);

            //event notification
            _eventPublisher.EntityDeleted(resume);
        }
        public IPagedList<Resume> GetPaging(string search = null, int languageId = 0, int storeId = 0, int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            var query = _resumeRepository.Table.Where(x => !x.Deleted);
            if (!String.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.FullName.Contains(search));
            }
            query = query
                    .Distinct()
                    .OrderBy(c => c.DisplayOrder)
                    .ThenByDescending(c => c.CreatedOnUtc);

            var result = new PagedList<Resume>(query, pageIndex, pageSize);
            return result;
        }
    }
    #endregion
}
