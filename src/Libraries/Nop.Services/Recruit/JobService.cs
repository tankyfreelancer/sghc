﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Recruit;
using Nop.Core.Domain.Stores;
using Nop.Data;
using Nop.Services.Caching.CachingDefaults;
using Nop.Services.Caching.Extensions;
using Nop.Services.Events;
using Nop.Services.Localization;

namespace Nop.Services.Recruit
{
    /// <summary>
    /// Job service
    /// </summary>
    public partial class JobService : IJobService
    {
        #region Fields

        private readonly CatalogSettings _catalogSettings;

        private readonly IStaticCacheManager _cacheManager;

        private readonly IEventPublisher _eventPublisher;

        private readonly ILocalizationService _localizationService;

        private readonly IRepository<Job> _jobRepository;

        private readonly IStoreContext _storeContext;


        #endregion



        #region Ctor
        public JobService(
            CatalogSettings catalogSettings,
            IStaticCacheManager cacheManager,
            IEventPublisher eventPublisher,
            ILocalizationService localizationService,
            IRepository<Job> jobRepository,
            IStoreContext storeContext
        )
        {
            _catalogSettings = catalogSettings;
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _localizationService = localizationService;
            _jobRepository = jobRepository;
            _storeContext = storeContext;
        }


        #endregion



        #region Methods

        public IList<Job>
        GetAll(int? recruitFacultyId = null, string search = null)
        {
            var query = _jobRepository.Table.Where(x => !x.Deleted);
            if (recruitFacultyId.HasValue)
            {
                query =
                    query
                        .Where(x =>
                            x.RecruitFacultyId == recruitFacultyId.Value);
            }
            if (!String.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.Title.Contains(search));
            }
            query =
                query
                    .Distinct()
                    .OrderBy(c => c.DisplayOrder)
                    .ThenBy(c => c.Title);

            var jobs = query.ToList();
            return jobs;
        }

        /// <summary>
        /// Gets a job
        /// </summary>
        /// <param name="jobId">Job identifier</param>
        /// <returns>Job</returns>
        public virtual Job GetJobById(int jobId)
        {
            if (jobId == 0)
                return null;

            return _jobRepository.ToCachedGetById(jobId);
        }

        /// <summary>
        /// Inserts a job
        /// </summary>
        /// <param name="job">Job</param>
        public virtual void InsertJob(Job job)
        {
            if (job == null)
                throw new ArgumentNullException(nameof(job));

            if (job.CreatedOnUtc == DateTime.MinValue)
            {
                job.CreatedOnUtc = DateTime.Now;
            }
            _jobRepository.Insert(job);

            //event notification
            _eventPublisher.EntityInserted(job);
        }

        /// <summary>
        /// Updates the job
        /// </summary>
        /// <param name="job">Job</param>
        public virtual void UpdateJob(Job job)
        {
            if (job == null)
                throw new ArgumentNullException(nameof(job));

            if (job.CreatedOnUtc == DateTime.MinValue)
            {
                job.CreatedOnUtc = DateTime.Now;
            }
            _jobRepository.Update(job);

            //event notification
            _eventPublisher.EntityUpdated(job);
        }

        /// <summary>
        /// Deletes a job
        /// </summary>
        /// <param name="job">Job</param>
        public virtual void DeleteJob(Job job)
        {
            if (job == null)
                throw new ArgumentNullException(nameof(job));

            job.Deleted = true;
            if (job.CreatedOnUtc == DateTime.MinValue)
            {
                job.CreatedOnUtc = DateTime.Now;
            }
            _jobRepository.Update(job);

            //event notification
            _eventPublisher.EntityDeleted(job);
        }

        public IPagedList<Job> GetPaging(string search = null, int languageId = 0, int storeId = 0, int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, int? recruitFacultyId = null)
        {
            var query = _jobRepository.Table.Where(x => !x.Deleted);
            if (!String.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.Title.Contains(search));
            }
            if (recruitFacultyId.HasValue)
            {
                query = query.Where(x => x.RecruitFacultyId == recruitFacultyId.Value);
            }
            query = query
                    .Distinct()
                    .OrderBy(c => c.DisplayOrder)
                    .ThenByDescending(c => c.CreatedOnUtc);

            var result = new PagedList<Job>(query, pageIndex, pageSize);
            return result;
        }
    }
    #endregion
}
