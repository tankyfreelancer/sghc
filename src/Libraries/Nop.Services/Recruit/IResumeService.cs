﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Recruit;

namespace Nop.Services.Recruit
{
    /// <summary>
    /// Resume service interface
    /// </summary>
    public partial interface IResumeService
    {
        /// <summary>
        /// Gets all countries
        /// </summary>
        /// <param name="languageId">Language identifier. It's used to sort countries by localized names (if specified); pass 0 to skip it</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Countries</returns>
        IList<Resume> GetAll(string search = null);
        IPagedList<Resume> GetPaging(string search = null,int languageId = 0, int storeId = 0, int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        /// <summary>
        /// Gets a resume

        /// </summary>
        /// <param name="resumeId">Resume identifier</param>
        /// <returns>Resume</returns>
        Resume GetResumeById(int resumeId);
        /// <summary>
        /// Gets a resume

        /// </summary>
        /// <param name="resumeId">Resume identifier</param>
        /// <returns>Resume</returns>
        Resume GetResumeByEmail(string email);

        /// <summary>
        /// Inserts a resume
        /// </summary>
        /// <param name="resume">Resume</param>
        void InsertResume(Resume resume);

        /// <summary>
        /// Updates the resume
        /// </summary>
        /// <param name="resume">Resume</param>
        void UpdateResume(Resume resume);

        /// <summary>
        /// Deletes a resume
        /// </summary>
        /// <param name="resume">Resume</param>
        void DeleteResume(Resume resume);
    }
}
