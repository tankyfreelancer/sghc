﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Recruit;
using Nop.Core.Domain.Stores;
using Nop.Data;
using Nop.Services.Caching.CachingDefaults;
using Nop.Services.Caching.Extensions;
using Nop.Services.Events;
using Nop.Services.Localization;

namespace Nop.Services.Recruit
{
    /// <summary>
    /// RecruitFaculty service
    /// </summary>
    public partial class RecruitFacultyService : IRecruitFacultyService
    {
#region Fields

        private readonly CatalogSettings _catalogSettings;

        private readonly IStaticCacheManager _cacheManager;

        private readonly IEventPublisher _eventPublisher;

        private readonly ILocalizationService _localizationService;

        private readonly IRepository<RecruitFaculty> _recruitFacultyRepository;

        private readonly IStoreContext _storeContext;


#endregion



#region Ctor
        public RecruitFacultyService(
            CatalogSettings catalogSettings,
            IStaticCacheManager cacheManager,
            IEventPublisher eventPublisher,
            ILocalizationService localizationService,
            IRepository<RecruitFaculty> recruitFacultyRepository,
            IStoreContext storeContext
        )
        {
            _catalogSettings = catalogSettings;
            _cacheManager = cacheManager;
            _eventPublisher = eventPublisher;
            _localizationService = localizationService;
            _recruitFacultyRepository = recruitFacultyRepository;
            _storeContext = storeContext;
        }


#endregion



#region Methods
        public IList<RecruitFaculty> GetAll(string search = null, bool showHidden = false)
        {
            var query = _recruitFacultyRepository.Table.Where(x=>!x.Deleted);
            if (!String.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.Name.Contains(search));
            }
            if(!showHidden){
                query = query.Where(x=>x.Active);
            }
            query = query
                    .Distinct()
                    .OrderBy(c => c.DisplayOrder)
                    .ThenBy(c => c.Name);

            return  query.ToList();
        }

        /// <summary>
        /// Gets a recruitFaculty

        /// </summary>
        /// <param name="recruitFacultyId">RecruitFaculty identifier</param>
        /// <returns>RecruitFaculty</returns>
        public virtual RecruitFaculty GetRecruitFacultyById(int recruitFacultyId)
        {
            if (recruitFacultyId == 0) return null;

            return _recruitFacultyRepository.ToCachedGetById(recruitFacultyId);
        }

        /// <summary>
        /// Inserts a recruitFaculty
        /// </summary>
        /// <param name="recruitFaculty">RecruitFaculty</param>
        public virtual void InsertRecruitFaculty(RecruitFaculty recruitFaculty)
        {
            if (recruitFaculty == null) throw new ArgumentNullException(nameof(recruitFaculty));

            _recruitFacultyRepository.Insert (recruitFaculty);

            //event notification
            _eventPublisher.EntityInserted (recruitFaculty);
        }

        /// <summary>
        /// Updates the recruitFaculty
        /// </summary>
        /// <param name="recruitFaculty">RecruitFaculty</param>
        public virtual void UpdateRecruitFaculty(RecruitFaculty recruitFaculty)
        {
            if (recruitFaculty == null) throw new ArgumentNullException(nameof(recruitFaculty));

            if(recruitFaculty.CreatedOnUtc == DateTime.MinValue){
                recruitFaculty.CreatedOnUtc = DateTime.Now;
            }
            _recruitFacultyRepository.Update (recruitFaculty);

            //event notification
            _eventPublisher.EntityUpdated (recruitFaculty);
        }

        /// <summary>
        /// Deletes a recruitFaculty
        /// </summary>
        /// <param name="recruitFaculty">RecruitFaculty</param>
        public virtual void DeleteRecruitFaculty(RecruitFaculty recruitFaculty)
        {
            if (recruitFaculty == null) throw new ArgumentNullException(nameof(recruitFaculty));

            recruitFaculty.Deleted = true;
            if(recruitFaculty.CreatedOnUtc == DateTime.MinValue){
                recruitFaculty.CreatedOnUtc = DateTime.Now;
            }
            _recruitFacultyRepository.Update (recruitFaculty);

            //event notification
            _eventPublisher.EntityDeleted (recruitFaculty);
        }

        public IPagedList<RecruitFaculty> GetPaging(string search = null,int languageId = 0, int storeId = 0, int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, bool isNews = false)
        {
            var query = _recruitFacultyRepository.Table.Where(x=>!x.Deleted);
            if (!String.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.Name.Contains(search));
            }
            query = query
                    .Distinct()
                    .OrderBy(c => c.DisplayOrder)
                    .ThenBy(c => c.Name);

             var result = new PagedList<RecruitFaculty>(query, pageIndex, pageSize);
            return result;
        }
    }
#endregion
}
