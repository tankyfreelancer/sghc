﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Recruit;

namespace Nop.Services.Recruit
{
    /// <summary>
    /// RecruitFaculty service interface
    /// </summary>
    public partial interface IRecruitFacultyService
    {
        /// <summary>
        /// Gets all countries
        /// </summary>
        /// <param name="languageId">Language identifier. It's used to sort countries by localized names (if specified); pass 0 to skip it</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Countries</returns>
        IList<RecruitFaculty> GetAll(string search = null, bool showHidden=false);
        IPagedList<RecruitFaculty> GetPaging(string search = null,int languageId = 0, int storeId = 0, int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, bool isNews = false);

        /// <summary>
        /// Gets a recruitFalcuty

        /// </summary>
        /// <param name="recruitFalcutyId">RecruitFaculty identifier</param>
        /// <returns>RecruitFaculty</returns>
        RecruitFaculty GetRecruitFacultyById(int recruitFalcutyId);

        /// <summary>
        /// Inserts a recruitFalcuty
        /// </summary>
        /// <param name="recruitFalcuty">RecruitFaculty</param>
        void InsertRecruitFaculty(RecruitFaculty recruitFalcuty);

        /// <summary>
        /// Updates the recruitFalcuty
        /// </summary>
        /// <param name="recruitFalcuty">RecruitFaculty</param>
        void UpdateRecruitFaculty(RecruitFaculty recruitFalcuty);

        /// <summary>
        /// Deletes a recruitFalcuty
        /// </summary>
        /// <param name="recruitFalcuty">RecruitFaculty</param>
        void DeleteRecruitFaculty(RecruitFaculty recruitFalcuty);
    }
}
