using System;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Seo;
using Nop.Core.Domain.Stores;

namespace Nop.Core.Domain.Recruit
{
    /// <summary>
    /// Represents a Patient
    /// </summary>
    public partial class Job : BaseEntity, ISlugSupported, IStoreMappingSupported, ILocalizedEntity
    {
        public Job()
        {
            CreatedOnUtc = DateTime.Now;
        }

        public int LanguageId { get; set; }
        public string Title { get; set; }

        public string Short { get; set; }

        public string Full { get; set; }

        public string Position { get; set; }

        public int? RecruitFacultyId { get; set; }

        public int Count { get; set; }

        public bool Active { get; set; }

        public bool Deleted { get; set; }
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets the meta keywords
        /// </summary>
        public string MetaKeywords { get; set; }

        /// <summary>
        /// Gets or sets the meta description
        /// </summary>
        public string MetaDescription { get; set; }

        /// <summary>
        /// Gets or sets the meta title
        /// </summary>
        public string MetaTitle { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public bool LimitedToStores { get; set; }
    }
}
