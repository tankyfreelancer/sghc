using System;

namespace Nop.Core.Domain.Recruit
{
    /// <summary>
    /// Represents a Patient
    /// </summary>
    public partial class Resume : BaseEntity
    {
        public Resume()
        {
            CreatedOnUtc = DateTime.Now;
        }

        public string FullName { get; set; }

        public bool Gender { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string TempAddress { get; set; }

        public string Major { get; set; }

        public string College { get; set; }

        public string Expirence { get; set; }

        public string Bio { get; set; }

        public bool Active { get; set; }

        public bool Deleted { get; set; }
        public string AttachFile { get; set; }
        public int? JobId { get; set; }
        public int DisplayOrder { get; set; }

        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public int WardId { get; set; }
        public int TempCityId { get; set; }
        public int TempDistrictId { get; set; }
        public int TempWardId { get; set; }
        public DateTime CreatedOnUtc { get; set; }
    }
}
