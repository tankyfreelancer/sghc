using System;

namespace Nop.Core.Domain.Recruit
{
    /// <summary>
    /// Represents a Patient
    /// </summary>
    public partial class RecruitFaculty : BaseEntity
    {
        public RecruitFaculty()
        {
            CreatedOnUtc = DateTime.Now;
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public int DisplayOrder { get; set; }

        public bool Active { get; set; }

        public bool Deleted { get; set; }

        public DateTime CreatedOnUtc { get; set; }
    }
}
