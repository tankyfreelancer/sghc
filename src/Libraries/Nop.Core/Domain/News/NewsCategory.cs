﻿namespace Nop.Core.Domain.News
{
    /// <summary>
    /// Represents a news category mapping
    /// </summary>
    public partial class NewsCategoryMap : BaseEntity
    {
        /// <summary>
        /// Gets or sets the news identifier
        /// </summary>
        public int NewsId { get; set; }

        /// <summary>
        /// Gets or sets the category identifier
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the news is featured
        /// </summary>
        public bool IsFeaturedNews { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }
    }
}
