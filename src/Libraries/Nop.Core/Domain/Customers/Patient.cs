﻿using System;

namespace Nop.Core.Domain.Customers
{
    /// <summary>
    /// Represents a Patient
    /// </summary>
    public partial class Patient : BaseEntity
    {
        public Patient()
        {
            Guid = Guid.NewGuid();
            Active = true;
            CreatedOnUtc = DateTime.Now;
        }

        /// <summary>
        /// Gets or sets the customer GUID
        /// </summary>
        public Guid Guid { get; set; }
        public int HisId { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }
        public string SurName { get; set; }
        public int Sex { get; set; }
        public string BirthDate { get; set; }
        public int BirthYear { get; set; }
        public string Mobile { get; set; }
        public int? SocialId { get; set; }
        public int? CountryId { get; set; }
        public string CountryCode { get; set; }
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public int? WardId { get; set; }
        public string Address { get; set; }
        public string  BHYTCode { get; set; }
        public string Code { get; set; }
        /// <summary>
        /// Gets or sets the email
        /// </summary>
        public string Email { get; set; }
        public string Passport { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the customer is active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the customer has been deleted
        /// </summary>
        public bool Deleted { get; set; }
        /// <summary>
        /// Gets or sets the date and time of entity creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }
        public int? CustomerId { get; set; }
        public int? Day { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
    }
}