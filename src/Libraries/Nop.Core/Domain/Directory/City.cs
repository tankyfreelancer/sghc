﻿using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Stores;

namespace Nop.Core.Domain.Directory
{
    /// <summary>
    /// Represents a City
    /// </summary>
    public partial class City : BaseEntity
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string HisId { get; set; }
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string CountryCode { get; set; }
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public int CountryId { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }

    }
}