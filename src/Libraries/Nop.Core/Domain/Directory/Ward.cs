﻿using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Stores;

namespace Nop.Core.Domain.Directory
{
    /// <summary>
    /// Represents a Ward
    /// </summary>
    public partial class Ward : BaseEntity
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string HisId { get; set; }
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string HisDistrictId { get; set; }
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }
       
    }
}