﻿using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Stores;

namespace Nop.Core.Domain.Directory
{
    /// <summary>
    /// Represents a District
    /// </summary>
    public partial class District : BaseEntity
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string HisId { get; set; }
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string HisCityId { get; set; }
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public int CityId { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }
       
    }
}