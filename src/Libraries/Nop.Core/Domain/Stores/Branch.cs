using Nop.Core.Domain.Localization;

namespace Nop.Core.Domain.Stores
{
    /// <summary>
    /// Represents a Branch
    /// </summary>
    public partial class Branch : BaseEntity, ILocalizedEntity
    {
        /// <summary>
        /// Gets or sets the Branch name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Branch name
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the Branch name
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the Branch name
        /// </summary>
        public string Fax { get; set; }

        /// <summary>
        /// Gets or sets the Branch name
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the Branch name
        /// </summary>
        public string Hotline { get; set; }

        /// <summary>
        /// Gets or sets the Branch name
        /// </summary>
        public string WorkTime { get; set; }

        /// <summary>
        /// Gets or sets the Branch name
        /// </summary>
        public bool HeadOffice { get; set; }

    }
}
