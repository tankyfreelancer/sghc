﻿using System.Collections.Generic;
using Nop.Core.Configuration;

namespace Nop.Core.Domain.Common
{
    /// <summary>
    /// Homepage settings
    /// </summary>
    public class HomepageSettings : ISettings
    {
        public HomepageSettings()
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string ScheduleHost { get; set; }
        public string VideoUrl { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string BannerFront { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string BannerBehind { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string PhoneSuport { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string PhoneOnline { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string BrandShortDescription { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Fax { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Email { get; set; }
        public string RssUrl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Address { get; set; }
        public string BankAccountPhone { get; set; }
        public string BankAccount { get; set; }

        public int AboutSlider01 { get; set; }
        public int AboutSlider02 { get; set; }
        public int AboutSlider03 { get; set; }
        public string AboutVision { get; set; }
        public string AboutMission { get; set; }
        public string AboutCoreValue { get; set; }
        public string AboutWhyChooseUs { get; set; }
        public int AboutPresidentAvatarId { get; set; }
        public string AboutPresidentTitle { get; set; }
        public string AboutPresidentDescription { get; set; }
        public string AboutPresidentContent { get; set; }
        public string AboutVideoUrl { get; set; }
        public string DoctorVideoUrl { get; set; }
        public int AboutPicture01Id { get; set; }
        public int AboutPicture02Id { get; set; }
        public int AboutPicture03Id { get; set; }
        public int AboutPicture04Id { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Address1 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string PhoneNumber1 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Fax1 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Email1 { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Address2 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string PhoneNumber2 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Fax2 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Email2 { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Address3 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string PhoneNumber3 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Fax3 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Email3 { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Address4 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string PhoneNumber4 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Fax4 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Email4 { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Address5 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string PhoneNumber5 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Fax5 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Email5 { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Address6 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string PhoneNumber6 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Fax6 { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the contacts form should have "VideoUrl"
        /// </summary>
        public string Email6 { get; set; }
        
        public string Branch1 { get; set; }
        public string Branch2 { get; set; }
        public string Branch3 { get; set; }
        public string Branch4 { get; set; }
        public string Branch5 { get; set; }
        public string Branch6 { get; set; }
        //sghc
        public string SmallTitle { get; set; }
        public string Title { get; set; }
        public string Slogan { get; set; }
        public int BannerImageUrl { get; set; }

        public string BannerVideoUrl { get; set; }
    // booking 
        public string BookingDescription { get; set; }
        public string WorkTime { get; set; }
        public string BookingWorkTime { get; set; }
        public string BookingWorkTimeMorning { get; set; }
        public string BookingWorkTimeAfternoon { get; set; }
    // doctor
        public string DoctorTitle { get; set; }
        public string DoctorDescription { get; set; }
        public int DoctorImageUrl { get; set; }
        public int Rss1PictureId { get; set; }
        public int Rss2PictureId { get; set; }
        public int BannerAboutPageId { get; set; }
        public int BannerRecruitPageId { get; set; }
        public int BannerContactPageId { get; set; }
        public int BannerSpecialistPageId { get; set; }
        public int BannerDoctorPageId { get; set; }
        public int BannerServicePageId { get; set; }
        public int BannerPackagePageId { get; set; }
        public int BannerUserManualPageId { get; set; }
        public int BannerNewsPageId { get; set; }
        public int BannerDefaultId { get; set; }
        public int HomePageBookingPictureId { get; set; }
        public int HomePageDoctorPictureId { get; set; }
        public int HomePageVendorPictureId { get; set; }
        public int HomePageDoctorMobilePictureId { get; set; }
        public int HomePageVendorMobilePictureId { get; set; }
        
        public string OTPMessage { get; set; }

    }
}