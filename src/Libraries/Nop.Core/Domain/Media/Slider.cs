﻿namespace Nop.Core.Domain.Media
{
    /// <summary>
    /// Represents a Slider
    /// </summary>
    public partial class Slider : BaseEntity
    {
        /// <summary>
        /// Gets or sets the Slider mime type
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the SEO friendly filename of the Slider
        /// </summary>
        public string SubTitle { get; set; }
        
        public bool IsMobile { get; set; }
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the "alt" attribute for "img" HTML element. If empty, then a default rule will be used (e.g. product name)
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the "title" attribute for "img" HTML element. If empty, then a default rule will be used (e.g. product name)
        /// </summary>
        public int PictureId { get; set; }
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the Slider is new
        /// </summary>
        public bool IsActive { get; set; }
    }
}
