
APP_NAME:=sghc
VERSION:=v1.0.2
DOCKER_REPO=754404031763.dkr.ecr.ap-southeast-1.amazonaws.com
DOCKER_HUB=perfecthospital
DOCKER_PASS=12345678912

run:
	dotnet run --project=src/Presentation/Nop.Web/Nop.Web.csproj
run-dev:
	CLR_OPENSSL_VERSION_OVERRIDE=1.1 ASPNETCORE_ENVIRONMENT=development dotnet run --project=src/Presentation/Nop.Web/Nop.Web.csproj
run-prod:
	ASPNETCORE_ENVIRONMENT=production dotnet run --project=src/Presentation/Nop.Web/Nop.Web.csproj
build: 
	dotnet build src/Plugins/Nop.Plugin.ExternalAuth.Facebook/Nop.Plugin.ExternalAuth.Facebook.csproj
	dotnet build src/Plugins/Nop.Plugin.ExternalAuth.Google/Nop.Plugin.ExternalAuth.Google.csproj
	dotnet build src/Presentation/Nop.Web/Nop.Web.csproj
build-web:
	dotnet build src/Presentation/Nop.Web/Nop.Web.csproj
clean:
	dotnet clean src/Presentation/Nop.Web/Nop.Web.csproj
clean-all:
	dotnet clean src/Presentation/Nop.Web/Nop.Web.csproj
	dotnet clean src/Presentation/Nop.Web.Framework/Nop.Web.Framework.csproj
	dotnet clean src/Libraries/Nop.Services/Nop.Services.csproj
	dotnet clean src/Libraries/Nop.Data/Nop.Data.csproj
	dotnet clean src/Libraries/Nop.Core/Nop.Core.csproj
	dotnet clean src/HIS/HIS.csproj
	dotnet clean src/Build/ClearPluginAssemblies.proj
	dotnet clean src/project.sln
release:
	dotnet publish src/Presentation/Nop.Web/Nop.Web.csproj -c Release -o published
build-docker: ## Build the container
	docker build --build-arg APP_NAME=$(APP_NAME) --build-arg SERVICE_NAME=$(PROJECT_NAME) --build-arg SERVICE_TYPE=$(APP_PROTOCOL) -t $(APP_NAME) .

remind:
	echo "Please update these files below if you modified them \
	 1. appsetting.json \
	 2. App_Data/dataSettings.json \
	 3. Plugins \
	 4. web.config \
	 5. wwwroot (css,js,html)"
publish-manual:
	docker login --username perfecthospital --password 12345678912 &&	docker tag sghc perfecthospital/sghc:v1.0.2 && docker push perfecthospital/sghc:v1.0.2
publish:
	docker login --username $(DOCKER_HUB) --password $(DOCKER_PASS);
	docker tag $(APP_NAME) $(DOCKER_HUB)/$(APP_NAME):$(VERSION);
	docker push $(DOCKER_HUB)/$(APP_NAME):$(VERSION);
publish-gcr:
	gcloud auth activate-service-account nguyencatpham@onsky-intel.iam.gserviceaccount.com --key-file=onsky-intel.json
	# gcloud auth configure-docker
	docker tag $(APP_NAME) asia.gcr.io/onsky-intel/$(APP_NAME):latest
	docker push asia.gcr.io/onsky-intel/$(APP_NAME):latest
publish-ecr:
	$(shell aws ecr get-login --no-include-email --region ap-southeast-1)
	--aws ecr create-repository --repository-name $(APP_NAME)
	--aws ecr batch-delete-image --repository-name $(APP_NAME) --image-ids imageTag=latest
	docker tag $(APP_NAME) $(DOCKER_REPO)/$(APP_NAME):latest
	@echo 'publish latest to $(DOCKER_REPO)'
	docker push $(DOCKER_REPO)/$(APP_NAME):latest
clearv2:
	--helm delete $(APP_NAME) 
	--docker rmi -f `docker images -a |grep '$(APP_NAME)'|awk '{print\$$3}'`
	--docker rmi -f `docker images -f "dangling=true" -q `
deployv2:
	helm install  $(APP_NAME) deployment --set ENV=prod \
	--set entryPoint=$(APP_NAME) \
	--set serverName=$(APP_NAME) \
	--set image.repository=$(DOCKER_REPO)/$(APP_NAME) \
	--set fullnameOverride=$(APP_NAME)
deploy: build-docker publish-ecr clearv2 deployv2
release-docker: build-docker publish-manual remind

deployv3:
	docker pull perfecthospital/sghc:latest
	docker rm -f sghc
	docker run -d -p 80:80 --restart=always --name=sghc perfecthospital/sghc:latest
up:
	docker-compose up -d
	docker-compose logs --tail=0 --follow
down:
	docker-compose down
recreate:
	docker-compose up --force-recreate --build -d
	docker image prune -f
logs:
	docker-compose logs --tail=0 --follow
force-up: recreate logs 
restore:
	docker run -d -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=jhasdj122#d' -p 1433:1433 --name db samuelmarks/mssql-server-fts-sqlpackage-linux
	docker exec -it db /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P 'jhasdj122#d' -Q 'CREATE DATABASE his_web'
	# docker exec -it db /opt/mssql-tools/bin/sqlcmd -S localhost\\SQLEXPRESS,1433 -U sa -P 'jhasdj122#d' -Q 'CREATE DATABASE his_web'
	docker cp his_db.bacpac db:/opt/downloads/his_db.bacpac
	docker exec -it db dotnet /opt/sqlpackage/sqlpackage.dll /tsn:localhost /tu:SA /tp:'jhasdj122#d' /A:Import /tdn:his_web /sf:his_db.bacpac
commit:
	git stash
	git fetch --all
	git pull origin master
	git stash pop
	git add .
	git commit -m "chore: update"
	git push origin master 
	git push ttv master
	git log --all --oneline --graph --decorate