# create the build instance 
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

WORKDIR /src                                                                    
COPY ./src ./
# install System.Drawing native dependencies
RUN apt-get update \
    && apt-get install -y --allow-unauthenticated \
        libc6-dev \
        libgdiplus \
        libx11-dev \
     && rm -rf /var/lib/apt/lists/*

# restore solution
RUN dotnet restore project.sln

WORKDIR /src/Presentation/Nop.Web   

# build project   
RUN dotnet build Nop.Web.csproj -c Release

# build plugins
# WORKDIR /src/Plugins/Nop.Plugin.DiscountRules.CustomerRoles
# RUN dotnet build Nop.Plugin.DiscountRules.CustomerRoles.csproj
# WORKDIR /src/Plugins/Nop.Plugin.ExchangeRate.EcbExchange
# RUN dotnet build Nop.Plugin.ExchangeRate.EcbExchange.csproj
WORKDIR /src/Plugins/Nop.Plugin.ExternalAuth.Facebook
RUN dotnet build Nop.Plugin.ExternalAuth.Facebook.csproj
WORKDIR /src/Plugins/Nop.Plugin.ExternalAuth.Google
RUN dotnet build Nop.Plugin.ExternalAuth.Google.csproj
# WORKDIR /src/Plugins/Nop.Plugin.MultiFactorAuth.GoogleAuthenticator
# RUN dotnet build Nop.Plugin.MultiFactorAuth.GoogleAuthenticator.csproj
# WORKDIR /src/Plugins/Nop.Plugin.Misc.SendinBlue
# RUN dotnet build Nop.Plugin.Misc.SendinBlue.csproj
# WORKDIR /src/Plugins/Nop.Plugin.Payments.CheckMoneyOrder
# RUN dotnet build Nop.Plugin.Payments.CheckMoneyOrder.csproj
# WORKDIR /src/Plugins/Nop.Plugin.Payments.Manual
# RUN dotnet build Nop.Plugin.Payments.Manual.csproj
# WORKDIR /src/Plugins/Nop.Plugin.Payments.PayPalStandard
# RUN dotnet build Nop.Plugin.Payments.PayPalStandard.csproj
# WORKDIR /src/Plugins/Nop.Plugin.Payments.Qualpay
# RUN dotnet build Nop.Plugin.Payments.Qualpay.csproj
# WORKDIR /src/Plugins/Nop.Plugin.Payments.Square
# RUN dotnet build Nop.Plugin.Payments.Square.csproj
# WORKDIR /src/Plugins/Nop.Plugin.Pickup.PickupInStore
# RUN dotnet build Nop.Plugin.Pickup.PickupInStore.csproj
# WORKDIR /src/Plugins/Nop.Plugin.Shipping.FixedByWeightByTotal
# RUN dotnet build Nop.Plugin.Shipping.FixedByWeightByTotal.csproj
# WORKDIR /src/Plugins/Nop.Plugin.Shipping.UPS
# RUN dotnet build Nop.Plugin.Shipping.UPS.csproj
# WORKDIR /src/Plugins/Nop.Plugin.Tax.Avalara
# RUN dotnet build Nop.Plugin.Tax.Avalara.csproj
# WORKDIR /src/Plugins/Nop.Plugin.Tax.FixedOrByCountryStateZip
# RUN dotnet build Nop.Plugin.Tax.FixedOrByCountryStateZip.csproj
# WORKDIR /src/Plugins/Nop.Plugin.Widgets.GoogleAnalytics
# RUN dotnet build Nop.Plugin.Widgets.GoogleAnalytics.csproj
# WORKDIR /src/Plugins/Nop.Plugin.Widgets.NivoSlider
# RUN dotnet build Nop.Plugin.Widgets.NivoSlider.csproj

# publish project
RUN dotnet publish /src/Presentation/Nop.Web/Nop.Web.csproj -c Release -o /app/published
# create the runtime instance 
FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS runtime 

# add globalization support
#RUN apk add --no-cache icu-libs
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false
ENV ASPNETCORE_ENVIRONMENT=production

WORKDIR /app        
RUN mkdir bin
RUN mkdir logs  
# install System.Drawing native dependencies
RUN apt-get update \
    && apt-get install -y --allow-unauthenticated \
        libc6-dev \
        libgdiplus \
        libx11-dev \
     && rm -rf /var/lib/apt/lists/*

COPY --from=build /app/published .
COPY ["cert/sghc.pfx", "/https/servercert.pfx"]
COPY ["cert/ca.crt", "/usr/local/share/ca-certificates/cacert.crt"]
RUN update-ca-certificates

EXPOSE 3000 55390 80 443
                            
ENTRYPOINT ["dotnet", "Nop.Web.dll"]
