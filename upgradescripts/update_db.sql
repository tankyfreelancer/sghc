﻿-- CREATE TABLE [dbo].[NewsCategory] (
--     [Id]              INT            IDENTITY (1, 1) NOT NULL,
--     [CreatedOnUtc]    DATETIME2 (7)  NOT NULL,
--     [MetaTitle]       NVARCHAR (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--     [MetaDescription] NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--     [MetaKeywords]    NVARCHAR (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--     [LimitedToStores] BIT            NOT NULL,
--     [AllowComments]   BIT            NOT NULL,
--     [EndDateUtc]      DATETIME2 (7)  NULL,
--     [StartDateUtc]    DATETIME2 (7)  NULL,
--     [Published]       BIT            NOT NULL,
--     [Full]            NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--     [Short]           NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--     [Title]           NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
--     [LanguageId]      INT            NOT NULL,
--     [CategoryId]      INT            NULL,
--     [Tags]            VARCHAR (100)  NULL,
--     [ShowOnHomePage]   BIT           DEFAULT ((0)) NULL,
--     [IncludeInTopMenu] BIT           DEFAULT ((0)) NULL,
--     CONSTRAINT [PK_NewsCategory] PRIMARY KEY CLUSTERED ([Id] ASC),
--     CONSTRAINT [FK_NewsCategory_Language_LanguageId] FOREIGN KEY ([LanguageId]) REFERENCES [dbo].[Language] ([Id]) ON DELETE CASCADE
-- );
-- -- Drop table

-- -- DROP TABLE his_web.dbo.NewsCategory GO

CREATE TABLE his_web.dbo.NewsCategory (
	Id int IDENTITY(1,1) NOT NULL,
	LanguageId int NOT NULL,
	CategoryId int NULL,
	Title nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Short nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Full] nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Published bit NOT NULL,
	StartDateUtc datetime2(7) NULL,
	EndDateUtc datetime2(7) NULL,
	AllowComments bit NOT NULL,
	LimitedToStores bit NOT NULL,
	MetaKeywords nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaDescription nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaTitle nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Tags nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	IncludeInTopMenu bit NOT NULL,
	ShowOnHomePage bit NOT NULL,
	MenuSeName varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	SpecialistService nvarchar(2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	SpecialistIntroduce nvarchar(2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	SpecialistCategoryId int NULL,
	CONSTRAINT PK_NewsCategory PRIMARY KEY (Id)
    CONSTRAINT [FK_NewsCategory_Language_LanguageId] FOREIGN KEY ([LanguageId]) REFERENCES [dbo].[Language] ([Id]) ON DELETE CASCADE
) GO


GO
CREATE NONCLUSTERED INDEX [IX_NewsCategory_LanguageId]
    ON [dbo].[NewsCategory]([LanguageId] ASC);

-- picture news
CREATE TABLE [dbo].[News_Picture_Mapping] (
    [Id]           INT IDENTITY (1, 1) NOT NULL,
    [DisplayOrder] INT NOT NULL,
    [PictureId]    INT NOT NULL,
    [NewsId]    INT NOT NULL,
    CONSTRAINT [PK_News_Picture_Mapping] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_News_Picture_Mapping_Picture_PictureId] FOREIGN KEY ([PictureId]) REFERENCES [dbo].[Picture] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_News_Picture_Mapping_News_NewsId] FOREIGN KEY ([NewsId]) REFERENCES [dbo].[News] ([Id]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_News_Picture_Mapping_PictureId]
    ON [dbo].[News_Picture_Mapping]([PictureId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_News_Picture_Mapping_NewsId]
    ON [dbo].[News_Picture_Mapping]([NewsId] ASC);

-- new categories mapping
CREATE TABLE [dbo].[News_Category_Mapping] (
    [Id]                INT IDENTITY (1, 1) NOT NULL,
    [DisplayOrder]      INT NOT NULL,
    [IsFeaturedNews] BIT NOT NULL,
    [CategoryId]        INT NOT NULL,
    [NewsId]         INT NOT NULL,
    CONSTRAINT [PK_News_Category_Mapping] PRIMARY KEY CLUSTERED ([Id] ASC),
);


GO
CREATE NONCLUSTERED INDEX [IX_News_Category_Mapping_CategoryId]
    ON [dbo].[News_Category_Mapping]([CategoryId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_News_Category_Mapping_NewsId]
    ON [dbo].[News_Category_Mapping]([NewsId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PCM_NewsId_Extended]
    ON [dbo].[News_Category_Mapping]([NewsId] ASC, [IsFeaturedNews] ASC)
    INCLUDE([CategoryId]);


GO
CREATE NONCLUSTERED INDEX [IX_PCM_News_and_Category]
    ON [dbo].[News_Category_Mapping]([CategoryId] ASC, [NewsId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_News_Category_Mapping_IsFeaturedNews]
    ON [dbo].[News_Category_Mapping]([IsFeaturedNews] ASC);

-- update news
alter table news add [ShowOnHomePage] bit not null default 0;
alter table news add [IncludeInTopMenu] bit not null default 0;

-- Drop table

-- DROP TABLE his_web.dbo.RecruitFaculty GO

CREATE TABLE his_web.dbo.RecruitFaculty (
	Id int IDENTITY(0,1) NOT NULL,
	Name NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Description nvarchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DisplayOrder int NULL,
	CreatedOnUtc datetime NULL,
	Active bit NOT NULL,
	Deleted bit NOT NULL,
	CONSTRAINT RecruitFaculty_PK PRIMARY KEY (Id)
) GO
ALTER TABLE [RecruitFaculty] ALTER COLUMN Name NVARCHAR(100) COLLATE Vietnamese_CI_AS
ALTER TABLE [RecruitFaculty] ALTER COLUMN Description NVARCHAR(MAX) COLLATE Vietnamese_CI_AS
-- Drop table

-- DROP TABLE his_web.dbo.Job GO

CREATE TABLE his_web.dbo.Job (
	Id int IDENTITY(0,1) NOT NULL,
	Title NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Short nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Full] nvarchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Position] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	RecruitFacultyId int NULL,
	Count int NULL,
	DisplayOrder int NULL,
	CreatedOnUtc datetime NULL,
	Active bit NOT NULL,
	Deleted bit NOT NULL,
	MetaTitle varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaKeywords varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaDescription varchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT Job_PK PRIMARY KEY (Id)
) GO
ALTER TABLE [Job] ALTER COLUMN Title NVARCHAR(255) COLLATE Vietnamese_CI_AS
ALTER TABLE [Job] ALTER COLUMN Short NVARCHAR(255) COLLATE Vietnamese_CI_AS
ALTER TABLE [Job] ALTER COLUMN [Full] NVARCHAR(MAX) COLLATE Vietnamese_CI_AS
ALTER TABLE [Job] ALTER COLUMN [Position] NVARCHAR(100) COLLATE Vietnamese_CI_AS
-- Drop table

-- DROP TABLE his_web.dbo.Resume GO

CREATE TABLE his_web.dbo.[Resume] (
	Id int IDENTITY(0,1) NOT NULL,
	FullName nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Gender bit NOT NULL,
	PhoneNumber nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Email nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Address nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	TempAddress nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Major nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Expirence nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AttachFile nvarchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Bio nvarchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	JobId int NULL,
	DisplayOrder int NULL,
	CreatedOnUtc datetime NULL,
	Active bit NOT NULL,
	Deleted bit NOT NULL,
	CONSTRAINT Resume_PK PRIMARY KEY (Id)
) GO
ALTER TABLE [Resume] ALTER COLUMN FullName NVARCHAR(255) COLLATE Vietnamese_CI_AS
ALTER TABLE [Resume] ALTER COLUMN TempAddress NVARCHAR(MAX) COLLATE Vietnamese_CI_AS
ALTER TABLE [Resume] ALTER COLUMN Major NVARCHAR(MAX) COLLATE Vietnamese_CI_AS
ALTER TABLE [Resume] ALTER COLUMN Expirence NVARCHAR(MAX) COLLATE Vietnamese_CI_AS
ALTER TABLE [Resume] ALTER COLUMN Bio NVARCHAR(MAX) COLLATE Vietnamese_CI_AS

CREATE TABLE his_web.dbo.Slider (
	Id int IDENTITY(0,1) NOT NULL,
	Title nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	SubTitle nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Description nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	IsActive bit NOT NULL,
	PictureId int NOT NULL,
	Url varchar(2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DisplayOrder int NOT NULL,
	MobileUrl varchar(2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	IsMobile bit NOT NULL,
	CONSTRAINT Slider_PK PRIMARY KEY (Id)
) GO

ALTER TABLE [Address] ADD CityId INT NULL
ALTER TABLE [Address] ADD DistrictId INT NULL
ALTER TABLE [Address] ADD WardId INT NULL
ALTER TABLE [Order] ADD OrderNote NVARCHAR(MAX) COLLATE Vietnamese_CI_AS
ALTER TABLE [Manufacturer] ADD Link NVARCHAR(400);
ALTER TABLE [News] ADD DisplayOrder int NULL;
ALTER TABLE [NewsCategory] ADD DisplayOrder int NULL;
ALTER TABLE [News] ADD VideoUrl NVARCHAR(MAX) COLLATE Vietnamese_CI_AS


ALTER TABLE [Resume] ADD CityId INT NULL
ALTER TABLE [Resume] ADD DistrictId INT NULL
ALTER TABLE [Resume] ADD WardId INT NULL
ALTER TABLE [Resume] ADD TempCityId INT NULL
ALTER TABLE [Resume] ADD TempDistrictId INT NULL
ALTER TABLE [Resume] ADD TempWardId INT NULL
ALTER TABLE [Resume] ADD College NVARCHAR(400);

ALTER TABLE [Customer] ADD OTP VARCHAR(10);
ALTER TABLE [Customer] ADD LastOTPGenerated  datetime2(7) NULL;
ALTER TABLE [Patient] ADD [Day] INT NULL;
ALTER TABLE [Patient] ADD [Month] INT NULL;
ALTER TABLE [Patient] ADD [Year] INT NULL;
ALTER TABLE [Booking] ADD [CustomerId] INT NULL;
ALTER TABLE [Order] ADD [PayMethod] INT NULL;

ALTER TABLE [Booking] ADD [Successful] BOOL NULL;
ALTER TABLE [Booking] ADD [Result] NVARCHAR(MAX) NULL;
ALTER TABLE [Booking] ADD [ErrorCode] INT NULL;
ALTER TABLE [Booking] ADD [ErrorMessage] NVARCHAR(MAX) NULL;

