ALTER DATABASE hisdb SET SINGLE_USER WITH ROLLBACK IMMEDIATE    

alter database hisdb collate Vietnamese_CI_AS

ALTER DATABASE hisdb SET MULTI_USER

USE [hisdb]
GO
/****** Object:  UserDefinedFunction [dbo].[nop_splitstring_to_table]    Script Date: 05/17/2020 14:46:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[nop_splitstring_to_table]
(
    @string NVARCHAR(MAX),
    @delimiter CHAR(1)
)
RETURNS @output TABLE(
    data NVARCHAR(MAX)
)
BEGIN
    DECLARE @start INT, @end INT
    SELECT @start = 1, @end = CHARINDEX(@delimiter, @string)

    WHILE @start < LEN(@string) + 1 BEGIN
        IF @end = 0 
            SET @end = LEN(@string) + 1

        INSERT INTO @output (data) 
        VALUES(SUBSTRING(@string, @start, @end - @start))
        SET @start = @end + 1
        SET @end = CHARINDEX(@delimiter, @string, @start)
    END
    RETURN
END
delete from hisdb.dbo.City

select count(1) from ward
group by hisid
having count(1)