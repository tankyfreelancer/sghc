-- DROP SCHEMA dbo;

CREATE SCHEMA dbo;
-- his_web.dbo.AclRecord definition

-- Drop table

-- DROP TABLE his_web.dbo.AclRecord GO

CREATE TABLE his_web.dbo.AclRecord (
	Id int IDENTITY(1,1) NOT NULL,
	CustomerRoleId int NOT NULL,
	EntityName nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	EntityId int NOT NULL,
	CONSTRAINT PK_AclRecord PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ActivityLog definition

-- Drop table

-- DROP TABLE his_web.dbo.ActivityLog GO

CREATE TABLE his_web.dbo.ActivityLog (
	Id int IDENTITY(1,1) NOT NULL,
	IpAddress nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	Comment nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CustomerId int NOT NULL,
	EntityName nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	EntityId int NULL,
	ActivityLogTypeId int NOT NULL,
	CONSTRAINT PK_ActivityLog PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ActivityLogType definition

-- Drop table

-- DROP TABLE his_web.dbo.ActivityLogType GO

CREATE TABLE his_web.dbo.ActivityLogType (
	Id int IDENTITY(1,1) NOT NULL,
	Enabled bit NOT NULL,
	Name nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	SystemKeyword nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_ActivityLogType PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Address definition

-- Drop table

-- DROP TABLE his_web.dbo.Address GO

CREATE TABLE his_web.dbo.Address (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	CustomAttributes nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	FaxNumber nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	PhoneNumber nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ZipPostalCode nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Address2 nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Address1 nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	City nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	County nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	StateProvinceId int NULL,
	CountryId int NULL,
	Company nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Email nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	LastName nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	FirstName nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT PK_Address PRIMARY KEY (Id)
) GO;


-- his_web.dbo.AddressAttribute definition

-- Drop table

-- DROP TABLE his_web.dbo.AddressAttribute GO

CREATE TABLE his_web.dbo.AddressAttribute (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	AttributeControlTypeId int NOT NULL,
	IsRequired bit NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_AddressAttribute PRIMARY KEY (Id)
) GO;


-- his_web.dbo.AddressAttributeValue definition

-- Drop table

-- DROP TABLE his_web.dbo.AddressAttributeValue GO

CREATE TABLE his_web.dbo.AddressAttributeValue (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	IsPreSelected bit NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	AddressAttributeId int NOT NULL,
	CONSTRAINT PK_AddressAttributeValue PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Affiliate definition

-- Drop table

-- DROP TABLE his_web.dbo.Affiliate GO

CREATE TABLE his_web.dbo.Affiliate (
	Id int IDENTITY(1,1) NOT NULL,
	Active bit NOT NULL,
	Deleted bit NOT NULL,
	FriendlyUrlName nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AdminComment nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AddressId int NOT NULL,
	CONSTRAINT PK_Affiliate PRIMARY KEY (Id)
) GO;


-- his_web.dbo.BackInStockSubscription definition

-- Drop table

-- DROP TABLE his_web.dbo.BackInStockSubscription GO

CREATE TABLE his_web.dbo.BackInStockSubscription (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	CustomerId int NOT NULL,
	ProductId int NOT NULL,
	StoreId int NOT NULL,
	CONSTRAINT PK_BackInStockSubscription PRIMARY KEY (Id)
) GO;


-- his_web.dbo.BlogComment definition

-- Drop table

-- DROP TABLE his_web.dbo.BlogComment GO

CREATE TABLE his_web.dbo.BlogComment (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	BlogPostId int NOT NULL,
	StoreId int NOT NULL,
	IsApproved bit NOT NULL,
	CommentText nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CustomerId int NOT NULL,
	CONSTRAINT PK_BlogComment PRIMARY KEY (Id)
) GO;


-- his_web.dbo.BlogPost definition

-- Drop table

-- DROP TABLE his_web.dbo.BlogPost GO

CREATE TABLE his_web.dbo.BlogPost (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	LimitedToStores bit NOT NULL,
	MetaTitle nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaDescription nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaKeywords nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	EndDateUtc datetime2(7) NULL,
	StartDateUtc datetime2(7) NULL,
	Tags nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AllowComments bit NOT NULL,
	BodyOverview nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Body nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Title nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	IncludeInSitemap bit NOT NULL,
	LanguageId int NOT NULL,
	CONSTRAINT PK_BlogPost PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Booking definition

-- Drop table

-- DROP TABLE his_web.dbo.Booking GO

CREATE TABLE his_web.dbo.Booking (
	Id int IDENTITY(1,1) NOT NULL,
	Guid uniqueidentifier NOT NULL,
	HisId bigint NULL,
	TransactionCodeTT nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	TransactionCodeGD nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	BookingDate nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DoctorId int NULL,
	SubjectId int NULL,
	RoomId int NULL,
	PatientName nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	PatientSurname nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Birthyear int NULL,
	Birthdate nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	SocialId int NULL,
	Mobile nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	PatientCode nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CountryCode nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CityId int NULL,
	DistrictId int NULL,
	WardId int NULL,
	Address nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Sex int NULL,
	Booking_number int NULL,
	BvTime nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MethodId int NULL,
	Amount decimal(18,0) NULL,
	AmountOriginal decimal(18,0) NULL,
	AmountGate decimal(18,0) NULL,
	Status int NULL,
	IsBHYT int NULL,
	DateCreate datetime2(7) NOT NULL,
	DateUpdate datetime2(7) NOT NULL,
	CONSTRAINT PK_Booking PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Branch definition

-- Drop table

-- DROP TABLE his_web.dbo.Branch GO

CREATE TABLE his_web.dbo.Branch (
	Id int IDENTITY(1,1) NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Address nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	PhoneNumber nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	WorkTime nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Fax nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Email nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Hotline nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	HeadOffice bit NOT NULL
) GO;


-- his_web.dbo.Campaign definition

-- Drop table

-- DROP TABLE his_web.dbo.Campaign GO

CREATE TABLE his_web.dbo.Campaign (
	Id int IDENTITY(1,1) NOT NULL,
	DontSendBeforeDateUtc datetime2(7) NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	CustomerRoleId int NOT NULL,
	StoreId int NOT NULL,
	Body nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Subject nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Name nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_Campaign PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Category definition

-- Drop table

-- DROP TABLE his_web.dbo.Category GO

CREATE TABLE his_web.dbo.Category (
	Id int IDENTITY(1,1) NOT NULL,
	UpdatedOnUtc datetime2(7) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	DisplayOrder int NOT NULL,
	Deleted bit NOT NULL,
	Published bit NOT NULL,
	LimitedToStores bit NOT NULL,
	SubjectToAcl bit NOT NULL,
	IncludeInTopMenu bit NOT NULL,
	ShowOnHomepage bit NOT NULL,
	PriceRanges nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	PageSizeOptions nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AllowCustomersToSelectPageSize bit NOT NULL,
	PageSize int NOT NULL,
	PictureId int NOT NULL,
	ParentCategoryId int NOT NULL,
	MetaTitle nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaDescription nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaKeywords nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CategoryTemplateId int NOT NULL,
	Description nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	HisId int NOT NULL,
	DefaultPrice decimal(18,0) NULL,
	CONSTRAINT PK_Category PRIMARY KEY (Id)
) GO;


-- his_web.dbo.CategoryTemplate definition

-- Drop table

-- DROP TABLE his_web.dbo.CategoryTemplate GO

CREATE TABLE his_web.dbo.CategoryTemplate (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	ViewPath nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_CategoryTemplate PRIMARY KEY (Id)
) GO;


-- his_web.dbo.CheckoutAttribute definition

-- Drop table

-- DROP TABLE his_web.dbo.CheckoutAttribute GO

CREATE TABLE his_web.dbo.CheckoutAttribute (
	Id int IDENTITY(1,1) NOT NULL,
	ConditionAttributeXml nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DefaultValue nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ValidationFileMaximumSize int NULL,
	ValidationFileAllowedExtensions nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ValidationMaxLength int NULL,
	ValidationMinLength int NULL,
	LimitedToStores bit NOT NULL,
	DisplayOrder int NOT NULL,
	AttributeControlTypeId int NOT NULL,
	TaxCategoryId int NOT NULL,
	IsTaxExempt bit NOT NULL,
	ShippableProductRequired bit NOT NULL,
	IsRequired bit NOT NULL,
	TextPrompt nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_CheckoutAttribute PRIMARY KEY (Id)
) GO;


-- his_web.dbo.CheckoutAttributeValue definition

-- Drop table

-- DROP TABLE his_web.dbo.CheckoutAttributeValue GO

CREATE TABLE his_web.dbo.CheckoutAttributeValue (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	IsPreSelected bit NOT NULL,
	WeightAdjustment decimal(18,4) NOT NULL,
	PriceAdjustment decimal(18,4) NOT NULL,
	ColorSquaresRgb nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CheckoutAttributeId int NOT NULL,
	CONSTRAINT PK_CheckoutAttributeValue PRIMARY KEY (Id)
) GO;


-- his_web.dbo.City definition

-- Drop table

-- DROP TABLE his_web.dbo.City GO

CREATE TABLE his_web.dbo.City (
	Id int IDENTITY(1,1) NOT NULL,
	Name nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CountryCode nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DisplayOrder int NOT NULL,
	CountryId int NOT NULL,
	HisId int NOT NULL,
	CONSTRAINT PK_City PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Country definition

-- Drop table

-- DROP TABLE his_web.dbo.Country GO

CREATE TABLE his_web.dbo.Country (
	Id int IDENTITY(1,1) NOT NULL,
	LimitedToStores bit NOT NULL,
	DisplayOrder int NOT NULL,
	Published bit NOT NULL,
	SubjectToVat bit NOT NULL,
	NumericIsoCode int NOT NULL,
	ThreeLetterIsoCode nvarchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	TwoLetterIsoCode nvarchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AllowsShipping bit NOT NULL,
	AllowsBilling bit NOT NULL,
	Name nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_Country PRIMARY KEY (Id)
) GO;


-- his_web.dbo.CrossSellProduct definition

-- Drop table

-- DROP TABLE his_web.dbo.CrossSellProduct GO

CREATE TABLE his_web.dbo.CrossSellProduct (
	Id int IDENTITY(1,1) NOT NULL,
	ProductId2 int NOT NULL,
	ProductId1 int NOT NULL,
	CONSTRAINT PK_CrossSellProduct PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Currency definition

-- Drop table

-- DROP TABLE his_web.dbo.Currency GO

CREATE TABLE his_web.dbo.Currency (
	Id int IDENTITY(1,1) NOT NULL,
	RoundingTypeId int NOT NULL,
	UpdatedOnUtc datetime2(7) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	DisplayOrder int NOT NULL,
	Published bit NOT NULL,
	LimitedToStores bit NOT NULL,
	CustomFormatting nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DisplayLocale nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Rate decimal(18,4) NOT NULL,
	CurrencyCode nvarchar(5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Name nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_Currency PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Customer definition

-- Drop table

-- DROP TABLE his_web.dbo.Customer GO

CREATE TABLE his_web.dbo.Customer (
	Id int IDENTITY(1,1) NOT NULL,
	ShippingAddress_Id int NULL,
	BillingAddress_Id int NULL,
	RegisteredInStoreId int NOT NULL,
	LastActivityDateUtc datetime2(7) NOT NULL,
	LastLoginDateUtc datetime2(7) NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	LastIpAddress nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	SystemName nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	IsSystemAccount bit NOT NULL,
	Deleted bit NOT NULL,
	Active bit NOT NULL,
	CannotLoginUntilDateUtc datetime2(7) NULL,
	FailedLoginAttempts int NOT NULL,
	RequireReLogin bit NOT NULL,
	HasShoppingCartItems bit NOT NULL,
	VendorId int NOT NULL,
	AffiliateId int NOT NULL,
	IsTaxExempt bit NOT NULL,
	AdminComment nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	EmailToRevalidate nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Email nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Username nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CustomerGuid uniqueidentifier NOT NULL,
	CONSTRAINT PK_Customer PRIMARY KEY (Id)
) GO;


-- his_web.dbo.CustomerAddresses definition

-- Drop table

-- DROP TABLE his_web.dbo.CustomerAddresses GO

CREATE TABLE his_web.dbo.CustomerAddresses (
	Address_Id int NOT NULL,
	Customer_Id int NOT NULL,
	CONSTRAINT PK_CustomerAddresses PRIMARY KEY (Address_Id,Customer_Id)
) GO;


-- his_web.dbo.CustomerAttribute definition

-- Drop table

-- DROP TABLE his_web.dbo.CustomerAttribute GO

CREATE TABLE his_web.dbo.CustomerAttribute (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	AttributeControlTypeId int NOT NULL,
	IsRequired bit NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_CustomerAttribute PRIMARY KEY (Id)
) GO;


-- his_web.dbo.CustomerAttributeValue definition

-- Drop table

-- DROP TABLE his_web.dbo.CustomerAttributeValue GO

CREATE TABLE his_web.dbo.CustomerAttributeValue (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	IsPreSelected bit NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CustomerAttributeId int NOT NULL,
	CONSTRAINT PK_CustomerAttributeValue PRIMARY KEY (Id)
) GO;


-- his_web.dbo.CustomerPassword definition

-- Drop table

-- DROP TABLE his_web.dbo.CustomerPassword GO

CREATE TABLE his_web.dbo.CustomerPassword (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	PasswordSalt nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	PasswordFormatId int NOT NULL,
	Password nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CustomerId int NOT NULL,
	CONSTRAINT PK_CustomerPassword PRIMARY KEY (Id)
) GO;


-- his_web.dbo.CustomerRole definition

-- Drop table

-- DROP TABLE his_web.dbo.CustomerRole GO

CREATE TABLE his_web.dbo.CustomerRole (
	Id int IDENTITY(1,1) NOT NULL,
	PurchasedWithProductId int NOT NULL,
	DefaultTaxDisplayTypeId int NOT NULL,
	OverrideTaxDisplayType bit NOT NULL,
	EnablePasswordLifetime bit NOT NULL,
	SystemName nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	IsSystemRole bit NOT NULL,
	Active bit NOT NULL,
	TaxExempt bit NOT NULL,
	FreeShipping bit NOT NULL,
	Name nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_CustomerRole PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Customer_CustomerRole_Mapping definition

-- Drop table

-- DROP TABLE his_web.dbo.Customer_CustomerRole_Mapping GO

CREATE TABLE his_web.dbo.Customer_CustomerRole_Mapping (
	CustomerRole_Id int NOT NULL,
	Customer_Id int NOT NULL,
	CONSTRAINT PK_Customer_CustomerRole_Mapping PRIMARY KEY (CustomerRole_Id,Customer_Id)
) GO;


-- his_web.dbo.DeliveryDate definition

-- Drop table

-- DROP TABLE his_web.dbo.DeliveryDate GO

CREATE TABLE his_web.dbo.DeliveryDate (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_DeliveryDate PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Discount definition

-- Drop table

-- DROP TABLE his_web.dbo.Discount GO

CREATE TABLE his_web.dbo.Discount (
	Id int IDENTITY(1,1) NOT NULL,
	AppliedToSubCategories bit NOT NULL,
	MaximumDiscountedQuantity int NULL,
	LimitationTimes int NOT NULL,
	DiscountLimitationId int NOT NULL,
	IsCumulative bit NOT NULL,
	CouponCode nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	RequiresCouponCode bit NOT NULL,
	EndDateUtc datetime2(7) NULL,
	StartDateUtc datetime2(7) NULL,
	MaximumDiscountAmount decimal(18,4) NULL,
	DiscountAmount decimal(18,4) NOT NULL,
	DiscountPercentage decimal(18,4) NOT NULL,
	UsePercentage bit NOT NULL,
	DiscountTypeId int NOT NULL,
	AdminComment nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_Discount PRIMARY KEY (Id)
) GO;


-- his_web.dbo.DiscountRequirement definition

-- Drop table

-- DROP TABLE his_web.dbo.DiscountRequirement GO

CREATE TABLE his_web.dbo.DiscountRequirement (
	Id int IDENTITY(1,1) NOT NULL,
	IsGroup bit NOT NULL,
	InteractionTypeId int NULL,
	ParentId int NULL,
	DiscountRequirementRuleSystemName nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DiscountId int NOT NULL,
	CONSTRAINT PK_DiscountRequirement PRIMARY KEY (Id)
) GO;


-- his_web.dbo.DiscountUsageHistory definition

-- Drop table

-- DROP TABLE his_web.dbo.DiscountUsageHistory GO

CREATE TABLE his_web.dbo.DiscountUsageHistory (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	OrderId int NOT NULL,
	DiscountId int NOT NULL,
	CONSTRAINT PK_DiscountUsageHistory PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Discount_AppliedToCategories definition

-- Drop table

-- DROP TABLE his_web.dbo.Discount_AppliedToCategories GO

CREATE TABLE his_web.dbo.Discount_AppliedToCategories (
	Discount_Id int NOT NULL,
	Category_Id int NOT NULL,
	CONSTRAINT PK_Discount_AppliedToCategories PRIMARY KEY (Discount_Id,Category_Id)
) GO;


-- his_web.dbo.Discount_AppliedToManufacturers definition

-- Drop table

-- DROP TABLE his_web.dbo.Discount_AppliedToManufacturers GO

CREATE TABLE his_web.dbo.Discount_AppliedToManufacturers (
	Discount_Id int NOT NULL,
	Manufacturer_Id int NOT NULL,
	CONSTRAINT PK_Discount_AppliedToManufacturers PRIMARY KEY (Discount_Id,Manufacturer_Id)
) GO;


-- his_web.dbo.Discount_AppliedToProducts definition

-- Drop table

-- DROP TABLE his_web.dbo.Discount_AppliedToProducts GO

CREATE TABLE his_web.dbo.Discount_AppliedToProducts (
	Discount_Id int NOT NULL,
	Product_Id int NOT NULL,
	CONSTRAINT PK_Discount_AppliedToProducts PRIMARY KEY (Discount_Id,Product_Id)
) GO;


-- his_web.dbo.District definition

-- Drop table

-- DROP TABLE his_web.dbo.District GO

CREATE TABLE his_web.dbo.District (
	Id int IDENTITY(1,1) NOT NULL,
	Name nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	DisplayOrder int NOT NULL,
	CityId int NOT NULL,
	HisId int NOT NULL,
	HisCityId int NOT NULL,
	CONSTRAINT PK_District PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Download definition

-- Drop table

-- DROP TABLE his_web.dbo.Download GO

CREATE TABLE his_web.dbo.Download (
	Id int IDENTITY(1,1) NOT NULL,
	IsNew bit NOT NULL,
	Extension nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Filename nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ContentType nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DownloadBinary varbinary NULL,
	DownloadUrl nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	UseDownloadUrl bit NOT NULL,
	DownloadGuid uniqueidentifier NOT NULL,
	CONSTRAINT PK_Download PRIMARY KEY (Id)
) GO;


-- his_web.dbo.EmailAccount definition

-- Drop table

-- DROP TABLE his_web.dbo.EmailAccount GO

CREATE TABLE his_web.dbo.EmailAccount (
	Id int IDENTITY(1,1) NOT NULL,
	UseDefaultCredentials bit NOT NULL,
	EnableSsl bit NOT NULL,
	Password nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Username nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Port int NOT NULL,
	Host nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	DisplayName nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Email nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_EmailAccount PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ExternalAuthenticationRecord definition

-- Drop table

-- DROP TABLE his_web.dbo.ExternalAuthenticationRecord GO

CREATE TABLE his_web.dbo.ExternalAuthenticationRecord (
	Id int IDENTITY(1,1) NOT NULL,
	ProviderSystemName nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	OAuthAccessToken nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	OAuthToken nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ExternalDisplayIdentifier nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ExternalIdentifier nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Email nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CustomerId int NOT NULL,
	CONSTRAINT PK_ExternalAuthenticationRecord PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Forums_Forum definition

-- Drop table

-- DROP TABLE his_web.dbo.Forums_Forum GO

CREATE TABLE his_web.dbo.Forums_Forum (
	Id int IDENTITY(1,1) NOT NULL,
	UpdatedOnUtc datetime2(7) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	DisplayOrder int NOT NULL,
	LastPostTime datetime2(7) NULL,
	LastPostCustomerId int NOT NULL,
	LastPostId int NOT NULL,
	LastTopicId int NOT NULL,
	NumPosts int NOT NULL,
	NumTopics int NOT NULL,
	Description nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	ForumGroupId int NOT NULL,
	CONSTRAINT PK_Forums_Forum PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Forums_Group definition

-- Drop table

-- DROP TABLE his_web.dbo.Forums_Group GO

CREATE TABLE his_web.dbo.Forums_Group (
	Id int IDENTITY(1,1) NOT NULL,
	UpdatedOnUtc datetime2(7) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	DisplayOrder int NOT NULL,
	Name nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_Forums_Group PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Forums_Post definition

-- Drop table

-- DROP TABLE his_web.dbo.Forums_Post GO

CREATE TABLE his_web.dbo.Forums_Post (
	Id int IDENTITY(1,1) NOT NULL,
	VoteCount int NOT NULL,
	UpdatedOnUtc datetime2(7) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	IPAddress nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Text] nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CustomerId int NOT NULL,
	TopicId int NOT NULL,
	CONSTRAINT PK_Forums_Post PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Forums_PostVote definition

-- Drop table

-- DROP TABLE his_web.dbo.Forums_PostVote GO

CREATE TABLE his_web.dbo.Forums_PostVote (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	IsUp bit NOT NULL,
	CustomerId int NOT NULL,
	ForumPostId int NOT NULL,
	CONSTRAINT PK_Forums_PostVote PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Forums_PrivateMessage definition

-- Drop table

-- DROP TABLE his_web.dbo.Forums_PrivateMessage GO

CREATE TABLE his_web.dbo.Forums_PrivateMessage (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	IsDeletedByRecipient bit NOT NULL,
	IsDeletedByAuthor bit NOT NULL,
	IsRead bit NOT NULL,
	[Text] nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Subject nvarchar(450) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	ToCustomerId int NOT NULL,
	FromCustomerId int NOT NULL,
	StoreId int NOT NULL,
	CONSTRAINT PK_Forums_PrivateMessage PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Forums_Subscription definition

-- Drop table

-- DROP TABLE his_web.dbo.Forums_Subscription GO

CREATE TABLE his_web.dbo.Forums_Subscription (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	TopicId int NOT NULL,
	ForumId int NOT NULL,
	CustomerId int NOT NULL,
	SubscriptionGuid uniqueidentifier NOT NULL,
	CONSTRAINT PK_Forums_Subscription PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Forums_Topic definition

-- Drop table

-- DROP TABLE his_web.dbo.Forums_Topic GO

CREATE TABLE his_web.dbo.Forums_Topic (
	Id int IDENTITY(1,1) NOT NULL,
	UpdatedOnUtc datetime2(7) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	LastPostTime datetime2(7) NULL,
	LastPostCustomerId int NOT NULL,
	LastPostId int NOT NULL,
	Views int NOT NULL,
	NumPosts int NOT NULL,
	Subject nvarchar(450) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	TopicTypeId int NOT NULL,
	CustomerId int NOT NULL,
	ForumId int NOT NULL,
	CONSTRAINT PK_Forums_Topic PRIMARY KEY (Id)
) GO;


-- his_web.dbo.GdprConsent definition

-- Drop table

-- DROP TABLE his_web.dbo.GdprConsent GO

CREATE TABLE his_web.dbo.GdprConsent (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	DisplayOnCustomerInfoPage bit NOT NULL,
	DisplayDuringRegistration bit NOT NULL,
	RequiredMessage nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	IsRequired bit NOT NULL,
	Message nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_GdprConsent PRIMARY KEY (Id)
) GO;


-- his_web.dbo.GdprLog definition

-- Drop table

-- DROP TABLE his_web.dbo.GdprLog GO

CREATE TABLE his_web.dbo.GdprLog (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	RequestDetails nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	RequestTypeId int NOT NULL,
	CustomerInfo nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ConsentId int NOT NULL,
	CustomerId int NOT NULL,
	CONSTRAINT PK_GdprLog PRIMARY KEY (Id)
) GO;


-- his_web.dbo.GenericAttribute definition

-- Drop table

-- DROP TABLE his_web.dbo.GenericAttribute GO

CREATE TABLE his_web.dbo.GenericAttribute (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOrUpdatedDateUTC datetime2(7) NULL,
	StoreId int NOT NULL,
	Value nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Key] nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	KeyGroup nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	EntityId int NOT NULL,
	CONSTRAINT PK_GenericAttribute PRIMARY KEY (Id)
) GO;


-- his_web.dbo.GiftCard definition

-- Drop table

-- DROP TABLE his_web.dbo.GiftCard GO

CREATE TABLE his_web.dbo.GiftCard (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	IsRecipientNotified bit NOT NULL,
	Message nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	SenderEmail nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	SenderName nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	RecipientEmail nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	RecipientName nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	GiftCardCouponCode nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	IsGiftCardActivated bit NOT NULL,
	Amount decimal(18,4) NOT NULL,
	GiftCardTypeId int NOT NULL,
	PurchasedWithOrderItemId int NULL,
	CONSTRAINT PK_GiftCard PRIMARY KEY (Id)
) GO;


-- his_web.dbo.GiftCardUsageHistory definition

-- Drop table

-- DROP TABLE his_web.dbo.GiftCardUsageHistory GO

CREATE TABLE his_web.dbo.GiftCardUsageHistory (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	UsedValue decimal(18,4) NOT NULL,
	UsedWithOrderId int NOT NULL,
	GiftCardId int NOT NULL,
	CONSTRAINT PK_GiftCardUsageHistory PRIMARY KEY (Id)
) GO;


-- his_web.dbo.[Language] definition

-- Drop table

-- DROP TABLE his_web.dbo.[Language] GO

CREATE TABLE his_web.dbo.[Language] (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	Published bit NOT NULL,
	DefaultCurrencyId int NOT NULL,
	LimitedToStores bit NOT NULL,
	Rtl bit NOT NULL,
	FlagImageFileName nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	UniqueSeoCode nvarchar(2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	LanguageCulture nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Name nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_Language PRIMARY KEY (Id)
) GO;


-- his_web.dbo.LocaleStringResource definition

-- Drop table

-- DROP TABLE his_web.dbo.LocaleStringResource GO

CREATE TABLE his_web.dbo.LocaleStringResource (
	Id int IDENTITY(1,1) NOT NULL,
	ResourceValue nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	ResourceName nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	LanguageId int NOT NULL,
	CONSTRAINT PK_LocaleStringResource PRIMARY KEY (Id)
) GO;


-- his_web.dbo.LocalizedProperty definition

-- Drop table

-- DROP TABLE his_web.dbo.LocalizedProperty GO

CREATE TABLE his_web.dbo.LocalizedProperty (
	Id int IDENTITY(1,1) NOT NULL,
	LocaleValue nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	LocaleKey nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	LocaleKeyGroup nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	LanguageId int NOT NULL,
	EntityId int NOT NULL,
	CONSTRAINT PK_LocalizedProperty PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Log definition

-- Drop table

-- DROP TABLE his_web.dbo.Log GO

CREATE TABLE his_web.dbo.Log (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	ReferrerUrl nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	PageUrl nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CustomerId int NULL,
	IpAddress nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	FullMessage nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ShortMessage nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	LogLevelId int NOT NULL,
	CONSTRAINT PK_Log PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Manufacturer definition

-- Drop table

-- DROP TABLE his_web.dbo.Manufacturer GO

CREATE TABLE his_web.dbo.Manufacturer (
	Id int IDENTITY(1,1) NOT NULL,
	UpdatedOnUtc datetime2(7) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	DisplayOrder int NOT NULL,
	Deleted bit NOT NULL,
	Published bit NOT NULL,
	LimitedToStores bit NOT NULL,
	SubjectToAcl bit NOT NULL,
	PriceRanges nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	PageSizeOptions nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AllowCustomersToSelectPageSize bit NOT NULL,
	PageSize int NOT NULL,
	PictureId int NOT NULL,
	MetaTitle nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaDescription nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaKeywords nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ManufacturerTemplateId int NOT NULL,
	Description nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_Manufacturer PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ManufacturerTemplate definition

-- Drop table

-- DROP TABLE his_web.dbo.ManufacturerTemplate GO

CREATE TABLE his_web.dbo.ManufacturerTemplate (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	ViewPath nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_ManufacturerTemplate PRIMARY KEY (Id)
) GO;


-- his_web.dbo.MeasureDimension definition

-- Drop table

-- DROP TABLE his_web.dbo.MeasureDimension GO

CREATE TABLE his_web.dbo.MeasureDimension (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	Ratio decimal(18,8) NOT NULL,
	SystemKeyword nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Name nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_MeasureDimension PRIMARY KEY (Id)
) GO;


-- his_web.dbo.MeasureWeight definition

-- Drop table

-- DROP TABLE his_web.dbo.MeasureWeight GO

CREATE TABLE his_web.dbo.MeasureWeight (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	Ratio decimal(18,8) NOT NULL,
	SystemKeyword nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Name nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_MeasureWeight PRIMARY KEY (Id)
) GO;


-- his_web.dbo.MessageTemplate definition

-- Drop table

-- DROP TABLE his_web.dbo.MessageTemplate GO

CREATE TABLE his_web.dbo.MessageTemplate (
	Id int IDENTITY(1,1) NOT NULL,
	LimitedToStores bit NOT NULL,
	EmailAccountId int NOT NULL,
	AttachedDownloadId int NOT NULL,
	DelayPeriodId int NOT NULL,
	DelayBeforeSend int NULL,
	IsActive bit NOT NULL,
	Body nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Subject nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	BccEmailAddresses nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_MessageTemplate PRIMARY KEY (Id)
) GO;


-- his_web.dbo.MigrationVersionInfo definition

-- Drop table

-- DROP TABLE his_web.dbo.MigrationVersionInfo GO

CREATE TABLE his_web.dbo.MigrationVersionInfo (
	AppliedOn datetime2(7) NOT NULL,
	Description nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Version bigint NOT NULL
) GO;


-- his_web.dbo.News definition

-- Drop table

-- DROP TABLE his_web.dbo.News GO

CREATE TABLE his_web.dbo.News (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	MetaTitle nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaDescription nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaKeywords nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	LimitedToStores bit NOT NULL,
	AllowComments bit NOT NULL,
	EndDateUtc datetime2(7) NULL,
	StartDateUtc datetime2(7) NULL,
	Published bit NOT NULL,
	[Full] nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Short nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Title nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	LanguageId int NOT NULL,
	IncludeInTopMenu bit NOT NULL,
	ShowOnHomePage bit NOT NULL,
	Tags nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CategoryId int NULL,
	CONSTRAINT PK_News PRIMARY KEY (Id)
) GO;


-- his_web.dbo.NewsCategory definition

-- Drop table

-- DROP TABLE his_web.dbo.NewsCategory GO

CREATE TABLE his_web.dbo.NewsCategory (
	Id int IDENTITY(1,1) NOT NULL,
	LanguageId int NOT NULL,
	CategoryId int NULL,
	Title nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Short nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Full] nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Published bit NOT NULL,
	StartDateUtc datetime2(7) NULL,
	EndDateUtc datetime2(7) NULL,
	AllowComments bit NOT NULL,
	LimitedToStores bit NOT NULL,
	MetaKeywords nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaDescription nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaTitle nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Tags nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	IncludeInTopMenu bit NOT NULL,
	ShowOnHomePage bit NOT NULL,
	CONSTRAINT PK_NewsCategory PRIMARY KEY (Id)
) GO;


-- his_web.dbo.NewsComment definition

-- Drop table

-- DROP TABLE his_web.dbo.NewsComment GO

CREATE TABLE his_web.dbo.NewsComment (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	StoreId int NOT NULL,
	IsApproved bit NOT NULL,
	CustomerId int NOT NULL,
	NewsItemId int NOT NULL,
	CommentText nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CommentTitle nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT PK_NewsComment PRIMARY KEY (Id)
) GO;


-- his_web.dbo.NewsLetterSubscription definition

-- Drop table

-- DROP TABLE his_web.dbo.NewsLetterSubscription GO

CREATE TABLE his_web.dbo.NewsLetterSubscription (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	StoreId int NOT NULL,
	Active bit NOT NULL,
	Email nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	NewsLetterSubscriptionGuid uniqueidentifier NOT NULL,
	CONSTRAINT PK_NewsLetterSubscription PRIMARY KEY (Id)
) GO;


-- his_web.dbo.News_Category_Mapping definition

-- Drop table

-- DROP TABLE his_web.dbo.News_Category_Mapping GO

CREATE TABLE his_web.dbo.News_Category_Mapping (
	NewsId int NOT NULL,
	CategoryId int NOT NULL,
	IsFeaturedNews bit NOT NULL,
	DisplayOrder int NOT NULL,
	Id int IDENTITY(0,1) NOT NULL
) GO;


-- his_web.dbo.News_Picture_Mapping definition

-- Drop table

-- DROP TABLE his_web.dbo.News_Picture_Mapping GO

CREATE TABLE his_web.dbo.News_Picture_Mapping (
	DisplayOrder int NOT NULL,
	PictureId int NOT NULL,
	NewsId int NOT NULL,
	Id int IDENTITY(0,1) NOT NULL
) GO;


-- his_web.dbo.[Order] definition

-- Drop table

-- DROP TABLE his_web.dbo.[Order] GO

CREATE TABLE his_web.dbo.[Order] (
	Id int IDENTITY(1,1) NOT NULL,
	RedeemedRewardPointsEntryId int NULL,
	CustomOrderNumber nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	Deleted bit NOT NULL,
	CustomValuesXml nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ShippingRateComputationMethodSystemName nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ShippingMethod nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	PaidDateUtc datetime2(7) NULL,
	SubscriptionTransactionId nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CaptureTransactionResult nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CaptureTransactionId nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AuthorizationTransactionResult nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AuthorizationTransactionCode nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AuthorizationTransactionId nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CardExpirationYear nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CardExpirationMonth nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CardCvv2 nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MaskedCreditCardNumber nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CardNumber nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CardName nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CardType nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AllowStoringCreditCardNumber bit NOT NULL,
	CustomerIp nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AffiliateId int NOT NULL,
	CustomerLanguageId int NOT NULL,
	CheckoutAttributesXml nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CheckoutAttributeDescription nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	RewardPointsHistoryEntryId int NULL,
	RefundedAmount decimal(18,4) NOT NULL,
	OrderTotal decimal(18,4) NOT NULL,
	OrderDiscount decimal(18,4) NOT NULL,
	OrderTax decimal(18,4) NOT NULL,
	TaxRates nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	PaymentMethodAdditionalFeeExclTax decimal(18,4) NOT NULL,
	PaymentMethodAdditionalFeeInclTax decimal(18,4) NOT NULL,
	OrderShippingExclTax decimal(18,4) NOT NULL,
	OrderShippingInclTax decimal(18,4) NOT NULL,
	OrderSubTotalDiscountExclTax decimal(18,4) NOT NULL,
	OrderSubTotalDiscountInclTax decimal(18,4) NOT NULL,
	OrderSubtotalExclTax decimal(18,4) NOT NULL,
	OrderSubtotalInclTax decimal(18,4) NOT NULL,
	VatNumber nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CustomerTaxDisplayTypeId int NOT NULL,
	CurrencyRate decimal(18,4) NOT NULL,
	CustomerCurrencyCode nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	PaymentMethodSystemName nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	PaymentStatusId int NOT NULL,
	ShippingStatusId int NOT NULL,
	OrderStatusId int NOT NULL,
	PickupInStore bit NOT NULL,
	PickupAddressId int NULL,
	ShippingAddressId int NULL,
	BillingAddressId int NOT NULL,
	CustomerId int NOT NULL,
	StoreId int NOT NULL,
	OrderGuid uniqueidentifier NOT NULL,
	CONSTRAINT PK_Order PRIMARY KEY (Id)
) GO;


-- his_web.dbo.OrderItem definition

-- Drop table

-- DROP TABLE his_web.dbo.OrderItem GO

CREATE TABLE his_web.dbo.OrderItem (
	Id int IDENTITY(1,1) NOT NULL,
	RentalEndDateUtc datetime2(7) NULL,
	RentalStartDateUtc datetime2(7) NULL,
	ItemWeight decimal(18,4) NULL,
	LicenseDownloadId int NULL,
	IsDownloadActivated bit NOT NULL,
	DownloadCount int NOT NULL,
	AttributesXml nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AttributeDescription nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	OriginalProductCost decimal(18,4) NOT NULL,
	DiscountAmountExclTax decimal(18,4) NOT NULL,
	DiscountAmountInclTax decimal(18,4) NOT NULL,
	PriceExclTax decimal(18,4) NOT NULL,
	PriceInclTax decimal(18,4) NOT NULL,
	UnitPriceExclTax decimal(18,4) NOT NULL,
	UnitPriceInclTax decimal(18,4) NOT NULL,
	Quantity int NOT NULL,
	ProductId int NOT NULL,
	OrderId int NOT NULL,
	OrderItemGuid uniqueidentifier NOT NULL,
	CONSTRAINT PK_OrderItem PRIMARY KEY (Id)
) GO;


-- his_web.dbo.OrderNote definition

-- Drop table

-- DROP TABLE his_web.dbo.OrderNote GO

CREATE TABLE his_web.dbo.OrderNote (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	DisplayToCustomer bit NOT NULL,
	DownloadId int NOT NULL,
	Note nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	OrderId int NOT NULL,
	CONSTRAINT PK_OrderNote PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Patient definition

-- Drop table

-- DROP TABLE his_web.dbo.Patient GO

CREATE TABLE his_web.dbo.Patient (
	Id int IDENTITY(1,1) NOT NULL,
	Guid uniqueidentifier NOT NULL,
	HisId int NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	SurName nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Sex int NULL,
	BirthDate nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	BirthYear int NULL,
	Mobile nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	SocialId int NULL,
	CountryId int NULL,
	CountryCode nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CityId int NULL,
	DistrictId int NULL,
	WardId int NULL,
	Address nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	BHYTCode nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Code nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Email nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Passport nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Active bit NOT NULL,
	Deleted bit NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	CustomerId int NULL,
	CONSTRAINT PK_Patient PRIMARY KEY (Id)
) GO;


-- his_web.dbo.PermissionRecord definition

-- Drop table

-- DROP TABLE his_web.dbo.PermissionRecord GO

CREATE TABLE his_web.dbo.PermissionRecord (
	Id int IDENTITY(1,1) NOT NULL,
	Category nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	SystemName nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Name nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_PermissionRecord PRIMARY KEY (Id)
) GO;


-- his_web.dbo.PermissionRecord_Role_Mapping definition

-- Drop table

-- DROP TABLE his_web.dbo.PermissionRecord_Role_Mapping GO

CREATE TABLE his_web.dbo.PermissionRecord_Role_Mapping (
	CustomerRole_Id int NOT NULL,
	PermissionRecord_Id int NOT NULL,
	CONSTRAINT PK_PermissionRecord_Role_Mapping PRIMARY KEY (CustomerRole_Id,PermissionRecord_Id)
) GO;


-- his_web.dbo.Picture definition

-- Drop table

-- DROP TABLE his_web.dbo.Picture GO

CREATE TABLE his_web.dbo.Picture (
	Id int IDENTITY(1,1) NOT NULL,
	VirtualPath nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	IsNew bit NOT NULL,
	TitleAttribute nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AltAttribute nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	SeoFilename nvarchar(300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MimeType nvarchar(40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_Picture PRIMARY KEY (Id)
) GO;


-- his_web.dbo.PictureBinary definition

-- Drop table

-- DROP TABLE his_web.dbo.PictureBinary GO

CREATE TABLE his_web.dbo.PictureBinary (
	Id int IDENTITY(1,1) NOT NULL,
	PictureId int NOT NULL,
	BinaryData varbinary NULL,
	CONSTRAINT PK_PictureBinary PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Poll definition

-- Drop table

-- DROP TABLE his_web.dbo.Poll GO

CREATE TABLE his_web.dbo.Poll (
	Id int IDENTITY(1,1) NOT NULL,
	EndDateUtc datetime2(7) NULL,
	StartDateUtc datetime2(7) NULL,
	LimitedToStores bit NOT NULL,
	DisplayOrder int NOT NULL,
	AllowGuestsToVote bit NOT NULL,
	ShowOnHomepage bit NOT NULL,
	Published bit NOT NULL,
	SystemKeyword nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	LanguageId int NOT NULL,
	CONSTRAINT PK_Poll PRIMARY KEY (Id)
) GO;


-- his_web.dbo.PollAnswer definition

-- Drop table

-- DROP TABLE his_web.dbo.PollAnswer GO

CREATE TABLE his_web.dbo.PollAnswer (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	NumberOfVotes int NOT NULL,
	Name nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	PollId int NOT NULL,
	CONSTRAINT PK_PollAnswer PRIMARY KEY (Id)
) GO;


-- his_web.dbo.PollVotingRecord definition

-- Drop table

-- DROP TABLE his_web.dbo.PollVotingRecord GO

CREATE TABLE his_web.dbo.PollVotingRecord (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	CustomerId int NOT NULL,
	PollAnswerId int NOT NULL,
	CONSTRAINT PK_PollVotingRecord PRIMARY KEY (Id)
) GO;


-- his_web.dbo.PredefinedProductAttributeValue definition

-- Drop table

-- DROP TABLE his_web.dbo.PredefinedProductAttributeValue GO

CREATE TABLE his_web.dbo.PredefinedProductAttributeValue (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	IsPreSelected bit NOT NULL,
	Cost decimal(18,4) NOT NULL,
	WeightAdjustment decimal(18,4) NOT NULL,
	PriceAdjustmentUsePercentage bit NOT NULL,
	PriceAdjustment decimal(18,4) NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	ProductAttributeId int NOT NULL,
	CONSTRAINT PK_PredefinedProductAttributeValue PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Product definition

-- Drop table

-- DROP TABLE his_web.dbo.Product GO

CREATE TABLE his_web.dbo.Product (
	Id int IDENTITY(1,1) NOT NULL,
	UpdatedOnUtc datetime2(7) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	Deleted bit NOT NULL,
	Published bit NOT NULL,
	DisplayOrder int NOT NULL,
	AvailableEndDateTimeUtc datetime2(7) NULL,
	AvailableStartDateTimeUtc datetime2(7) NULL,
	Height decimal(18,4) NOT NULL,
	Width decimal(18,4) NOT NULL,
	[Length] decimal(18,4) NOT NULL,
	Weight decimal(18,4) NOT NULL,
	HasDiscountsApplied bit NOT NULL,
	HasTierPrices bit NOT NULL,
	MarkAsNewEndDateTimeUtc datetime2(7) NULL,
	MarkAsNewStartDateTimeUtc datetime2(7) NULL,
	MarkAsNew bit NOT NULL,
	BasepriceBaseUnitId int NOT NULL,
	BasepriceBaseAmount decimal(18,4) NOT NULL,
	BasepriceUnitId int NOT NULL,
	BasepriceAmount decimal(18,4) NOT NULL,
	BasepriceEnabled bit NOT NULL,
	MaximumCustomerEnteredPrice decimal(18,4) NOT NULL,
	MinimumCustomerEnteredPrice decimal(18,4) NOT NULL,
	CustomerEntersPrice bit NOT NULL,
	ProductCost decimal(18,4) NOT NULL,
	OldPrice decimal(18,4) NOT NULL,
	Price decimal(18,4) NOT NULL,
	CallForPrice bit NOT NULL,
	PreOrderAvailabilityStartDateTimeUtc datetime2(7) NULL,
	AvailableForPreOrder bit NOT NULL,
	DisableWishlistButton bit NOT NULL,
	DisableBuyButton bit NOT NULL,
	NotReturnable bit NOT NULL,
	AllowAddingOnlyExistingAttributeCombinations bit NOT NULL,
	AllowedQuantities nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	OrderMaximumQuantity int NOT NULL,
	OrderMinimumQuantity int NOT NULL,
	AllowBackInStockSubscriptions bit NOT NULL,
	BackorderModeId int NOT NULL,
	NotifyAdminForQuantityBelow int NOT NULL,
	LowStockActivityId int NOT NULL,
	MinStockQuantity int NOT NULL,
	DisplayStockQuantity bit NOT NULL,
	DisplayStockAvailability bit NOT NULL,
	StockQuantity int NOT NULL,
	WarehouseId int NOT NULL,
	UseMultipleWarehouses bit NOT NULL,
	ProductAvailabilityRangeId int NOT NULL,
	ManageInventoryMethodId int NOT NULL,
	IsTelecommunicationsOrBroadcastingOrElectronicServices bit NOT NULL,
	TaxCategoryId int NOT NULL,
	IsTaxExempt bit NOT NULL,
	DeliveryDateId int NOT NULL,
	AdditionalShippingCharge decimal(18,4) NOT NULL,
	ShipSeparately bit NOT NULL,
	IsFreeShipping bit NOT NULL,
	IsShipEnabled bit NOT NULL,
	RentalPricePeriodId int NOT NULL,
	RentalPriceLength int NOT NULL,
	IsRental bit NOT NULL,
	RecurringTotalCycles int NOT NULL,
	RecurringCyclePeriodId int NOT NULL,
	RecurringCycleLength int NOT NULL,
	IsRecurring bit NOT NULL,
	UserAgreementText nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	HasUserAgreement bit NOT NULL,
	SampleDownloadId int NOT NULL,
	HasSampleDownload bit NOT NULL,
	DownloadActivationTypeId int NOT NULL,
	DownloadExpirationDays int NULL,
	MaxNumberOfDownloads int NOT NULL,
	UnlimitedDownloads bit NOT NULL,
	DownloadId int NOT NULL,
	IsDownload bit NOT NULL,
	AutomaticallyAddRequiredProducts bit NOT NULL,
	RequiredProductIds nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	RequireOtherProducts bit NOT NULL,
	OverriddenGiftCardAmount decimal(18,4) NULL,
	GiftCardTypeId int NOT NULL,
	IsGiftCard bit NOT NULL,
	Gtin nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ManufacturerPartNumber nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Sku nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	LimitedToStores bit NOT NULL,
	SubjectToAcl bit NOT NULL,
	NotApprovedTotalReviews int NOT NULL,
	ApprovedTotalReviews int NOT NULL,
	NotApprovedRatingSum int NOT NULL,
	ApprovedRatingSum int NOT NULL,
	AllowCustomerReviews bit NOT NULL,
	MetaTitle nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaDescription nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaKeywords nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ShowOnHomepage bit NOT NULL,
	VendorId int NOT NULL,
	ProductTemplateId int NOT NULL,
	AdminComment nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	FullDescription nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Guide nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Promotion nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ShortDescription nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	VisibleIndividually bit NOT NULL,
	ParentGroupedProductId int NOT NULL,
	ProductTypeId int NOT NULL,
	[Role] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Sex int NOT NULL,
	Birthyear int NOT NULL,
	HisId int NOT NULL,
	CONSTRAINT PK_Product PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ProductAttribute definition

-- Drop table

-- DROP TABLE his_web.dbo.ProductAttribute GO

CREATE TABLE his_web.dbo.ProductAttribute (
	Id int IDENTITY(1,1) NOT NULL,
	Description nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_ProductAttribute PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ProductAttributeCombination definition

-- Drop table

-- DROP TABLE his_web.dbo.ProductAttributeCombination GO

CREATE TABLE his_web.dbo.ProductAttributeCombination (
	Id int IDENTITY(1,1) NOT NULL,
	PictureId int NOT NULL,
	NotifyAdminForQuantityBelow int NOT NULL,
	OverriddenPrice decimal(18,4) NULL,
	Gtin nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ManufacturerPartNumber nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Sku nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AllowOutOfStockOrders bit NOT NULL,
	StockQuantity int NOT NULL,
	AttributesXml nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ProductId int NOT NULL,
	CONSTRAINT PK_ProductAttributeCombination PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ProductAttributeValue definition

-- Drop table

-- DROP TABLE his_web.dbo.ProductAttributeValue GO

CREATE TABLE his_web.dbo.ProductAttributeValue (
	Id int IDENTITY(1,1) NOT NULL,
	PictureId int NOT NULL,
	DisplayOrder int NOT NULL,
	IsPreSelected bit NOT NULL,
	Quantity int NOT NULL,
	CustomerEntersQty bit NOT NULL,
	Cost decimal(18,4) NOT NULL,
	WeightAdjustment decimal(18,4) NOT NULL,
	PriceAdjustmentUsePercentage bit NOT NULL,
	PriceAdjustment decimal(18,4) NOT NULL,
	ImageSquaresPictureId int NOT NULL,
	ColorSquaresRgb nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	AssociatedProductId int NOT NULL,
	AttributeValueTypeId int NOT NULL,
	ProductAttributeMappingId int NOT NULL,
	CONSTRAINT PK_ProductAttributeValue PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ProductAvailabilityRange definition

-- Drop table

-- DROP TABLE his_web.dbo.ProductAvailabilityRange GO

CREATE TABLE his_web.dbo.ProductAvailabilityRange (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_ProductAvailabilityRange PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ProductReview definition

-- Drop table

-- DROP TABLE his_web.dbo.ProductReview GO

CREATE TABLE his_web.dbo.ProductReview (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	HelpfulNoTotal int NOT NULL,
	HelpfulYesTotal int NOT NULL,
	Rating int NOT NULL,
	CustomerNotifiedOfReply bit NOT NULL,
	ReplyText nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ReviewText nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Title nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	IsApproved bit NOT NULL,
	StoreId int NOT NULL,
	ProductId int NOT NULL,
	CustomerId int NOT NULL,
	CONSTRAINT PK_ProductReview PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ProductReviewHelpfulness definition

-- Drop table

-- DROP TABLE his_web.dbo.ProductReviewHelpfulness GO

CREATE TABLE his_web.dbo.ProductReviewHelpfulness (
	Id int IDENTITY(1,1) NOT NULL,
	CustomerId int NOT NULL,
	WasHelpful bit NOT NULL,
	ProductReviewId int NOT NULL,
	CONSTRAINT PK_ProductReviewHelpfulness PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ProductReview_ReviewType_Mapping definition

-- Drop table

-- DROP TABLE his_web.dbo.ProductReview_ReviewType_Mapping GO

CREATE TABLE his_web.dbo.ProductReview_ReviewType_Mapping (
	Id int IDENTITY(1,1) NOT NULL,
	Rating int NOT NULL,
	ReviewTypeId int NOT NULL,
	ProductReviewId int NOT NULL,
	CONSTRAINT PK_ProductReview_ReviewType_Mapping PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ProductTag definition

-- Drop table

-- DROP TABLE his_web.dbo.ProductTag GO

CREATE TABLE his_web.dbo.ProductTag (
	Id int IDENTITY(1,1) NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_ProductTag PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ProductTemplate definition

-- Drop table

-- DROP TABLE his_web.dbo.ProductTemplate GO

CREATE TABLE his_web.dbo.ProductTemplate (
	Id int IDENTITY(1,1) NOT NULL,
	IgnoredProductTypes nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DisplayOrder int NOT NULL,
	ViewPath nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_ProductTemplate PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ProductWarehouseInventory definition

-- Drop table

-- DROP TABLE his_web.dbo.ProductWarehouseInventory GO

CREATE TABLE his_web.dbo.ProductWarehouseInventory (
	Id int IDENTITY(1,1) NOT NULL,
	ReservedQuantity int NOT NULL,
	StockQuantity int NOT NULL,
	WarehouseId int NOT NULL,
	ProductId int NOT NULL,
	CONSTRAINT PK_ProductWarehouseInventory PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Product_Category_Mapping definition

-- Drop table

-- DROP TABLE his_web.dbo.Product_Category_Mapping GO

CREATE TABLE his_web.dbo.Product_Category_Mapping (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	IsFeaturedProduct bit NOT NULL,
	CategoryId int NOT NULL,
	ProductId int NOT NULL,
	CONSTRAINT PK_Product_Category_Mapping PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Product_Manufacturer_Mapping definition

-- Drop table

-- DROP TABLE his_web.dbo.Product_Manufacturer_Mapping GO

CREATE TABLE his_web.dbo.Product_Manufacturer_Mapping (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	IsFeaturedProduct bit NOT NULL,
	ManufacturerId int NOT NULL,
	ProductId int NOT NULL,
	CONSTRAINT PK_Product_Manufacturer_Mapping PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Product_Picture_Mapping definition

-- Drop table

-- DROP TABLE his_web.dbo.Product_Picture_Mapping GO

CREATE TABLE his_web.dbo.Product_Picture_Mapping (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	PictureId int NOT NULL,
	ProductId int NOT NULL,
	CONSTRAINT PK_Product_Picture_Mapping PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Product_ProductAttribute_Mapping definition

-- Drop table

-- DROP TABLE his_web.dbo.Product_ProductAttribute_Mapping GO

CREATE TABLE his_web.dbo.Product_ProductAttribute_Mapping (
	Id int IDENTITY(1,1) NOT NULL,
	ConditionAttributeXml nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DefaultValue nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ValidationFileMaximumSize int NULL,
	ValidationFileAllowedExtensions nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ValidationMaxLength int NULL,
	ValidationMinLength int NULL,
	DisplayOrder int NOT NULL,
	AttributeControlTypeId int NOT NULL,
	IsRequired bit NOT NULL,
	TextPrompt nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ProductAttributeId int NOT NULL,
	ProductId int NOT NULL,
	CONSTRAINT PK_Product_ProductAttribute_Mapping PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Product_ProductTag_Mapping definition

-- Drop table

-- DROP TABLE his_web.dbo.Product_ProductTag_Mapping GO

CREATE TABLE his_web.dbo.Product_ProductTag_Mapping (
	ProductTag_Id int NOT NULL,
	Product_Id int NOT NULL,
	CONSTRAINT PK_Product_ProductTag_Mapping PRIMARY KEY (ProductTag_Id,Product_Id)
) GO;


-- his_web.dbo.Product_SpecificationAttribute_Mapping definition

-- Drop table

-- DROP TABLE his_web.dbo.Product_SpecificationAttribute_Mapping GO

CREATE TABLE his_web.dbo.Product_SpecificationAttribute_Mapping (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	ShowOnProductPage bit NOT NULL,
	AllowFiltering bit NOT NULL,
	CustomValue nvarchar(4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	SpecificationAttributeOptionId int NOT NULL,
	AttributeTypeId int NOT NULL,
	ProductId int NOT NULL,
	CONSTRAINT PK_Product_SpecificationAttribute_Mapping PRIMARY KEY (Id)
) GO;


-- his_web.dbo.QueuedEmail definition

-- Drop table

-- DROP TABLE his_web.dbo.QueuedEmail GO

CREATE TABLE his_web.dbo.QueuedEmail (
	Id int IDENTITY(1,1) NOT NULL,
	EmailAccountId int NOT NULL,
	SentOnUtc datetime2(7) NULL,
	SentTries int NOT NULL,
	DontSendBeforeDateUtc datetime2(7) NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	AttachedDownloadId int NOT NULL,
	AttachmentFileName nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AttachmentFilePath nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Body nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Subject nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Bcc nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CC nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ReplyToName nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ReplyTo nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ToName nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[To] nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	FromName nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[From] nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	PriorityId int NOT NULL,
	CONSTRAINT PK_QueuedEmail PRIMARY KEY (Id)
) GO;


-- his_web.dbo.RecurringPayment definition

-- Drop table

-- DROP TABLE his_web.dbo.RecurringPayment GO

CREATE TABLE his_web.dbo.RecurringPayment (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	InitialOrderId int NOT NULL,
	Deleted bit NOT NULL,
	LastPaymentFailed bit NOT NULL,
	IsActive bit NOT NULL,
	StartDateUtc datetime2(7) NOT NULL,
	TotalCycles int NOT NULL,
	CyclePeriodId int NOT NULL,
	CycleLength int NOT NULL,
	CONSTRAINT PK_RecurringPayment PRIMARY KEY (Id)
) GO;


-- his_web.dbo.RecurringPaymentHistory definition

-- Drop table

-- DROP TABLE his_web.dbo.RecurringPaymentHistory GO

CREATE TABLE his_web.dbo.RecurringPaymentHistory (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	OrderId int NOT NULL,
	RecurringPaymentId int NOT NULL,
	CONSTRAINT PK_RecurringPaymentHistory PRIMARY KEY (Id)
) GO;


-- his_web.dbo.RelatedProduct definition

-- Drop table

-- DROP TABLE his_web.dbo.RelatedProduct GO

CREATE TABLE his_web.dbo.RelatedProduct (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	ProductId2 int NOT NULL,
	ProductId1 int NOT NULL,
	CONSTRAINT PK_RelatedProduct PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ReturnRequest definition

-- Drop table

-- DROP TABLE his_web.dbo.ReturnRequest GO

CREATE TABLE his_web.dbo.ReturnRequest (
	Id int IDENTITY(1,1) NOT NULL,
	UpdatedOnUtc datetime2(7) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	ReturnRequestStatusId int NOT NULL,
	StaffNotes nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	UploadedFileId int NOT NULL,
	CustomerComments nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	RequestedAction nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	ReasonForReturn nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Quantity int NOT NULL,
	CustomerId int NOT NULL,
	OrderItemId int NOT NULL,
	StoreId int NOT NULL,
	CustomNumber nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT PK_ReturnRequest PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ReturnRequestAction definition

-- Drop table

-- DROP TABLE his_web.dbo.ReturnRequestAction GO

CREATE TABLE his_web.dbo.ReturnRequestAction (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_ReturnRequestAction PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ReturnRequestReason definition

-- Drop table

-- DROP TABLE his_web.dbo.ReturnRequestReason GO

CREATE TABLE his_web.dbo.ReturnRequestReason (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_ReturnRequestReason PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ReviewType definition

-- Drop table

-- DROP TABLE his_web.dbo.ReviewType GO

CREATE TABLE his_web.dbo.ReviewType (
	Id int IDENTITY(1,1) NOT NULL,
	IsRequired bit NOT NULL,
	VisibleToAllCustomers bit NOT NULL,
	DisplayOrder int NOT NULL,
	Description nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_ReviewType PRIMARY KEY (Id)
) GO;


-- his_web.dbo.RewardPointsHistory definition

-- Drop table

-- DROP TABLE his_web.dbo.RewardPointsHistory GO

CREATE TABLE his_web.dbo.RewardPointsHistory (
	Id int IDENTITY(1,1) NOT NULL,
	OrderId int NULL,
	ValidPoints int NULL,
	EndDateUtc datetime2(7) NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	Message nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	UsedAmount decimal(18,4) NOT NULL,
	PointsBalance int NULL,
	Points int NOT NULL,
	StoreId int NOT NULL,
	CustomerId int NOT NULL,
	CONSTRAINT PK_RewardPointsHistory PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ScheduleTask definition

-- Drop table

-- DROP TABLE his_web.dbo.ScheduleTask GO

CREATE TABLE his_web.dbo.ScheduleTask (
	Id int IDENTITY(1,1) NOT NULL,
	LastSuccessUtc datetime2(7) NULL,
	LastEndUtc datetime2(7) NULL,
	LastStartUtc datetime2(7) NULL,
	StopOnError bit NOT NULL,
	Enabled bit NOT NULL,
	[Type] nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Seconds int NOT NULL,
	Name nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_ScheduleTask PRIMARY KEY (Id)
) GO;


-- his_web.dbo.SearchTerm definition

-- Drop table

-- DROP TABLE his_web.dbo.SearchTerm GO

CREATE TABLE his_web.dbo.SearchTerm (
	Id int IDENTITY(1,1) NOT NULL,
	Count int NOT NULL,
	StoreId int NOT NULL,
	Keyword nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT PK_SearchTerm PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Setting definition

-- Drop table

-- DROP TABLE his_web.dbo.Setting GO

CREATE TABLE his_web.dbo.Setting (
	Id int IDENTITY(1,1) NOT NULL,
	StoreId int NOT NULL,
	Value nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Name nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_Setting PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Shipment definition

-- Drop table

-- DROP TABLE his_web.dbo.Shipment GO

CREATE TABLE his_web.dbo.Shipment (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	AdminComment nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DeliveryDateUtc datetime2(7) NULL,
	ShippedDateUtc datetime2(7) NULL,
	TotalWeight decimal(18,4) NULL,
	TrackingNumber nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	OrderId int NOT NULL,
	CONSTRAINT PK_Shipment PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ShipmentItem definition

-- Drop table

-- DROP TABLE his_web.dbo.ShipmentItem GO

CREATE TABLE his_web.dbo.ShipmentItem (
	Id int IDENTITY(1,1) NOT NULL,
	WarehouseId int NOT NULL,
	Quantity int NOT NULL,
	OrderItemId int NOT NULL,
	ShipmentId int NOT NULL,
	CONSTRAINT PK_ShipmentItem PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ShippingMethod definition

-- Drop table

-- DROP TABLE his_web.dbo.ShippingMethod GO

CREATE TABLE his_web.dbo.ShippingMethod (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	Description nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_ShippingMethod PRIMARY KEY (Id)
) GO;


-- his_web.dbo.ShippingMethodRestrictions definition

-- Drop table

-- DROP TABLE his_web.dbo.ShippingMethodRestrictions GO

CREATE TABLE his_web.dbo.ShippingMethodRestrictions (
	Country_Id int NOT NULL,
	ShippingMethod_Id int NOT NULL,
	CONSTRAINT PK_ShippingMethodRestrictions PRIMARY KEY (Country_Id,ShippingMethod_Id)
) GO;


-- his_web.dbo.ShoppingCartItem definition

-- Drop table

-- DROP TABLE his_web.dbo.ShoppingCartItem GO

CREATE TABLE his_web.dbo.ShoppingCartItem (
	Id int IDENTITY(1,1) NOT NULL,
	UpdatedOnUtc datetime2(7) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	RentalEndDateUtc datetime2(7) NULL,
	RentalStartDateUtc datetime2(7) NULL,
	Quantity int NOT NULL,
	CustomerEnteredPrice decimal(18,4) NOT NULL,
	AttributesXml nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ProductId int NOT NULL,
	CustomerId int NOT NULL,
	ShoppingCartTypeId int NOT NULL,
	StoreId int NOT NULL,
	CONSTRAINT PK_ShoppingCartItem PRIMARY KEY (Id)
) GO;


-- his_web.dbo.SpecificationAttribute definition

-- Drop table

-- DROP TABLE his_web.dbo.SpecificationAttribute GO

CREATE TABLE his_web.dbo.SpecificationAttribute (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	Name nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_SpecificationAttribute PRIMARY KEY (Id)
) GO;


-- his_web.dbo.SpecificationAttributeOption definition

-- Drop table

-- DROP TABLE his_web.dbo.SpecificationAttributeOption GO

CREATE TABLE his_web.dbo.SpecificationAttributeOption (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	ColorSquaresRgb nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	SpecificationAttributeId int NOT NULL,
	CONSTRAINT PK_SpecificationAttributeOption PRIMARY KEY (Id)
) GO;


-- his_web.dbo.StateProvince definition

-- Drop table

-- DROP TABLE his_web.dbo.StateProvince GO

CREATE TABLE his_web.dbo.StateProvince (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	Published bit NOT NULL,
	Abbreviation nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CountryId int NOT NULL,
	CONSTRAINT PK_StateProvince PRIMARY KEY (Id)
) GO;


-- his_web.dbo.StockQuantityHistory definition

-- Drop table

-- DROP TABLE his_web.dbo.StockQuantityHistory GO

CREATE TABLE his_web.dbo.StockQuantityHistory (
	Id int IDENTITY(1,1) NOT NULL,
	WarehouseId int NULL,
	CombinationId int NULL,
	ProductId int NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	Message nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	StockQuantity int NOT NULL,
	QuantityAdjustment int NOT NULL,
	CONSTRAINT PK_StockQuantityHistory PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Store definition

-- Drop table

-- DROP TABLE his_web.dbo.Store GO

CREATE TABLE his_web.dbo.Store (
	Id int IDENTITY(1,1) NOT NULL,
	CompanyVat nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CompanyPhoneNumber nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CompanyAddress nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CompanyName nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DisplayOrder int NOT NULL,
	DefaultLanguageId int NOT NULL,
	Hosts nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	SslEnabled bit NOT NULL,
	Url nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_Store PRIMARY KEY (Id)
) GO;


-- his_web.dbo.StoreMapping definition

-- Drop table

-- DROP TABLE his_web.dbo.StoreMapping GO

CREATE TABLE his_web.dbo.StoreMapping (
	Id int IDENTITY(1,1) NOT NULL,
	StoreId int NOT NULL,
	EntityName nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	EntityId int NOT NULL,
	CONSTRAINT PK_StoreMapping PRIMARY KEY (Id)
) GO;


-- his_web.dbo.TaxCategory definition

-- Drop table

-- DROP TABLE his_web.dbo.TaxCategory GO

CREATE TABLE his_web.dbo.TaxCategory (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_TaxCategory PRIMARY KEY (Id)
) GO;


-- his_web.dbo.TierPrice definition

-- Drop table

-- DROP TABLE his_web.dbo.TierPrice GO

CREATE TABLE his_web.dbo.TierPrice (
	Id int IDENTITY(1,1) NOT NULL,
	EndDateTimeUtc datetime2(7) NULL,
	StartDateTimeUtc datetime2(7) NULL,
	Price decimal(18,4) NOT NULL,
	Quantity int NOT NULL,
	CustomerRoleId int NULL,
	StoreId int NOT NULL,
	ProductId int NOT NULL,
	CONSTRAINT PK_TierPrice PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Topic definition

-- Drop table

-- DROP TABLE his_web.dbo.Topic GO

CREATE TABLE his_web.dbo.Topic (
	Id int IDENTITY(1,1) NOT NULL,
	LimitedToStores bit NOT NULL,
	SubjectToAcl bit NOT NULL,
	MetaTitle nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaDescription nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaKeywords nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	TopicTemplateId int NOT NULL,
	Published bit NOT NULL,
	Body nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Title nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Password nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	IsPasswordProtected bit NOT NULL,
	AccessibleWhenStoreClosed bit NOT NULL,
	DisplayOrder int NOT NULL,
	IncludeInFooterColumn3 bit NOT NULL,
	IncludeInFooterColumn2 bit NOT NULL,
	IncludeInFooterColumn1 bit NOT NULL,
	IncludeInTopMenu bit NOT NULL,
	IncludeInSitemap bit NOT NULL,
	SystemName nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT PK_Topic PRIMARY KEY (Id)
) GO;


-- his_web.dbo.TopicTemplate definition

-- Drop table

-- DROP TABLE his_web.dbo.TopicTemplate GO

CREATE TABLE his_web.dbo.TopicTemplate (
	Id int IDENTITY(1,1) NOT NULL,
	DisplayOrder int NOT NULL,
	ViewPath nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_TopicTemplate PRIMARY KEY (Id)
) GO;


-- his_web.dbo.UrlRecord definition

-- Drop table

-- DROP TABLE his_web.dbo.UrlRecord GO

CREATE TABLE his_web.dbo.UrlRecord (
	Id int IDENTITY(1,1) NOT NULL,
	LanguageId int NOT NULL,
	IsActive bit NOT NULL,
	Slug nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	EntityName nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	EntityId int NOT NULL,
	CONSTRAINT PK_UrlRecord PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Vendor definition

-- Drop table

-- DROP TABLE his_web.dbo.Vendor GO

CREATE TABLE his_web.dbo.Vendor (
	Id int IDENTITY(1,1) NOT NULL,
	PageSizeOptions nvarchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AllowCustomersToSelectPageSize bit NOT NULL,
	PageSize int NOT NULL,
	MetaTitle nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaDescription nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	MetaKeywords nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	DisplayOrder int NOT NULL,
	Deleted bit NOT NULL,
	Active bit NOT NULL,
	AdminComment nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	AddressId int NOT NULL,
	PictureId int NOT NULL,
	Description nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Email nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_Vendor PRIMARY KEY (Id)
) GO;


-- his_web.dbo.VendorAttribute definition

-- Drop table

-- DROP TABLE his_web.dbo.VendorAttribute GO

CREATE TABLE his_web.dbo.VendorAttribute (
	Id int IDENTITY(1,1) NOT NULL,
	AttributeControlTypeId int NOT NULL,
	DisplayOrder int NOT NULL,
	IsRequired bit NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_VendorAttribute PRIMARY KEY (Id)
) GO;


-- his_web.dbo.VendorAttributeValue definition

-- Drop table

-- DROP TABLE his_web.dbo.VendorAttributeValue GO

CREATE TABLE his_web.dbo.VendorAttributeValue (
	Id int IDENTITY(1,1) NOT NULL,
	VendorAttributeId int NOT NULL,
	DisplayOrder int NOT NULL,
	IsPreSelected bit NOT NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_VendorAttributeValue PRIMARY KEY (Id)
) GO;


-- his_web.dbo.VendorNote definition

-- Drop table

-- DROP TABLE his_web.dbo.VendorNote GO

CREATE TABLE his_web.dbo.VendorNote (
	Id int IDENTITY(1,1) NOT NULL,
	CreatedOnUtc datetime2(7) NOT NULL,
	Note nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	VendorId int NOT NULL,
	CONSTRAINT PK_VendorNote PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Ward definition

-- Drop table

-- DROP TABLE his_web.dbo.Ward GO

CREATE TABLE his_web.dbo.Ward (
	Id int IDENTITY(1,1) NOT NULL,
	Name nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	DisplayOrder int NOT NULL,
	DistrictId int NOT NULL,
	HisId int NOT NULL,
	HisDistrictId int NOT NULL,
	CONSTRAINT PK_Ward PRIMARY KEY (Id)
) GO;


-- his_web.dbo.Warehouse definition

-- Drop table

-- DROP TABLE his_web.dbo.Warehouse GO

CREATE TABLE his_web.dbo.Warehouse (
	Id int IDENTITY(1,1) NOT NULL,
	AddressId int NOT NULL,
	AdminComment nvarchar COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Name nvarchar(400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_Warehouse PRIMARY KEY (Id)
) GO;


CREATE TABLE his_web.dbo.News_Category_Picture_Mapping (
	DisplayOrder int NOT NULL,
	PictureId int NOT NULL,
	NewsCategoryId int NOT NULL,
	Id int IDENTITY(0,1) NOT NULL
) GO;