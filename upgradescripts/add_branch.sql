use his_web;
go

CREATE TABLE [dbo].[Branch] (    [Id]              INT            IDENTITY (1, 1) NOT NULL,    [Name]       NVARCHAR (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    [Address] NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    [PhoneNumber]    NVARCHAR (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,     [WorkTime]    NVARCHAR (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,   [Fax]    NVARCHAR (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    [Email]    NVARCHAR (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    [Hotline]    NVARCHAR (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,    [HeadOffice] BIT            NOT NULL,);
go
