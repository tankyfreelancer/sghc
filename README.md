# Hospital website project

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://github.com/nguyencatpham/hospital-website)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://github.com/nguyencatpham/hospital-website)


### Backup db
```sh
sqlcmd -S localhost -U SA -Q "BACKUP DATABASE [his_web] TO DISK = N'/var/opt/mssql/data/db.bak' WITH NOFORMAT, NOINIT, NAME = 'his_web', SKIP, NOREWIND, NOUNLOAD, STATS = 10"
```
### Restore db in linux container
```bash
docker run -d -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=jhasdj122#d' -p 1433:1433 --name db samuelmarks/mssql-server-fts-sqlpackage-linux
docker exec -it db /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'jhasdj122#d' -Q 'CREATE DATABASE his_web'
docker cp his_db.bacpac db:/opt/downloads/his_db.bacpac
docker exec -it db dotnet /opt/sqlpackage/sqlpackage.dll /tsn:localhost /tu:SA /tp:'jhasdj122#d' /A:Import /tdn:his_web /sf:his_db.bacpac
```

### Restore with sqlcmd
```bash
sqlcmd -S localhost -U SA -Q "RESTORE DATABASE [his_web] FROM DISK = N'/var/opt/mssql/data/db.bak' WITH FILE = 1, NOUNLOAD, REPLACE, NORECOVERY, STATS = 5"
```
Then run this sql query to fix `Database Cannot Be Opened as It is in The Middle of a Restore`
```sql
RESTORE DATABASE his_web WITH RECOVERY
```
### SSL

> Run on the host for enable SSL for Kestrel and Linux docker container
> https://docs.microsoft.com/en-us/aspnet/core/security/docker-compose-https?view=aspnetcore-5.0
> https://www.fearofoblivion.com/setting-up-asp-net-dev-certs-for-wsl ????
```bash
openssl pkcs12 -inkey cert/private_key.pem -in cert/ca.crt -export -out cert/sghc.pfx
```
Password: V4Tguc13X2jH

> Then enable SSL in admin page

```
/Admin/Store/Edit/1
```
> Set setting SSL is true

```
securitysettings.forcesslforallpages = True
```
### Deploy
> Deploy via docker

```sh
$ make deploy-publish
$ make up
```

> To deploy plugin, copy directory Nop.Web/Plugins to host
### Troubleshooting

1. https://stackoverflow.com/questions/16836473/asp-net-http-error-500-19-internal-server-error-0x8007000d
Install package
```
https://www.iis.net/downloads/microsoft/url-rewrite
```
```
https://download.visualstudio.microsoft.com/download/pr/5bed16f2-fd1a-4027-bee3-3d6a1b5844cc/dd22ca2820fadb57fd5378e1763d27cd/dotnet-hosting-3.1.4-win.exe
```

2. Did not redirect to https
> Set setting SSL is true

```
securitysettings.forcesslforallpages = True
```

### Problem

```
Unhandled exception. System.Exception: Plugin 'External authentication'. Could not load file or assembly 'Nop.Services, Version=4.3.0.0, Culture=neutral, PublicKeyToken=null'. The system cannot find the file specified.
```

Plugins External Auth can't copy Nop.Core","Nop.Data","Nop.Services","Nop.Web","Nop.Web.Framework" into src/Presentation/Nop.Web/Plugins/bin. So, I copied them to that manually, then add workaround code at src/Presentation/Nop.Web.Framework/Infrastructure/Extensions/ApplicationPartManagerExtensions.cs:431